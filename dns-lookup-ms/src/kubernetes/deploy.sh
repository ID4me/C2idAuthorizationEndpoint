#!/bin/bash
set -e

_fullscriptpath="$(readlink -f ${BASH_SOURCE[0]})"
BASEDIR="$(dirname $_fullscriptpath)"

DOCKERREGISTRY=$DOCKER_REGISTRY
DOCKERREGISTRYPATH=test
IMAGENAME=dns-lookup-ms

NAMESPACE=$1
KUBECOMMAND="$KUBECTL --kubeconfig=$KUBECONFIG --namespace=${NAMESPACE}"
VERSION=$2

CONFIGMAPFILE=dns-lookup-ms_configmap_${NAMESPACE}-ns.yaml
if [ -f ${CONFIGMAPFILE} ]; then
	echo "Applying config map file: ${CONFIGMAPFILE}"
	$KUBECOMMAND apply --filename=${CONFIGMAPFILE}
fi

sed --in-place "s#__DOCKER_REGISTRY__#${DOCKERREGISTRY}#" dns-lookup-ms-dc.yaml
sed --in-place "s#__IMAGE_VERSION__#${VERSION}#" dns-lookup-ms-dc.yaml
$KUBECOMMAND apply --filename=dns-lookup-ms-svc.yaml --record
$KUBECOMMAND apply --filename=dns-lookup-ms-dc.yaml --record
$KUBECOMMAND rollout status deployment/dns-lookup-ms --watch
