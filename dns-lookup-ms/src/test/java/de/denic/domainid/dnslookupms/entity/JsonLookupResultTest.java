/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.dnslookupms.entity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import de.denic.domainid.DomainId;
import de.denic.domainid.dns.LookupResult;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.Test;

import java.net.URI;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

public class JsonLookupResultTest {

  private static final ObjectWriter JSON_LOOKUP_RESULT_WRITER = new ObjectMapper().writerFor(JsonLookupResult.class);
  private static final boolean VALIDATED = true;
  private static final DomainId TEST_DOMAIN_ID = new DomainId("d\u00f6main-id.test");
  private static final URI IDENT_AUTHORITY_URL = URI.create("//ident-a\u00fcthority.test");
  private static final URI IDENT_AGENT_URL = URI.create("//ident-\u00e4gent.test");
  private JsonLookupResult CUT;

  @Test
  public void serializeToJsonWithBothURIs() throws JsonProcessingException {
    CUT = new JsonLookupResult(TEST_DOMAIN_ID, new LookupResult.Impl<>(VALIDATED, new ImmutablePair<>
            (IDENT_AUTHORITY_URL, IDENT_AGENT_URL)));
    final String json = JSON_LOOKUP_RESULT_WRITER.writeValueAsString(CUT);
    System.out.println(json);
    assertTrue("JSON serialization contains id field", json.contains("\"id\":\"d\u00f6main-id.test\""));
    assertTrue("JSON serialization contains dnssecValidated field", json.contains("\"dnssecValidated\":true"));
    assertTrue("JSON serialization contains identityAgent field", json.contains(
            "\"identityAgent\":\"//ident-%C3%A4gent.test\""));
    assertTrue("JSON serialization contains identityAgentURI field", json.contains(
            "\"identityAgentURI\":\"https://ident-%C3%A4gent.test\""));
    assertTrue("JSON serialization contains identityAuthority field", json.contains
            ("\"identityAuthority\":\"//ident-a%C3%BCthority.test\""));
    assertTrue("JSON serialization contains identityAuthorityURI field", json.contains
            ("\"identityAuthorityURI\":\"https://ident-a%C3%BCthority.test\""));
  }

  @Test
  public void serializeToJsonMissingIdentityAgentURI() throws JsonProcessingException {
    CUT = new JsonLookupResult(TEST_DOMAIN_ID, new LookupResult.Impl<>(VALIDATED, new ImmutablePair<>
            (IDENT_AUTHORITY_URL, null)));
    final String json = JSON_LOOKUP_RESULT_WRITER.writeValueAsString(CUT);
    assertTrue("JSON serialization contains id field", json.contains("\"id\":\"d\u00f6main-id.test\""));
    assertTrue("JSON serialization contains dnssecValidated field", json.contains("\"dnssecValidated\":true"));
    assertFalse("JSON serialization contains NO identityAgent field", json.contains("identityAgent"));
    assertFalse("JSON serialization contains NO identityAgentURI field", json.contains("identityAgentURI"));
    assertTrue("JSON serialization contains identityAuthority field", json.contains
            ("\"identityAuthority\":\"//ident-a%C3%BCthority.test\""));
    assertTrue("JSON serialization contains identityAuthorityURI field", json.contains
            ("\"identityAuthorityURI\":\"https://ident-a%C3%BCthority.test\""));
  }

}