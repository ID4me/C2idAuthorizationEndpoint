package de.denic.domainid.dnslookupms.metric;

import de.denic.domainid.dnslookupms.DnsLookupMicroService;
import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.stereotype.Component;

import javax.annotation.concurrent.Immutable;

@Component
@Immutable
public final class Version implements InfoContributor {

  private static final String IMPLEMENTATION_VERSION = DnsLookupMicroService.class.getPackage().getImplementationVersion();

  @Override
  public void contribute(final Info.Builder builder) {
    builder.withDetail("version", IMPLEMENTATION_VERSION);
  }
}
