/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.dnslookupms.controller;

import de.denic.domainid.DomainId;
import de.denic.domainid.dns.DomainIdDnsClient;
import de.denic.domainid.dnslookupms.entity.JsonLookupResult;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.concurrent.Immutable;
import javax.validation.constraints.NotNull;

import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Immutable
@Controller
public class LookupController {

  private static final Logger LOG = LoggerFactory.getLogger(LookupController.class);

  private final DomainIdDnsClient dnsClient;

  /**
   * @param dnsClient Required
   */
  public LookupController(final DomainIdDnsClient dnsClient) {
    Validate.notNull(dnsClient, "Missing DNS client component");
    this.dnsClient = dnsClient;
  }

  /**
   * @param domainId Required
   * @return Never <code>null</code>
   */
  // Hint: https://stackoverflow.com/questions/16332092/spring-mvc-pathvariable-with-dot-is-getting-truncated
  @RequestMapping(path = "/{id:.+}", method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
  public ResponseEntity<JsonLookupResult> viaPath(@PathVariable("id") @NotNull final DomainId domainId) {
    LOG.info("Lookup of DomainID '{}'", domainId);
    final JsonLookupResult jsonLookupResult = new JsonLookupResult(domainId,
            dnsClient.lookupIdentityAuthAndAgentHostNameOf(domainId));
    return ResponseEntity.status((jsonLookupResult.isEmpty() ? NOT_FOUND : OK)).body(jsonLookupResult);
  }

  /**
   * @see #viaPath(DomainId)
   */
  @RequestMapping(path = "/lookup/{id:.+}", method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
  public ResponseEntity<JsonLookupResult> viaLookupPath(@PathVariable("id") @NotNull final DomainId domainId) {
    return viaPath(domainId);
  }

}
