/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.dnslookupms.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.denic.domainid.DomainId;
import de.denic.domainid.dns.LookupResult;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.tuple.Pair;

import javax.annotation.concurrent.Immutable;
import java.net.URI;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;

@Immutable
public final class JsonLookupResult {

  private final LookupResult<Pair<URI, URI>> domainIdLookupResult;
  private final DomainId domainId;

  /**
   * @param domainId             Required
   * @param domainIdLookupResult Required
   */
  public JsonLookupResult(final DomainId domainId, final LookupResult<Pair<URI, URI>> domainIdLookupResult) {
    Validate.notNull(domainId, "Missing DomainID");
    Validate.notNull(domainIdLookupResult, "Missing lookup result");
    this.domainIdLookupResult = domainIdLookupResult;
    this.domainId = domainId;
  }

  private static String nullSafeToAsciiString(final URI value) {
    return (value == null ? null : value.toASCIIString());
  }

  @JsonIgnore
  public boolean isEmpty() {
    return domainIdLookupResult.isEmpty();
  }

  @JsonProperty("dnssecValidated")
  public boolean isValidated() {
    return domainIdLookupResult.isValidated();
  }

  /**
   * @return Never <code>null</code>
   */
  @JsonProperty("id")
  public String getDomainId() {
    return getId4Me();
  }

  /**
   * @return Never <code>null</code>
   */
  @JsonProperty("id4me")
  public String getId4Me() {
    return domainId.getName();
  }

  /**
   * @return Optional
   */
  @JsonProperty("identityAgent")
  @JsonInclude(NON_EMPTY)
  public String getIdentityAgent() {
    return (domainIdLookupResult.isEmpty() ? null : nullSafeToAsciiString(domainIdLookupResult.get().getValue()));
  }

  /**
   * @return Optional
   */
  @JsonProperty("identityAgentURI")
  @JsonInclude(NON_EMPTY)
  public String getIdentityAgentURI() {
    final String result = getIdentityAgent();
    return (result == null ? null : "https:" + result);
  }

  /**
   * @return Optional
   */
  @JsonProperty("identityAuthority")
  @JsonInclude(NON_EMPTY)
  public String getIdentityAuthority() {
    return (domainIdLookupResult.isEmpty() ? null : nullSafeToAsciiString(domainIdLookupResult.get().getKey()));
  }

  /**
   * @return Optional
   */
  @JsonProperty("identityAuthorityURI")
  @JsonInclude(NON_EMPTY)
  public String getIdentityAuthorityURI() {
    final String result = getIdentityAuthority();
    return (result == null ? null : "https:" + result);
  }

  @Override
  public String toString() {
    return domainId + " (" + domainIdLookupResult + ')';
  }

}
