/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.identityagent.controller;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.impl.crypto.RsaProvider;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URI;
import java.security.KeyPair;

import static io.jsonwebtoken.SignatureAlgorithm.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class UserInfoEndpointControllerTest {

  private static final String TEST_SUBJECT = "foo-sub";
  private static final URI USER_INFO_ENDPOINT_URI = URI.create("/user-info-endpoint");
  private static final KeyPair TEST_KEY_PAIR = RsaProvider.generateKeyPair(2048);

  @Autowired
  private TestRestTemplate restTemplate;

  @Test
  public void unsignedJsonWebToken() {
    final String bearerAccessToken = Jwts.builder().claim("sub", TEST_SUBJECT).compact();
    final RequestEntity<?> request = RequestEntity.get(USER_INFO_ENDPOINT_URI).header("Authorization", "Bearer " + bearerAccessToken).build();
    {
      assertStatusCodeIssueing(request, 400, "Unsigned Claims JWTs are not supported.");
    }
  }

  @Test
  public void missingIssuerClaim() {
    final String bearerAccessToken = Jwts.builder().signWith(RS256, TEST_KEY_PAIR.getPrivate()).claim("sub", TEST_SUBJECT).compact();
    final RequestEntity<?> request = RequestEntity.get(USER_INFO_ENDPOINT_URI).header("Authorization", "Bearer " + bearerAccessToken).build();
    {
      assertStatusCodeIssueing(request, 400, "Processing Access Token failed: Missing claim 'iss'");
    }
  }

  @Test
  public void relativeIssuerUriClaim() {
    final String bearerAccessToken = Jwts.builder().signWith(RS256, TEST_KEY_PAIR.getPrivate()).claim("sub", TEST_SUBJECT).claim("iss",
            "some-issuer").compact();
    final RequestEntity<?> request = RequestEntity.get(USER_INFO_ENDPOINT_URI).header("Authorization", "Bearer " + bearerAccessToken).build();
    {
      assertStatusCodeIssueing(request, 400, "Processing Access Token failed: URI 'some-issuer' is not absolute");
    }
  }

  @Test
  public void unknownHostOfIssuerUri() {
    final String bearerAccessToken = Jwts.builder().claim("sub", TEST_SUBJECT).claim("iss",
            "http://unknown.issuer.test").signWith(RS256, TEST_KEY_PAIR.getPrivate()).compact();
    final RequestEntity<?> request = RequestEntity.get(USER_INFO_ENDPOINT_URI).header("Authorization", "Bearer " + bearerAccessToken).build();
    {
      assertStatusCodeIssueing(request, 400, "Processing Access Token failed: Requesting 'http://unknown.issuer.test/.well-known/openid-configuration' failed: java.net.UnknownHostException: unknown.issuer.test");
    }
  }

  @Test
  public void missingKeyIdJwtHeader() {
    final String bearerAccessToken = Jwts.builder().claim("sub", TEST_SUBJECT).claim("iss",
            "https://auth.freedom-id.de").signWith(RS256, TEST_KEY_PAIR.getPrivate()).compact();
    final RequestEntity<?> request = RequestEntity.get(USER_INFO_ENDPOINT_URI).header("Authorization", "Bearer " + bearerAccessToken).build();
    {
      assertStatusCodeIssueing(request, 400, "Processing Access Token failed: Missing key ID");
    }
  }

  @Test
  public void unknownKeyIdJwtHeader() {
    final String bearerAccessToken = Jwts.builder().setHeaderParam("kid", "UNKNOWN").claim("sub",
            TEST_SUBJECT).claim("iss", "https://auth.freedom-id.de").signWith(RS256, TEST_KEY_PAIR.getPrivate()).compact();
    final RequestEntity<?> request = RequestEntity.get(USER_INFO_ENDPOINT_URI).header("Authorization", "Bearer " + bearerAccessToken).build();
    {
      assertStatusCodeIssueing(request, 400, "Processing Access Token failed: Unknown key ID 'UNKNOWN'");
    }
  }

  @Test
  public void statingHmacSignatureInHeader() {
    final String bearerAccessToken = Jwts.builder().setHeaderParam("kid", "FOvy").claim("sub",
            TEST_SUBJECT).claim("iss", "https://auth.freedom-id.de").signWith(HS256, TEST_KEY_PAIR.getPrivate().getEncoded())
            .compact();
    final RequestEntity<?> request = RequestEntity.get(USER_INFO_ENDPOINT_URI).header("Authorization", "Bearer " + bearerAccessToken).build();
    {
      assertStatusCodeIssueing(request, 400, "Unsupported JWT: The parsed JWT indicates it was signed with the HS256 signature algorithm");
    }
  }

  @Test
  public void mismatchingKeyId() {
    final String bearerAccessToken = Jwts.builder().setHeaderParam("kid", "FOvy").claim("sub",
            TEST_SUBJECT).claim("iss", "https://auth.freedom-id.de").signWith(RS512, TEST_KEY_PAIR.getPrivate()).compact();
    final RequestEntity<?> request = RequestEntity.get(USER_INFO_ENDPOINT_URI).header("Authorization", "Bearer " + bearerAccessToken).build();
    {
      assertStatusCodeIssueing(request, 400, "JWT signature error: JWT signature does not match locally computed signature.");
    }
  }

  private void assertStatusCodeIssueing(final RequestEntity<?> request, final int expectedStatusCode, final CharSequence expectedBodyContent) {
    final ResponseEntity<String> responseEntity = restTemplate.exchange(request, String.class);
    assertEquals("Status code", expectedStatusCode, responseEntity.getStatusCode().value());
    final String responseBody = responseEntity.getBody();
    System.out.println("Response body: " + responseBody);
    assertTrue("Body content contains '" + expectedBodyContent + "'", responseBody.contains
            (expectedBodyContent));
  }

}