/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.identityagent.entity;

import io.jsonwebtoken.*;
import org.apache.commons.lang3.Validate;

import javax.annotation.concurrent.Immutable;
import javax.ws.rs.WebApplicationException;
import java.util.Objects;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;

@Immutable
public class BearerAccessToken {

  public static final String BEARER_PREFIX = "Bearer ";

  private final Jws<Claims> jws;

  /**
   * @param value              Required
   * @param signingKeyResolver Required
   */
  public BearerAccessToken(final String value, final SigningKeyResolver signingKeyResolver) throws WebApplicationException {
    Validate.notEmpty(value, "Missing value");
    Validate.isTrue(value.startsWith(BEARER_PREFIX), "Not a Bearer Access Token: {}", value);
    try {
      jws = Jwts.parser().setSigningKeyResolver(signingKeyResolver).parseClaimsJws(value.substring(BEARER_PREFIX
              .length()));
    } catch (final ExpiredJwtException e) {
      throw new WebApplicationException("Expired JWT: " + e.getMessage(), BAD_REQUEST);
    } catch (final MalformedJwtException e) {
      throw new WebApplicationException("Malformed JWT: " + e.getMessage(), BAD_REQUEST);
    } catch (final UnsupportedJwtException e) {
      throw new WebApplicationException("Unsupported JWT: " + e.getMessage(), BAD_REQUEST);
    } catch (SignatureException e) {
      throw new WebApplicationException("JWT signature error: " + e.getMessage(), BAD_REQUEST);
    }
  }

  /**
   * @return Never <code>null</code>
   */
  public Claims getClaims() {
    return jws.getBody();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    BearerAccessToken that = (BearerAccessToken) o;
    return Objects.equals(jws, that.jws);
  }

  @Override
  public int hashCode() {
    return Objects.hash(jws);
  }

  @Override
  public String toString() {
    return "Bearer " + jws;
  }
}
