/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.identityagent;

import de.denic.domainid.dns.DomainIdDnsClient;
import de.denic.domainid.dns.impl.DomainIdDnsClientDnsJavaBasedImpl;
import de.denic.domainid.identityagent.entity.OpenIdConfigurationProvider;
import de.denic.domainid.identityagent.entity.impl.JsonFileOpenIdConfigurationProvider;
import de.denic.domainid.identityagent.entity.impl.SigningKeyResolverImpl;
import io.jsonwebtoken.SigningKeyResolver;
import io.prometheus.client.hotspot.DefaultExports;
import io.prometheus.client.spring.boot.EnablePrometheusEndpoint;
import io.prometheus.client.spring.boot.EnableSpringBootMetricsCollector;
import io.prometheus.client.spring.web.EnablePrometheusTiming;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.xbill.DNS.SimpleResolver;

import java.io.IOException;
import java.net.UnknownHostException;

import static javax.ws.rs.client.ClientBuilder.newClient;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.SystemUtils.*;

@EnablePrometheusEndpoint
@EnablePrometheusTiming
@EnableSpringBootMetricsCollector
@SpringBootApplication
public class JsonFileBasedIdentityAgent extends WebMvcConfigurerAdapter {

  private static final Logger LOG = LoggerFactory.getLogger(JsonFileBasedIdentityAgent.class);

  @Autowired
  private Environment environment;

  public static void main(String[] args) {
    SpringApplication.run(JsonFileBasedIdentityAgent.class, args);
    DefaultExports.initialize();
    LOG.info("Running on JRE version {}, JRE specification version {}, JVM implementation version {}",
            JAVA_RUNTIME_VERSION, JAVA_VM_SPECIFICATION_VERSION, JAVA_VM_VERSION);
  }

  @Primary
  @Bean
  public OpenIdConfigurationProvider createOpenIdConfigProvider() throws IOException {
    final String classpathFile = environment.getProperty("openId.configuration.classpathFile");
    final ClassPathResource classPathResource = new ClassPathResource(classpathFile);
    LOG.info("Loading OpenID Configuration from {}", classPathResource);
    return new JsonFileOpenIdConfigurationProvider(classPathResource.getInputStream());
  }

  @Primary
  @Bean
  public SigningKeyResolver createSigningKeyResolver() {
    return new SigningKeyResolverImpl(newClient());
  }

  @Primary
  @Bean
  public DomainIdDnsClient createDomainIdDnsClient() throws UnknownHostException {
    final String dnsResolverHost = environment.getProperty("dns.resolver.host");
    LOG.info("Environment config of DNS resolver host: '{}'", dnsResolverHost);
    final SimpleResolver resolver = (isBlank(dnsResolverHost) ? new SimpleResolver() : new SimpleResolver(dnsResolverHost));
    return new DomainIdDnsClientDnsJavaBasedImpl(resolver);
  }

}
