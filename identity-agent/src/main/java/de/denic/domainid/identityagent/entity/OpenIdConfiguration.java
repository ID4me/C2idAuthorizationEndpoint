/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.identityagent.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.Validate;

import javax.annotation.concurrent.Immutable;
import java.net.URI;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static java.util.Collections.unmodifiableSet;

@Immutable
public final class OpenIdConfiguration {

  private final Set<Claim> supportedClaims = new HashSet<>();
  private final URI userInfoEndpoint;

  /**
   * @param userInfoEndpoint Required
   * @param supportedClaims  Optional
   */
  public OpenIdConfiguration(final URI userInfoEndpoint, final Set<Claim> supportedClaims) {
    Validate.notNull(userInfoEndpoint, "Missing user info endpoint URI");
    this.userInfoEndpoint = userInfoEndpoint;
    if (supportedClaims != null) {
      this.supportedClaims.addAll(supportedClaims);
    }
  }

  @JsonProperty("userinfo_endpoint")
  public URI getUserInfoEndpoint() {
    return userInfoEndpoint;
  }

  @JsonProperty("claims_supported")
  public Set<Claim> getSupportedClaims() {
    return unmodifiableSet(supportedClaims);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    OpenIdConfiguration that = (OpenIdConfiguration) o;
    return Objects.equals(supportedClaims, that.supportedClaims) &&
            Objects.equals(userInfoEndpoint, that.userInfoEndpoint);
  }

  @Override
  public int hashCode() {
    return Objects.hash(supportedClaims, userInfoEndpoint);
  }

  @Override
  public String toString() {
    return userInfoEndpoint + ", " + supportedClaims;
  }

  public interface Mutable {

    /**
     * @return Optional
     */
    URI getUserInfoEndpoint();

    /**
     * @param endpoint Optional
     * @return Never <code>null</code>.
     */
    Mutable withUserInfoEndpoint(URI endpoint);

    /**
     * @return Never <code>null</code> but usually unmodifiable.
     */
    Set<Claim> getSupportedClaims();

    /**
     * @return Never <code>null</code>.
     */
    OpenIdConfiguration immutable() throws IllegalStateException;

    /**
     * @return Never <code>null</code>.
     */
    Mutable copy();

  }
}
