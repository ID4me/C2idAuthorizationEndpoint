/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.identityagent.controller;

import de.denic.domainid.DomainId;
import de.denic.domainid.dns.DomainIdDnsClient;
import de.denic.domainid.dns.LookupResult;
import de.denic.domainid.identityagent.entity.BearerAccessToken;
import de.denic.domainid.identityagent.entity.ClaimValues;
import de.denic.domainid.wellknown.openid.OpenIdConfigurationClientException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.SigningKeyResolver;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.concurrent.Immutable;
import javax.validation.constraints.NotNull;
import javax.ws.rs.WebApplicationException;
import java.net.URI;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.FORBIDDEN;
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED_VALUE;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Immutable
@RestController
@RequestMapping("/user-info-endpoint")
public class UserInfoEndpointController {

  private static final Logger LOG = LoggerFactory.getLogger(UserInfoEndpointController.class);

  private final SigningKeyResolver signingKeyResolver;
  private final DomainIdDnsClient dnsClient;

  /**
   * @param signingKeyResolver Required
   * @param dnsClient          Required
   */
  public UserInfoEndpointController(final DomainIdDnsClient dnsClient, final SigningKeyResolver signingKeyResolver) {
    Validate.notNull(signingKeyResolver, "Missing signing key resolver");
    Validate.notNull(dnsClient, "Missing DNS client component");
    this.signingKeyResolver = signingKeyResolver;
    this.dnsClient = dnsClient;
  }

  @RequestMapping(method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
  public ResponseEntity<ClaimValues> getWithAuthorizationHeader(@NotNull @RequestHeader("Authorization") final
                                                                String bearerAccessTokenValue) {
    LOG.info("Received Access Token to exchange: {}", bearerAccessTokenValue);
    final BearerAccessToken bearerAccessToken;
    try {
      bearerAccessToken = new BearerAccessToken(bearerAccessTokenValue, signingKeyResolver);
    } catch (final IllegalArgumentException | OpenIdConfigurationClientException e) {
      throw new WebApplicationException("Processing Access Token failed: " + e.getMessage(), BAD_REQUEST);
    }

    LOG.info("Decoded received Access Token: {}", bearerAccessToken);
    final Claims claims = bearerAccessToken.getClaims();
    if (claims == null) {
      throw new WebApplicationException("Processing Access Token failed: Missing claims.", BAD_REQUEST);
    }

    checkIssuerIsStatedInDomainIdDnsEntry(URI.create(claims.getIssuer()), getDomainIdFrom(claims));
    return ok(ClaimValues.defaultValuesOf(claims));
  }

  private void checkIssuerIsStatedInDomainIdDnsEntry(final URI issuer, final DomainId domainId) {
    final String issuerHostAndPath = issuer.getHost() + issuer.getPath();
    final LookupResult<Pair<URI, URI>> identityAgentLookup = dnsClient.lookupIdentityAuthAndAgentHostNameOf(domainId);
    final URI identityAuthorityURI = identityAgentLookup.get().getKey();
    final String issuerHostAndPathFromDNS = identityAuthorityURI.getHost() + identityAuthorityURI.getPath();
    if (!issuerHostAndPathFromDNS.equals(issuerHostAndPath)) {
      throw new WebApplicationException("Unauthorized issuer '" + issuer + "' (expected '" + identityAuthorityURI + "')", FORBIDDEN);
    }
  }

  /**
   * @param claims Required
   * @return Never <code>null</code>
   */
  private DomainId getDomainIdFrom(final Claims claims) {
    final String domainIdClaim = defaultIfNull(claims.get("id4me", String.class), claims.get("identifier",
            String.class));
    if (isEmpty(domainIdClaim)) {
      throw new WebApplicationException("Missing at least one claim 'id4me' or 'identifier'.", BAD_REQUEST);
    }

    return new DomainId(domainIdClaim);
  }

  //@RequestMapping(method = GET, produces = APPLICATION_JSON_UTF8_VALUE)
  public ResponseEntity<ClaimValues> getWithUriQueryParameter(@NotNull @RequestParam("access_token") final
                                                              String bearerAccessTokenValue) {
    return getWithAuthorizationHeader(bearerAccessTokenValue);
  }

  @RequestMapping(method = POST, produces = APPLICATION_JSON_UTF8_VALUE)
  public ResponseEntity<ClaimValues> postWithAuthorizationHeader(@NotNull @RequestHeader("Authorization") final
                                                                 String bearerAccessTokenValue) {
    return getWithAuthorizationHeader(bearerAccessTokenValue);
  }

  @RequestMapping(method = POST, consumes = APPLICATION_FORM_URLENCODED_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
  public ResponseEntity<ClaimValues> postWithFormEncodedBodyParameter(@NotNull @RequestParam("access_token") final
                                                                      String bearerAccessTokenValue) {
    return getWithUriQueryParameter(bearerAccessTokenValue);
  }

}
