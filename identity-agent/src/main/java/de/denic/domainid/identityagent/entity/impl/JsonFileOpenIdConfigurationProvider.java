/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.identityagent.entity.impl;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import de.denic.domainid.identityagent.entity.Claim;
import de.denic.domainid.identityagent.entity.OpenIdConfiguration;
import de.denic.domainid.identityagent.entity.OpenIdConfigurationProvider;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static java.util.Collections.emptySet;
import static java.util.Collections.unmodifiableSet;

@Immutable
public final class JsonFileOpenIdConfigurationProvider implements OpenIdConfigurationProvider {

  private static final Logger LOG = LoggerFactory.getLogger(JsonFileOpenIdConfigurationProvider.class);
  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
  private static final ObjectReader CONFIGURATION_ENTITY_READER = OBJECT_MAPPER.readerFor(MutableImpl.class);

  private final MutableImpl mutableConfiguration;

  /**
   * @param configurationData Required
   */
  public JsonFileOpenIdConfigurationProvider(final InputStream configurationData) {
    Validate.notNull(configurationData, "Missing configuration data");
    try {
      mutableConfiguration = CONFIGURATION_ENTITY_READER.readValue(configurationData);
    } catch (IOException e) {
      throw new IllegalArgumentException("Reading OpenID Configuration data failed", e);
    }
    LOG.info("OpenID configuration read: {}", mutableConfiguration);
  }

  @Override
  public OpenIdConfiguration.Mutable provide() {
    return mutableConfiguration.copy();
  }

  @NotThreadSafe
  @JsonIgnoreProperties(ignoreUnknown = true)
  private static final class MutableImpl implements OpenIdConfiguration.Mutable {

    private final Set<Claim> supportedClaims;
    private URI userInfoEndpoint;

    /**
     * @param supportedClaims Optional
     */
    MutableImpl(@JsonProperty("supportedClaims") final Set<Claim> supportedClaims) {
      this.supportedClaims = (supportedClaims == null ? emptySet() : new HashSet<>(supportedClaims));
    }

    @Override
    public URI getUserInfoEndpoint() {
      return userInfoEndpoint;
    }

    @Override
    public OpenIdConfiguration.Mutable withUserInfoEndpoint(final URI endpoint) {
      this.userInfoEndpoint = endpoint;
      return this;
    }

    @Override
    public Set<Claim> getSupportedClaims() {
      return unmodifiableSet(supportedClaims);
    }

    @Override
    public OpenIdConfiguration immutable() throws IllegalStateException {
      return new OpenIdConfiguration(userInfoEndpoint, supportedClaims);
    }

    @Override
    public OpenIdConfiguration.Mutable copy() {
      return new MutableImpl(supportedClaims);
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      MutableImpl mutable = (MutableImpl) o;
      return Objects.equals(supportedClaims, mutable.supportedClaims) &&
              Objects.equals(userInfoEndpoint, mutable.userInfoEndpoint);
    }

    @Override
    public int hashCode() {
      return Objects.hash(supportedClaims, userInfoEndpoint);
    }

    @Override
    public String toString() {
      return "Endpoint: " + userInfoEndpoint + ", claims: " + supportedClaims;
    }

  }

}
