/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.identityagent.controller;

import io.jsonwebtoken.*;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.concurrent.Immutable;
import javax.validation.constraints.NotNull;
import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.http.ResponseEntity.noContent;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@Immutable
@RestController
@RequestMapping("/callback/authentication")
public class AuthenticationCallbackController {

  private static final Logger LOG = LoggerFactory.getLogger(AuthenticationCallbackController.class);
  private static final String APPLICATION_JWT_VALUE = "application/jwt";

  private final JwtParser jwtParser;

  /**
   * @param signingKeyResolver Required
   */
  public AuthenticationCallbackController(final SigningKeyResolver signingKeyResolver) {
    Validate.notNull(signingKeyResolver, "Missing signing key resolver");
    jwtParser = Jwts.parser().setSigningKeyResolver(signingKeyResolver);
  }

  @RequestMapping(path = "/{uuid}", method = PUT, consumes = APPLICATION_JWT_VALUE, produces =
          APPLICATION_JSON_UTF8_VALUE)
  public ResponseEntity<String> putAuthenticationUUID(@NotNull @PathVariable("uuid") final
                                                      UUID authenticationUUID, @RequestBody final String content) {
    LOG.info("Received Authentication Callback on UUID '{}', JWT content: {}", authenticationUUID, content);
    try {
      final Jws<Claims> jwtContent = jwtParser.parseClaimsJws(content);
      LOG.info("Authentication Callback on UUID '{}', parsed content: {}", authenticationUUID, jwtContent.getBody());
    } catch (final RuntimeException e) {
      LOG.warn("Handling body as JWT failed: {}", content, e);
    }
    return noContent().build();
  }

}
