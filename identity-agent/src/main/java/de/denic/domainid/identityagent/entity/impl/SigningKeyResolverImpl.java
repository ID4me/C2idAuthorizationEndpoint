/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.identityagent.entity.impl;

import de.denic.domainid.jwks.JWKSetClient;
import de.denic.domainid.jwks.JWKSetParser;
import de.denic.domainid.wellknown.openid.OpenIdConfiguration;
import de.denic.domainid.wellknown.openid.OpenIdConfigurationClient;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.SigningKeyResolver;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.concurrent.Immutable;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Client;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.Key;
import java.security.PublicKey;
import java.util.Optional;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static org.apache.commons.lang3.StringUtils.isEmpty;

@Immutable
public final class SigningKeyResolverImpl implements SigningKeyResolver {

  private static final Logger LOG = LoggerFactory.getLogger(SigningKeyResolverImpl.class);

  private final OpenIdConfigurationClient openIdConfigurationClient;
  private final JWKSetClient jwkSetClient;

  /**
   * @param openIdConfigurationClient Required
   * @param jwkSetClient              Required
   */
  public SigningKeyResolverImpl(final OpenIdConfigurationClient openIdConfigurationClient,
                                final JWKSetClient jwkSetClient) {
    Validate.notNull(openIdConfigurationClient, "Missing OpenID Configuration client");
    Validate.notNull(jwkSetClient, "Missing JWK-Set client");
    this.openIdConfigurationClient = openIdConfigurationClient;
    this.jwkSetClient = jwkSetClient;
  }

  /**
   * @param restClient Required
   */
  public SigningKeyResolverImpl(final Client restClient) {
    this(new OpenIdConfigurationClient.Impl(restClient),
            new JWKSetClient.Impl(restClient, new JWKSetParser.Impl()));
  }

  @Override
  public Key resolveSigningKey(final JwsHeader header, final Claims claims) {
    final URI issuerUri = UriComponentsBuilder.fromUri(issuerUriFrom(claims)).build().toUri();
    final OpenIdConfiguration openIdConfigurationOfIssuer = openIdConfigurationClient.getFromBaseURI(issuerUri);
    final URI jwkSetUriOfIssuer = openIdConfigurationOfIssuer.getJwkSetURI();
    final Optional<PublicKey> result = jwkSetClient.getPublicKeyOfIdFrom(jwkSetUriOfIssuer, header.getKeyId());
    LOG.info("Querying issuer's URI '{}' for key ID '{}' yields {}", jwkSetUriOfIssuer, header.getKeyId(), result
            .orElse(null));
    return result.orElseThrow(() -> new IllegalArgumentException("Unknown key ID '" + header.getKeyId() + "'"));
  }

  private URI issuerUriFrom(final Claims claims) {
    final String issuer = claims.getIssuer();
    if (isEmpty(issuer)) {
      throw new WebApplicationException("Processing Access Token failed: Missing claim 'iss'", BAD_REQUEST);
    }

    try {
      return new URI(issuer);
    } catch (URISyntaxException e) {
      throw new WebApplicationException("Processing Access Token failed: Invalid claim 'iss'", BAD_REQUEST);
    }
  }

  @Override
  public Key resolveSigningKey(final JwsHeader header, final String plaintext) {
    throw new RuntimeException("Not implemented");
  }

}
