/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.identityagent.entity;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.denic.domainid.subject.OpenIdSubject;
import de.denic.domainid.subject.PseudonymSubject;
import io.jsonwebtoken.Claims;
import org.apache.commons.lang3.Validate;

import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;
import java.net.URI;
import java.nio.charset.Charset;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.time.ZonedDateTime.now;
import static java.util.Collections.emptyMap;
import static java.util.Collections.unmodifiableMap;

@JsonInclude(NON_NULL)
@Immutable
public final class ClaimValues {

  private static final String NAME = "name";
  private static final String GIVEN_NAME = "given_name";
  private static final String FAMILY_NAME = "family_name";
  private static final String MIDDLE_NAME = "middle_name";
  private static final String NICKNAME = "nickname";
  private static final String UPDATED_AT = "updated_at";
  private static final String EMAIL_VERIFIED = "email_verified";
  private static final String EMAIL = "email";
  private static final String PHONE_NUMBER_VERIFIED = "phone_number_verified";
  private static final String PREFERRED_USERNAME = "preferred_username";
  private static final String GENDER = "gender";
  private static final String WEBSITE = "website";
  private static final Charset UTF8 = Charset.forName("UTF8");

  private final OpenIdSubject subject;
  private final ZonedDateTime updatedAt;
  private final String name, givenName, middleName, familyName, nickName, preferredUserName, email;
  private final Boolean emailVerified, phoneNumberVerified, genderMale;
  private final URI website;
  private final Map<String, String> id4meClaims;

  private ClaimValues(final OpenIdSubject subject, final Boolean genderMale, final String name, final String givenName,
                      final String middleName, final String familyName, final String nickName, final String preferredUserName,
                      final String email, final Boolean emailVerified, final Boolean phoneNumberVerified,
                      final URI website, final ZonedDateTime updatedAt, final Map<String, String> id4meClaims) {
    this.subject = subject;
    this.name = name;
    this.givenName = givenName;
    this.updatedAt = updatedAt;
    this.familyName = familyName;
    this.nickName = nickName;
    this.middleName = middleName;
    this.preferredUserName = preferredUserName;
    this.emailVerified = emailVerified;
    this.email = email;
    this.phoneNumberVerified = phoneNumberVerified;
    this.genderMale = genderMale;
    this.website = website;
    this.id4meClaims = id4meClaims;
  }

  /**
   * @param claims Required
   * @return Never <code>null</code>
   */
  public static ClaimValues defaultValuesOf(final Claims claims) {
    Validate.notNull(claims, "Missing claims");
    final OpenIdSubject subject = new PseudonymSubject(claims.getSubject());
    final List<?> claimClaim = claims.get("clm", List.class);
    final String name = ifRequestedGiveDefaultStringValue(claimClaim, NAME);
    final String givenName = ifRequestedGiveDefaultStringValue(claimClaim, GIVEN_NAME);
    final String familyName = ifRequestedGiveDefaultStringValue(claimClaim, FAMILY_NAME);
    final String middleName = ifRequestedGiveDefaultStringValue(claimClaim, MIDDLE_NAME);
    final String nickName = ifRequestedGiveDefaultStringValue(claimClaim, NICKNAME);
    final String preferredUserName = ifRequestedGiveDefaultStringValue(claimClaim, PREFERRED_USERNAME);
    final String email = ifRequestedGiveDefaultEmailValue(claimClaim, EMAIL, subject);
    final Boolean emailVerified = ifRequestedGiveDefaultBooleanValue(claimClaim, EMAIL_VERIFIED);
    final Boolean phoneNumberVerified = ifRequestedGiveDefaultBooleanValue(claimClaim, PHONE_NUMBER_VERIFIED);
    final Boolean genderMale = ifRequestedGiveRandomBooleanValue(claimClaim, GENDER, subject);
    final ZonedDateTime updatedAt = ifRequestedGiveDefaultTimestampValue(claimClaim, UPDATED_AT);
    final URI website = ifRequestedGiveDefaultUriValue(claimClaim, WEBSITE, subject);
    final Map<String, String> id4meClaims = ifRequestedGiveDefaultId4meClaims(claimClaim);
    return new ClaimValues(subject, genderMale, name, givenName, middleName, familyName, nickName, preferredUserName,
            email, emailVerified, phoneNumberVerified, website, updatedAt, id4meClaims);
/*
profile	string	URL defaultValuesOf the End-User's profile page. The contents defaultValuesOf this Web page SHOULD be about the End-User.
picture	string	URL defaultValuesOf the End-User's profile picture. This URL MUST refer to an image file (for example, a PNG, JPEG, or GIF image file), rather than to a Web page containing an image. Note that this URL SHOULD specifically reference a profile photo defaultValuesOf the End-User suitable for displaying when describing the End-User, rather than an arbitrary photo taken by the End-User.
website	string	URL defaultValuesOf the End-User's Web page or blog. This Web page SHOULD contain information published by the End-User or an organization that the End-User is affiliated with.
birthdate	string	End-User's birthday, represented as an ISO 8601:2004 [ISO8601‑2004] YYYY-MM-DD format. The year MAY be 0000, indicating that it is omitted. To represent only the year, YYYY format is allowed. Note that depending on the underlying platform's date related function, providing just year can result in varying month and day, so the implementers need to take this factor into account to correctly process the dates.
zoneinfo	string	String from zoneinfo [zoneinfo] time zone database representing the End-User's time zone. For example, Europe/Paris or America/Los_Angeles.
locale	string	End-User's locale, represented as a BCP47 [RFC5646] language tag. This is typically an ISO 639-1 Alpha-2 [ISO639‑1] language code in lowercase and an ISO 3166-1 Alpha-2 [ISO3166‑1] country code in uppercase, separated by a dash. For example, en-US or fr-CA. As a compatibility note, some implementations have used an underscore as the separator rather than a dash, for example, en_US; Relying Parties MAY choose to accept this locale syntax as well.
phone_number	string	End-User's preferred telephone number. E.164 [E.164] is RECOMMENDED as the format defaultValuesOf this Claim, for example, +1 (425) 555-1212 or +56 (2) 687 2400. If the phone number contains an extension, it is RECOMMENDED that the extension be represented using the RFC 3966 [RFC3966] extension syntax, for example, +1 (604) 555-1234;ext=5678.
address	JSON object	End-User's preferred postal address. The value defaultValuesOf the address member is a JSON [RFC4627] structure containing some or all defaultValuesOf the members defined in Section 5.1.1.
*/
  }

  private static Map<String, String> ifRequestedGiveDefaultId4meClaims(final List<?> claimClaim) {
    if (claimClaim == null) {
      return emptyMap();
    }

    final Map<String, String> result = new HashMap<>();
    claimClaim.stream().
            filter(claimObject -> String.class.equals(claimObject.getClass())).
            map(claimObject -> (String) claimObject).
            filter(claim -> claim.startsWith("id4me.")).
            forEach(id4meClaim -> result.put(id4meClaim, "Id4me value of " + id4meClaim.substring("id4me.".length())));
    return unmodifiableMap(result);
  }

  private static URI ifRequestedGiveDefaultUriValue(final Collection<?> requestedClaims, final String nameOfClaim,
                                                    final OpenIdSubject subject) {
    return ifRequestedGiveDefaultValue(requestedClaims, nameOfClaim, URI.create("http://www." + subject.getName()));
  }

  private static Boolean ifRequestedGiveRandomBooleanValue(final Collection<?> requestedClaims, final String nameOfClaim,
                                                           final OpenIdSubject subject) {
    return ifRequestedGiveDefaultValue(requestedClaims, nameOfClaim, (subject.getName().getBytes(UTF8)[0] %
            2 == 0 ? TRUE : FALSE));
  }

  private static String ifRequestedGiveDefaultEmailValue(final Collection<?> requestedClaims, final String
          nameOfClaim, final OpenIdSubject subject) {
    return ifRequestedGiveDefaultValue(requestedClaims, nameOfClaim, "id4me-test@" + subject.getName());
  }

  private static Boolean ifRequestedGiveDefaultBooleanValue(final Collection<?> requestedClaims, final String nameOfClaim) {
    return ifRequestedGiveDefaultValue(requestedClaims, nameOfClaim, FALSE);
  }

  private static ZonedDateTime ifRequestedGiveDefaultTimestampValue(final Collection<?> requestedClaims, final String
          nameOfClaim) {
    return ifRequestedGiveDefaultValue(requestedClaims, nameOfClaim, now());
  }

  private static String ifRequestedGiveDefaultStringValue(final Collection<?> requestedClaims, final String nameOfClaim) {
    return ifRequestedGiveDefaultValue(requestedClaims, nameOfClaim, "Value of " + nameOfClaim);
  }

  private static <TYPE> TYPE ifRequestedGiveDefaultValue(final Collection<?> requestedClaims, final String
          nameOfClaim, final TYPE defaultValue) {
    return (requestedClaims == null ? null : (requestedClaims.contains(nameOfClaim) ? defaultValue : null));
  }

  /**
   * @param subject Required
   * @return Never <code>null</code>
   */
  public static Builder subject(final OpenIdSubject subject) {
    return new Builder(subject);
  }

  /**
   * @return Never <code>null</code>
   */
  @JsonProperty("sub")
  public OpenIdSubject getSubject() {
    return subject;
  }

  /**
   * @return Optional
   */
  @JsonProperty(NAME)
  public String getName() {
    return name;
  }

  /**
   * @return Optional
   */
  @JsonProperty(GIVEN_NAME)
  public String getGivenName() {
    return givenName;
  }

  /**
   * @return Optional
   */
  @JsonProperty(MIDDLE_NAME)
  public String getMiddleName() {
    return middleName;
  }

  /**
   * @return Optional
   */
  @JsonProperty(FAMILY_NAME)
  public String getFamilyName() {
    return familyName;
  }

  /**
   * @return Optional
   */
  @JsonProperty(NICKNAME)
  public String getNickName() {
    return nickName;
  }

  /**
   * @return Optional
   */
  @JsonProperty(PREFERRED_USERNAME)
  public String getPreferredUserName() {
    return preferredUserName;
  }

  /**
   * @return Optional
   */
  @JsonProperty(EMAIL)
  public String getEmail() {
    return email;
  }

  /**
   * @return Optional
   */
  @JsonProperty(EMAIL_VERIFIED)
  public Boolean getEmailVerified() {
    return emailVerified;
  }

  /**
   * @return Optional
   */
  @JsonProperty(PHONE_NUMBER_VERIFIED)
  public Boolean getPhoneNumberVerified() {
    return phoneNumberVerified;
  }

  /**
   * @return Optional
   */
  @JsonProperty(UPDATED_AT)
  public Long getUpdatedAtValue() {
    return (updatedAt == null ? null : updatedAt.toEpochSecond());
  }

  /**
   * @return Optional
   */
  @JsonProperty(GENDER)
  public String getGenderMaleValue() {
    return genderMale == null ? null : (genderMale ? "male" : "female");
  }

  /**
   * @return Optional
   */
  @JsonProperty(WEBSITE)
  public String getWebsiteValue() {
    return (website == null ? null : website.toASCIIString());
  }

  /**
   * @return Never <code>null</code> but unmodifiable.
   */
  @JsonAnyGetter
  public Map<String, String> getId4meClaims() {
    return id4meClaims;
  }

  /**
   * @see #getUpdatedAtValue()
   */
  @JsonIgnore
  public ZonedDateTime getUpdatedAt() {
    return updatedAt;
  }

  @NotThreadSafe
  public static final class Builder {

    private final OpenIdSubject subject;

    private Builder(final OpenIdSubject subject) {
      Validate.notNull(subject, "Missing subject");
      this.subject = subject;
    }

    public ClaimValues build() {
      return new ClaimValues(subject, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }

  }
}
