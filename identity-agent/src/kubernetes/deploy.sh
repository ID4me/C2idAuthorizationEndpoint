#!/bin/bash
set -e

_fullscriptpath="$(readlink -f ${BASH_SOURCE[0]})"
BASEDIR="$(dirname $_fullscriptpath)"

DOCKERREGISTRY=$DOCKER_REGISTRY
DOCKERREGISTRYPATH=test
IMAGENAME=identity-agent

NAMESPACE=$1
KUBECOMMAND="$KUBECTL --kubeconfig=$KUBECONFIG --namespace=${NAMESPACE}"
VERSION=$2

CONFIGMAPFILE=identity-agent_configmap_${NAMESPACE}-ns.yaml
if [ -f ${CONFIGMAPFILE} ]; then
	echo "Applying config map file: ${CONFIGMAPFILE}"
	$KUBECOMMAND apply --filename=${CONFIGMAPFILE}
fi

sed --in-place "s#__DOCKER_REGISTRY__/${DOCKERREGISTRYPATH}/${IMAGENAME}:latest#${DOCKERREGISTRY}/${DOCKERREGISTRYPATH}/${IMAGENAME}:${VERSION}#" identity-agent-dc.yaml
$KUBECOMMAND apply --filename=identity-agent-svc.yaml --record
$KUBECOMMAND apply --filename=identity-agent-dc.yaml --record
$KUBECOMMAND rollout status deployment/identity-agent --watch
