/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.identityagent.entity;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SigningKeyResolver;
import io.jsonwebtoken.impl.DefaultClaims;
import io.jsonwebtoken.impl.DefaultJwsHeader;
import io.jsonwebtoken.impl.crypto.MacProvider;
import org.junit.Before;
import org.junit.Test;

import java.security.Key;
import java.util.HashMap;
import java.util.Map;

import static io.jsonwebtoken.SignatureAlgorithm.HS256;
import static org.easymock.EasyMock.*;
import static org.junit.Assert.assertEquals;

public class BearerAccessTokenTest {

  private static final String KEY_ID_VALUE = "kid-value";
  private static final String SUBJECT_VALUE = "subject-value";

  private SigningKeyResolver signingKeyResolverMock;

  @Before
  public void setup() {
    signingKeyResolverMock = createMock(SigningKeyResolver.class);
  }

  @Test
  public void test() {
    final Key someKey = MacProvider.generateKey();
    final Map<String, Object> headers = new HashMap<>();
    headers.put("kid", KEY_ID_VALUE);
    final String compactJws = Jwts.builder().setHeader(headers).setSubject(SUBJECT_VALUE)
            .signWith(HS256, someKey).compact();
    final JwsHeader jwsHeader = new DefaultJwsHeader();
    jwsHeader.setKeyId(KEY_ID_VALUE);
    jwsHeader.setAlgorithm(HS256.getValue());
    final Claims claims = new DefaultClaims();
    claims.setSubject(SUBJECT_VALUE);
    expect(signingKeyResolverMock.resolveSigningKey(jwsHeader, claims)).andReturn(someKey);
    replay(signingKeyResolverMock);
    {
      final Claims claimsReceived = new BearerAccessToken("Bearer " + compactJws, signingKeyResolverMock).getClaims();
      verify(signingKeyResolverMock);
      assertEquals("Subject claim", SUBJECT_VALUE, claimsReceived.getSubject());
    }
  }

}
