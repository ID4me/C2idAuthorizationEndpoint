/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.identityagent.entity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import de.denic.domainid.subject.PseudonymSubject;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.impl.DefaultClaims;
import org.junit.Test;

import static de.denic.domainid.identityagent.entity.ClaimValues.defaultValuesOf;
import static de.denic.domainid.identityagent.entity.ClaimValues.subject;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;

public class ClaimValuesTest {

  private static final ObjectWriter CLAIM_VALUES_JSON_WRITER = new ObjectMapper().writerFor(ClaimValues.class);

  @Test
  public void serializeOnlyRequiredFields() throws JsonProcessingException {
    final ClaimValues CUT = subject(new PseudonymSubject("subject")).build();
    final String json = CLAIM_VALUES_JSON_WRITER.writeValueAsString(CUT);
    assertEquals("JSON serialization", "{\"sub\":\"subject\"}", json);
  }

  @Test
  public void noClaimRequested() throws JsonProcessingException {
    final Claims noClaims = new DefaultClaims();
    noClaims.setSubject("subject");
    {
      final ClaimValues CUT = defaultValuesOf(noClaims);
      final String json = CLAIM_VALUES_JSON_WRITER.writeValueAsString(CUT);
      assertEquals("Check content via JSON", "{\"sub\":\"subject\"}", json);
    }
  }

  @Test
  public void websiteClaimRequested() throws JsonProcessingException {
    final Claims claims = new DefaultClaims();
    claims.setSubject("subject");
    claims.put("clm", singletonList("website"));
    {
      final ClaimValues CUT = defaultValuesOf(claims);
      final String json = CLAIM_VALUES_JSON_WRITER.writeValueAsString(CUT);
      assertEquals("Check content via JSON", "{\"sub\":\"subject\",\"website\":\"http://www.subject\"}",
              json);
    }
  }

  @Test
  public void id4meClaimsRequested() throws JsonProcessingException {
    final Claims claims = new DefaultClaims();
    claims.setSubject("subject");
    claims.put("clm", asList("id4me.claim1", "id4me.claim999"));
    {
      final ClaimValues CUT = defaultValuesOf(claims);
      final String json = CLAIM_VALUES_JSON_WRITER.writeValueAsString(CUT);
      assertEquals("Check content via JSON", "{\"sub\":\"subject\",\"id4me.claim1\":\"Id4me value of claim1\"," +
                      "\"id4me.claim999\":\"Id4me value of claim999\"}",
              json);
    }
  }

}