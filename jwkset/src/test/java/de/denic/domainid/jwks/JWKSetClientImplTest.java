/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.jwks;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.security.interfaces.RSAPublicKey;
import java.util.List;

import static javax.ws.rs.core.Response.Status.OK;
import static org.easymock.EasyMock.*;
import static org.junit.Assert.assertTrue;

public class JWKSetClientImplTest {

  private static final MediaType APPLICATION_JWK_SET_JSON = MediaType.valueOf("application/jwk-set+json");

  private JWKSetClient CUT;
  private Client clientMock;
  private WebTarget webTargetMock;
  private Invocation.Builder invocationBuilderMock;
  private Response responseMock;

  @Before
  public void setUp() {
    clientMock = createMock(Client.class);
    webTargetMock = createMock(WebTarget.class);
    invocationBuilderMock = createMock(Invocation.Builder.class);
    responseMock = createMock(Response.class);
    CUT = new JWKSetClient.Impl(clientMock, new JWKSetParser.Impl());
  }

  @Test
  public void getPublicRSAKeysFrom() {
    final URI testURI = URI.create("http://key.source.test");
    expect(clientMock.target(testURI)).andReturn(webTargetMock);
    expect(webTargetMock.getUri()).andReturn(testURI).times(2);
    expect(webTargetMock.request()).andReturn(invocationBuilderMock);
    expect(invocationBuilderMock.accept(APPLICATION_JWK_SET_JSON)).andReturn
            (invocationBuilderMock);
    expect(invocationBuilderMock.get()).andReturn(responseMock);
    expect(responseMock.getStatus()).andReturn(200);
    expect(responseMock.getStatusInfo()).andReturn(OK);
    expect(responseMock.readEntity(String.class)).andReturn("{}");
    responseMock.close();
    replayMocks();
    {
      final List<Pair<KeyID, RSAPublicKey>> publicRsaKeys = CUT.getPublicRSAKeysFrom(testURI);
      verifyMocks();
      assertTrue("No keys", publicRsaKeys.isEmpty());
    }
  }

  private void verifyMocks() {
    verify(clientMock, webTargetMock, invocationBuilderMock, responseMock);
  }

  private void replayMocks() {
    replay(clientMock, webTargetMock, invocationBuilderMock, responseMock);
  }
}