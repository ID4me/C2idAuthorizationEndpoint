/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.jwks;

import org.junit.Test;

import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.*;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.Arrays;

import static org.apache.commons.codec.binary.Base64.decodeBase64;
import static org.apache.commons.codec.binary.Base64.encodeBase64String;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SignatureAndKeyPairTest {

  /**
   * Test key material generated <a href="https://mkjwk.org">here</a>.
   */
  private static final String SHA256_WITH_RSA = "SHA256withRSA";
  private static final String E_FROM_TEST_KEY = "AQAB";
  private static final String N_FROM_TEST_KEY = "9ZzYTdKhqjmA4p-LBqQpW1Py6YsmjB7jPAtZMkB-U1Kv78n7UUmt5Y1NWGaHZ_enRBD3S5Xa9e9egy5_Kf5f_JNrNgCYE_lrmWeiQoH1HAku-rNgc82ZnOzhllag4iD82OjL3GbJflAMfopdHQ9yYWykoDCBQhsaZeb0-YyMMhFUKP8gg66t2H-KX5LO7eRw6KwvjzJ4G-OS7NN7sfKKT4L41XspKEbsTSr4l3WQmX1TIQEOChiFI0dJEFfKdgyBfrp3YvHSnuwMNzpy0TljouJ21yW8rpiCC0tovngvE01kqA2aCL_n771sKUTESdU-Dk9uNybday8PGoQ3exFKvQ==";
  private static final String D_FROM_TEST_KEY = "S0TJtO-7w2Lj8I9cF6jvU6Gia5fNNFuy34_Kwe-pxvcsG5wWfI7Lz_FBprmk_WOTyvQ4FN3TwFKe8Cyro77kw46Z6G6-LEDfjJ-Jxj_1UaYu031tLY_wZ3rglyXtH99kHUUaVGzbiu4VJiJY7rLxJpIY0U6MDiooKNpicKd1v1GefDjunctytZN3yDSHwzc3lFfskrB1A0tRU7f6UK4iom3NjCrrVpsdV6b_Ro6SIVPVRLHgu8JzbydCKYvtqCUnMzbVf2zHgHWzKdq7R862yiW-AWDdjEsse-elOFu8YZoxjwOv9jv29sN2tz7sFX7VlcPCfEP6B46jUeOtkmGtMQ==";
  private static final BigInteger PRIVATE_EXPONENT_FROM_TEST_KEY = new BigInteger(1,
          decodeBase64(D_FROM_TEST_KEY));
  private static final Charset ASCII = Charset.forName("ASCII");
  private static final String TEST_STRING_TO_SIGN_AND_VERIFY = "The quick brown fox jumps over the lazy dog";
  private static final byte[] TEST_DATA_TO_SIGN_AND_VERIFY = TEST_STRING_TO_SIGN_AND_VERIFY.getBytes(ASCII);
  private static final KeyFactory RSA_KEY_FACTORY;

  static {
    try {
      RSA_KEY_FACTORY = KeyFactory.getInstance("RSA");
    } catch (final NoSuchAlgorithmException e) {
      throw new ExceptionInInitializerError(e);
    }
  }

  @Test
  public void matchKeyPairBySigningTestData() throws Exception {
    assertEquals("Correct padding of N", 0, N_FROM_TEST_KEY.length() % 4);
    assertEquals("Correct padding of E", 0, E_FROM_TEST_KEY.length() % 4);
    assertEquals("Correct padding of D", 0, D_FROM_TEST_KEY.length() % 4);
    System.out.println("Data to sign: " + TEST_STRING_TO_SIGN_AND_VERIFY);
    final byte[] testDataToSign = TEST_DATA_TO_SIGN_AND_VERIFY;
    System.out.println("Data to sign as ASCII: " + Arrays.toString(testDataToSign));
    System.out.println("Data to sign length: " + testDataToSign.length);
    final BigInteger modulus = new BigInteger(1, decodeBase64(N_FROM_TEST_KEY));
    final PrivateKey privateKey;
    {
      final BigInteger privateExponent = PRIVATE_EXPONENT_FROM_TEST_KEY;
      final RSAPrivateKeySpec privateKeySpec = new RSAPrivateKeySpec(modulus, privateExponent);
      assertEquals("Private key exponent", privateExponent, privateKeySpec.getPrivateExponent());
      assertEquals("Private key modulus", modulus, privateKeySpec.getModulus());
      privateKey = RSA_KEY_FACTORY.generatePrivate(privateKeySpec);
      System.out.println("Private key: " + privateKey);
      System.out.println("Private key algorithm: " + privateKey.getAlgorithm());
      System.out.println("Private key format: " + privateKey.getFormat());
      System.out.println("Private key encoded: " + encodeBase64String(privateKey.getEncoded()));
    }
    final byte[] signatureData;
    {
      final Signature signatureForSigning = Signature.getInstance(SHA256_WITH_RSA);
      signatureForSigning.initSign(privateKey);
      signatureForSigning.update(testDataToSign);
      signatureData = signatureForSigning.sign();
      System.out.println("Signature: " + Arrays.toString(signatureData));
      System.out.println("Signature as Base64: " + encodeBase64String(signatureData));
    }
    final PublicKey publicKey;
    {
      final BigInteger publicExponent = new BigInteger(1, decodeBase64(E_FROM_TEST_KEY));
      final RSAPublicKeySpec publicKeySpec = new RSAPublicKeySpec(modulus, publicExponent);
      assertEquals("Public key exponent", publicExponent, publicKeySpec.getPublicExponent());
      assertEquals("Public key modulus", modulus, publicKeySpec.getModulus());
      publicKey = RSA_KEY_FACTORY.generatePublic(publicKeySpec);
      System.out.println("Public key encoded: " + encodeBase64String(publicKey.getEncoded()));
    }
    {
      final Signature signatureForVerification = Signature.getInstance(SHA256_WITH_RSA);
      signatureForVerification.initVerify(publicKey);
      signatureForVerification.update(testDataToSign);
      assertTrue("Signature verified", signatureForVerification.verify(signatureData));
    }
  }

}
