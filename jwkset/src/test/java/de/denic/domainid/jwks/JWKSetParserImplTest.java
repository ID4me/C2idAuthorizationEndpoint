/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.jwks;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Before;
import org.junit.Test;

import java.security.KeyPair;
import java.security.PublicKey;
import java.security.Signature;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class JWKSetParserImplTest {

  /**
   * Test key material generated <a href="https://mkjwk.org">here</a>.
   */
  private static final String JWK_SET_RSA_INPUT = "{ \"keys\":" +
          " [" +
          "  {" +
          "   \"kty\": \"RSA\"," +
          "   \"d\": \"S0TJtO-7w2Lj8I9cF6jvU6Gia5fNNFuy34_Kwe-pxvcsG5wWfI7Lz_FBprmk_WOTyvQ4FN3TwFKe8Cyro77kw46Z6G6-LEDfjJ-Jxj_1UaYu031tLY_wZ3rglyXtH99kHUUaVGzbiu4VJiJY7rLxJpIY0U6MDiooKNpicKd1v1GefDjunctytZN3yDSHwzc3lFfskrB1A0tRU7f6UK4iom3NjCrrVpsdV6b_Ro6SIVPVRLHgu8JzbydCKYvtqCUnMzbVf2zHgHWzKdq7R862yiW-AWDdjEsse-elOFu8YZoxjwOv9jv29sN2tz7sFX7VlcPCfEP6B46jUeOtkmGtMQ\"," +
          "   \"e\": \"AQAB\"," +
          "   \"use\": \"sig\"," +
          "   \"kid\": \"test\"," +
          "   \"alg\": \"RS256\"," +
          "   \"n\": \"9ZzYTdKhqjmA4p-LBqQpW1Py6YsmjB7jPAtZMkB-U1Kv78n7UUmt5Y1NWGaHZ_enRBD3S5Xa9e9egy5_Kf5f_JNrNgCYE_lrmWeiQoH1HAku-rNgc82ZnOzhllag4iD82OjL3GbJflAMfopdHQ9yYWykoDCBQhsaZeb0-YyMMhFUKP8gg66t2H-KX5LO7eRw6KwvjzJ4G-OS7NN7sfKKT4L41XspKEbsTSr4l3WQmX1TIQEOChiFI0dJEFfKdgyBfrp3YvHSnuwMNzpy0TljouJ21yW8rpiCC0tovngvE01kqA2aCL_n771sKUTESdU-Dk9uNybday8PGoQ3exFKvQ\"" +
          "  }" +
          " ]" +
          "}";
  /**
   * Test key material generated <a href="https://mkjwk.org">here</a>.
   */
  private static final String JWK_SET_EC_INPUT = "{ \"keys\": [" +
          "  {" +
          "   \"kty\": \"EC\"," +
          "   \"d\": \"CHwHFV8wiwC5C0RU7ep9CX-Q0zkj00hgz-nbEETKHr1XU4XgOF8RCNHT8c_r7v-N\"," +
          "   \"use\": \"sig\"," +
          "   \"crv\": \"P-384\"," +
          "   \"kid\": \"test\"," +
          "   \"x\": \"NO5JeboWJj3KnV22gRS9kJ8ByQEwebS8IbgrqoAMPCeBT7dqHRDyzv3yJg8GaWlv\"," +
          "   \"y\": \"PFroYZJUPY4w58EAA00DiRYxU3qPjbKDa6fNjy_KzTWeZem_RknKPn5aHmhF-nIm\"," +
          "   \"alg\": \"ES256\"" +
          "  }" +
          " ]" +
          "}";
  private static final String TEST_DATA_TO_SIGN = "The quick brown fox jumps over the lazy dog";
  private static final String SHA256_WITH = "SHA256with";

  private JWKSetParser CUT;

  @Before
  public void setUp() {
    CUT = new JWKSetParser.Impl();
  }

  @Test
  public void keyPairFromRSAData() throws Exception {
    final List<Pair<KeyID, KeyPair>> keyPairs = CUT.toKeyPairs(JWK_SET_RSA_INPUT);
    assertEquals("Amount of key pairs", 1, keyPairs.size());
    assertSignatureVerificationWith(keyPairs.get(0).getRight());
  }

  @Test
  public void publicKeyFromRSAData() {
    final List<Pair<KeyID, PublicKey>> publicKeys = CUT.toPublicKeys(JWK_SET_RSA_INPUT);
    assertEquals("Amount of public keys", 1, publicKeys.size());
  }

  @Test
  public void noRsaKeyPairFromECData() {
    final List<Pair<KeyID, KeyPair>> keyPairs = CUT.toKeyPairs(JWK_SET_EC_INPUT);
    assertTrue("No key pairs", keyPairs.isEmpty());
  }

  private void assertSignatureVerificationWith(final KeyPair keyPair) throws Exception {
    final byte[] signedData;
    {
      final Signature signer = Signature.getInstance(SHA256_WITH + keyPair.getPrivate().getAlgorithm());
      signer.initSign(keyPair.getPrivate());
      signer.update(TEST_DATA_TO_SIGN.getBytes("ASCII"));
      signedData = signer.sign();
    }
    {
      final Signature verifier = Signature.getInstance(SHA256_WITH + keyPair.getPublic().getAlgorithm());
      verifier.initVerify(keyPair.getPublic());
      verifier.update(TEST_DATA_TO_SIGN.getBytes("ASCII"));
      assertTrue("Successful verification of test signature", verifier.verify(signedData));
    }
  }

}
