/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.jwks;

import org.apache.commons.lang3.Validate;

import javax.annotation.concurrent.Immutable;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.Objects;

import static org.apache.commons.codec.binary.Base64.decodeBase64;

@Immutable
public final class JwkSetRsaKeyPairParams {

  private static final KeyFactory RSA_KEY_FACTORY;

  static {
    try {
      RSA_KEY_FACTORY = KeyFactory.getInstance("RSA");
    } catch (final NoSuchAlgorithmException e) {
      throw new ExceptionInInitializerError(e);
    }
  }

  private final KeyID keyID;
  private final KeyPair keyPair;

  /**
   * @param keyId  Required
   * @param paramD Required
   * @param paramE Required
   * @param paramN Required
   */
  public JwkSetRsaKeyPairParams(final String keyId, final String paramD, final String paramE, final String paramN) throws
          IllegalArgumentException {
    Validate.notEmpty(paramD, "Missing parameter D");
    Validate.notEmpty(paramE, "Missing parameter E");
    Validate.notEmpty(paramN, "Missing parameter N");
    this.keyID = new KeyID(keyId);
    final byte[] dBytes = decodeBase64(paramD);
    final byte[] eBytes = decodeBase64(paramE);
    final byte[] nBytes = decodeBase64(paramN);
    final BigInteger d = new BigInteger(1, dBytes);
    final BigInteger e = new BigInteger(1, eBytes);
    final BigInteger n = new BigInteger(1, nBytes);
    final KeySpec publicKeySpec = new RSAPublicKeySpec(n, e);
    final KeySpec privateKeySpec = new RSAPrivateKeySpec(n, d);
    try {
      this.keyPair = new KeyPair(RSA_KEY_FACTORY.generatePublic(publicKeySpec),
              RSA_KEY_FACTORY.generatePrivate(privateKeySpec));
    } catch (final InvalidKeySpecException ex) {
      throw new IllegalArgumentException("Invalid material to build a keypair", ex);
    }
  }

  /**
   * @return Never <code>null</code>
   */
  public KeyID getKeyID() {
    return keyID;
  }

  /**
   * @return Never <code>null</code>
   */
  public KeyPair getKeyPair() {
    return keyPair;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    JwkSetRsaKeyPairParams that = (JwkSetRsaKeyPairParams) o;
    return Objects.equals(keyID, that.keyID) &&
            Objects.equals(keyPair, that.keyPair);
  }

  @Override
  public int hashCode() {
    return Objects.hash(keyID, keyPair);
  }

  @Override
  public String toString() {
    return "JwkSetRsaKeyPairParams ID=" + keyID;
  }
}
