/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.jwks;

import org.apache.commons.lang3.Validate;

import javax.annotation.concurrent.Immutable;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.Objects;

import static org.apache.commons.codec.binary.Base64.decodeBase64;

@Immutable
public final class JwkSetRsaPubKeyParams {

  private static final KeyFactory RSA_KEY_FACTORY;

  static {
    try {
      RSA_KEY_FACTORY = KeyFactory.getInstance("RSA");
    } catch (final NoSuchAlgorithmException e) {
      throw new ExceptionInInitializerError(e);
    }
  }

  private final KeyID keyID;
  private final RSAPublicKey publicKey;

  /**
   * @param keyId  Required
   * @param paramE Required
   * @param paramN Required
   */
  public JwkSetRsaPubKeyParams(final String keyId, final String paramE, final String paramN) throws
          IllegalArgumentException {
    Validate.notEmpty(paramE, "Missing parameter E");
    Validate.notEmpty(paramN, "Missing parameter N");
    this.keyID = new KeyID(keyId);
    final byte[] eBytes = decodeBase64(paramE);
    final byte[] nBytes = decodeBase64(paramN);
    final BigInteger e = new BigInteger(1, eBytes);
    final BigInteger n = new BigInteger(1, nBytes);
    final KeySpec publicKeySpec = new RSAPublicKeySpec(n, e);
    try {
      this.publicKey = (RSAPublicKey) RSA_KEY_FACTORY.generatePublic(publicKeySpec);
    } catch (final InvalidKeySpecException ex) {
      throw new IllegalArgumentException("Invalid material to build a keypair", ex);
    }
  }

  /**
   * @return Never <code>null</code>
   */
  public KeyID getKeyID() {
    return keyID;
  }

  /**
   * @return Never <code>null</code>
   */
  public PublicKey getPublicKey() {
    return publicKey;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    JwkSetRsaPubKeyParams that = (JwkSetRsaPubKeyParams) o;
    return Objects.equals(keyID, that.keyID) &&
            Objects.equals(publicKey, that.publicKey);
  }

  @Override
  public int hashCode() {
    return Objects.hash(keyID, publicKey);
  }

  @Override
  public String toString() {
    return "JwkSetRsaPubKeyParams ID=" + keyID;
  }

}
