/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.jwks;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.concurrent.Immutable;
import java.io.IOException;
import java.security.KeyPair;
import java.security.PublicKey;
import java.util.List;
import java.util.function.Predicate;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static java.util.stream.StreamSupport.stream;
import static org.apache.commons.codec.binary.Base64.encodeBase64String;
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;
import static org.apache.commons.lang3.StringUtils.isEmpty;

public interface JWKSetParser {

  /**
   * @param jwkSet Optional
   * @return Never <code>null</code>
   */
  List<Pair<KeyID, KeyPair>> toKeyPairs(String jwkSet);

  /**
   * @param jwkSetInput Optional
   * @return Never <code>null</code>
   */
  List<Pair<KeyID, PublicKey>> toPublicKeys(String jwkSetInput);

  /**
   * @param jwkSetInput Optional
   * @param filter      Optional
   * @return Never <code>null</code>
   */
  List<Pair<KeyID, PublicKey>> toFilteredPublicKeys(String jwkSetInput, Predicate<? super JsonNode> filter);

  @Immutable
  final class Impl implements JWKSetParser {

    private static final Logger LOG = LoggerFactory.getLogger(Impl.class);
    private static final JsonFactory JSON_FACTORY = new JsonFactory();
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private static final boolean NOT_PARALLEL = false;
    private static final Predicate<? super JsonNode> RSA_KEY_TYPE = keysArrayElementJsonNode -> "RSA"
            .equals(keysArrayElementJsonNode.get("kty").asText());
    private static final Predicate<? super JsonNode> SIGNATURE_USE_OF_KEY = keysArrayElementJsonNode -> "sig"
            .equals(keysArrayElementJsonNode.get("use").asText());

    @Override
    public List<Pair<KeyID, PublicKey>> toPublicKeys(final String jwkSetInput) {
      return toFilteredPublicKeys(jwkSetInput, anyValue -> true);
    }

    @Override
    public List<Pair<KeyID, PublicKey>> toFilteredPublicKeys(final String jwkSetInput, final Predicate<? super
            JsonNode> filter) {
      if (isEmpty(jwkSetInput)) {
        return emptyList();
      }

      final Predicate<? super JsonNode> filterToApply = defaultIfNull(filter, anyValue -> true);
      try (final JsonParser jwksJSONParser = JSON_FACTORY.createParser(jwkSetInput)) {
        final JsonNode jwksRootNode = OBJECT_MAPPER.readTree(jwksJSONParser);
        final List<Pair<KeyID, PublicKey>> result = stream(jwksRootNode.withArray("keys").spliterator(), NOT_PARALLEL)
                .filter(RSA_KEY_TYPE).filter(SIGNATURE_USE_OF_KEY).filter(filterToApply)
                .map(rsaSigJsonNode -> new JwkSetRsaPubKeyParams(rsaSigJsonNode.get("kid").asText(), rsaSigJsonNode.get
                        ("e").asText(),
                        rsaSigJsonNode.get("n").asText()))
                .map(rsaKeySpecPubKey -> new ImmutablePair<>(rsaKeySpecPubKey.getKeyID(), rsaKeySpecPubKey.getPublicKey()))
                .collect
                        (toList());
        if (LOG.isDebugEnabled()) {
          LOG.debug("Parsed JWK-set {} to RSA public keys {}", jwkSetInput,
                  result.stream().map(pubKey -> pubKey.getKey() + ":" + encodeBase64String(pubKey.getValue().getEncoded()
                  )).collect(toList()));
        }
        return result;
      } catch (final IOException e) {
        throw new RuntimeException("Parsing JWKSet '" + jwkSetInput + "' for RSA public keys failed", e);
      }
    }

    @Override
    public List<Pair<KeyID, KeyPair>> toKeyPairs(final String jwkSetInput) {
      if (isEmpty(jwkSetInput)) {
        return emptyList();
      }

      try (final JsonParser jwksJSONParser = JSON_FACTORY.createParser(jwkSetInput)) {
        final JsonNode jwksRootNode = OBJECT_MAPPER.readTree(jwksJSONParser);
        final List<Pair<KeyID, KeyPair>> result = stream(jwksRootNode.withArray("keys").spliterator(),
                NOT_PARALLEL).filter(RSA_KEY_TYPE).filter(SIGNATURE_USE_OF_KEY)
                .map(rsaSigJsonNode -> new JwkSetRsaKeyPairParams(rsaSigJsonNode.get("kid").asText(), rsaSigJsonNode.get
                        ("d").asText(), rsaSigJsonNode.get("e").asText(), rsaSigJsonNode.get("n").asText()))
                .map(rsaKeyParams -> new ImmutablePair<>(rsaKeyParams.getKeyID(), rsaKeyParams.getKeyPair()))
                .collect(toList());
        if (LOG.isDebugEnabled()) {
          LOG.debug("Parsed JWK-set {} to RSA key pairs of pub. keys {}", jwkSetInput, result.stream()
                  .map(keyPair -> keyPair.getKey() + ":" + encodeBase64String(keyPair.getRight().getPublic().getEncoded()))
                  .collect(toList()));
        }
        return result;
      } catch (final IOException e) {
        throw new RuntimeException("Parsing JWKSet '" + jwkSetInput + "' for RSA key pairs failed", e);
      }
    }

  }

}
