/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.jwks;

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.concurrent.Immutable;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

public interface JWKSetClient {

  /**
   * @param jwksURI Required
   * @return Never <code>null</code>
   */
  List<Pair<KeyID, RSAPublicKey>> getPublicRSAKeysFrom(URI jwksURI) throws JWKSetClientException;

  /**
   * @param jwksURI Required
   * @param keyId   Required
   * @return Never <code>null</code>
   */
  Optional<PublicKey> getPublicKeyOfIdFrom(URI jwksURI, String keyId) throws JWKSetClientException;

  @Immutable
  final class Impl implements JWKSetClient {

    private static final Logger LOG = LoggerFactory.getLogger(Impl.class);
    private static final String EMPTY_BODY_CONTENT = "";
    private static final MediaType APPLICATION_JWK_SET_JSON = MediaType.valueOf("application/jwk-set+json");

    private final Client restApiClient;
    private final JWKSetParser jwkSetParser;

    /**
     * @param restClient   Required
     * @param jwkSetParser Required
     */
    public Impl(final Client restClient, final JWKSetParser jwkSetParser) {
      Validate.notNull(restClient, "Missing REST client");
      Validate.notNull(jwkSetParser, "Missing JWK Set parser");
      this.restApiClient = restClient;
      this.jwkSetParser = jwkSetParser;
    }

    @Override
    public List<Pair<KeyID, RSAPublicKey>> getPublicRSAKeysFrom(final URI jwksURI) throws JWKSetClientException {
      final String jwkSetEntity = queryForJwkSetEntity(jwksURI);
      final List<Pair<KeyID, PublicKey>> publicRSAKeys = jwkSetParser.toPublicKeys(jwkSetEntity);
      return publicRSAKeys.stream().filter(element -> element.getValue() instanceof RSAPublicKey).map(element -> new
              ImmutablePair<>(element.getKey(), (RSAPublicKey) element.getValue()))
              .collect(toList());
    }

    @Override
    public Optional<PublicKey> getPublicKeyOfIdFrom(final URI jwksURI, final String keyId) throws
            JWKSetClientException {
      Validate.isTrue(isNotEmpty(keyId), "Missing key ID");
      final String jwkSetEntity = queryForJwkSetEntity(jwksURI);
      final List<Pair<KeyID, PublicKey>> publicKeys = jwkSetParser.toFilteredPublicKeys(jwkSetEntity
              , jsonNode -> {
                final JsonNode kidNode = jsonNode.get("kid");
                return kidNode != null && keyId.equals(kidNode.asText());
              });
      return publicKeys.stream().map(Pair::getValue).findAny();
    }

    private String queryForJwkSetEntity(final URI jwksURI) {
      Validate.notNull(jwksURI, "Missing JWK-Set URI");
      final WebTarget target = restApiClient.target(jwksURI);
      LOG.info("==> {}: >{}< ({})", "GET", EMPTY_BODY_CONTENT, target.getUri());
      final Instant startingAt = Instant.now();
      try (final Response jwksResponse = target.request().accept(APPLICATION_JWK_SET_JSON).get()) {
        final Duration invocationDuration = Duration.between(startingAt, Instant.now());
        LOG.info("<== {} {} ({}, elapsed {})", jwksResponse.getStatus(), jwksResponse.getStatusInfo(), target.getUri(),
                invocationDuration);
        return jwksResponse.readEntity(String.class);
      }
    }

  }

}
