/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.dns;

import org.junit.Test;

import static org.junit.Assert.*;

public class LookupResultTest {

  private static final boolean VALIDATED = true;

  private LookupResult<String> CUT;

  @Test
  public void testValidatedNULL() {
    CUT = new LookupResult.Impl<>(VALIDATED, (String) null);
    assertTrue("Validated?", CUT.isValidated());
    assertTrue("Empty?", CUT.isEmpty());
    assertFalse("Not available?", CUT.isAvailable());
    assertNull("No value", CUT.get());
  }

  @Test
  public void testNoValidatedValue() {
    final String someValue = "VALUE";
    CUT = new LookupResult.Impl<>(!VALIDATED, someValue);
    assertFalse("No validated?", CUT.isValidated());
    assertFalse("Not empty?", CUT.isEmpty());
    assertTrue("Available?", CUT.isAvailable());
    assertEquals("Value", someValue, CUT.get());
  }

}
