/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.dns.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import javax.annotation.concurrent.NotThreadSafe;

import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@NotThreadSafe
public final class InputStreamReadingRunnable implements Runnable {

  private static final Logger LOG = LoggerFactory.getLogger(InputStreamReadingRunnable.class);
  private static final Charset ASCII = Charset.forName("ASCII");

  private final BufferedReader reader;
  private final String errorMessageInCaseOfReadErrors;
  private final StringBuilder response = new StringBuilder();

  public InputStreamReadingRunnable(final InputStream inputStream, final String errorMessageInCaseOfReadErrors) {
    Validate.notNull(inputStream, "Missing input stream to handle");
    this.errorMessageInCaseOfReadErrors = errorMessageInCaseOfReadErrors;
    this.reader = new BufferedReader(new InputStreamReader(inputStream, ASCII));
  }

  public InputStreamReadingRunnable(final InputStream inputStream) {
    this(inputStream, "Reading content from stream failed");
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void run() {
    while (true) {
      try {
        final String line = reader.readLine();
        if (line == null) {
          return;
        }

        System.err.println("!!! \t" + line);
        response.append(line).append('\n');
      } catch (final IOException e) {
        if (LOG.isInfoEnabled()) {
          LOG.info(errorMessageInCaseOfReadErrors + ": " + e.getClass().getName() + ": " + e.getMessage());
          if (LOG.isDebugEnabled()) {
            LOG.debug("Stack trace of last exception:", e);
          }
        }
      }
    }
  }

  public String getResponse() {
    return response.toString();
  }
}
