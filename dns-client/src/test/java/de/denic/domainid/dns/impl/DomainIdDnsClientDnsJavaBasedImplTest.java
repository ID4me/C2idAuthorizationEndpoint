/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.dns.impl;

import de.denic.domainid.DomainId;
import de.denic.domainid.dns.DomainIdDnsClient;
import de.denic.domainid.dns.LookupResult;
import de.denic.domainid.dns.util.AbstractUnitTestStartingLocalNameserver;
import de.denic.domainid.dns.util.LoggingResolverProxy;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Before;
import org.junit.Test;
import org.xbill.DNS.SimpleResolver;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.io.IOException;
import java.net.IDN;
import java.net.InetAddress;
import java.net.URI;
import java.net.UnknownHostException;

import static java.net.InetAddress.getLoopbackAddress;
import static org.junit.Assert.*;

public class DomainIdDnsClientDnsJavaBasedImplTest extends AbstractUnitTestStartingLocalNameserver {

  private static final String SMALL_O_UMLAUT = "\u00f6";
  /**
   * Infos regarding usage of TLD "test": https://datatracker.ietf.org/doc/html/rfc6761#section-6.2
   */
  private static final DomainId TEST_DOMAIN_ID = new DomainId("domain-id.test");
  private static final DomainId OPENID_DOMAIN_ID = new DomainId("open-id.test");
  private static final DomainId DOMAIN_ID_WITH_ISS_CLP = new DomainId("domain-id-iss-clp.test");
  private static final DomainId TEST_DOMAIN_ID_WITH_PATH_IN_IAG_IAU = new DomainId("path-in-iau-iag-domain-id.test");
  private static final DomainId TEST_INTERNATIONALIZED_DOMAIN_ID = new DomainId("d" + SMALL_O_UMLAUT + "main-id.test");
  private static final InternetAddress UNMAPPED_INTERNET_ADDRESS, MAPPED_INTERNET_ADDRESS;
  private static final InetAddress IP_ADDRESS_OF_VALIDATING_NAMESERVER;
  private static final DomainId TEST_DOMAIN_ID_WITH_MISMATCHING_SUBDOMAIN_VERSION_TAG_PAIR = new DomainId("mismatching-subdomain-and-version.test");

  static {
    try {
      UNMAPPED_INTERNET_ADDRESS = InternetAddress.parse("unknown@provider.test")[0];
      MAPPED_INTERNET_ADDRESS = InternetAddress.parse("mapped@provider.test")[0];
      IP_ADDRESS_OF_VALIDATING_NAMESERVER = InetAddress.getByName("8.8.8.8"); // NOPMD
    } catch (final AddressException | UnknownHostException e) {
      throw new ExceptionInInitializerError(e);
    }
  }

  private DomainIdDnsClient CUT;

  @Before
  public void setup() throws IOException {
    final SimpleResolver resolverTargettingJNamedProcess = new SimpleResolver();
    resolverTargettingJNamedProcess.setAddress(getLoopbackAddress());
    resolverTargettingJNamedProcess.setPort(35353);
    CUT = new DomainIdDnsClientDnsJavaBasedImpl(resolverTargettingJNamedProcess);
  }

  @Test
  public void successfulLookupOfSubdomain_openid() {
    final LookupResult<Pair<URI, URI>> optHostURIs = CUT.lookupIdentityAuthAndAgentHostNameOf(OPENID_DOMAIN_ID);
    assertFalse("Identity Agent available", optHostURIs.isEmpty());
    assertEquals("Host of the Identity Authority", "id-auth.open-id.test",
            optHostURIs.get().getKey().getHost());
    assertEquals("Host of the Identity Agent", "ident-agent.open-id.test",
            optHostURIs.get().getValue().getHost());
    assertFalse("Response NOT validated", optHostURIs.isValidated());
  }

  @Test
  public void successfulLookupOfDomainIdWithIssuerAndClaimsProviderField() {
    final LookupResult<Pair<URI, URI>> optHostURIs = CUT.lookupIdentityAuthAndAgentHostNameOf(DOMAIN_ID_WITH_ISS_CLP);
    assertFalse("Identity Agent available", optHostURIs.isEmpty());
    assertEquals("Host of the Identity Authority", "issuer.domain-id.test",
            optHostURIs.get().getKey().getHost());
    assertEquals("Host of the Identity Agent", "claims-provider.domain-id.test",
            optHostURIs.get().getValue().getHost());
    assertFalse("Response NOT validated", optHostURIs.isValidated());
  }

  @Test
  public void successfulFallbackLookupToSubdomain_domainid() {
    final LookupResult<URI> optIdentityAgentHost = CUT.lookupIdentityAgentHostNameOf(TEST_DOMAIN_ID);
    assertFalse("Identity Agent available", optIdentityAgentHost.isEmpty());
    assertEquals("Host of the Identity Agent", "ident-agent.domain-id.test", optIdentityAgentHost.get().getHost());
    assertResponseNotValidated(optIdentityAgentHost);
  }

  @Test
  public void successfulLookupOfInternationalizedIdentityAgentHostName() {
    final LookupResult<URI> optIdentityAgentHost = CUT.lookupIdentityAgentHostNameOf(TEST_INTERNATIONALIZED_DOMAIN_ID);
    assertFalse("Identity Agent available", optIdentityAgentHost.isEmpty());
    assertEquals("Host of the Identity Agent", IDN.toASCII("ident-agent.d" + SMALL_O_UMLAUT + "main-id.test"),
            optIdentityAgentHost.get().getHost());
    assertResponseNotValidated(optIdentityAgentHost);
  }

  @Test
  public void lookupOfUnknownDomain() {
    final LookupResult<URI> optIdentityAgentHost = CUT.lookupIdentityAgentHostNameOf(new DomainId("unknown.test"));
    assertTrue("No Identity Agent available", optIdentityAgentHost.isEmpty());
    assertResponseNotValidated(optIdentityAgentHost);
  }

  @Test
  public void lookupOfDomainMissingDomainId() {
    final LookupResult<URI> optIdentityAgentHost = CUT
            .lookupIdentityAgentHostNameOf(new DomainId("missing-domain-id.test"));
    assertTrue("No Identity Agent available", optIdentityAgentHost.isEmpty());
    assertResponseNotValidated(optIdentityAgentHost);
  }

  @Test
  public void lookupOfDomainIdMissingVersion_Tag() {
    final LookupResult<URI> optIdentityAgentHost = CUT
            .lookupIdentityAgentHostNameOf(new DomainId("missing-v-tag-domain-id.test"));
    assertTrue("No Identity Agent available", optIdentityAgentHost.isEmpty());
    assertResponseNotValidated(optIdentityAgentHost);
  }

  @Test
  public void lookupOfDomainIdWithUnrecognizedVersion_TagValue() {
    final LookupResult<URI> optIdentityAgentHost = CUT
            .lookupIdentityAgentHostNameOf(new DomainId("unrecognized-v-tag-domain-id.test"));
    assertTrue("No Identity Agent available", optIdentityAgentHost.isEmpty());
    assertResponseNotValidated(optIdentityAgentHost);
  }

  @Test
  public void lookupIdentAgentOfDomainIdWithMalformedTXT_RR_IagTagWithoutValue() {
    final LookupResult<URI> optIdentityAgentHost = CUT.lookupIdentityAgentHostNameOf(new DomainId("malformed-domain-id.test"));
    assertTrue("No Identity Agent available", optIdentityAgentHost.isEmpty());
    assertResponseNotValidated(optIdentityAgentHost);
  }

  private void assertResponseNotValidated(LookupResult<URI> optIdentityAgentHost) {
    assertFalse("Response NOT validated", optIdentityAgentHost.isValidated());
  }

  @Test
  public void lookupHostsOfDomainIdWithMalformedTXT_RR_IagTagWithoutValue() {
    final LookupResult<Pair<URI, URI>> optHosts = CUT.lookupIdentityAuthAndAgentHostNameOf(new DomainId
            ("malformed-domain-id.test"));
    assertFalse("Some host information available", optHosts.isEmpty());
    assertEquals("Identity Authority host", "//id-auth.malformed-domain-id.test", optHosts.get().getKey().toASCIIString());
    assertNull("No Identity Agent host", optHosts.get().getValue());
    assertFalse("Response NOT validated", optHosts.isValidated());
  }

  @Test
  public void queryValidatingResolvingNameServers() throws UnknownHostException {
    final SimpleResolver resolverTargettingDenicValidatingNameServer = new SimpleResolver();
    resolverTargettingDenicValidatingNameServer.setAddress(IP_ADDRESS_OF_VALIDATING_NAMESERVER);
    resolverTargettingDenicValidatingNameServer.setPort(53);
    CUT = new DomainIdDnsClientDnsJavaBasedImpl(new LoggingResolverProxy(resolverTargettingDenicValidatingNameServer));
    {
      final LookupResult<URI> optIdentityAgentHost = CUT.lookupIdentityAgentHostNameOf(TEST_DOMAIN_ID);
      assertTrue("No Identity Agent available", optIdentityAgentHost.isEmpty());
      assertTrue("Validated response", optIdentityAgentHost.isValidated());
    }
  }

  @Test
  public void lookupDomainIdNotMappedByInternetAddress() {
    final LookupResult<DomainId> optIdentityAgentHost = CUT.lookupDomainIdMappedTo(UNMAPPED_INTERNET_ADDRESS);
    assertTrue("No DomainID mapped", optIdentityAgentHost.isEmpty());
    assertFalse("Response NOT validated", optIdentityAgentHost.isValidated());
  }

  @Test
  public void lookupDomainIdMappedByInternetAddress() {
    final LookupResult<DomainId> optIdentityAgentHost = CUT.lookupDomainIdMappedTo(MAPPED_INTERNET_ADDRESS);
    assertFalse("Got DomainID mapped by mail address", optIdentityAgentHost.isEmpty());
    assertEquals("Mapped DomainID", TEST_DOMAIN_ID, optIdentityAgentHost.get());
    assertFalse("Response NOT validated", optIdentityAgentHost.isValidated());
  }

  @Test
  public void successfulLookupOfIdentityAgentUriWithPath() throws Exception {
    final LookupResult<URI> optIdentityAgentHost = CUT
            .lookupIdentityAgentHostNameOf(TEST_DOMAIN_ID_WITH_PATH_IN_IAG_IAU);
    assertFalse("Identity Agent available", optIdentityAgentHost.isEmpty());
    assertEquals("Value of the Identity Agent", new URI(null, null, "domain-id.test", -1, "/ident-agent", null, null),
            optIdentityAgentHost.get());
    assertResponseNotValidated(optIdentityAgentHost);
  }

  @Test
  public void notSuccessfulLookupOfZoneWithMismatchingSubdomain_VersionTag_Pairs() {
    final LookupResult<URI> optIdentityAgentHost = CUT
            .lookupIdentityAgentHostNameOf(TEST_DOMAIN_ID_WITH_MISMATCHING_SUBDOMAIN_VERSION_TAG_PAIR);
    assertTrue("Identity Agent available", optIdentityAgentHost.isEmpty());
    assertResponseNotValidated(optIdentityAgentHost);
  }

}
