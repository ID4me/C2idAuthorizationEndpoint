/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.dns.impl;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.xbill.DNS.Name;

import static org.xbill.DNS.Name.fromConstantString;

/**
 * Simply contains all the DNS-related constants defined in DomainID specification.
 */
public interface SpecificationConstants {

  String IDENTITY_AUTHORITY_TAG = "iau";
  String ISSUER_TAG = "iss";
  String IDENTITY_AGENT_TAG = "iag";
  String CLAIMS_PROVIDER_TAG = "clp";
  String DOMAINID_VERSION_TAG_VALUE = "DID1";
  String OPENID_VERSION_TAG_VALUE = "OID1";
  String VERSION_TAG = "v";
  Name DOMAINID_SUBDOMAIN = fromConstantString("_domainid");
  Name OPENID_SUBDOMAIN = fromConstantString("_openid");
  Pair<Name, String> DOMAINID_SUBDOMAIN_AND_VERSION_TAG = new ImmutablePair<>(DOMAINID_SUBDOMAIN,
          DOMAINID_VERSION_TAG_VALUE);
  Pair<Name, String> OPENID_SUBDOMAIN_AND_VERSION_TAG = new ImmutablePair<>(OPENID_SUBDOMAIN,
          OPENID_VERSION_TAG_VALUE);

}
