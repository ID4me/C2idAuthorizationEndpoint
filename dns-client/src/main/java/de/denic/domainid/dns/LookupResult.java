/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.dns;

import javax.annotation.concurrent.Immutable;
import java.util.Objects;
import java.util.Optional;

import static java.util.Optional.ofNullable;

/**
 * @param <TYPE> Type of the provided information.
 */
public interface LookupResult<TYPE> {

  /**
   * @see LookupResult#empty(boolean)
   */
  @SuppressWarnings({"rawtypes", "unchecked"})
  LookupResult EMPTY_VALIDATED = new Impl(true, Optional.empty());
  /**
   * @see LookupResult#empty(boolean)
   */
  @SuppressWarnings({"rawtypes", "unchecked"})
  LookupResult EMPTY_UNVALIDATED = new Impl(false, Optional.empty());

  /**
   * "Pattern" copied from source code of {@link java.util.Collections#emptyList()}.
   *
   * @param <U> Generic type of {@link LookupResult}
   */
  @SuppressWarnings("unchecked")
  static <U> LookupResult<U> empty(final boolean validated) {
    return (validated ? EMPTY_VALIDATED : EMPTY_UNVALIDATED);
  }

  boolean isValidated();

  boolean isEmpty();

  default boolean isAvailable() {
    return !isEmpty();
  }

  /**
   * @return <code>null</code> ONLY if {@link #isEmpty()} returns <code>true</code>.
   */
  TYPE get();

  @Immutable
  final class Impl<T> implements LookupResult<T> {

    private final boolean validated;
    private final Optional<T> optionalValue;

    /**
     * @param validated     Whether resolver has provided validated response or not (usually indicated via AD bit in his response).
     * @param optionalValue Optional
     */
    public Impl(final boolean validated, final T optionalValue) {
      this(validated, ofNullable(optionalValue));
    }

    /**
     * @param validated     Whether resolver has provided validated response or not (usually indicated via AD bit in his response).
     * @param optionalValue Optional (but not good programming style to provide <code>null</code> ...)
     */
    public Impl(final boolean validated, final Optional<T> optionalValue) {
      this.validated = validated;
      this.optionalValue = optionalValue == null ? Optional.empty() : optionalValue;
    }

    @Override
    public boolean isEmpty() {
      return !optionalValue.isPresent();
    }

    @Override
    public T get() {
      return optionalValue.orElse(null);
    }

    @Override
    public boolean isValidated() {
      return validated;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Impl<?> impl = (Impl<?>) o;
      return validated == impl.validated &&
              Objects.equals(optionalValue, impl.optionalValue);
    }

    @Override
    public int hashCode() {
      return Objects.hash(validated, optionalValue);
    }

    @Override
    public String toString() {
      return (validated ? "V" : "NOT v") + "alidated: "
              + (optionalValue.isPresent() ? optionalValue.get().toString() : "<EMPTY>");
    }

  }

}
