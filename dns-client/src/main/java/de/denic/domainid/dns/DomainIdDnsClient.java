/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.dns;

import de.denic.domainid.DomainId;
import org.apache.commons.lang3.tuple.Pair;

import javax.mail.internet.InternetAddress;
import java.net.URI;

public interface DomainIdDnsClient {

  /**
   * @param domainId Required
   * @return Never <code>null</code>, optional value contains <code>internationalized</code> host name (like
   * <code>ident-agent.d\u00f6main-id.test</code> instead of <code>ident-agent.xn--dmain-id-n4a.test</code>)
   */
  LookupResult<URI> lookupIdentityAgentHostNameOf(DomainId domainId) throws DomainIdDnsClientException;

  /**
   * @param domainId Required
   * @return Never <code>null</code>, optional value contains <code>internationalized</code> host name (like
   * <code>ident-agent.d\u00f6main-id.test</code> instead of <code>ident-agent.xn--dmain-id-n4a.test</code>),
   * with <code>Identity Authority</code> as {@link Pair#getKey()} and <code>Identity Agent</code> as
   * {@link Pair#getValue()}.
   */
  LookupResult<Pair<URI, URI>> lookupIdentityAuthAndAgentHostNameOf(DomainId domainId) throws DomainIdDnsClientException;

  /**
   * @param internetAddress Required
   * @return Never <code>null</code>
   */
  LookupResult<DomainId> lookupDomainIdMappedTo(InternetAddress internetAddress) throws DomainIdDnsClientException;

}
