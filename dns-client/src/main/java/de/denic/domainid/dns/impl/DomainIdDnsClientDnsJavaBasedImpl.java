/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.dns.impl;

import de.denic.domainid.DomainId;
import de.denic.domainid.dns.*;
import org.apache.commons.codec.binary.Base32;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xbill.DNS.*;

import javax.mail.internet.InternetAddress;
import java.io.IOException;
import java.net.URI;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.function.Function;
import java.util.function.Predicate;

import static de.denic.domainid.dns.LookupResult.empty;
import static java.net.IDN.toASCII;
import static java.util.Arrays.stream;
import static java.util.Collections.emptyList;
import static org.apache.commons.codec.digest.DigestUtils.sha256;
import static org.apache.commons.lang3.ArrayUtils.isEmpty;
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;
import static org.apache.commons.lang3.StringUtils.defaultString;
import static org.apache.commons.lang3.StringUtils.join;
import static org.xbill.DNS.DClass.IN;
import static org.xbill.DNS.Flags.AD;
import static org.xbill.DNS.Flags.DO;
import static org.xbill.DNS.Message.newQuery;
import static org.xbill.DNS.Name.*;
import static org.xbill.DNS.Record.newRecord;
import static org.xbill.DNS.Section.ANSWER;
import static org.xbill.DNS.Type.TXT;

public final class DomainIdDnsClientDnsJavaBasedImpl implements DomainIdDnsClient, SpecificationConstants {

  private static final Logger LOG = LoggerFactory.getLogger(DomainIdDnsClientDnsJavaBasedImpl.class);
  private static final Base32 HEX_ENCODING_BASE32 = new Base32(true);
  private static final int EDNS_VERSION_ZERO = 0;
  private static final int PAYLOAD_4096 = 4096;
  @SuppressWarnings("rawtypes")
  private static final List NO_EDNS_OPTIONS = emptyList();
  private static final Predicate<? super Record> IS_TXT_RECORD = (record -> record instanceof TXTRecord);
  private static final Function<? super Record, ? extends TXTRecord> CAST_TO_TXT_RECORD = (record -> (TXTRecord) record);

  private final Resolver resolver;
  private final String resolverDescription;

  /**
   * @param resolver Optional, if missing an instance of {@link SimpleResolver} gets applied.
   */
  public DomainIdDnsClientDnsJavaBasedImpl(final Resolver resolver) {
    try {
      this.resolver = defaultIfNull(resolver, new SimpleResolver());
    } catch (final UnknownHostException e) {
      throw new RuntimeException("Internal error setting up default DNS resolver", e);
    }

    this.resolver.setEDNS(EDNS_VERSION_ZERO, PAYLOAD_4096, DO, NO_EDNS_OPTIONS);
    if (this.resolver instanceof SimpleResolver) {
      final SimpleResolver tempResolverRef = (SimpleResolver) this.resolver;
      this.resolverDescription = tempResolverRef.getAddress().toString();
    } else {
      this.resolverDescription = this.resolver.toString();
    }
  }

  @Override
  public LookupResult<URI> lookupIdentityAgentHostNameOf(final DomainId domainId) throws IdentityAgentLookupException {
    final LookupResult<Pair<URI, URI>> lookupResult = lookupIdentityAuthAndAgentHostNameOf(domainId);
    if (lookupResult.isEmpty()) {
      return empty(lookupResult.isValidated());
    }

    return new LookupResult.Impl<>(lookupResult.isValidated(), lookupResult.get().getValue());
  }

  @Override
  public LookupResult<Pair<URI, URI>> lookupIdentityAuthAndAgentHostNameOf(final DomainId domainId) throws
          DomainIdDnsClientException {
    final LookupResult<Pair<URI, URI>> openIdSubDomainLookup = lookupApplyingSubdomain(OPENID_SUBDOMAIN_AND_VERSION_TAG, domainId);
    return (openIdSubDomainLookup.isEmpty() ? lookupApplyingSubdomain(DOMAINID_SUBDOMAIN_AND_VERSION_TAG, domainId) :
            openIdSubDomainLookup);
  }

  /**
   * @param subDomainAndVersion Required
   * @param domainId            Required
   * @return Never <code>null</code>, <code>Identity Authority</code> as {@link Pair#getKey()}, <code>Identity
   * Agent</code> as {@link Pair#getValue()}
   */
  private LookupResult<Pair<URI, URI>> lookupApplyingSubdomain(final Pair<Name, String> subDomainAndVersion,
                                                               final DomainId domainId) {
    Validate.notNull(domainId, "Missing Domain ID");
    Validate.notNull(subDomainAndVersion, "Missing sub-domain/version info");
    final Name domainToLookup;
    final Record[] answerSectionsRecords;
    final boolean validatedResponse;
    try {
      domainToLookup = concatenate(subDomainAndVersion.getKey(), fromString(toASCII(domainId.getName()), root));
      LOG.info("Looking up TXT RRs of '{}' (resolver {})", domainToLookup, resolverDescription);
      final Message txtRRsOfDomainQuery = newQuery(newRecord(domainToLookup, TXT, IN));
      final Message queryResponse = resolver.send(txtRRsOfDomainQuery);
      LOG.debug("Lookup of '{}' yields '{}'", txtRRsOfDomainQuery, queryResponse);
      validatedResponse = checkForAuthenticatedResponse(queryResponse, TXT, domainToLookup);
      answerSectionsRecords = queryResponse.getSectionArray(ANSWER);
    } catch (final IOException e) {
      throw new IdentityAgentLookupException(domainId.getName(), e);
    }

    if (answerSectionsRecords == null || isEmpty(answerSectionsRecords)) {
      LOG.info("Lookup yields no TXT RR with '{}'", domainToLookup);
      return empty(validatedResponse);
    }

    final Optional<Pair<String, String>> optIauAndIagHostValues = stream(answerSectionsRecords).filter(IS_TXT_RECORD)
            .map(CAST_TO_TXT_RECORD).map(txtRecord -> parseIdentityAuthorityAndAgentHostNameFrom(txtRecord,
                    subDomainAndVersion.getValue()))
            .filter(Objects::nonNull).findAny();
    if (!optIauAndIagHostValues.isPresent()) {
      return empty(validatedResponse);
    }

    final Pair<String, String> hostValuePair = optIauAndIagHostValues.get();
    final URI identAuthorityURI = (hostValuePair.getKey() == null ? null : URI.create("//" + hostValuePair.getKey()));
    final URI identAgentURI = (hostValuePair.getValue() == null ? null : URI.create("//" + hostValuePair.getValue()));
    return new LookupResult.Impl<>(validatedResponse, new ImmutablePair<>(identAuthorityURI, identAgentURI));
  }

  /**
   * @param txtRecord               Required
   * @param expectedVersionTagValue Not empty
   * @return Optional
   */
  private Pair<String, String> parseIdentityAuthorityAndAgentHostNameFrom(final TXTRecord txtRecord,
                                                                          final String expectedVersionTagValue) {
    Validate.notEmpty(expectedVersionTagValue, "Missing expected version tag value");
    final String joinedDomainIdTxtRecordContent = getJoinedValueFromTXTRecord(txtRecord);
    final Properties txtRecordAsProperties = new Properties();
    stream(joinedDomainIdTxtRecordContent.split(";")).map(keyValueString -> keyValueString.split("="))
            .forEach(keyValueArray -> {
              if (keyValueArray.length > 1) {
                txtRecordAsProperties.put(keyValueArray[0].trim(), keyValueArray[1].trim());
              }
            });
    final String domainIdVersionTagValue = txtRecordAsProperties.getProperty(VERSION_TAG);
    if (!expectedVersionTagValue.equals(domainIdVersionTagValue)) {
      LOG.warn("Received domainID TXT record does not contain expected version tag '{}' but '{}'",
              expectedVersionTagValue,
              defaultString(domainIdVersionTagValue, ""));
      return null;
    }

    final String identityAuthorityOrIssuerTagValue = defaultString(
            txtRecordAsProperties.getProperty(ISSUER_TAG),
            txtRecordAsProperties.getProperty(IDENTITY_AUTHORITY_TAG));
    final String identityAgentOrClaimsProviderTagValue = defaultString(
            txtRecordAsProperties.getProperty(CLAIMS_PROVIDER_TAG),
            txtRecordAsProperties.getProperty(IDENTITY_AGENT_TAG));
    return new ImmutablePair<>(identityAuthorityOrIssuerTagValue, identityAgentOrClaimsProviderTagValue);
  }

  @Override
  public LookupResult<DomainId> lookupDomainIdMappedTo(final InternetAddress internetAddress)
          throws InternetAddressToDomainIdMappingException {
    Validate.notNull(internetAddress, "Missing mail address to map to DomainID");
    final String addressOfInternetAddress = internetAddress.getAddress();
    final int lastIndexOfAtSign = addressOfInternetAddress.lastIndexOf('@');
    final String localPartOfInternetAddress = addressOfInternetAddress.substring(0, lastIndexOfAtSign);
    final Name domainToLookup;
    final boolean validatedResponse;
    final Record[] answerSectionsRecords;
    try {
      final Name domainOfInternetAddress = fromString(addressOfInternetAddress.substring(lastIndexOfAtSign + 1), root);
      final Name domainIdSubDomainOfDomainOfAddress = concatenate(DOMAINID_SUBDOMAIN, domainOfInternetAddress);
      domainToLookup = fromString(
              HEX_ENCODING_BASE32.encodeAsString(sha256(localPartOfInternetAddress)).replaceAll("=", ""),
              domainIdSubDomainOfDomainOfAddress);
      LOG.info("Looking up TXT RRs of '{}'", domainToLookup);
      final Message txtRRsOfDomainQuery = newQuery(newRecord(domainToLookup, TXT, IN));
      final Message queryResponse = resolver.send(txtRRsOfDomainQuery);
      validatedResponse = checkForAuthenticatedResponse(queryResponse, TXT, domainToLookup);
      answerSectionsRecords = queryResponse.getSectionArray(ANSWER);
    } catch (final IOException e) {
      throw new InternetAddressToDomainIdMappingException(internetAddress, e);
    }

    if (answerSectionsRecords == null || isEmpty(answerSectionsRecords)) {
      LOG.debug("Lookup yields no TXT RR with '{}'", domainToLookup);
      return empty(validatedResponse);
    }

    return new LookupResult.Impl<>(validatedResponse,
            stream(answerSectionsRecords).filter(IS_TXT_RECORD).map(CAST_TO_TXT_RECORD)
                    .map(this::parseDomainIdFrom).filter(Objects::nonNull).map(DomainId::new)
                    .findAny());
  }

  /*
   * TODO: Later on throw exception instead of simply logging the fact of unauthenticated DNS response!
   */
  private boolean checkForAuthenticatedResponse(final Message response, final int record, final Name domain) {
    final boolean validated = response.getHeader().getFlag(AD);
    if (!validated) {
      LOG.warn("CAUTION: Received unauthenticated DNS response asking for {} records of '{}' (resolver '{}')",
              Type.string(record), domain, resolverDescription);
    }
    return validated;
  }

  private String parseDomainIdFrom(final TXTRecord txtRecord) {
    return join(getJoinedValueFromTXTRecord(txtRecord), "");
  }

  private String getJoinedValueFromTXTRecord(final TXTRecord txtRecord) {
    Validate.notNull(txtRecord, "Missing TXT record to parse");
    return join(txtRecord.getStrings(), "");
  }

}
