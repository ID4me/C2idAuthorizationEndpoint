/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid;

import de.denic.domainid.connect2id.common.AccessToken;
import de.denic.domainid.connect2id.common.ClientId;
import org.apache.commons.lang3.Validate;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;

import static java.net.URLEncoder.encode;
import static org.apache.commons.lang3.SystemUtils.USER_DIR;

/**
 * Just to hold on one place all parameters to target integration tests against instances running on
 * <code>Staging environment</code>.
 */
public abstract class StagingNamespaceParameters {

  public static final URL CONNECT2ID_URL;
  public static final String LOCAL_PATH_TO_DOMAINID_CA_TRUST_STORE = USER_DIR + "/src/dist/trusted-certificates.jks";
  public static final String CONNECT2ID_HOST = "c2id.staging.svc.cluster.local";
  public static final String AUTHENTICATION_ENDPOINT_HOST = "auth.staging.freedom-id.de";
  public static final URL AUTHENTICATION_ENDPOINT_URL, USER_DATABASE_RESOURCE_URL;
  public static final ClientId INTEGRATION_TEST_CLIENT_ID = new ClientId("integration-test-rp");
  public static final String INTEGRATION_TEST_CLIENT_SECRET = "IntegrationTestClientSecret01IntegrationTestClientSecret02";
  public static final String INTEGRATION_TEST_CLIENT_NAME = "Integration Test Client Name";
  public static final URI CLIENT_REDIRECT_URI;
  public static final String CLIENT_REDIRECT_URI_ENCODED;
  public static final DomainId TEST_DOMAIN_ID = new DomainId("test.denic.de");
  public static final String TEST_DOMAIN_ID_PASSWORD = "password.of.test.denic.de";
  public static final URL C2ID_AUTHORISATION_STORE_URL;
  private static final Charset ASCII = Charset.forName("ASCII");
  private static final String CLIENT_REDIRECT_URI_VALUE = "https://foo.redirect-uri.bar";
  private static AccessToken authorisationStoreAccessToken, clientRegistrationApiAccessToken;

  static {
    try {
      CONNECT2ID_URL = new URL("http://" + CONNECT2ID_HOST + ":8080"); // NOPMD
      USER_DATABASE_RESOURCE_URL = new URL("https://" + AUTHENTICATION_ENDPOINT_HOST + "/user");
      // To apply running tests againt cluster namespace 'staging':
      AUTHENTICATION_ENDPOINT_URL = new URL("https://" + AUTHENTICATION_ENDPOINT_HOST + "/login");
      // To apply running tests locally with 'kubectl port-forward ... 8123':
      //AUTHENTICATION_ENDPOINT_URL = new URL("http://localhost:8123/login");
      C2ID_AUTHORISATION_STORE_URL = new URL(CONNECT2ID_URL + "/authz-store/rest/v2/");
      CLIENT_REDIRECT_URI = new URI(CLIENT_REDIRECT_URI_VALUE);
      CLIENT_REDIRECT_URI_ENCODED = encode(CLIENT_REDIRECT_URI_VALUE, ASCII.name());
    } catch (final MalformedURLException | URISyntaxException | UnsupportedEncodingException e) {
      throw new ExceptionInInitializerError(e);
    }
  }

  public static AccessToken getClientRegistrationApiAccessToken() {
    synchronized (StagingNamespaceParameters.class) {
      if (clientRegistrationApiAccessToken == null) {
        clientRegistrationApiAccessToken = new AccessToken(requiredSystemProperty
                ("C2ID_CLIENT_REGISTRATION_API_TOKEN"));
      }
    }
    return clientRegistrationApiAccessToken;
  }

  public static AccessToken getAuthorisationStoreAccessToken() {
    synchronized (StagingNamespaceParameters.class) {
      if (authorisationStoreAccessToken == null) {
        authorisationStoreAccessToken = new AccessToken(requiredSystemProperty("C2ID_AUTHZ_STORE_API_TOKEN"));
      }
    }
    return authorisationStoreAccessToken;
  }

  private static String requiredSystemProperty(final String propertyName) {
    Validate.notEmpty(propertyName, "Missing name of System Property");
    final String propertyValue = System.getProperty(propertyName);
    Validate.notEmpty(propertyValue, "Missing value of System Property " + propertyName);
    return propertyValue.replaceAll("\"", "");
  }

}
