/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.tls;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509ExtendedTrustManager;
import javax.net.ssl.X509TrustManager;

import de.denic.domainid.StagingNamespaceParameters;

public final class MyX509TrustManager implements X509TrustManager {

  private static final char[] NO_PASSPHRASE = new char[0];

  private final X509TrustManager backingX509TrustManager;

  public MyX509TrustManager() {
    try {
      this.backingX509TrustManager = createBackingX509TrustManager();
    } catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException
        | NoSuchProviderException e) {
      throw new RuntimeException(
          "Internal error: Couldn't initialize an appropriate backing instance of " + X509ExtendedTrustManager.class,
          e);
    }
    System.err.println("### Backing trust manager: " + backingX509TrustManager);
  }

  private static X509TrustManager createBackingX509TrustManager()
      throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException, NoSuchProviderException {
    final KeyStore keyStoreOfTrustedRootCertificates = KeyStore.getInstance("JKS");
    try (final InputStream certTrustStoreFileInputStream = new FileInputStream(
        StagingNamespaceParameters.LOCAL_PATH_TO_DOMAINID_CA_TRUST_STORE)) {
      keyStoreOfTrustedRootCertificates.load(certTrustStoreFileInputStream, NO_PASSPHRASE);
    }
    final TrustManagerFactory backingTrustManagerFactory = TrustManagerFactory.getInstance("PKIX", "BC");
    backingTrustManagerFactory.init(keyStoreOfTrustedRootCertificates);
    final TrustManager trustManagersProvidedByBackingFactory[] = backingTrustManagerFactory.getTrustManagers();
    for (int i = 0; i < trustManagersProvidedByBackingFactory.length; i++) {
      if (trustManagersProvidedByBackingFactory[i] instanceof X509TrustManager) {
        return (X509TrustManager) trustManagersProvidedByBackingFactory[i];
      }
    }

    throw new RuntimeException("Internal error: Couldn't initialize an appropriate backing instance of "
        + X509ExtendedTrustManager.class.getName());
  }

  @Override
  public void checkClientTrusted(final X509Certificate[] chain, final String authType) throws CertificateException {
    backingX509TrustManager.checkClientTrusted(chain, authType);
  }

  @Override
  public void checkServerTrusted(final X509Certificate[] chain, final String authType) throws CertificateException {
    System.err.println("### Checking server trust for auth type '" + authType + "', cert chain contains " + chain.length
        + " elements");
    backingX509TrustManager.checkServerTrusted(chain, authType);
    for (int i = 0; i < chain.length; i++) {
      final X509Certificate x509Certificate = chain[i];
      System.err.println("###### Checking validity of: " + x509Certificate);
      x509Certificate.checkValidity();
    }
  }

  @Override
  public X509Certificate[] getAcceptedIssuers() {
    return backingX509TrustManager.getAcceptedIssuers();
  }

}