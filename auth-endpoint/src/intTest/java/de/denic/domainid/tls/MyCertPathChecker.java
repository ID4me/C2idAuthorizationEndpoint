/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.tls;

import java.security.cert.CertPathValidatorException;
import java.security.cert.Certificate;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.PKIXCertPathChecker;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Set;

import javax.annotation.concurrent.Immutable;

@Immutable
public final class MyCertPathChecker extends PKIXCertPathChecker {

  private static final Set<String> NO_EXTENSIONS_SUPPORTED = null;

  @Override
  public void check(final Certificate certificate, final Collection<String> unresolvedCriticalExtensions)
      throws CertPathValidatorException {
    if (certificate instanceof X509Certificate) {
      final X509Certificate x509Certificate = (X509Certificate) certificate;
      try {
        x509Certificate.checkValidity();
      } catch (final CertificateNotYetValidException e) {
        throw new CertPathValidatorException(
            "Cert " + certificate + " not valid before " + x509Certificate.getNotBefore(), e);
      } catch (final CertificateExpiredException e) {
        throw new CertPathValidatorException(
            "Cert " + certificate + " not valid after " + x509Certificate.getNotAfter(), e);
      }
    }
  }

  @Override
  public Set<String> getSupportedExtensions() {
    return NO_EXTENSIONS_SUPPORTED;
  }

  @Override
  public void init(final boolean forward) throws CertPathValidatorException {
    // Intentionlly left empty
  }

  @Override
  public boolean isForwardCheckingSupported() {
    return true;
  }

}