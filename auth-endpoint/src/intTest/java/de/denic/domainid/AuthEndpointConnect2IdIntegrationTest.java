/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebClientOptions;
import com.gargoylesoftware.htmlunit.WebResponse;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import de.denic.domainid.connect2id.authzstore.Connect2IdAuthorizationStoreAPI;
import de.denic.domainid.connect2id.clientregistration.OpenIdConnectClientRegistration;
import de.denic.domainid.connect2id.clientregistration.entity.OpenIdClientMetadata;
import de.denic.domainid.connect2id.common.Connect2IdException;
import de.denic.domainid.connect2id.tokenendpoint.TokenEndpointClient;
import de.denic.domainid.connect2id.tokenendpoint.TokenEndpointResponseEntity;
import de.denic.domainid.subject.PseudonymSubject;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SigningKeyResolver;
import org.apache.commons.text.RandomStringGenerator;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.junit.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.Key;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import static java.util.Collections.singletonList;
import static java.util.concurrent.TimeUnit.SECONDS;
import static javax.ws.rs.client.Entity.entity;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
import static org.apache.commons.lang3.ArrayUtils.addAll;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.join;
import static org.junit.Assert.*;

public class AuthEndpointConnect2IdIntegrationTest extends StagingNamespaceParameters implements SigningKeyResolver {

  private static final Logger LOG = LoggerFactory.getLogger(AuthEndpointConnect2IdIntegrationTest.class);
  private static final Charset UTF8 = Charset.forName("UTF8");
  private static final String[] DEFAULT_AUTHENTICATION_REQUEST_PARAMS = new String[]{"response_type=code",
          "client_id=" + INTEGRATION_TEST_CLIENT_ID, "redirect_uri=" + CLIENT_REDIRECT_URI_ENCODED, "scope=openid"};
  private static final RandomStringGenerator RANDOM_STATE_VALUE_GENERATOR = new RandomStringGenerator.Builder()
          .withinRange('a', 'z').build();
  private static final RandomStringGenerator RANDOM_NONCE_VALUE_GENERATOR = new RandomStringGenerator.Builder()
          .withinRange('a', 'z').build();
  private static final long WEB_CLIENT_TIMEOUT_SECS = 20L;

  private static Client restClient;
  private static OpenIdConnectClientRegistration openIdConnectClientRegistration;

  private WebClient webClient;
  private String currentRandomState, currentRandomNonce;

  private static void assertBusinessSessionCookiesAvailableWith(final WebClient webClient) {
    assertEquals("Received session cookies", 2, webClient.getCookieManager().getCookies().size());
  }

  @BeforeClass
  public static void setUpClass() {
    restClient = ClientBuilder.newClient();
    //restClient.register(new HttpRequestResponseHeaderLoggingFilter());
    openIdConnectClientRegistration = new OpenIdConnectClientRegistration.Impl(CONNECT2ID_URL, getClientRegistrationApiAccessToken(), restClient);
    final OpenIdClientMetadata integrationTestClientMetaData = new OpenIdClientMetadata(singletonList(CLIENT_REDIRECT_URI),
            INTEGRATION_TEST_CLIENT_ID,
            INTEGRATION_TEST_CLIENT_NAME,
            INTEGRATION_TEST_CLIENT_SECRET);
    assertTestDomainIdAvailableInUserDatabase();
    try {
      openIdConnectClientRegistration.deleteClientWith(INTEGRATION_TEST_CLIENT_ID);
    } catch (Connect2IdException e) {
      System.out.println("Deleting client '" + INTEGRATION_TEST_CLIENT_ID + "' failed: " + e);
    }
    openIdConnectClientRegistration.registerClient(integrationTestClientMetaData);
  }

  private static void assertTestDomainIdAvailableInUserDatabase() {
    try {
      final Response response = restClient.target(USER_DATABASE_RESOURCE_URL.toExternalForm()).path(TEST_DOMAIN_ID.getName()).
              queryParam("password", TEST_DOMAIN_ID_PASSWORD).request().buildPut(entity(EMPTY, APPLICATION_JSON_TYPE)).invoke();
      System.out.println("Registering test DomainID '" + TEST_DOMAIN_ID + "' response status: " + response.getStatus
              () + "/" + response.getStatusInfo() + ", entity: " + response.readEntity(String.class));
    } catch (Exception e) {
      System.out.println("Registering test DomainID '" + TEST_DOMAIN_ID + "' failed: " + e);
    }
  }

  @AfterClass
  public static void tearDownClass() {
    openIdConnectClientRegistration.deleteClientWith(INTEGRATION_TEST_CLIENT_ID);
    if (restClient != null) {
      restClient.close();
    }
  }

  @Before
  public void setUp() {
    webClient = new WebClient();
    final WebClientOptions webClientOptions = webClient.getOptions();
    webClientOptions.setUseInsecureSSL(true);
    webClientOptions.setPrintContentOnFailingStatusCode(true);
    webClientOptions.setRedirectEnabled(false);
    webClientOptions.setCssEnabled(false);
    webClientOptions.setTimeout((int) SECONDS.toMillis(WEB_CLIENT_TIMEOUT_SECS));
    currentRandomState = RANDOM_STATE_VALUE_GENERATOR.generate(12);
    //revokeAuthenticationsOf(TEST_DOMAIN_ID);
  }

  @After
  public void tearDown() {
    if (webClient != null) {
      webClient.close();
    }
  }

  private void sleepToGiveC2IdServerTimeForSessionReplication() throws InterruptedException {
    final long secondsToSleep = 2L;
    System.out.println("=== SLEEPING FOR " + secondsToSleep + " SECONDS ... ===");
    Thread.sleep(SECONDS.toMillis(secondsToSleep));
  }

  @Test
  public void authenticateSuccessfullyAndReauthenticateApplyingPromptValueNONE() throws Exception {
    successfulAuthenticationWorkflow();
    sleepToGiveC2IdServerTimeForSessionReplication();
    try {
      fail("Expecting FailingHttpStatusCodeException REDIRECTING to client's page WITHOUT any kind of re-authentication, but got: "
              + issueOpenIDAuthenticationRequestWithRandomNonce("prompt=none").asXml());
    } catch (final FailingHttpStatusCodeException expected) {
      assertEquals("REDIRECT status code", 303, expected.getStatusCode());
      final String locationHeaderValue = expected.getResponse().getResponseHeaderValue("Location");
      System.out.println("==> REDIRECT LOCATION immediately after issuing an OpenID Auth Request: " + locationHeaderValue);
      final List<NameValuePair> locationHeaderParams = parseLocationHeaderToNameValuePairs(expected);
      assertContainsNameValue(locationHeaderParams, "state", currentRandomState);
      assertContainsNoName(locationHeaderParams, "error");
    }
  }

  private void assertContainsNoName(List<NameValuePair> locationHeaderParams, String nameToQueryFor) {
    assertFalse("Location header contains param '" + nameToQueryFor + "'", locationHeaderParams.stream().anyMatch(nameValue
            -> nameValue.getName().equals(nameToQueryFor)));
  }

  @Test
  public void workflowApplyingPromptValueNONE() throws Exception {
    try {
      fail("Expecting FailingHttpStatusCodeException signaling REDIRECT, but got: "
              + issueOpenIDAuthenticationRequestWithRandomNonce("prompt=none").asXml());
    } catch (final FailingHttpStatusCodeException expected) {
      assertEquals("REDIRECT status code", 303, expected.getStatusCode());
      final List<NameValuePair> locationHeaderParams = parseLocationHeaderToNameValuePairs(expected);
      assertContainsNameValue(locationHeaderParams, "state", currentRandomState);
      assertContainsNameValue(locationHeaderParams, "error", "login_required");
      assertContainsNameValue(locationHeaderParams, "error_description", "Login required");
    }
  }

  private List<NameValuePair> parseLocationHeaderToNameValuePairs(FailingHttpStatusCodeException exception) {
    final URI locationHeaderValue = URI.create(exception.getResponse().getResponseHeaderValue("Location"));
    return URLEncodedUtils.parse(locationHeaderValue.getQuery(), UTF8);
  }

  @Test
  public void workflowApplyingPromptValueSELECT_ACCOUNT() throws Exception {
    final HtmlPage authenticationPage = issueOpenIDAuthenticationRequestWithRandomNonce("prompt=select_account");
    System.out.println(authenticationPage.asXml());
    try {
      fail("Expecting FailingHttpStatusCodeException signaling REDIRECT, but got: "
              + authenticateOnIdentityAuthority(authenticationPage).asXml());
    } catch (final FailingHttpStatusCodeException expected) {
      assertEquals("REDIRECT status code", 303, expected.getStatusCode());
      final List<NameValuePair> locationQueryParams = parseLocationHeaderToNameValuePairs(expected);
      assertContainsNameValue(locationQueryParams, "state", currentRandomState);
    }
  }

  @Test
  public void workflowApplyingClaimNAME() throws Exception {
    final HtmlPage authenticationPage = issueOpenIDAuthenticationRequestWithRandomNonce(
            "claims={\"userinfo\":{\"name\":{\"essential\":true}}}");
    LOG.info("### Auth page: {}", authenticationPage.asXml());
    final HtmlPage consentPage = authenticateOnIdentityAuthority(authenticationPage);
    LOG.info("### Consent page: {}", consentPage.asXml());
    try {
      fail("Expecting FailingHttpStatusCodeException signaling REDIRECT, but got: "
              + acceptConsentOn(consentPage).asXml());
    } catch (final FailingHttpStatusCodeException expected) {
      LOG.info("### Accept page headers: {}", expected.getResponse().getResponseHeaders());
      LOG.info("### Accept page content: {}", expected.getResponse().getContentAsString());
      assertEquals("REDIRECT status code", 303, expected.getStatusCode());
      final List<NameValuePair> locationQueryParams = parseLocationHeaderToNameValuePairs(expected);
      assertContainsNameValue(locationQueryParams, "state", currentRandomState);
      assertTrue("'code' encoded in redirection location URI", locationQueryParams.stream()
              .anyMatch(nameValuePair -> "code".equals(nameValuePair.getName())));
    }
  }

  @Test
  public void workflowSkippingConsentWithRejectedClaims() throws Exception {
    final String issuedClaims = "claims={\"userinfo\":{\"name\":null,\"nickname\":null}}";
    { // First step: Authenticate and consent on ONE claim only!
      assertTrue("Precondition: NO session cookies!", webClient.getCookieManager().getCookies().isEmpty());
      final HtmlPage authenticationPage = issueOpenIDAuthenticationRequest(issuedClaims);
      final HtmlPage consentPage = authenticateOnIdentityAuthority(authenticationPage);
      assertBusinessSessionCookiesAvailableWith(webClient);
      try {
        fail("Expecting FailingHttpStatusCodeException signaling REDIRECT, but got: "
                + acceptConsentOn(consentPage, "name"::equals).asXml());
      } catch (final FailingHttpStatusCodeException expected) {
        assertEquals("REDIRECT status code", 303, expected.getStatusCode());
        assertExchangeCodeForAccessTokenAtTokenEndpoint(expected.getResponse());
      }
    }
    sleepToGiveC2IdServerTimeForSessionReplication();
    webClient.getCookieManager().clearCookies();
    { // Applying NO session cookies: NO consenting step, but authentication!
      assertTrue("Precondition: NO session cookies!", webClient.getCookieManager().getCookies().isEmpty());
      final HtmlPage authenticationPage = issueOpenIDAuthenticationRequest(issuedClaims);
      try {
        fail("Expecting FailingHttpStatusCodeException signaling REDIRECT, but got: "
                + authenticateOnIdentityAuthority(authenticationPage).asXml());
      } catch (final FailingHttpStatusCodeException expected) {
        assertEquals("REDIRECT status code", 303, expected.getStatusCode());
        assertExchangeCodeForAccessTokenAtTokenEndpoint(expected.getResponse());
        assertBusinessSessionCookiesAvailableWith(webClient);
      }
    }
    sleepToGiveC2IdServerTimeForSessionReplication();
    { // Applying session cookies: NEITHER authentication NOR consenting step!
      assertEquals("Precondition: Session cookies available!", 2, webClient.getCookieManager().getCookies().size());
      try {
        fail("Expecting FailingHttpStatusCodeException signaling REDIRECT, but got: "
                + issueOpenIDAuthenticationRequest(issuedClaims).asXml());
      } catch (final FailingHttpStatusCodeException expected) {
        assertEquals("REDIRECT status code", 303, expected.getStatusCode());
        assertExchangeCodeForAccessTokenAtTokenEndpoint(expected.getResponse());
        assertBusinessSessionCookiesAvailableWith(webClient);
      }
    }
  }

  private void revokeAuthenticationsOf(final PseudonymSubject subject) {
    new Connect2IdAuthorizationStoreAPI.Impl(C2ID_AUTHORISATION_STORE_URL, getAuthorisationStoreAccessToken(),
            restClient).revokeAllAuthorizationsOf(subject);
  }

  private void assertContainsNameValue(final List<NameValuePair> locationQueryParams, final String nameToQueryFor,
                                       final String expectedValue) {
    final Optional<NameValuePair> locationQueryNameValuePair = locationQueryParams.stream()
            .filter(nameValuePair -> nameToQueryFor.equals(nameValuePair.getName())).findFirst();
    assertTrue("'" + nameToQueryFor + "' encoded in redirection location URI", locationQueryNameValuePair.isPresent());
    assertEquals("'" + nameToQueryFor + "' encoded in redirection location URI", expectedValue,
            locationQueryNameValuePair.get().getValue());
  }

  private HtmlPage issueOpenIDAuthenticationRequest(final String... additionalOpenIDAuthenticationRequestParameters)
          throws IOException {
    final String[] allButRandomAuthenticationRequestParams = addAll(DEFAULT_AUTHENTICATION_REQUEST_PARAMS,
            additionalOpenIDAuthenticationRequestParameters);
    final String[] allAuthenticationRequestParams = addAll(allButRandomAuthenticationRequestParams,
            "state=" + currentRandomState);
    final URL targetURL = new URL(AUTHENTICATION_ENDPOINT_URL + "?" + join(allAuthenticationRequestParams, '&'));
    return webClient.getPage(targetURL);
  }

  private HtmlPage issueOpenIDAuthenticationRequestWithRandomNonce(
          final String... additionalOpenIDAuthenticationRequestParameters) throws IOException {
    currentRandomNonce = RANDOM_NONCE_VALUE_GENERATOR.generate(12);
    final String[] withAdditionalRandomParams = addAll(additionalOpenIDAuthenticationRequestParameters,
            "nonce=" + currentRandomNonce);
    return issueOpenIDAuthenticationRequest(withAdditionalRandomParams);
  }

  private HtmlPage authenticateOnIdentityAuthority(final HtmlPage identityAuthorityAuthenticationPage) throws IOException {
    final HtmlForm authenticationForm = identityAuthorityAuthenticationPage.getForms().get(0);
    authenticationForm.getInputByName("domainID").setValueAttribute(TEST_DOMAIN_ID.getName());
    authenticationForm.getInputByName("password").setValueAttribute(TEST_DOMAIN_ID_PASSWORD);
    return authenticationForm.getButtonByName("submit").click();
  }

  private HtmlPage acceptConsentOn(final HtmlPage consentPage) throws IOException {
    return acceptConsentOn(consentPage, claim -> true);
  }

  private HtmlPage acceptConsentOn(final HtmlPage consentPage, final Predicate<String> acceptClaimPredicate)
          throws IOException {
    final String formActionAttribute = "/consent";
    final Optional<HtmlForm> optConsentHtmlForm = consentPage.getForms().stream()
            .filter(form -> formActionAttribute.equals(form.getActionAttribute())).findFirst();
    assertTrue("Found HTML form with action attribute '" + formActionAttribute + "'", optConsentHtmlForm.isPresent());
    final HtmlForm consentForm = optConsentHtmlForm.get();
    consentForm.getInputsByName("claim").stream().filter(input -> acceptClaimPredicate.test(input.getValueAttribute()))
            .forEach(input -> input.setChecked(true));
    consentForm.getInputByName("remember").setChecked(true);
    return consentForm.getButtonsByName("consenting").stream().peek(System.out::println)
            .filter(button -> "true".equals(button.getValueAttribute())).findFirst().orElseThrow(() ->
                    new RuntimeException("Found no button with name 'consenting' to click")).click();
  }

  private void successfulAuthenticationWorkflow() throws IOException {
    final HtmlPage identityAuthorityAuthenticationPage = issueOpenIDAuthenticationRequestWithRandomNonce();
    try {
      fail("Expecting FailingHttpStatusCodeException REDIRECTING to client's page, but got: "
              + authenticateOnIdentityAuthority(identityAuthorityAuthenticationPage).asXml());
    } catch (final FailingHttpStatusCodeException expected) {
      assertEquals("REDIRECT status code", 303, expected.getStatusCode());
      System.out.println(
              "==> REDIRECT LOCATION after authentication: " + expected.getResponse().getResponseHeaderValue("Location"));
    }
  }

  private void assertExchangeCodeForAccessTokenAtTokenEndpoint(final WebResponse finalResponse)
          throws URISyntaxException {
    final URI locationHeaderURI = URI.create(finalResponse.getResponseHeaderValue("Location"));
    final URI redirectionURI = new URI(locationHeaderURI.getScheme(), locationHeaderURI.getUserInfo(),
            locationHeaderURI.getHost(), locationHeaderURI.getPort(), locationHeaderURI.getPath(), null, null);
    final Optional<String> optAccessCode = URLEncodedUtils.parse(locationHeaderURI.getQuery(), UTF8).stream()
            .filter(nameValuePair -> "code".equals(nameValuePair.getName()))
            .map(NameValuePair::getValue).findFirst();
    final TokenEndpointResponseEntity tokenEndpointResponseEntity = new TokenEndpointClient.Impl(restClient).exchange(
            CONNECT2ID_URL.toURI().resolve("/token"), INTEGRATION_TEST_CLIENT_ID, INTEGRATION_TEST_CLIENT_SECRET, redirectionURI,
            optAccessCode.orElseThrow(() -> new RuntimeException("Found no Access Code")));
    final String accessTokenValue = tokenEndpointResponseEntity.getAccessToken();
    try {
      Jwts.parser().setSigningKeyResolver(this).parseClaimsJws(accessTokenValue);
    } catch (final IllegalArgumentException expected) {
      // EXPECTED cause we do not provide pub key to verify signature, see method resolveSigningKey(...)
    }
  }

  @Override
  public Key resolveSigningKey(final JwsHeader accessTokenHeader, final Claims accessTokenClaims) {
    System.out.println("Access Token claims = [" + accessTokenClaims + "]");
    assertEquals("Claim 'id4me.identifier'", TEST_DOMAIN_ID.getName(), accessTokenClaims.get("id4me.identifier"));
    assertEquals("Claim 'client_id'", INTEGRATION_TEST_CLIENT_ID.getPlain(), accessTokenClaims.get("client_id"));
    return null; // Intentionally cause not interested in verification of JWT signature here.
  }

  @Override
  public Key resolveSigningKey(final JwsHeader header, final String plaintext) {
    throw new UnsupportedOperationException("header = [" + header + "], plaintext = [" + plaintext + "]");
  }

}
