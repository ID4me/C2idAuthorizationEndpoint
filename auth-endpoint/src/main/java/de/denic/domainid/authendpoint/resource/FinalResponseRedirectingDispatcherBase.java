/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.authendpoint.resource;

import com.codahale.metrics.Counter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.SharedMetricRegistries;
import de.denic.domainid.connect2id.authzsession.entity.FinalResponse;
import de.denic.domainid.connect2id.common.SubjectSessionId;
import org.apache.commons.lang3.Validate;

import javax.annotation.concurrent.Immutable;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import java.net.URI;
import java.time.Duration;
import java.util.Optional;

import static com.codahale.metrics.MetricRegistry.name;
import static java.time.Duration.ofHours;
import static java.util.Locale.ENGLISH;

/**
 * Additionally throws {@link WebApplicationException} with response code {@link javax.ws.rs.core.Response.Status#SEE_OTHER} on
 * invocation of {@link #handle(FinalResponse)} yielding a redirecting response sent back to the client.
 *
 * @see NonRedirectableErrorHandlingDispatcherBase
 */
@Immutable
public abstract class FinalResponseRedirectingDispatcherBase
        extends NonRedirectableErrorHandlingDispatcherBase<ResponseBuilder> {

  private static final Counter MODE_FRAGMENT_IN_FINAL_RESPONSE_COUNTER = new Counter();
  private static final Counter MODE_QUERY_IN_FINAL_RESPONSE_COUNTER = new Counter();
  private static final Counter MODE_FORM_POST_IN_FINAL_RESPONSE_COUNTER = new Counter();
  private static final Counter MODE_OTHER_IN_FINAL_RESPONSE_COUNTER = new Counter();
  private static final NewCookie[] NO_NEW_SESSION_COOKIES = new NewCookie[0];
  private static final Duration ONE_HOUR_DURATION = ofHours(1L);

  static {
    final MetricRegistry metricRegistry = SharedMetricRegistries.tryGetDefault();
    if (metricRegistry != null) {
      metricRegistry.register(name(FinalResponseRedirectingDispatcherBase.class.getSimpleName(), "modeFragment",
              "count"), MODE_FRAGMENT_IN_FINAL_RESPONSE_COUNTER);
      metricRegistry.register(name(FinalResponseRedirectingDispatcherBase.class.getSimpleName(), "modeFormPost",
              "count"), MODE_FORM_POST_IN_FINAL_RESPONSE_COUNTER);
      metricRegistry.register(name(FinalResponseRedirectingDispatcherBase.class.getSimpleName(), "modeQuery",
              "count"), MODE_QUERY_IN_FINAL_RESPONSE_COUNTER);
      metricRegistry.register(name(FinalResponseRedirectingDispatcherBase.class.getSimpleName(), "modeOther",
              "count"), MODE_OTHER_IN_FINAL_RESPONSE_COUNTER);
    }
  }

  private final SubjectSessionCookies subjectSessionCookies;

  /**
   * @param subjectSessionCookies Required
   */
  protected FinalResponseRedirectingDispatcherBase(final SubjectSessionCookies subjectSessionCookies) {
    Validate.notNull(subjectSessionCookies, "Missing subject session cookies");
    this.subjectSessionCookies = subjectSessionCookies;
  }

  @Override
  public final ResponseBuilder handle(final FinalResponse response) {
    final Counter counterToIncrement;
    switch (response.getMode().toLowerCase(ENGLISH)) {
      case "query":
        counterToIncrement = MODE_QUERY_IN_FINAL_RESPONSE_COUNTER;
        break;
      case "form_post":
        counterToIncrement = MODE_FORM_POST_IN_FINAL_RESPONSE_COUNTER;
        break;
      case "fragment":
        counterToIncrement = MODE_FRAGMENT_IN_FINAL_RESPONSE_COUNTER;
        break;
      default:
        counterToIncrement = MODE_OTHER_IN_FINAL_RESPONSE_COUNTER;
    }
    counterToIncrement.inc();
    final Optional<SubjectSessionId> optSubjectSessionId = response.getSubjectSessionId();
    final NewCookie[] newSubjectSessionCookies = optSubjectSessionId.map(subjectSessionId -> subjectSessionCookies
            .cookiesOfNewActiveIdentity(response.getSubjectSessionId().get(), ONE_HOUR_DURATION)).orElse
            (NO_NEW_SESSION_COOKIES);
    final URI redirectionLocation = response.getParameters().getUri();
    return Response.seeOther(redirectionLocation).cookie(newSubjectSessionCookies);
  }

}
