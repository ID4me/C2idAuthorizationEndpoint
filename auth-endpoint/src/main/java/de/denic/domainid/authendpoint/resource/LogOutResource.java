/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.authendpoint.resource;

import com.codahale.metrics.annotation.Timed;
import de.denic.domainid.authendpoint.model.LogOutRequest;
import de.denic.domainid.connect2id.authzsession.Connect2IdAuthorizationSessionAPI;
import de.denic.domainid.connect2id.authzsession.entity.FinalResponse;
import de.denic.domainid.connect2id.common.NotFoundException;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.concurrent.Immutable;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.time.Duration;

import static de.denic.domainid.authendpoint.resource.SubjectSessionCookies.from;
import static javax.ws.rs.core.MediaType.TEXT_HTML;
import static javax.ws.rs.core.Response.Status.GONE;

@Path("logout")
@Immutable
public final class LogOutResource {

  private static final Logger LOG = LoggerFactory.getLogger(LogOutResource.class);
  private static final String TEXT_HTML_CHARSET_UTF8 = TEXT_HTML + ";charset=UTF-8";

  private final Connect2IdAuthorizationSessionAPI connect2IdAuthorisationSessionAPI;

  /**
   * @param connect2IdAuthorisationSessionAPI Required
   */
  public LogOutResource(final Connect2IdAuthorizationSessionAPI connect2IdAuthorisationSessionAPI) {
    Validate.notNull(connect2IdAuthorisationSessionAPI, "Missing Connect2Id Authorisation Session API component");
    this.connect2IdAuthorisationSessionAPI = connect2IdAuthorisationSessionAPI;
  }

  @POST
  @Produces(TEXT_HTML_CHARSET_UTF8)
  @Timed
  public Response viaPOST(@Context final HttpHeaders httpHeaders,
                          @NotNull @FormParam("sessionID") final String sessionID) {
    try {
      return handle(LogOutRequest.build().sessionId(sessionID).build(), from(httpHeaders));
    } catch (final NotFoundException e) {
      LOG.warn("Log-out fails cause Connect2Id Authorisation Session '{}' not available", sessionID, e);
      throw new WebApplicationException(Response.status(GONE).type(TEXT_HTML).entity(
              "<html><body><h2>Error: Log-out fails cause required backend resource not available any more.</h2></body></html>")
              .build());
    }
  }

  private Response handle(final LogOutRequest request, final SubjectSessionCookies subjectSessionCookies) {
    LOG.info("Received logout: {} (cookies: {})", request, subjectSessionCookies);
    // TODO: Report Logout to Identity Agent?
    // reportAuthenticationResultToIdentityAgent(request, FAILED_AUTHENTICATION);
    final FinalResponse finalResponse = connect2IdAuthorisationSessionAPI.cancelSession(request.getSessionId());
    return new MyVisitor(subjectSessionCookies).handle(finalResponse);
  }

  // private void reportAuthenticationResultToIdentityAgent(final LogOutRequest request,
  // final boolean successfulAuthentication) {
  // final DomainId domainId = request.getDomainId();
  // final LookupResult<HostName> subjectsIdentityAgentHostName = lookupIdentityAgentHostNameOf(domainId);
  // if (subjectsIdentityAgentHostName.isAvailable()) {
  // final ClientId clientId = connect2IdAuthorisationSessionAPI.getSession(request.getSessionId())
  // .getInitialAuthenticationRequest().getClientId();
  // try {
  // identityAgentClient.authentication(domainId, subjectsIdentityAgentHostName.get(),
  // request.getCreationTimeStamp(), successfulAuthentication, clientId);
  // } catch (final IdentityAgentClientException e) {
  // LOG.warn("Callback on DomainID's '{}' Identity Agent '{}' failed: {}", domainId, subjectsIdentityAgentHostName,
  // e);
  // }
  // }
  // }

  // private LookupResult<HostName> lookupIdentityAgentHostNameOf(final DomainId domainId) {
  // final LookupResult<HostName> identityAgentsHostName = domainIdDnsClient.lookupIdentityAgentHostNameOf(domainId);
  // if (identityAgentsHostName.isAvailable()) {
  // LOG.info("Found Identity Agent of DomainID '{}': '{}'", domainId, identityAgentsHostName.get());
  // } else {
  // LOG.warn("Empty lookup of Identity Agent of DomainID '{}'", domainId);
  // }
  // return identityAgentsHostName;
  // }

  @Immutable
  private static final class MyVisitor extends NonRedirectableErrorHandlingDispatcherBase<Response> {

    private static final Duration LIFETIME_OF_NEWLY_SET_COOKIES = Duration.ofDays(1L);
    private static final NewCookie[] NO_NEW_COOKIES = new NewCookie[0];

    private final SubjectSessionCookies subjectSessionCookies;

    /**
     * @param subjectSessionCookies Optional
     */
    private MyVisitor(final SubjectSessionCookies subjectSessionCookies) {
      this.subjectSessionCookies = subjectSessionCookies;
    }

    @Override
    public Response handle(final FinalResponse entity) {
      final URI redirectionLocation = entity.getParameters().getUri();
      final NewCookie[] cookiesToSet = (subjectSessionCookies == null ? NO_NEW_COOKIES : subjectSessionCookies
              .cookiesToChangeWithLogout(LIFETIME_OF_NEWLY_SET_COOKIES));
      return Response.seeOther(redirectionLocation).cookie(cookiesToSet).build();
    }

  }

}
