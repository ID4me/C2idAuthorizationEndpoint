/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.authendpoint;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.Metric;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.SharedMetricRegistries;
import com.warrenstrange.googleauth.GoogleAuthenticator;
import de.denic.domainid.authendpoint.config.Config;
import de.denic.domainid.authendpoint.config.Connect2IdConfig;
import de.denic.domainid.authendpoint.config.DnsConfig;
import de.denic.domainid.authendpoint.config.ResolverConfig;
import de.denic.domainid.authendpoint.metric.Version;
import de.denic.domainid.authendpoint.resource.*;
import de.denic.domainid.authendpoint.view.ValidationErrorMessageHTMLBodyWriter;
import de.denic.domainid.connect2id.authzsession.Connect2IdAuthorizationSessionAPI;
import de.denic.domainid.connect2id.authzstore.Connect2IdAuthorizationStoreAPI;
import de.denic.domainid.connect2id.clientregistration.OpenIdConnectClientRegistration;
import de.denic.domainid.connect2id.healthcheck.Connect2IdServerHealthCheck;
import de.denic.domainid.connect2id.subjectsessionstore.Connect2IdSubjectSessionStoreAPI;
import de.denic.domainid.dbaccess.UserAuthenticationDAO;
import de.denic.domainid.dns.DomainIdDnsClient;
import de.denic.domainid.dns.impl.DomainIdDnsClientDnsJavaBasedImpl;
import de.denic.domainid.identityagent.IdentityAgentClient;
import de.denic.domainid.identityagent.impl.IdentityAgentClientImpl;
import de.denic.domainid.jwks.JWKSetClient;
import de.denic.domainid.jwks.JWKSetParser;
import de.denic.domainid.jwks.KeyID;
import de.denic.domainid.wellknown.openid.OpenIdConfiguration;
import de.denic.domainid.wellknown.openid.OpenIdConfigurationClient;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.configuration.ConfigurationException;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.jersey.setup.JerseyEnvironment;
import io.dropwizard.lifecycle.Managed;
import io.dropwizard.lifecycle.setup.LifecycleEnvironment;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.ViewBundle;
import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.dropwizard.DropwizardExports;
import io.prometheus.client.exporter.MetricsServlet;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.jdbi.v3.core.Jdbi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xbill.DNS.SimpleResolver;

import javax.annotation.concurrent.Immutable;
import javax.ws.rs.client.Client;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.security.*;
import java.security.interfaces.RSAPublicKey;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.codahale.metrics.MetricRegistry.name;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.codec.binary.Base64.encodeBase64String;
import static org.apache.commons.lang3.SystemUtils.JAVA_RUNTIME_VERSION;
import static org.apache.commons.lang3.SystemUtils.JAVA_VM_VERSION;

@Immutable
public final class App extends Application<Config> implements Managed {

  private static final Logger LOG = LoggerFactory.getLogger(App.class);
  private static final String SHA256_WITH = "SHA256with";
  private static final Charset UTF8 = Charset.forName("UTF8");
  private static final byte[] TEST_DATA_TO_SIGN = "The quick brown fox jumps over the lazy dog".getBytes(UTF8);

  private static void furtherEnrichConfigByQueryingWellKnownOpenIdConfiguration(final Config configuration, final Client restClient)
      throws URISyntaxException, ConfigurationException {
    final OpenIdConfiguration openIdConfiguration = new OpenIdConfigurationClient.Impl(restClient)
        .getFromBaseURI(configuration.getConnect2IdConfig().getBaseURL().toURI());
    final URI issuerURI = openIdConfiguration.getIssuer();
    LOG.info("Connect2Id server Issuer URI: {}", issuerURI);
    configuration.setIssuerURI(issuerURI);
    checkMatchingOfConfiguredPrivateAndLookedUpPublicJWKSKeyByQuerying(openIdConfiguration.getJwkSetURI(), configuration, restClient);
  }

  private static void checkMatchingOfConfiguredPrivateAndLookedUpPublicJWKSKeyByQuerying(final URI jwkSetURI, final Config configuration, final Client restClient)
      throws ConfigurationException {
    final List<Pair<KeyID, RSAPublicKey>> publicRSAKeysFromC2ID = new JWKSetClient.Impl(restClient, new JWKSetParser.Impl()).getPublicRSAKeysFrom(jwkSetURI);
    if (publicRSAKeysFromC2ID.isEmpty()) {
      throw new RuntimeException("Configured Connect2Id server does not provide public JWKS RSA key under " + jwkSetURI);
    }

    final List<PrivateKey> configuredPrivateKeys = configuration.getPrivateKeyConfig().getAllAvailablePrivateKeys();
    final List<ImmutablePair<PrivateKey, byte[]>> privateKeyAndItsSignedData = configuredPrivateKeys.stream()
        .map(privateKey -> new ImmutablePair<>(privateKey, createSignatureData(privateKey))).collect(toList());
    final Optional<ImmutablePair<KeyID, KeyPair>> optMatchingKeyPair = publicRSAKeysFromC2ID.stream()
        .map(keyIdAndPubKey -> new ImmutablePair<>(keyIdAndPubKey.getKey(),
            new KeyPair(keyIdAndPubKey.getValue(), getPrivateKeyCreatingSignatureVerifiableWith(keyIdAndPubKey.getValue(), privateKeyAndItsSignedData))))
        .filter(keyIdAndPair -> keyIdAndPair.getValue().getPrivate() != null).findFirst();
    if (!optMatchingKeyPair.isPresent()) {
      throw new PrivateKeyConfigurationException("No configured private key matches any public key offered on JWK-Set endpoint " + jwkSetURI);
    }

    final Pair<KeyID, KeyPair> matchingKeyPair = optMatchingKeyPair.get();
    LOG.info("Found configured private key matching public key {}", encodeBase64String(matchingKeyPair.getValue().getPublic().getEncoded()));
    configuration.getPrivateKeyConfig().setMatchingPrivateKey(new ImmutablePair<>(matchingKeyPair.getKey(), matchingKeyPair.getValue().getPrivate()));
  }

  /**
   * @param privateKeyAndItsSignedData Required
   * @param publicKey                  Required
   * @return Optional
   */
  private static PrivateKey getPrivateKeyCreatingSignatureVerifiableWith(final PublicKey publicKey,
      final List<ImmutablePair<PrivateKey, byte[]>> privateKeyAndItsSignedData) {
    final Optional<PrivateKey> optMatchingPrivateKey = privateKeyAndItsSignedData.stream().filter(privKeyAndSignedData -> {
      try {
        final Signature verifier = Signature.getInstance(SHA256_WITH + publicKey.getAlgorithm());
        verifier.initVerify(publicKey);
        verifier.update(TEST_DATA_TO_SIGN);
        return verifier.verify(privKeyAndSignedData.getRight());
      } catch (final NoSuchAlgorithmException | InvalidKeyException | SignatureException e) {
        // Ignored: Definitely NOT matching public key!
        return false;
      }
    }).map(ImmutablePair::getLeft).findFirst();
    return optMatchingPrivateKey.orElse(null);
  }

  private static byte[] createSignatureData(final PrivateKey privateKey) {
    try {
      final Signature signingSignature = Signature.getInstance(SHA256_WITH + privateKey.getAlgorithm());
      signingSignature.initSign(privateKey);
      signingSignature.update(TEST_DATA_TO_SIGN);
      return signingSignature.sign();
    } catch (NoSuchAlgorithmException | SignatureException | InvalidKeyException e) {
      throw new RuntimeException("Digitally signing arbitrary payload with configured private key failed", e);
    }
  }

  public static void main(final String[] args) throws Exception {
    new App().run(args);
  }

  @Override
  public String getName() {
    return "OAuth 2.0 Authorization Endpoint version " + App.class.getPackage().getImplementationVersion();
  }

  @Override
  public void run(final Config configuration, final Environment environment) throws Exception {
    LOG.info("Running on JRE version {}, JVM implementation version {}", JAVA_RUNTIME_VERSION, JAVA_VM_VERSION);
    final CollectorRegistry collectorRegistry = CollectorRegistry.defaultRegistry;
    collectorRegistry.register(new DropwizardExports(environment.metrics()));
    final MetricsServlet metricServlet = new MetricsServlet(collectorRegistry);
    environment.admin().addServlet("prometheusMetrics", metricServlet).addMapping("/prometheusMetrics");
    final LifecycleEnvironment lifecycleEnvironment = environment.lifecycle();
    lifecycleEnvironment.manage(this);
    lifecycleEnvironment.manage(new Version());
    final DataSourceFactory dataSourceFactory = configuration.getDataSourceFactory();
    final Jdbi jdbi = new JdbiFactory().build(environment, dataSourceFactory, "authenticationDB");
    LOG.info("Using user authentication database {}", configuration.getDataSourceFactory().getUrl());
    final Client restClientTargettingC2IdServer = new JerseyClientBuilder(environment).using(configuration.getJerseyClientConfiguration()).build("C2ID");
    furtherEnrichConfigByQueryingWellKnownOpenIdConfiguration(configuration, restClientTargettingC2IdServer);
    final Connect2IdConfig c2IdConfig = configuration.getConnect2IdConfig();
    final Connect2IdAuthorizationSessionAPI.Impl c2IdAuthorisationSessionAPI = new Connect2IdAuthorizationSessionAPI.Impl(c2IdConfig.getAuthorisationSessionAPIURL(),
        c2IdConfig.getAuthorisationSessionAPIBearerAccessToken(), restClientTargettingC2IdServer);
    final Connect2IdAuthorizationStoreAPI c2IdAuthorisationStoreAPI = new Connect2IdAuthorizationStoreAPI.Impl(c2IdConfig.getAuthorisationStoreAPIURL(),
        c2IdConfig.getAuthorisationStoreAPIBearerAccessToken(), restClientTargettingC2IdServer);
    final Connect2IdSubjectSessionStoreAPI c2IdSubjectSessionStore = new Connect2IdSubjectSessionStoreAPI.Impl(c2IdConfig.getSubjectSessionStoreURL(),
        c2IdConfig.getSubjectSessionStoreBearerAccessToken(), restClientTargettingC2IdServer);
    final UserAuthenticationDAO userAuthenticationDAO = jdbi.onDemand(UserAuthenticationDAO.class);
    registerUserAuthenticationMetrics(userAuthenticationDAO);
    userAuthenticationDAO.migrateDatabaseIfNecessaryApplying(dataSourceFactory.getDriverClass(), dataSourceFactory.getUrl(), dataSourceFactory.getUser(),
        dataSourceFactory.getPassword());
    final DomainIdDnsClient domainIdDnsClient = prepareDomainIdDnsClient(configuration);
    final JerseyEnvironment jerseyEnvironment = environment.jersey();
    final Pair<KeyID, Key> matchingSigningKey = configuration.getPrivateKeyConfig().getMatchingPrivateKey();
    final Client restClientTargettingIdentityAgent = new JerseyClientBuilder(environment).using(configuration.getJerseyClientConfiguration()).build("IdentityAgent");
    final IdentityAgentClient identityAgentClient = new IdentityAgentClientImpl(restClientTargettingIdentityAgent, matchingSigningKey,
        configuration.getIssuerURI().orElse(null));
    jerseyEnvironment.register(ValidationErrorMessageHTMLBodyWriter.class);
    final OpenIdConnectClientRegistration openIdConnectClientRegistration = new OpenIdConnectClientRegistration.Impl(c2IdConfig.getBaseURL(),
        c2IdConfig.getOpenIdConnectClientRegistrationBearerAccessToken(), restClientTargettingC2IdServer);
    jerseyEnvironment.register(new OpenIdAuthenticationRequestResource(c2IdAuthorisationSessionAPI, c2IdSubjectSessionStore, c2IdAuthorisationStoreAPI,
        userAuthenticationDAO, openIdConnectClientRegistration));
    jerseyEnvironment.register(new AuthenticationResource(c2IdAuthorisationSessionAPI, userAuthenticationDAO, domainIdDnsClient, identityAgentClient,
        c2IdAuthorisationStoreAPI, new GoogleAuthenticator()));
    jerseyEnvironment.register(new ConsentingResource(c2IdAuthorisationSessionAPI, c2IdSubjectSessionStore));
    jerseyEnvironment.register(new LogOutResource(c2IdAuthorisationSessionAPI));
    jerseyEnvironment.register(new SessionInfoResource(c2IdSubjectSessionStore));
    jerseyEnvironment.register(new UserDatabaseResource(userAuthenticationDAO, c2IdAuthorisationStoreAPI));
    environment.healthChecks().register("connect2IdServer", new Connect2IdServerHealthCheck(restClientTargettingC2IdServer, c2IdConfig));
  }

  private void registerUserAuthenticationMetrics(final UserAuthenticationDAO userAuthenticationDAO) {
    final MetricRegistry metricRegistry = SharedMetricRegistries.tryGetDefault();
    if (metricRegistry != null) {
      final Metric userAuthCounterMetric = (Gauge<Integer>) userAuthenticationDAO::userAuthenticationCount;
      metricRegistry.register(name(UserAuthenticationDAO.class, "userAuthentication", "count"), userAuthCounterMetric);
      final Metric userAuthWithTOTPCounterMetric = (Gauge<Integer>) userAuthenticationDAO::userAuthenticationWithTOTPCount;
      metricRegistry.register(name(UserAuthenticationDAO.class, "userAuthenticationWithTOTP", "count"), userAuthWithTOTPCounterMetric);
    }
  }

  private DomainIdDnsClient prepareDomainIdDnsClient(final Config configuration) throws UnknownHostException {
    SimpleResolver resolver = null;
    final DnsConfig dnsConfig = configuration.getDnsConfig();
    if (dnsConfig != null) {
      final ResolverConfig resolverConfig = dnsConfig.getResolverConfig();
      if (resolverConfig != null) {
        final InetAddress resolverHost = resolverConfig.getHost();
        if (resolverHost != null) {
          resolver = new SimpleResolver();
          resolver.setAddress(resolverHost);
          final Integer resolverPort = resolverConfig.getPort();
          if (resolverPort != null) {
            resolver.setPort(resolverPort);
          }
        }
      }
    }
    return new DomainIdDnsClientDnsJavaBasedImpl(resolver);
  }

  @Override
  public void initialize(final Bootstrap<Config> bootstrap) {
    bootstrap.addBundle(new ViewBundle<Config>() {
      @Override
      public Map<String, Map<String, String>> getViewConfiguration(final Config config) {
        return config.getViewRendererConfiguration();
      }
    });
    // Enable config variable substitution with environment variables
    bootstrap.setConfigurationSourceProvider(new SubstitutingSourceProvider(bootstrap.getConfigurationSourceProvider(), new EnvironmentVariableSubstitutor(false)));
    bootstrap.addBundle(new AssetsBundle("/assets/style", "/style"));
  }

  @Override
  public void start() {
    // Not interested in
  }

  @Override
  public void stop() {
    LOG.info("STOPPING");
  }

}
