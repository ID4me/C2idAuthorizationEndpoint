/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.authendpoint.model;

import de.denic.domainid.DomainId;
import de.denic.domainid.connect2id.authzsession.ResponseType;
import de.denic.domainid.connect2id.authzsession.State;
import de.denic.domainid.connect2id.common.*;
import de.denic.domainid.openid.OpenIdAuthenticationRequestParamKeys;
import org.apache.commons.lang3.Validate;

import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;
import java.net.URI;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Callable;

import static java.util.Optional.ofNullable;

// TODO: Duplizierter Typ!
@Immutable
public final class OpenIdAuthenticationRequest implements OpenIdAuthenticationRequestParamKeys {

  private final RedirectURI redirectURI;
  private final State state;
  private final ClientId clientId;
  private final Scope scope;
  private final ResponseType responseType;
  private final DomainId loginHint;
  private final Nonce nonce;
  private final MaxAge maxAge;
  private final Prompt prompt;
  private final Claims claims;
  private final RequestURI requestURI;
  private final IdTokenHint idTokenHint;
  private final Display display;
  private final Request request;

  /**
   * @param responseType Required
   * @param scope        Required
   * @param clientId     Required
   * @param state        Required
   * @param redirectURI  Required
   * @param loginHint    Optional
   * @param nonce        Optional
   * @param maxAge       Optional
   * @param prompt       Optional
   * @param claims       Optional
   * @param requestURI   Optional
   * @param idTokenHint  Optional
   * @param display      Optional
   * @param request      Optional
   */
  public OpenIdAuthenticationRequest(final ResponseType responseType, final Scope scope, final ClientId clientId,
                                     final State state, final RedirectURI redirectURI, final DomainId loginHint, final Nonce nonce,
                                     final MaxAge maxAge, final Prompt prompt, final Claims claims, final RequestURI requestURI,
                                     final IdTokenHint idTokenHint, final Display display, final Request request) {
    Validate.notNull(responseType, "Missing response type");
    Validate.notNull(scope, "Missing scope");
    Validate.notNull(clientId, "Missing client ID");
    Validate.notNull(state, "Missing state");
    Validate.notNull(redirectURI, "Missing redirect URI");
    this.responseType = responseType;
    this.scope = scope;
    this.clientId = clientId;
    this.state = state;
    this.redirectURI = redirectURI;
    this.loginHint = loginHint;
    this.nonce = nonce;
    this.maxAge = maxAge;
    this.prompt = prompt;
    this.claims = claims;
    this.requestURI = requestURI;
    this.idTokenHint = idTokenHint;
    this.display = display;
    this.request = request;
  }

  private static void appendOptionalParamTo(final StringBuilder target, final String key, final UrlEncodedValue value) {
    appendOptionalParamTo(target, key, (value == null ? null : value::getEncoded));
  }

  private static void appendOptionalParamTo(final StringBuilder target, final String key, final Object value) {
    appendOptionalParamTo(target, key, (value == null ? null : value::toString));
  }

  private static void appendOptionalParamTo(final StringBuilder target, final String key,
                                            final Callable<?> valueProvider) {
    if (valueProvider == null) {
      return;
    }

    target.append('&').append(key).append('=');
    try {
      target.append(valueProvider.call());
    } catch (final Exception e) {
      throw new RuntimeException("Internal error", e);
    }
  }

  private static void appendParamTo(final StringBuilder target, final String key, final UrlEncodedValue value) {
    Validate.notNull(value, "Missing value");
    appendOptionalParamTo(target, key, value::getEncoded);
  }

  private static StringBuilder appendPlainFieldValue(final StringBuilder target, final String key,
                                                     final UrlEncodedValue value) {
    return appendPlainFieldValue(target, key, (value == null ? null : value.getPlain()));
  }

  private static StringBuilder appendPlainFieldValue(final StringBuilder target, final String key, final Object value) {
    if (target.length() > 0) {
      target.append(", ");
    }
    target.append(key).append(' ');
    if (value == null) {
      target.append("<NO>");
    } else {
      target.append("'").append(value).append("'");
    }
    return target;
  }

  public static Builder build() {
    return new BuilderImpl();
  }

  /**
   * @return Never <code>null</code>
   */
  public URI toOpenIdAuthenticationRequestURI() {
    final StringBuilder uriValue = new StringBuilder(64).append(RESPONSE_TYPE).append('=').append(responseType);
    appendParamTo(uriValue, SCOPE, scope);
    appendParamTo(uriValue, CLIENT_ID, clientId);
    appendParamTo(uriValue, STATE, state);
    appendParamTo(uriValue, REDIRECT_URI, redirectURI);
    appendOptionalParamTo(uriValue, NONCE, nonce);
    appendOptionalParamTo(uriValue, MAX_AGE, maxAge);
    appendOptionalParamTo(uriValue, PROMPT, prompt);
    appendOptionalParamTo(uriValue, CLAIMS, claims);
    appendOptionalParamTo(uriValue, REQUEST_URI, requestURI);
    appendOptionalParamTo(uriValue, ID_TOKEN_HINT, idTokenHint);
    appendOptionalParamTo(uriValue, DISPLAY, display);
    appendOptionalParamTo(uriValue, LOGIN_HINT, loginHint);
    appendOptionalParamTo(uriValue, REQUEST, request);
    return URI.create(uriValue.toString());
  }

  /**
   * @return Never <code>null</code>.
   */
  public ClientId getClientId() {
    return clientId;
  }

  /**
   * @return Optional
   */
  public DomainId getLoginHint() {
    return loginHint;
  }

  /**
   * @return Never <code>null</code>
   */
  public Optional<Claims> getClaims() {
    return ofNullable(claims);
  }

  /**
   * @return Never <code>null</code>
   */
  public Optional<Prompt> getPrompt() {
    return ofNullable(prompt);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    OpenIdAuthenticationRequest that = (OpenIdAuthenticationRequest) o;
    return Objects.equals(redirectURI, that.redirectURI) &&
            Objects.equals(state, that.state) &&
            Objects.equals(clientId, that.clientId) &&
            Objects.equals(scope, that.scope) &&
            Objects.equals(responseType, that.responseType) &&
            Objects.equals(loginHint, that.loginHint) &&
            Objects.equals(nonce, that.nonce) &&
            Objects.equals(maxAge, that.maxAge) &&
            Objects.equals(prompt, that.prompt) &&
            Objects.equals(claims, that.claims) &&
            Objects.equals(requestURI, that.requestURI) &&
            Objects.equals(idTokenHint, that.idTokenHint) &&
            Objects.equals(display, that.display) &&
            Objects.equals(request, that.request);
  }

  @Override
  public int hashCode() {
    return Objects.hash(redirectURI, state, clientId, scope, responseType, loginHint, nonce, maxAge, prompt, claims, requestURI, idTokenHint, display, request);
  }

  @Override
  public String toString() {
    final StringBuilder result = new StringBuilder(64);
    appendPlainFieldValue(result, RESPONSE_TYPE, responseType);
    appendPlainFieldValue(result, SCOPE, scope);
    appendPlainFieldValue(result, CLIENT_ID, clientId);
    appendPlainFieldValue(result, STATE, state);
    appendPlainFieldValue(result, REDIRECT_URI, redirectURI);
    appendPlainFieldValue(result, LOGIN_HINT, loginHint);
    appendPlainFieldValue(result, NONCE, nonce);
    appendPlainFieldValue(result, MAX_AGE, maxAge);
    appendPlainFieldValue(result, PROMPT, prompt);
    appendPlainFieldValue(result, CLAIMS, claims);
    appendPlainFieldValue(result, REQUEST_URI, requestURI);
    appendPlainFieldValue(result, ID_TOKEN_HINT, idTokenHint);
    appendPlainFieldValue(result, REQUEST, request);
    return result.toString();
  }

  public interface Builder {

    /**
     * @return <code>this</code> instance
     */
    Builder responseType(ResponseType responseType);

    /**
     * @return <code>this</code> instance
     */
    Builder scope(Scope scope);

    /**
     * @return <code>this</code> instance
     */
    Builder clientId(ClientId clientId);

    /**
     * @return <code>this</code> instance
     */
    Builder state(State state);

    /**
     * @return <code>this</code> instance
     */
    Builder redirectURI(RedirectURI redirectURI);

    /**
     * @return <code>this</code> instance
     */
    Builder loginHint(DomainId loginHint);

    /**
     * @return <code>this</code> instance
     */
    Builder nonce(Nonce nonce);

    /**
     * @return <code>this</code> instance
     */
    Builder maxAge(MaxAge maxAge);

    /**
     * @return <code>this</code> instance
     */
    Builder prompt(Prompt prompt);

    /**
     * @return <code>this</code> instance
     */
    Builder claims(Claims claims);

    /**
     * @return <code>this</code> instance
     */
    Builder requestURI(RequestURI requestURI);

    /**
     * @return <code>this</code> instance
     */
    Builder idTokenHint(IdTokenHint idTokenHint);

    /**
     * @return <code>this</code> instance
     */
    Builder display(Display display);

    /**
     * @return <code>this</code> instance
     */
    Builder request(Request request);

    /**
     * @return Never <code>null</code>
     */
    OpenIdAuthenticationRequest build();

  }

  @NotThreadSafe
  public static final class BuilderImpl implements Builder {

    private RedirectURI redirectURI;
    private State state;
    private ClientId clientId;
    private Scope scope;
    private ResponseType responseType;
    private DomainId loginHint;
    private Nonce nonce;
    private MaxAge maxAge;
    private Prompt prompt;
    private Claims claims;
    private RequestURI requestURI;
    private IdTokenHint idTokenHint;
    private Display display;
    private Request request;

    @Override
    public Builder responseType(final ResponseType responseType) {
      this.responseType = responseType;
      return this;
    }

    @Override
    public Builder scope(final Scope scope) {
      this.scope = scope;
      return this;
    }

    @Override
    public Builder clientId(final ClientId clientId) {
      this.clientId = clientId;
      return this;
    }

    @Override
    public Builder state(final State state) {
      this.state = state;
      return this;
    }

    @Override
    public Builder redirectURI(final RedirectURI redirectURI) {
      this.redirectURI = redirectURI;
      return this;
    }

    @Override
    public Builder loginHint(final DomainId loginHint) {
      this.loginHint = loginHint;
      return this;
    }

    @Override
    public Builder nonce(final Nonce nonce) {
      this.nonce = nonce;
      return this;
    }

    @Override
    public Builder maxAge(final MaxAge maxAge) {
      this.maxAge = maxAge;
      return this;
    }

    @Override
    public Builder prompt(final Prompt prompt) {
      this.prompt = prompt;
      return this;
    }

    @Override
    public Builder claims(final Claims claims) {
      this.claims = claims;
      return this;
    }

    @Override
    public Builder requestURI(final RequestURI requestURI) {
      this.requestURI = requestURI;
      return this;
    }

    @Override
    public Builder idTokenHint(final IdTokenHint idTokenHint) {
      this.idTokenHint = idTokenHint;
      return this;
    }

    @Override
    public Builder display(final Display display) {
      this.display = display;
      return this;
    }

    @Override
    public Builder request(final Request request) {
      this.request = request;
      return this;
    }

    @Override
    public OpenIdAuthenticationRequest build() {
      return new OpenIdAuthenticationRequest(responseType, scope, clientId, state, redirectURI, loginHint, nonce,
              maxAge, prompt, claims, requestURI, idTokenHint, display, request);
    }

  }

}
