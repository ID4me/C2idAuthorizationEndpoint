/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.authendpoint.config;

import static org.apache.commons.codec.binary.Base64.decodeBase64;
import static org.apache.commons.codec.binary.Base64.encodeBase64String;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.EncodedKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;

import javax.annotation.concurrent.ThreadSafe;
import javax.validation.Valid;

import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonProperty;

@ThreadSafe
public final class Base64KeyMaterialConfig {

  private static final Logger LOG = LoggerFactory.getLogger(Base64KeyMaterialConfig.class);

  @Valid
  private volatile KeyFactory keyFactory;

  @Valid
  private volatile PrivateKey privateKey;

  @JsonProperty("algorithm")
  public void setKeyAlgorithm(final String keyAlgorithm) throws NoSuchAlgorithmException {
    keyFactory = KeyFactory.getInstance(keyAlgorithm);
  }

  /**
   * @return Optional
   */
  public PrivateKey getPrivateKey() {
    return privateKey;
  }

  @JsonProperty("pkcs8EncodedPrivateKey")
  public void setBase64PrivateKey(final String base64PrivateKey) throws InvalidKeySpecException {
    LOG.info("Building private key from BASE64 encoded material");
    Validate.notNull(keyFactory, "Missing key factory set up by provided algorithm");
    final byte[] privateKeyBytes = decodeBase64(base64PrivateKey);
    final EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
    this.privateKey = keyFactory.generatePrivate(privateKeySpec);
  }

  @Override
  public String toString() {
    return privateKey == null ? "No key available yet"
        : "Private key material: " + encodeBase64String(privateKey.getEncoded()) + " (algorithm "
            + privateKey.getAlgorithm() + ", format " + privateKey.getFormat() + ")";
  }

}
