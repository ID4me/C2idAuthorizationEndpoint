/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.authendpoint.model;

import de.denic.domainid.DomainId;
import de.denic.domainid.connect2id.authzsession.SessionId;
import org.apache.commons.lang3.Validate;

import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Objects;

import static java.lang.String.format;
import static org.apache.commons.codec.digest.DigestUtils.sha256Hex;

@Immutable
public final class AuthenticationRequest {

  private final String passwordHash;
  private final DomainId domainId;
  private final SessionId sessionId;
  private final ZonedDateTime creationTimeStamp;

  /**
   * @param domainId          Required
   * @param password          Optional
   * @param sessionId         Required
   * @param creationTimeStamp Required
   */
  private AuthenticationRequest(final DomainId domainId, final String password, final SessionId sessionId,
                                final ZonedDateTime creationTimeStamp) {
    Validate.notNull(domainId, "Missing domain ID");
    Validate.notNull(creationTimeStamp, "Missing creation timestamp");
    Validate.notNull(sessionId, "Missing session ID");
    this.domainId = domainId;
    this.passwordHash = (password == null ? null : sha256Hex(password));
    this.sessionId = sessionId;
    this.creationTimeStamp = creationTimeStamp;
  }

  public static Builder builder() {
    return new BuilderImpl();
  }

  /**
   * @return Never <code>null</code>
   */
  public SessionId getSessionId() {
    return sessionId;
  }

  /**
   * @return Never <code>null</code>
   */
  public DomainId getDomainId() {
    return domainId;
  }

  public String getHexSHA256HashOfPassword() {
    return passwordHash;
  }

  /**
   * @return Never <code>null</code>
   */
  public ZonedDateTime getCreationTimeStamp() {
    return creationTimeStamp;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    AuthenticationRequest that = (AuthenticationRequest) o;
    return Objects.equals(passwordHash, that.passwordHash) &&
            Objects.equals(domainId, that.domainId) &&
            Objects.equals(sessionId, that.sessionId) &&
            Objects.equals(creationTimeStamp, that.creationTimeStamp);
  }

  @Override
  public int hashCode() {
    return Objects.hash(passwordHash, domainId, sessionId, creationTimeStamp);
  }

  @Override
  public String toString() {
    return format("domainID '%1$s', password SHA256 hash '%2$s', sessionID '%3$s'", domainId, passwordHash, sessionId);
  }

  public interface Builder {

    Builder domainId(DomainId domainId);

    Builder password(String password);

    Builder sessionId(SessionId sessionId);

    /**
     * @return Never <code>null</code>
     */
    AuthenticationRequest build();

  }

  @NotThreadSafe
  public static final class BuilderImpl implements Builder {

    private final ZonedDateTime creationTimeStamp;
    private SessionId sessionId;
    private String password;
    private DomainId domainId;

    public BuilderImpl() {
      this.creationTimeStamp = LocalDateTime.now().atZone(ZoneId.systemDefault());
    }

    @Override
    public Builder domainId(final DomainId domainID) {
      this.domainId = domainID;
      return this;
    }

    @Override
    public Builder password(final String password) {
      this.password = password;
      return this;
    }

    @Override
    public Builder sessionId(final SessionId sessionId) {
      this.sessionId = sessionId;
      return this;
    }

    @Override
    public AuthenticationRequest build() {
      return new AuthenticationRequest(domainId, password, sessionId, creationTimeStamp);
    }

  }

}
