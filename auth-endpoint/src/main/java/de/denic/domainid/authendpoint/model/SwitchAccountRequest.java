/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.authendpoint.model;

import static java.lang.String.format;

import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;

import org.apache.commons.lang3.Validate;

import de.denic.domainid.DomainId;
import de.denic.domainid.connect2id.authzsession.SessionId;
import de.denic.domainid.connect2id.common.SubjectSessionId;

@Immutable
public final class SwitchAccountRequest {

  private final SessionId sessionId;
  private final SubjectSessionId subjectSessionId;
  private final DomainId domainId;

  /**
   * @param sessionId
   *          Required
   * @param subjectSessionId
   *          Required
   * @param domainId
   *          Required
   */
  public SwitchAccountRequest(final SessionId sessionId, final SubjectSessionId subjectSessionId,
      final DomainId domainId) {
    Validate.notNull(domainId, "Missing domainID");
    Validate.notNull(sessionId, "Missing session ID");
    Validate.notNull(subjectSessionId, "Missing subject session ID");
    this.sessionId = sessionId;
    this.subjectSessionId = subjectSessionId;
    this.domainId = domainId;
  }

  /**
   * @return Never <code>null</code>
   */
  public SessionId getSessionId() {
    return sessionId;
  }

  /**
   * @return Never <code>null</code>
   */
  public SubjectSessionId getSubjectSessionId() {
    return subjectSessionId;
  }

  /**
   * @return Never <code>null</code>
   */
  public DomainId getDomainId() {
    return domainId;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((domainId == null) ? 0 : domainId.hashCode());
    result = prime * result + ((sessionId == null) ? 0 : sessionId.hashCode());
    result = prime * result + ((subjectSessionId == null) ? 0 : subjectSessionId.hashCode());
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final SwitchAccountRequest other = (SwitchAccountRequest) obj;
    if (domainId == null) {
      if (other.domainId != null) {
        return false;
      }
    } else if (!domainId.equals(other.domainId)) {
      return false;
    }
    if (sessionId == null) {
      if (other.sessionId != null) {
        return false;
      }
    } else if (!sessionId.equals(other.sessionId)) {
      return false;
    }
    if (subjectSessionId == null) {
      if (other.subjectSessionId != null) {
        return false;
      }
    } else if (!subjectSessionId.equals(other.subjectSessionId)) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return format("domainId '%1$s', subjectSessionID '%2$s', sessionID '%3$s'", domainId, subjectSessionId, sessionId);
  }

  public static interface Builder {

    Builder sessionId(SessionId sessionId);

    Builder subjectSessionId(SubjectSessionId subjectSessionId);

    Builder domainId(DomainId domainId);

    /**
     * @return Never <code>null</code>
     */
    SwitchAccountRequest build();

  }

  @NotThreadSafe
  public static final class BuilderImpl implements Builder {

    private SessionId sessionId;
    private SubjectSessionId subjectSessionId;
    private DomainId domainId;

    @Override
    public Builder sessionId(final SessionId sessionId) {
      this.sessionId = sessionId;
      return this;
    }

    @Override
    public Builder subjectSessionId(final SubjectSessionId subjectSessionId) {
      this.subjectSessionId = subjectSessionId;
      return this;
    }

    @Override
    public Builder domainId(final DomainId domainId) {
      this.domainId = domainId;
      return this;
    }

    @Override
    public SwitchAccountRequest build() {
      return new SwitchAccountRequest(sessionId, subjectSessionId, domainId);
    }

  }

  public static Builder builder() {
    return new BuilderImpl();
  }

}
