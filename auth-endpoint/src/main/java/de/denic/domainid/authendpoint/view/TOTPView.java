/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.authendpoint.view;

import de.denic.domainid.DomainId;
import de.denic.domainid.connect2id.authzsession.SessionId;
import org.apache.commons.lang3.Validate;

import javax.annotation.concurrent.Immutable;

@Immutable
public class TOTPView extends AuthEndpointView {

  private final SessionId sessionId;
  private final DomainId domainId;
  private final String hexSHA256OfHexSHA256PasswordHash;

  /**
   * @param domainId                         Required
   * @param sessionId                        Required
   * @param hexSHA256OfHexSHA256PasswordHash Required
   */
  public TOTPView(final DomainId domainId, final SessionId sessionId, final String hexSHA256OfHexSHA256PasswordHash) {
    super("totp.ftl");
    Validate.notNull(sessionId, "Missing session ID");
    Validate.notNull(domainId, "Missing domain ID");
    Validate.notEmpty(hexSHA256OfHexSHA256PasswordHash, "Missing hashed password");
    this.sessionId = sessionId;
    this.domainId = domainId;
    this.hexSHA256OfHexSHA256PasswordHash = hexSHA256OfHexSHA256PasswordHash;
  }

  /**
   * @return Never <code>null</code>
   */
  public SessionId getSessionId() {
    return sessionId;
  }

  /**
   * @return Never <code>null</code>
   */
  public DomainId getDomainId() {
    return domainId;
  }

  /**
   * @return Never empty.
   */
  public String getHexSHA256OfHexSHA256PasswordHash() {
    return hexSHA256OfHexSHA256PasswordHash;
  }

}
