/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.authendpoint.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.denic.domainid.connect2id.common.AccessToken;
import de.denic.domainid.connect2id.config.Connect2IdBaseConfig;

import javax.annotation.concurrent.Immutable;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.net.MalformedURLException;
import java.net.URL;

@Immutable
public final class Connect2IdConfig extends Connect2IdBaseConfig {

  @Valid
  @NotNull
  @JsonProperty("authorisationSessionAPIBearerAccessToken")
  private volatile AccessToken authorisationSessionAPIBearerAccessToken;

  @Valid
  @NotNull
  @JsonProperty("authorisationSessionAPIPath")
  private volatile String authorisationSessionAPIPath;
  private volatile URL authorisationSessionAPIUrl;

  @Valid
  @NotNull
  @JsonProperty("authorisationStoreAPIBearerAccessToken")
  private volatile AccessToken authorisationStoreBearerAccessToken;

  @Valid
  @NotNull
  @JsonProperty("authorisationStoreAPIPath")
  private volatile String authorisationStoreAPIPath;
  private volatile URL authorisationStoreUrl;

  @Valid
  @NotNull
  @JsonProperty("subjectSessionStoreBearerAccessToken")
  private volatile AccessToken subjectSessionStoreBearerAccessToken;

  @Valid
  @NotNull
  @JsonProperty("subjectSessionStorePath")
  private volatile String subjectSessionStorePath;
  private volatile URL subjectSessionStoreUrl;

  @Valid
  @NotNull
  @JsonProperty("openIdConnectClientRegistrationBearerAccessToken")
  private volatile AccessToken openIdConnectClientRegistrationBearerAccessToken;

  public final void setAuthorisationSessionAPIPath(final String path) throws MalformedURLException {
    this.authorisationSessionAPIPath = path;
    authorisationSessionAPIUrl = new URL(getBaseURL(), this.authorisationSessionAPIPath);
  }

  public void setAuthorisationStoreAPIPath(final String path) throws MalformedURLException {
    this.authorisationStoreAPIPath = path;
    authorisationStoreUrl = new URL(getBaseURL(), this.authorisationStoreAPIPath);
  }

  public URL getAuthorisationSessionAPIURL() {
    return authorisationSessionAPIUrl;
  }

  public AccessToken getAuthorisationSessionAPIBearerAccessToken() {
    return authorisationSessionAPIBearerAccessToken;
  }

  public void setAuthorisationSessionAPIBearerAccessToken(final AccessToken bearerAccessToken) {
    this.authorisationSessionAPIBearerAccessToken = bearerAccessToken;
  }

  public URL getAuthorisationStoreAPIURL() {
    return authorisationStoreUrl;
  }

  public AccessToken getAuthorisationStoreAPIBearerAccessToken() {
    return authorisationStoreBearerAccessToken;
  }

  public void setAuthorisationStoreAPIBearerAccessToken(final AccessToken bearerAccessToken) {
    this.authorisationStoreBearerAccessToken = bearerAccessToken;
  }

  public void setSubjectSessionStorePath(final String subjectSessionStorePath) throws MalformedURLException {
    this.subjectSessionStorePath = subjectSessionStorePath;
    this.subjectSessionStoreUrl = new URL(getBaseURL(), this.subjectSessionStorePath);
  }

  public URL getSubjectSessionStoreURL() {
    return subjectSessionStoreUrl;
  }

  public AccessToken getSubjectSessionStoreBearerAccessToken() {
    return subjectSessionStoreBearerAccessToken;
  }

  public void setSubjectSessionStoreBearerAccessToken(final AccessToken subjectSessionStoreBearerAccessToken) {
    this.subjectSessionStoreBearerAccessToken = subjectSessionStoreBearerAccessToken;
  }

  public AccessToken getOpenIdConnectClientRegistrationBearerAccessToken() {
    return openIdConnectClientRegistrationBearerAccessToken;
  }

  public void setOpenIdConnectClientRegistrationBearerAccessToken(final AccessToken openIdConnectClientRegistrationBearerAccessToken) {
    this.openIdConnectClientRegistrationBearerAccessToken = openIdConnectClientRegistrationBearerAccessToken;
  }

}
