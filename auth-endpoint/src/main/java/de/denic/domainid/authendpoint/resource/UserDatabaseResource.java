/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.authendpoint.resource;

import de.denic.domainid.DomainId;
import de.denic.domainid.connect2id.authzstore.Connect2IdAuthorizationStoreAPI;
import de.denic.domainid.dbaccess.UserAuthentication;
import de.denic.domainid.dbaccess.UserAuthenticationDAO;
import de.denic.domainid.subject.PseudonymSubject;

import org.apache.commons.lang3.Validate;

import javax.annotation.concurrent.Immutable;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.created;
import static javax.ws.rs.core.Response.ok;
import static org.apache.commons.codec.digest.DigestUtils.sha256Hex;

@Path("user")
@Immutable
public class UserDatabaseResource {

  private final UserAuthenticationDAO userAuthenticationDAO;
  private final Connect2IdAuthorizationStoreAPI c2IdAuthorizationStoreAPI;

  /**
   * @param userAuthenticationDAO Required
   */
  public UserDatabaseResource(final UserAuthenticationDAO userAuthenticationDAO, final Connect2IdAuthorizationStoreAPI c2IdAuthorizationStoreAPI) {
    Validate.notNull(userAuthenticationDAO, "Missing user authentication data access object");
    Validate.notNull(c2IdAuthorizationStoreAPI, "Missing Connect2Id Authorization Store API component");
    this.userAuthenticationDAO = userAuthenticationDAO;
    this.c2IdAuthorizationStoreAPI = c2IdAuthorizationStoreAPI;

  }

  @GET
  @Produces(APPLICATION_JSON)
  public List<DomainId> listUserNames() {
    return userAuthenticationDAO.findAllUserNames();
  }

  @GET
  @Path("{userName}")
  @Produces(APPLICATION_JSON)
  public UserAuthentication getByUserName(@PathParam("userName") final String userName) {
    final UserAuthentication result = userAuthenticationDAO.findUserAuthenticationBy(new DomainId(userName));
    if (result == null) {
      throw new WebApplicationException(NOT_FOUND);
    }

    return result;
  }

  @PUT
  @Path("{userName}")
  @Produces(APPLICATION_JSON)
  public Response create(@PathParam("userName") @NotNull final String userName, @QueryParam("password") @NotNull final String plainPassword) {
    final String sha256HexOfPassword = (plainPassword.isEmpty() ? "" : sha256Hex(plainPassword));
    final DomainId domainId = new DomainId(userName);
    final int itemsUpdated = userAuthenticationDAO.updateUserAuthentication(domainId, sha256HexOfPassword);
    if (itemsUpdated < 1) {
      final int itemsCreated = userAuthenticationDAO.insertEndUserIdentity(domainId, sha256HexOfPassword);
      if (itemsCreated < 1) {
        throw new WebApplicationException(BAD_REQUEST);
      }

      return created(null).entity(userAuthenticationDAO.findUserAuthenticationBy(domainId)).build();
    }

    return ok(userAuthenticationDAO.findUserAuthenticationBy(domainId)).build();
  }

  @POST
  @Path("{userName}/setpw")
  @Produces(APPLICATION_JSON)
  public Response updatePassword(@PathParam("userName") @NotNull final String userName, @QueryParam("password") @NotNull final String plainPassword) {
    final String sha256HexOfPassword = sha256Hex(plainPassword);
    final DomainId domainId = new DomainId(userName);
    final int itemsUpdated = userAuthenticationDAO.updateUserAuthentication(domainId, sha256HexOfPassword);
    if (itemsUpdated < 1) {
      throw new WebApplicationException(NOT_FOUND);
    }

    return ok(userAuthenticationDAO.findUserAuthenticationBy(domainId)).build();
  }

  @POST
  @Path("{userName}/enableagentpwreset")
  @Produces(APPLICATION_JSON)
  public Response enableAgentPwReset(@PathParam("userName") @NotNull final String userName) {
    final DomainId domainId = new DomainId(userName);
    final int itemsUpdated = userAuthenticationDAO.updateAgentPwResetEnabled(domainId, true);
    if (itemsUpdated < 1) {
      throw new WebApplicationException(NOT_FOUND);
    }

    return ok(userAuthenticationDAO.findUserAuthenticationBy(domainId)).build();
  }

  @POST
  @Path("{userName}/disableagentpwreset")
  @Produces(APPLICATION_JSON)
  public Response disableAgentPwReset(@PathParam("userName") @NotNull final String userName) {
    final DomainId domainId = new DomainId(userName);
    final int itemsUpdated = userAuthenticationDAO.updateAgentPwResetEnabled(domainId, false);
    if (itemsUpdated < 1) {
      throw new WebApplicationException(NOT_FOUND);
    }

    return ok(userAuthenticationDAO.findUserAuthenticationBy(domainId)).build();
  }

  @POST
  @Path("{userName}/deactivate")
  @Produces(APPLICATION_JSON)
  public Response deactivateUser(@PathParam("userName") @NotNull final String userName) {
    final DomainId domainId = new DomainId(userName);
    final int itemsUpdated = userAuthenticationDAO.updateActive(domainId, false);
    if (itemsUpdated < 1) {
      throw new WebApplicationException(NOT_FOUND);
    }

    UserAuthentication userAuthentication = userAuthenticationDAO.findUserAuthenticationBy(domainId);
    final PseudonymSubject pseudonymSubject = userAuthentication.getPseudonymUserName();
    c2IdAuthorizationStoreAPI.revokeAllAuthorizationsOf(pseudonymSubject);

    return ok(userAuthenticationDAO.findUserAuthenticationBy(domainId)).build();
  }

  @POST
  @Path("{userName}/activate")
  @Produces(APPLICATION_JSON)
  public Response activateUser(@PathParam("userName") @NotNull final String userName) {
    final DomainId domainId = new DomainId(userName);
    final int itemsUpdated = userAuthenticationDAO.updateActive(domainId, true);
    if (itemsUpdated < 1) {
      throw new WebApplicationException(NOT_FOUND);
    }

    return ok(userAuthenticationDAO.findUserAuthenticationBy(domainId)).build();
  }

  @DELETE
  @Path("{userName}")
  @Produces(APPLICATION_JSON)
  public int deleteByUserName(@PathParam("userName") final String userName) {
    final int removedItems = userAuthenticationDAO.removeByUserName(new DomainId(userName));
    if (removedItems < 1) {
      throw new WebApplicationException(NOT_FOUND);
    }

    return removedItems;
  }
}
