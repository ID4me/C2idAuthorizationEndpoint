/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.authendpoint.resource;

import com.codahale.metrics.annotation.Timed;
import com.warrenstrange.googleauth.IGoogleAuthenticator;
import de.denic.domainid.DomainId;
import de.denic.domainid.authendpoint.model.AuthenticationRequest;
import de.denic.domainid.authendpoint.model.HexSHA256PasswordHash;
import de.denic.domainid.authendpoint.model.TwoFactorAuthOptIn;
import de.denic.domainid.authendpoint.view.TOTPView;
import de.denic.domainid.connect2id.authzsession.Connect2IdAuthorizationSessionAPI;
import de.denic.domainid.connect2id.authzsession.SessionId;
import de.denic.domainid.connect2id.authzsession.entity.FinalResponse;
import de.denic.domainid.connect2id.authzstore.Connect2IdAuthorizationStoreAPI;
import de.denic.domainid.connect2id.authzstore.entity.OpenIdConnectAuthorization;
import de.denic.domainid.connect2id.common.AuthenticationMethodReference;
import de.denic.domainid.connect2id.common.Claim;
import de.denic.domainid.connect2id.common.ClientId;
import de.denic.domainid.connect2id.common.NotFoundException;
import de.denic.domainid.connect2id.subjectsessionstore.entity.SubjectSessionOptionalData;
import de.denic.domainid.dbaccess.UserAuthentication;
import de.denic.domainid.dbaccess.UserAuthenticationDAO;
import de.denic.domainid.dns.DomainIdDnsClient;
import de.denic.domainid.dns.LookupResult;
import de.denic.domainid.identityagent.IdentityAgentClient;
import de.denic.domainid.identityagent.IdentityAgentClientException;
import de.denic.domainid.subject.OpenIdSubject;
import de.denic.domainid.subject.PseudonymSubject;
import de.denic.domainid.subject.SubjectUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.concurrent.Immutable;
import javax.security.auth.Subject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.security.Principal;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static de.denic.domainid.authendpoint.resource.SubjectSessionCookies.from;
import static de.denic.domainid.connect2id.common.AuthenticationMethodReference.Value.otp;
import static de.denic.domainid.connect2id.common.AuthenticationMethodReference.Value.pwd;
import static de.denic.domainid.connect2id.common.AuthenticationMethodReference.ofMany;
import static de.denic.domainid.subject.SubjectUtils.*;
import static java.lang.Integer.parseUnsignedInt;
import static java.util.Arrays.asList;
import static java.util.Arrays.fill;
import static java.util.Collections.emptySet;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static javax.ws.rs.core.MediaType.TEXT_HTML;
import static javax.ws.rs.core.Response.Status.GONE;
import static org.apache.commons.codec.digest.DigestUtils.sha256Hex;

@Path("authenticate")
@Immutable
public final class AuthenticationResource {

  private static final Logger LOG = LoggerFactory.getLogger(AuthenticationResource.class);
  private static final boolean SUCCESSFUL_AUTHENTICATION = true;
  private static final boolean FAILED_AUTHENTICATION = !SUCCESSFUL_AUTHENTICATION;
  private static final OpenIdSubject UNKNOWN_SUBJECT = null;
  private static final String TEXT_HTML_CHARSET_UTF8 = TEXT_HTML + ";charset=UTF-8";

  private final Connect2IdAuthorizationSessionAPI c2IdAuthorizationSessionAPI;
  private final UserAuthenticationDAO userAuthenticationDAO;
  private final DomainIdDnsClient domainIdDnsClient;
  private final IdentityAgentClient identityAgentClient;
  private final Connect2IdAuthorizationStoreAPI c2IdAuthorizationStoreAPI;
  private final IGoogleAuthenticator googleAuthenticator;

  /**
   * @param c2IdAuthorizationSessionAPI Required
   * @param userAuthenticationDAO       Required
   * @param domainIdDnsClient           Required
   * @param identityAgentClient         Required
   * @param c2IdAuthorizationStoreAPI   Required
   * @param googleAuthenticator         Required
   */
  public AuthenticationResource(final Connect2IdAuthorizationSessionAPI c2IdAuthorizationSessionAPI, final UserAuthenticationDAO userAuthenticationDAO,
      final DomainIdDnsClient domainIdDnsClient, final IdentityAgentClient identityAgentClient, final Connect2IdAuthorizationStoreAPI c2IdAuthorizationStoreAPI,
      final IGoogleAuthenticator googleAuthenticator) {
    Validate.notNull(c2IdAuthorizationSessionAPI, "Missing Connect2Id Authorization Session API component");
    Validate.notNull(c2IdAuthorizationStoreAPI, "Missing Connect2Id Authorization Store API component");
    Validate.notNull(userAuthenticationDAO, "Missing user authentication data access object");
    Validate.notNull(domainIdDnsClient, "Missing DomainID DNS client");
    Validate.notNull(identityAgentClient, "Missing Identity Agent client");
    Validate.notNull(googleAuthenticator, "Missing Google Authenticator");
    this.c2IdAuthorizationSessionAPI = c2IdAuthorizationSessionAPI;
    this.userAuthenticationDAO = userAuthenticationDAO;
    this.domainIdDnsClient = domainIdDnsClient;
    this.identityAgentClient = identityAgentClient;
    this.c2IdAuthorizationStoreAPI = c2IdAuthorizationStoreAPI;
    this.googleAuthenticator = googleAuthenticator;
  }

  private static boolean passwordOKCheckOf(final String passwordOKCheck, final UserAuthentication userAuthentication) {
    final String expectedPasswortOKCheck = sha256Hex(userAuthentication.getPasswordHexSHA256());
    return expectedPasswortOKCheck.equals(passwordOKCheck);
  }

  @POST
  @Produces(TEXT_HTML_CHARSET_UTF8)
  @Timed
  public Response viaPOST(@Context final HttpHeaders httpHeaders, @NotNull @FormParam("domainID") final DomainId domainId,
      @NotNull @FormParam("password") final String password, @NotNull @FormParam("sessionID") final SessionId sessionId) {
    try {
      return handle(AuthenticationRequest.builder().domainId(domainId).password(password).sessionId(sessionId).build(), from(httpHeaders));
    } catch (final NotFoundException e) {
      LOG.warn("Authentication fails cause Connect2Id Authorisation Session '{}' not available", sessionId, e);
      throw new WebApplicationException(Response.status(GONE).type(TEXT_HTML_CHARSET_UTF8)
          .entity("<html><body><h2>Error: Consenting fails cause required backend resource not available any more.</h2></body></html>").build());
    }
  }

  private Response handle(final AuthenticationRequest request, final SubjectSessionCookies subjectSessionCookies) {
    LOG.info("Received credentials to authenticate: {} (cookies: {})", request, subjectSessionCookies);
    final ClientId clientId = c2IdAuthorizationSessionAPI.getSession(request.getSessionId()).getInitialRequest().getClientId();
    final Optional<Subject> optAuthenticatedSubject = authenticateCredentialsFrom(request);
    if (optAuthenticatedSubject.isPresent()) {
      final Subject authenticatedSubject = optAuthenticatedSubject.get();
      if (!authenticatedSubject.getPrivateCredentials(TwoFactorAuthOptIn.class).isEmpty()) {
        return prepareForTwoFactorAuthentication(authenticatedSubject, request.getSessionId());
      }

      return successfullyAuthenticated(request, subjectSessionCookies, clientId, authenticatedSubject, pwd);
    }

    return authenticationFailureResponse(request, clientId, UNKNOWN_SUBJECT, subjectSessionCookies);
  }

  private Response successfullyAuthenticated(final AuthenticationRequest request, final SubjectSessionCookies subjectSessionCookies, final ClientId clientId,
      final Subject authenticatedSubject, final AuthenticationMethodReference.Value... authMethodReferences) {
    reportAuthenticationResultToIdentityAgent(request, SubjectUtils.from(authenticatedSubject).single(OpenIdSubject.class), clientId, SUCCESSFUL_AUTHENTICATION);
    final PseudonymSubject pseudonymSubject = SubjectUtils.from(authenticatedSubject).single(PseudonymSubject.class);
    final Optional<OpenIdConnectAuthorization> optOpenIdConnectAuthorisation = c2IdAuthorizationStoreAPI.loadAuthorizationOf(pseudonymSubject, clientId);
    final SubjectSessionOptionalData subjectSessionOptionalData = optOpenIdConnectAuthorisation.map(OpenIdConnectAuthorization::getOptionalData).orElse(null);
    checkSubjectSessionsOptionalDataForConsistency(pseudonymSubject, clientId, subjectSessionOptionalData,
        SubjectUtils.from(authenticatedSubject).single(DomainId.class));
    return c2IdAuthorizationSessionAPI.authenticate(request.getSessionId(), authenticatedSubject, ofMany(authMethodReferences))
        .dispatchedBy(new MyDispatcher(subjectSessionCookies, subjectSessionOptionalData, c2IdAuthorizationSessionAPI, userAuthenticationDAO)).build();
  }

  /**
   * @param pseudonymSubject             Required
   * @param clientId                     Required
   * @param subjectSessionOptionalData   Optional
   * @param currentAuthenticatedDomainId Required
   */
  private void checkSubjectSessionsOptionalDataForConsistency(final PseudonymSubject pseudonymSubject, final ClientId clientId,
      final SubjectSessionOptionalData subjectSessionOptionalData, final DomainId currentAuthenticatedDomainId) {
    if (subjectSessionOptionalData == null) {
      return; // Nothing to check
    }

    Validate.notNull(currentAuthenticatedDomainId, "Missing Domain ID");
    final DomainId domainIdFromSubjectSessionOptionalData = subjectSessionOptionalData.getDomainId().orElse(null);
    if (!currentAuthenticatedDomainId.equals(domainIdFromSubjectSessionOptionalData)) {
      c2IdAuthorizationStoreAPI.changeOptionalData(pseudonymSubject, clientId, subjectSessionOptionalData.withDomainId(currentAuthenticatedDomainId));
    }
  }

  /**
   * @param subjectToAuthenticate Required to hold an instance of {@link HexSHA256PasswordHash} as private credential
   * @param sessionId             Required
   */
  private Response prepareForTwoFactorAuthentication(final Subject subjectToAuthenticate, final SessionId sessionId) {
    final HexSHA256PasswordHash sha256PasswordHash = subjectToAuthenticate.getPrivateCredentials(HexSHA256PasswordHash.class).stream().findFirst()
        .orElseThrow(() -> new RuntimeException("Missing password SHA256 hash value"));
    final String hexSHA256OfHexSHA256PasswordHash = sha256Hex(sha256PasswordHash.getValue());
    final DomainId domainId = SubjectUtils.from(subjectToAuthenticate).single(DomainId.class);
    return Response.ok(new TOTPView(domainId, sessionId, hexSHA256OfHexSHA256PasswordHash)).build();
  }

  @POST
  @Produces(TEXT_HTML_CHARSET_UTF8)
  @Timed
  @Path("totp")
  public Response totpPOST(@Context final HttpHeaders httpHeaders, @NotNull @FormParam("domainID") final DomainId domainId,
      @NotNull @FormParam("verificationCode") final String verificationCodeValue, @NotNull @FormParam("sessionID") final SessionId sessionId,
      @NotNull @FormParam("passwordOK") final String passwordOKCheck) {
    final SubjectSessionCookies subjectSessionCookies = from(httpHeaders);
    LOG.info("Received TOTP verification code: {} (domain ID: '{}', PW check {}, cookies: {})", verificationCodeValue, domainId, passwordOKCheck, subjectSessionCookies);
    final UserAuthentication userAuthentication = userAuthenticationDAO.findUserAuthenticationBy(domainId);
    final Subject authenticatedSubject = subjectWith(domainId, userAuthentication.getPseudonymUserName());
    final int verificationCode;
    try {
      verificationCode = parseUnsignedInt(verificationCodeValue);
    } catch (final NumberFormatException e) {
      return prepareForTwoFactorAuthentication(authenticatedSubject, sessionId);
    }

    if (!userAuthentication.isActive() || !passwordOKCheckOf(passwordOKCheck, userAuthentication)) {
      final ClientId clientId = c2IdAuthorizationSessionAPI.getSession(sessionId).getInitialRequest().getClientId();
      final AuthenticationRequest request = AuthenticationRequest.builder().domainId(domainId).sessionId(sessionId).build();
      return authenticationFailureResponse(request, clientId, SubjectUtils.from(authenticatedSubject).single(OpenIdSubject.class), subjectSessionCookies);
    }

    final char[] sharedSecret = userAuthenticationDAO.selectTOTPSharedSecretOf(domainId);
    final boolean totpAuthorized = googleAuthenticator.authorize(new String(sharedSecret), verificationCode);
    fill(sharedSecret, ' '); // Wipe out shared secret from memory
    if (totpAuthorized) {
      final AuthenticationRequest request = AuthenticationRequest.builder().domainId(domainId).sessionId(sessionId).build();
      final ClientId clientId = c2IdAuthorizationSessionAPI.getSession(request.getSessionId()).getInitialRequest().getClientId();
      return successfullyAuthenticated(request, subjectSessionCookies, clientId, authenticatedSubject, pwd, otp);
    }

    return prepareForTwoFactorAuthentication(authenticatedSubject, sessionId);
  }

  private Response authenticationFailureResponse(final AuthenticationRequest request, final ClientId clientId, final OpenIdSubject subject,
      final SubjectSessionCookies subjectSessionCookies) {
    reportAuthenticationResultToIdentityAgent(request, subject, clientId, FAILED_AUTHENTICATION);
    final FinalResponse finalResponse = c2IdAuthorizationSessionAPI.cancelSession(request.getSessionId());
    return new MyDispatcher(subjectSessionCookies, new SubjectSessionOptionalData(request.getDomainId()), c2IdAuthorizationSessionAPI, userAuthenticationDAO)
        .handle(finalResponse).build();
  }

  /**
   * @param request Required
   * @return {@link Subject} having {@link Principal}s of type {@link DomainId}
   */
  private Optional<Subject> authenticateCredentialsFrom(final AuthenticationRequest request) {
    final DomainId domainId = request.getDomainId();
    final UserAuthentication userAuthentication = userAuthenticationDAO.findUserAuthenticationBy(domainId);
    if (userAuthentication == null) {
      LOG.info("Unknown DomainID '{}'", domainId);
      return empty();
    }

    if (!userAuthentication.isActive()) {
      LOG.info("Deactivated DomainID '{}'", domainId);
      return empty();
    }

    if (!userAuthentication.getPasswordHexSHA256().equals(request.getHexSHA256HashOfPassword())) {
      LOG.info("Mismatching credentials of DomainID '{}'", domainId);
      return empty();
    }

    final Set<Principal> principals = new HashSet<>(asList(domainId, userAuthentication.getPseudonymUserName()));
    final Set<Object> privateCredentials = new HashSet<>();
    privateCredentials.add(new HexSHA256PasswordHash(request.getHexSHA256HashOfPassword()));
    if (userAuthentication.isTwoFactorAuthEnabled()) {
      privateCredentials.add(TwoFactorAuthOptIn.INSTANCE);
    }
    return of(new Subject(READ_ONLY, principals, NO_CREDENTIALS, privateCredentials));
  }

  private void reportAuthenticationResultToIdentityAgent(final AuthenticationRequest request, final OpenIdSubject subject, final ClientId clientId,
      final boolean successfulAuthentication) {
    final DomainId domainId = request.getDomainId();
    final LookupResult<URI> subjectsIdentityAgentHostName = lookupIdentityAgentHostNameOf(domainId);
    if (subjectsIdentityAgentHostName.isAvailable()) {
      try {
        identityAgentClient.authentication(domainId, subject, subjectsIdentityAgentHostName.get(), request.getCreationTimeStamp(), successfulAuthentication, clientId);
      } catch (final IdentityAgentClientException e) {
        LOG.warn("Callback on DomainID's '{}' Identity Agent '{}' failed: {}", domainId, subjectsIdentityAgentHostName, e);
      }
    }
  }

  private LookupResult<URI> lookupIdentityAgentHostNameOf(final DomainId domainId) {
    final LookupResult<URI> identityAgentsHostName = domainIdDnsClient.lookupIdentityAgentHostNameOf(domainId);
    if (identityAgentsHostName.isAvailable()) {
      LOG.info("Found Identity Agent of DomainID '{}': '{}'", domainId, identityAgentsHostName.get());
    } else {
      LOG.warn("Empty lookup of Identity Agent of DomainID '{}'", domainId);
    }
    return identityAgentsHostName;
  }

  @Immutable
  private static final class MyDispatcher extends ConsentPageSkippingDispatcherBase {

    private final SubjectSessionOptionalData authorisationAdditionalData;

    /**
     * @param authorisationAdditionalData Optional
     * @see ConsentPageSkippingDispatcherBase#ConsentPageSkippingDispatcherBase(Connect2IdAuthorizationSessionAPI, UserAuthenticationDAO, SubjectSessionCookies)
     */
    private MyDispatcher(final SubjectSessionCookies subjectSessionCookies, final SubjectSessionOptionalData authorisationAdditionalData,
        final Connect2IdAuthorizationSessionAPI c2IdAuthorisationSessionAPI, final UserAuthenticationDAO userAuthenticationDAO) {
      super(c2IdAuthorisationSessionAPI, userAuthenticationDAO, subjectSessionCookies);
      this.authorisationAdditionalData = authorisationAdditionalData;
    }

    @Override
    protected Set<Claim> loadRejectedClaimsOf(final PseudonymSubject subject, final ClientId clientId) {
      return (authorisationAdditionalData == null ? emptySet() : authorisationAdditionalData.getRejectedClaims());
    }
  }
}
