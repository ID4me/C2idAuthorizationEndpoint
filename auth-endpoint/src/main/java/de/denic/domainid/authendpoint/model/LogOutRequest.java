/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.authendpoint.model;

import static java.lang.String.format;

import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;

import de.denic.domainid.connect2id.authzsession.SessionId;

@Immutable
public final class LogOutRequest {

  private final SessionId sessionId;

  /**
   * @param sessionIdValue
   *          Required
   */
  private LogOutRequest(final String sessionIdValue) {
    this.sessionId = new SessionId(sessionIdValue);
  }

  /**
   * @return Never <code>null</code>
   */
  public SessionId getSessionId() {
    return sessionId;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((sessionId == null) ? 0 : sessionId.hashCode());
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final LogOutRequest other = (LogOutRequest) obj;
    if (sessionId == null) {
      if (other.sessionId != null) {
        return false;
      }
    } else if (!sessionId.equals(other.sessionId)) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return format("sessionID '%1$s'", sessionId);
  }

  public static interface Builder {

    Builder sessionId(String sessionId);

    /**
     * @return Never <code>null</code>
     */
    LogOutRequest build();

  }

  @NotThreadSafe
  public static final class BuilderImpl implements Builder {

    private String sessionId;

    @Override
    public Builder sessionId(final String sessionID) {
      this.sessionId = sessionID;
      return this;
    }

    @Override
    public LogOutRequest build() {
      return new LogOutRequest(sessionId);
    }

  }

  public static Builder build() {
    return new BuilderImpl();
  }

}
