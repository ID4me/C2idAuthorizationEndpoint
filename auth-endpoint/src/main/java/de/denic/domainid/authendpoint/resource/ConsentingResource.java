/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.authendpoint.resource;

import com.codahale.metrics.annotation.Timed;
import de.denic.domainid.DomainId;
import de.denic.domainid.authendpoint.model.ConsentingRequest;
import de.denic.domainid.connect2id.authzsession.Connect2IdAuthorizationSessionAPI;
import de.denic.domainid.connect2id.authzsession.ConsentedData;
import de.denic.domainid.connect2id.authzsession.entity.FinalResponse;
import de.denic.domainid.connect2id.common.Claim;
import de.denic.domainid.connect2id.common.NotFoundException;
import de.denic.domainid.connect2id.common.Scope;
import de.denic.domainid.connect2id.common.SubjectSessionId;
import de.denic.domainid.connect2id.subjectsessionstore.Connect2IdSubjectSessionStoreAPI;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.concurrent.Immutable;
import javax.validation.constraints.NotNull;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import java.util.Set;

import static de.denic.domainid.authendpoint.resource.SubjectSessionCookies.from;
import static de.denic.domainid.connect2id.common.Scope.OPEN_ID;
import static java.util.Arrays.spliterator;
import static java.util.Collections.emptySet;
import static java.util.stream.Collectors.toSet;
import static java.util.stream.StreamSupport.stream;
import static javax.ws.rs.core.MediaType.TEXT_HTML;
import static javax.ws.rs.core.Response.Status.GONE;
import static org.apache.commons.lang3.StringUtils.isEmpty;

@Path("consent")
@Immutable
public final class ConsentingResource {

  private static final Logger LOG = LoggerFactory.getLogger(ConsentingResource.class);
  private static final Scope ONLY_OPENID_SCOPE_CONSENTED_IMPLICITELY = OPEN_ID;
  private static final boolean NOT_PARALLEL = false;
  private static final String TEXT_HTML_CHARSET_UTF8 = TEXT_HTML + ";charset=UTF-8";

  private final Connect2IdAuthorizationSessionAPI c2IdAuthorizationSessionAPI;
  private final Connect2IdSubjectSessionStoreAPI c2IdSubjectSessionStoreAPI;

  /**
   * @param c2IdAuthorizationSessionAPI Required
   * @param c2IdSubjectSessionStoreAPI  Required
   */
  public ConsentingResource(final Connect2IdAuthorizationSessionAPI c2IdAuthorizationSessionAPI,
                            final Connect2IdSubjectSessionStoreAPI c2IdSubjectSessionStoreAPI) {
    Validate.notNull(c2IdAuthorizationSessionAPI, "Missing Connect2Id Authorisation Session API component");
    Validate.notNull(c2IdSubjectSessionStoreAPI, "Missing Connect2Id Subject Session Store API component");
    this.c2IdAuthorizationSessionAPI = c2IdAuthorizationSessionAPI;
    this.c2IdSubjectSessionStoreAPI = c2IdSubjectSessionStoreAPI;
  }

  @POST
  @Produces(TEXT_HTML_CHARSET_UTF8)
  @Timed
  public Response viaPOST(@Context final HttpHeaders httpHeaders, @NotNull @FormParam("consenting") final boolean consented,
                          @NotNull @FormParam("sessionID") final String sessionID, @FormParam("claim") final Set<Claim> consentedClaims,
                          @FormParam("suggestedClaims") final String commaSeparatedSuggestedClaims,
                          @FormParam("remember") final boolean rememberDecision) {
    final Set<Claim> suggestedClaims;
    if (commaSeparatedSuggestedClaims != null) {
      suggestedClaims = stream(spliterator(commaSeparatedSuggestedClaims.split(",")), NOT_PARALLEL)
              .filter(value -> !isEmpty(value)).map(Claim::new).collect(toSet());
    } else {
      suggestedClaims = emptySet();
    }
    try {
      return handle(ConsentingRequest.build().sessionId(sessionID).consented(consented)
              .consentedClaims(consentedClaims).suggestedClaims(suggestedClaims).rememberDecision(rememberDecision).build(), from(httpHeaders));
    } catch (final NotFoundException e) {
      LOG.warn("Consenting fails cause Connect2Id Authorisation Session '{}' not available", sessionID, e);
      return Response.status(GONE).type(TEXT_HTML).entity(
              "<html><body><h2>Error: Consenting fails cause required backend resource not available any more</h2></body></html>")
              .build();
    }
  }

  private Response handle(final ConsentingRequest request, final SubjectSessionCookies subjectSessionCookies) {
    LOG.info("Received user's consent: {} (cookies: {})", request, subjectSessionCookies);
    final MyDispatcher myDispatcher = new MyDispatcher(subjectSessionCookies);
    if (!request.isConsented()) {
      final FinalResponse finalResponse = c2IdAuthorizationSessionAPI.cancelSession(request.getSessionId());
      return myDispatcher.handle(finalResponse).build();
    }

    final ConsentedData consentedData = new ConsentedData(ONLY_OPENID_SCOPE_CONSENTED_IMPLICITELY,
            request.getConsentedClaims(), request.getRefusedClaims());
    return c2IdAuthorizationSessionAPI.provide(request.getSessionId(), getDomainIdFromAuthzSession
                    (subjectSessionCookies.getCurrentSessionId().orElseThrow(() -> new RuntimeException("Missing current " +
                            "subject session cookie: " + subjectSessionCookies))),
            consentedData, request.doRememberDecision()).dispatchedBy(myDispatcher).build();
  }

  private DomainId getDomainIdFromAuthzSession(final SubjectSessionId sessionId) {
    return c2IdSubjectSessionStoreAPI.sessionOf(sessionId).getOptionalData().getDomainId().orElseThrow(() -> new
            RuntimeException("Missing Domain ID within optional data of C2ID Subject Session Store '" + sessionId +
            "'"));
  }

  @Immutable
  private static final class MyDispatcher extends FinalResponseRedirectingDispatcherBase {

    private MyDispatcher(final SubjectSessionCookies subjectSessionCookies) {
      super(subjectSessionCookies);
    }

  }

}
