/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.authendpoint.config;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.denic.domainid.jwks.JWKSetParser;
import de.denic.domainid.jwks.KeyID;
import io.dropwizard.validation.ValidationMethod;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.concurrent.ThreadSafe;
import javax.validation.Valid;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.security.Key;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.util.LinkedList;
import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.SystemUtils.USER_HOME;

@ThreadSafe
public final class PrivateKeyConfig {

  private static final Logger LOG = LoggerFactory.getLogger(PrivateKeyConfig.class);
  private static final Charset ASCII = Charset.forName("ASCII");

  @Valid
  private final Base64KeyMaterialConfig base64KeyMaterialConfig = new Base64KeyMaterialConfig();

  @JsonIgnore
  private volatile List<PrivateKey> privateKeysFromJwkSetFile = emptyList();

  @JsonIgnore
  private volatile Pair<KeyID, Key> matchingPrivateKey;

  @JsonProperty("jwkSetFile")
  public void setJwkSetFile(final String jwkSetFileName) throws IOException {
    if (isEmpty(jwkSetFileName)) {
      return;
    }

    final File jwkSetFile;
    if (jwkSetFileName.startsWith("~")) {
      jwkSetFile = new File(jwkSetFileName.replace("~", USER_HOME)).getAbsoluteFile();
    } else {
      jwkSetFile = new File(jwkSetFileName).getAbsoluteFile();
    }
    LOG.info("Loading JWK-Set from file {}", jwkSetFile);
    try (final InputStream jwkSetFileInput = new FileInputStream(jwkSetFile)) {
      final String jwkSetInput = IOUtils.toString(jwkSetFileInput, ASCII);
      final List<Pair<KeyID, KeyPair>> keyPairsFromJwkSet = new JWKSetParser.Impl().toKeyPairs(jwkSetInput);
      this.privateKeysFromJwkSetFile = keyPairsFromJwkSet.stream().map(keyPair -> keyPair.getValue().getPrivate())
              .collect(toList());
    }
  }

  /**
   * @return Never <code>null</code>
   */
  @JsonProperty("base64KeyMaterial")
  public Base64KeyMaterialConfig getBase64KeyMaterialConfig() {
    return base64KeyMaterialConfig;
  }

  @JsonIgnore
  @ValidationMethod(message = ".jwkSetFile, .base64KeyMaterial: Missing at least one configuration")
  public boolean isKeyConfigured() {
    return !((base64KeyMaterialConfig.getPrivateKey() == null) && (privateKeysFromJwkSetFile.isEmpty()));
  }

  @JsonIgnore
  public Pair<KeyID, Key> getMatchingPrivateKey() {
    return matchingPrivateKey;
  }

  @JsonIgnore
  public void setMatchingPrivateKey(final Pair<KeyID, Key> matchingPrivateKey) {
    this.matchingPrivateKey = matchingPrivateKey;
  }

  @JsonIgnore
  public List<PrivateKey> getAllAvailablePrivateKeys() {
    final List<PrivateKey> result = new LinkedList<>(privateKeysFromJwkSetFile);
    if (base64KeyMaterialConfig.getPrivateKey() != null) {
      result.add(base64KeyMaterialConfig.getPrivateKey());
    }
    return result;
  }

}
