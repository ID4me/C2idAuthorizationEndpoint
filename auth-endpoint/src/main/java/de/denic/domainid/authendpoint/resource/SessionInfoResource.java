/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.authendpoint.resource;

import de.denic.domainid.authendpoint.model.SessionInfo;
import de.denic.domainid.authendpoint.view.SessionInfoView;
import de.denic.domainid.connect2id.common.SubjectSessionId;
import de.denic.domainid.connect2id.subjectsessionstore.Connect2IdSubjectSessionStoreAPI;
import org.apache.commons.lang3.Validate;

import javax.annotation.concurrent.Immutable;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.*;
import java.net.URI;
import java.time.Duration;
import java.util.List;

import static javax.ws.rs.core.MediaType.TEXT_HTML;

@Path("/sessionInfo")
@Immutable
public final class SessionInfoResource {

  private static final URI SESSION_INFO_RESOURCE_URI = UriBuilder.fromResource(SessionInfoResource.class).build();
  private static final Duration ZERO_MINUTES_LIFETIME = Duration.ofMinutes(0L);
  private static final String TEXT_HTML_CHARSET_UTF8 = TEXT_HTML + ";charset=UTF-8";

  private final Connect2IdSubjectSessionStoreAPI connect2IdSubjectSessionStoreAPI;

  /**
   * @param connect2IdSubjectSessionStoreAPI Required
   */
  public SessionInfoResource(final Connect2IdSubjectSessionStoreAPI connect2IdSubjectSessionStoreAPI) {
    Validate.notNull(connect2IdSubjectSessionStoreAPI, "Missing Connect2Id Session Store API component");
    this.connect2IdSubjectSessionStoreAPI = connect2IdSubjectSessionStoreAPI;
  }

  @GET
  @Produces(TEXT_HTML_CHARSET_UTF8)
  public Response getInfo(@Context final HttpHeaders httpHeaders) {
    final SessionInfo sessionInfo = new SessionInfo(httpHeaders);
    final List<SubjectSessionId> allSubjectSessionIDs = sessionInfo.getSubjectSessionCookies().getAllSubjectSessionIds();
    return Response.ok(new SessionInfoView(sessionInfo, connect2IdSubjectSessionStoreAPI.sessionsOf(allSubjectSessionIDs))).build();
  }

  @POST
  @Path("/clear")
  @Produces(TEXT_HTML_CHARSET_UTF8)
  public Response clearSession(@Context final HttpHeaders httpHeaders) {
    final SubjectSessionCookies sessionCookies = SubjectSessionCookies.from(httpHeaders);
    final NewCookie[] newCookies = sessionCookies.cookiesToChangeWithLogout(ZERO_MINUTES_LIFETIME);
    return Response.seeOther(SESSION_INFO_RESOURCE_URI).cookie(newCookies).build();
  }

}
