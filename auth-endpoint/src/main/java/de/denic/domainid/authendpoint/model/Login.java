/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.authendpoint.model;

import static java.util.Collections.emptyList;
import static java.util.Collections.sort;
import static java.util.Collections.unmodifiableList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.concurrent.Immutable;

import org.apache.commons.lang3.Validate;

import de.denic.domainid.DomainId;
import de.denic.domainid.connect2id.authzsession.SessionId;
import de.denic.domainid.connect2id.subjectsessionstore.entity.SubjectSession;

@Immutable
public final class Login {

  private final DomainId domainId;
  private final SessionId sessionId;
  private final List<SubjectSession> knownSessionsOfSubject;

  /**
   * @param sessionId
   *          Required
   * @param domainId
   *          Optional
   * @param knownSessionsOfSubject
   *          Optional
   */
  public Login(final SessionId sessionId, final DomainId domainId,
      final Collection<SubjectSession> knownSessionsOfSubject) {
    Validate.notNull(sessionId, "Missing session ID");
    this.domainId = domainId;
    this.sessionId = sessionId;
    if (knownSessionsOfSubject == null) {
      this.knownSessionsOfSubject = emptyList();
    } else {
      final List<SubjectSession> temp = new ArrayList<>(knownSessionsOfSubject);
      sort(temp);
      this.knownSessionsOfSubject = unmodifiableList(temp);
    }
  }

  /**
   * @return Optional
   */
  public DomainId getDomainId() {
    return domainId;
  }

  /**
   * @return Optional
   */
  public SessionId getSessionId() {
    return sessionId;
  }

  /**
   * @return Never <code>null</code> but unmodifiable.
   */
  public List<SubjectSession> getKnownSessionsOfSubject() {
    return knownSessionsOfSubject;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((domainId == null) ? 0 : domainId.hashCode());
    result = prime * result + ((sessionId == null) ? 0 : sessionId.hashCode());
    result = prime * result + ((knownSessionsOfSubject == null) ? 0 : knownSessionsOfSubject.hashCode());
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final Login other = (Login) obj;
    if (domainId == null) {
      if (other.domainId != null) {
        return false;
      }
    } else if (!domainId.equals(other.domainId)) {
      return false;
    }
    if (sessionId == null) {
      if (other.sessionId != null) {
        return false;
      }
    } else if (!sessionId.equals(other.sessionId)) {
      return false;
    }
    if (knownSessionsOfSubject == null) {
      if (other.knownSessionsOfSubject != null) {
        return false;
      }
    } else if (!knownSessionsOfSubject.equals(other.knownSessionsOfSubject)) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "domainId=" + domainId + ", sessionId=" + sessionId + ", subjectSession=" + knownSessionsOfSubject;
  }

}
