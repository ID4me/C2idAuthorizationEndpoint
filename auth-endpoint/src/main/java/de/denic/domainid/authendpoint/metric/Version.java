package de.denic.domainid.authendpoint.metric;

import com.codahale.metrics.Gauge;
import de.denic.domainid.authendpoint.App;
import io.dropwizard.lifecycle.Managed;

import javax.annotation.concurrent.Immutable;

import static com.codahale.metrics.MetricRegistry.name;
import static com.codahale.metrics.SharedMetricRegistries.getDefault;

@Immutable
public final class Version implements Managed {

  private static final Gauge<String> VERSION_GAUGE = () -> App.class.getPackage().getImplementationVersion();
  private static final String NAME_OF_METRIC = name(Version.class, "string");

  @Override
  public void start() {
    getDefault().register(NAME_OF_METRIC, VERSION_GAUGE);
  }

  @Override
  public void stop() {
    // NoOp
  }
}
