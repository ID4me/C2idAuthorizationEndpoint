/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.authendpoint.view;

import static javax.ws.rs.core.MediaType.TEXT_HTML;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.nio.charset.Charset;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;

import io.dropwizard.jersey.validation.ValidationErrorMessage;

@Provider
@Produces(TEXT_HTML)
public final class ValidationErrorMessageHTMLBodyWriter implements MessageBodyWriter<ValidationErrorMessage> {

  private static final long DONT_KNOW_IN_ADVANCE = -1L;
  private static final Charset UTF8 = Charset.forName("UTF8");

  @Override
  public boolean isWriteable(final Class<?> type, final Type genericType, final Annotation[] annotations,
      final MediaType mediaType) {
    return ValidationErrorMessage.class.isAssignableFrom(type);
  }

  @Override
  public long getSize(final ValidationErrorMessage validationErrorMessage, final Class<?> type, final Type genericType,
      final Annotation[] annotations, final MediaType mediaType) {
    return DONT_KNOW_IN_ADVANCE;
  }

  @Override
  public void writeTo(final ValidationErrorMessage validationErrorMessage, final Class<?> type, final Type genericType,
      final Annotation[] annotations, final MediaType mediaType, final MultivaluedMap<String, Object> httpHeaders,
      final OutputStream entityStream) throws IOException, WebApplicationException {
    try (final PrintWriter writer = new PrintWriter(new OutputStreamWriter(entityStream, UTF8))) {
      writer.println("<html>");
      writer.println("  <body>");
      writer.println("    <table>");
      writer.println("      <caption>Validation errors</caption>");
      writer.println("      <thead>");
      writer.println("        <tr>");
      writer.println("          <th>#</th>");
      writer.println("          <th>Message</th>");
      writer.println("        </tr>");
      writer.println("      </thead>");
      writer.println("      <tbody>");
      int count = 0;
      for (final String error : validationErrorMessage.getErrors()) {
        writer.println("        <tr>");
        writer.println("          <th>" + (++count) + "</th>");
        writer.println("          <td>" + error + "</td>");
        writer.println("        </tr>");
      }
      writer.println("      </tbody>");
      writer.println("    </table>");
      writer.println("  </body>");
      writer.println("</html>");
    }
  }

}
