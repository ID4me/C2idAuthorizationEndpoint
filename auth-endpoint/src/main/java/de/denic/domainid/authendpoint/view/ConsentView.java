/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.authendpoint.view;

import de.denic.domainid.DomainId;
import de.denic.domainid.connect2id.authzsession.entity.ConsentPromptResponse;
import de.denic.domainid.connect2id.common.Claim;
import org.apache.commons.lang3.Validate;

import java.nio.charset.Charset;
import java.util.*;

import static java.util.Collections.sort;
import static java.util.Collections.unmodifiableList;
import static java.util.ResourceBundle.getBundle;

public final class ConsentView extends DomainIdView {

  private static final Charset UTF_8 = Charset.forName("utf-8");
  private static final ResourceBundle CLAIM_NAMES_RESOURCE_BUNDLE = getBundle("claimNamesResourceBundle");
  private static final ResourceBundle CLAIM_DESCRIPTIONS_RESOURCE_BUNDLE = getBundle("claimDescriptionsResourceBundle");

  private final ConsentPromptResponse entity;

  public ConsentView(final ConsentPromptResponse entity, final DomainId domainId) {
    super("consent.ftl", domainId);
    this.entity = entity;
  }

  /**
   * @param claim Required to be not empty.
   * @return Never <code>null</code>.
   */
  public static String getNameOfClaim(final String claim) {
    Validate.notEmpty(claim, "Missing claim");
    try {
      return CLAIM_NAMES_RESOURCE_BUNDLE.getString(claim);
    } catch (MissingResourceException e) {
      return claim;
    }
  }

  /**
   * @param claim Required to be not empty.
   * @return Never <code>null</code>.
   */
  public static String getDescriptionOfClaim(final String claim) {
    Validate.notEmpty(claim, "Missing claim");
    try {
      return CLAIM_DESCRIPTIONS_RESOURCE_BUNDLE.getString(claim);
    } catch (MissingResourceException e) {
      return claim;
    }
  }

  public ConsentPromptResponse getConsent() {
    return entity;
  }

  /**
   * @return Never <code>null</code> but unmodifiable.
   */
  public List<Claim> getClaimsSortedAppropriately() {
    final ConsentPromptResponse.Claims claims = getConsent().getClaims();
    final List<Claim> essentialClaims = new LinkedList<>(claims.getConsented().getEssential());
    essentialClaims.addAll(claims.getNew().getEssential());
    sort(essentialClaims);
    final List<Claim> notEssentialClaims = new LinkedList<>(claims.getConsented().getVoluntary());
    notEssentialClaims.addAll(claims.getNew().getVoluntary());
    sort(notEssentialClaims);
    final List<Claim> result = new ArrayList<>(essentialClaims.size() + notEssentialClaims.size());
    result.addAll(essentialClaims);
    result.addAll(notEssentialClaims);
    return unmodifiableList(result);
  }

}
