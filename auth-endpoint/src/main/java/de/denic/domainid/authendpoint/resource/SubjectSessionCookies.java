/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.authendpoint.resource;

import de.denic.domainid.connect2id.common.SubjectSessionId;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.concurrent.Immutable;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.NewCookie;
import java.time.Duration;
import java.util.*;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.util.Collections.emptyList;
import static java.util.Collections.unmodifiableList;
import static java.util.Optional.empty;
import static java.util.stream.Collectors.partitioningBy;
import static java.util.stream.Collectors.toList;

@Immutable
public final class SubjectSessionCookies {

  public static final String NAME_PREFIX = "sub_sid_";
  public static final String NAME_OF_CURRENT = NAME_PREFIX + "current";

  private static final Logger LOG = LoggerFactory.getLogger(SubjectSessionCookies.class);
  private static final String EMPTY_COOKIE_VALUE = "";
  private static final List<Cookie> EMPTY_LIST_OF_COOKIES = emptyList();
  private static final int EXPIRED = 0;
  private static final String IDENTITY_COOKIE_COMMENT = "OIdAuthentSubjSessId";
  private static final String CURRENT_IDENTITY_COOKIE_COMMENT = "Current" + IDENTITY_COOKIE_COMMENT;
  private static final boolean VIA_HTTP_AS_WELL_AS_HTTPS = false;
  private static final String ROOT_PATH = "/";
  private static final String NO_DOMAIN = null;
  private final List<IdentityCookie> sortedIdentityCookies;
  private final Integer indexOfCurrentIdentity;

  /**
   * @param currentIdentitySessionCookie Optional
   * @param identitySessionCookies       Optional
   */
  private SubjectSessionCookies(final Cookie currentIdentitySessionCookie, final List<Cookie> identitySessionCookies) {
    indexOfCurrentIdentity = (currentIdentitySessionCookie == null ? null
            : parseIndexFrom(currentIdentitySessionCookie.getValue()));
    if (identitySessionCookies == null) {
      sortedIdentityCookies = emptyList();
    } else {
      sortedIdentityCookies = unmodifiableList(
              identitySessionCookies.stream().map(IdentityCookie::new).sorted().collect(toList()));
    }
  }

  private static int parseIndexFrom(final String value) {
    Validate.notEmpty(value, "Missing value");
    Validate.isTrue(value.startsWith(NAME_PREFIX), "Invalid session cookie name prefix: ", value);
    return Integer.parseInt(value.substring(NAME_PREFIX.length()));
  }

  private static String indexToName(final Integer index) {
    return index == null ? "NONE" : String.format(NAME_PREFIX + "%1$03d", index);
  }

  /**
   * Factory method.
   *
   * @param httpHeaders Required
   * @return Never <code>null</code>
   */
  public static SubjectSessionCookies from(final HttpHeaders httpHeaders) {
    Validate.notNull(httpHeaders, "Missing HTTP headers");
    final List<Cookie> cookies = httpHeaders.getCookies().entrySet().stream()
            .filter(entry -> entry.getKey().startsWith(NAME_PREFIX)).map(Map.Entry::getValue).collect(toList());
    return from(cookies);
  }

  /**
   * Factory method.
   *
   * @param cookies Optional
   * @return Never <code>null</code>
   */
  public static SubjectSessionCookies from(final Collection<Cookie> cookies) {
    final Map<Boolean, List<Cookie>> partitionedCookies = (cookies == null ? EMPTY_LIST_OF_COOKIES : cookies).stream()
            .collect(partitioningBy(cookie -> NAME_OF_CURRENT.equals(cookie.getName())));
    final List<Cookie> currentIdentityCookies = partitionedCookies.get(TRUE);
    final List<Cookie> identityCookies = partitionedCookies.get(FALSE);
    if (currentIdentityCookies.size() > 1) {
      throw new RuntimeException(
              "Internal error: More than one header of name '" + NAME_OF_CURRENT + "': " + currentIdentityCookies);
    }

    final Cookie currentIdentitySessionCookie = (currentIdentityCookies.isEmpty() ? null
            : currentIdentityCookies.get(0));
    return new SubjectSessionCookies(currentIdentitySessionCookie, identityCookies);
  }

  /**
   * @param subjectSessionId Required
   * @param lifetime         Optional
   * @return Never <code>null</code>, contains two elements.
   */
  public NewCookie[] cookiesOfNewActiveIdentity(final SubjectSessionId subjectSessionId, final Duration lifetime) {
    Validate.notNull(subjectSessionId, "Missing subject session ID");
    final IdentityCookie identityCookie = getKnownOrNewIdentityCookieOf(subjectSessionId);
    final int maxAgeSeconds = durationToLifetimeSeconds(lifetime);
    final NewCookie newIdentityCookie = new NewCookie(identityCookie.getName(), identityCookie.getValue(), ROOT_PATH,
            NO_DOMAIN, IDENTITY_COOKIE_COMMENT, maxAgeSeconds, VIA_HTTP_AS_WELL_AS_HTTPS);
    final NewCookie newCurrentIdentityCookie = new NewCookie(NAME_OF_CURRENT, newIdentityCookie.getName(), ROOT_PATH,
            NO_DOMAIN, CURRENT_IDENTITY_COOKIE_COMMENT, maxAgeSeconds, VIA_HTTP_AS_WELL_AS_HTTPS);
    final NewCookie[] newCookies = {newCurrentIdentityCookie, newIdentityCookie};
    LOG.info("Cookies newly set: {}", Arrays.toString(newCookies));
    return newCookies;
  }

  private int durationToLifetimeSeconds(final Duration lifetime) {
    return lifetime == null ? EXPIRED : (int) lifetime.getSeconds();
  }

  /**
   * @param sessionId Required
   * @return Never <code>null</code>.
   */
  private IdentityCookie getKnownOrNewIdentityCookieOf(final SubjectSessionId sessionId) {
    Validate.notNull(sessionId, "Missing subject session ID");
    return sortedIdentityCookies.stream().filter(cookie -> sessionId.equals(cookie.getSessionId())).findAny()
            .orElse(new IdentityCookie(lowestFreePosition(), sessionId));
  }

  private int lowestFreePosition() {
    if (sortedIdentityCookies.isEmpty()) {
      return 0;
    }

    if (sortedIdentityCookies.size() < 1_000) {
      // TODO: Refactor it! Ziehe Nutzen aus dem Umstand, dass die Eintraege SORTIERT sind!
      for (final IdentityCookie identityCookie : sortedIdentityCookies) {
        final int nextPositionCandidate = identityCookie.getIndex() + 1;
        final boolean positionCandidateIsUsed = sortedIdentityCookies.stream()
                .anyMatch(cookie -> nextPositionCandidate == cookie.getIndex());
        if (!positionCandidateIsUsed) {
          return nextPositionCandidate;
        }
      }
    }

    throw new RuntimeException("Cookie namespace exhausted!");
  }

  /**
   * @return Never <code>null</code>
   */
  public Optional<SubjectSessionId> getCurrentSessionId() {
    if (indexOfCurrentIdentity == null || sortedIdentityCookies.isEmpty()) {
      return empty();
    }

    return sortedIdentityCookies.stream().filter(cookie -> indexOfCurrentIdentity == cookie.getIndex())
            .findFirst().map(IdentityCookie::getSessionId);
  }

  /**
   * @return Never <code>null</code>
   */
  public List<SubjectSessionId> getAllSubjectSessionIds() {
    return sortedIdentityCookies.stream().map(IdentityCookie::getSessionId).collect(toList());
  }

  /**
   * @return Never <code>null</code>
   */
  public NewCookie[] cookiesToChangeWithLogout(final Duration lifetime) {
    final List<NewCookie> cookiesToChange = sortedIdentityCookies.stream().map(identityCookie -> new NewCookie(identityCookie.getName(),
            EMPTY_COOKIE_VALUE, ROOT_PATH, NO_DOMAIN, IDENTITY_COOKIE_COMMENT, EXPIRED,
            VIA_HTTP_AS_WELL_AS_HTTPS)).collect(toList());
    cookiesToChange.add(new NewCookie(NAME_OF_CURRENT, EMPTY_COOKIE_VALUE, ROOT_PATH,
            NO_DOMAIN, IDENTITY_COOKIE_COMMENT, EXPIRED, VIA_HTTP_AS_WELL_AS_HTTPS));
    return cookiesToChange.toArray(new NewCookie[0]);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    SubjectSessionCookies that = (SubjectSessionCookies) o;
    return Objects.equals(sortedIdentityCookies, that.sortedIdentityCookies) &&
            Objects.equals(indexOfCurrentIdentity, that.indexOfCurrentIdentity);
  }

  @Override
  public int hashCode() {
    return Objects.hash(sortedIdentityCookies, indexOfCurrentIdentity);
  }

  @Override
  public String toString() {
    return "Current: " + indexToName(indexOfCurrentIdentity) + " " + sortedIdentityCookies;
  }

  @Immutable
  public final static class IdentityCookie implements Comparable<IdentityCookie> {

    private final SubjectSessionId value;
    private final int index;

    /**
     * @param index Required to be within range [0..999].
     * @param value Required.
     */
    private IdentityCookie(final int index, final SubjectSessionId value) {
      Validate.inclusiveBetween(0, 999, index, "Invalid subject session cookie position: ");
      Validate.notNull(value, "Missing cookie value");
      this.index = index;
      this.value = value;
    }

    /**
     * @param cookie Required.
     */
    private IdentityCookie(final Cookie cookie) {
      this(parseIndexFrom(cookie.getName()), new SubjectSessionId(cookie.getValue()));
    }

    @Override
    public int compareTo(final IdentityCookie other) {
      return Integer.compare(index, other.index);
    }

    private int getIndex() {
      return index;
    }

    /**
     * @return Never <code>null</code>
     */
    public SubjectSessionId getSessionId() {
      return value;
    }

    /**
     * @return Never <code>null</code>
     */
    public String getValue() {
      return value.getPlain();
    }

    /**
     * @return Never <code>null</code>
     */
    public String getName() {
      return indexToName(index);
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      IdentityCookie that = (IdentityCookie) o;
      return index == that.index &&
              Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
      return Objects.hash(value, index);
    }

    @Override
    public String toString() {
      return indexToName(index) + "='" + value + "'";
    }

  }

}
