/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.authendpoint.model;

import de.denic.domainid.connect2id.authzsession.SessionId;
import de.denic.domainid.connect2id.common.Claim;
import org.apache.commons.lang3.Validate;

import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static java.lang.String.format;
import static java.util.Collections.unmodifiableSet;

@Immutable
public final class ConsentingRequest {

  private final SessionId sessionId;
  private final ZonedDateTime creationTimeStamp;
  private final boolean consented;
  private final Set<Claim> consentedClaims;
  private final Set<Claim> refusedClaims;
  private final boolean rememberDecision;

  /**
   * @param sessionIdValue    Required
   * @param creationTimeStamp Required
   * @param consentedClaims   Optional
   * @param suggestedClaims   Optional
   * @param rememberDecision  Required
   */
  private ConsentingRequest(final String sessionIdValue, final boolean consented,
                            final ZonedDateTime creationTimeStamp, final Set<Claim> consentedClaims, final Set<Claim> suggestedClaims, final boolean rememberDecision) {
    Validate.notNull(creationTimeStamp, "Missing creation timestamp");
    this.sessionId = new SessionId(sessionIdValue);
    this.creationTimeStamp = creationTimeStamp;
    this.consented = consented;
    this.rememberDecision = rememberDecision;
    this.consentedClaims = unmodifiableSet(consentedClaims);
    final Set<Claim> tempRefusedClaims = new HashSet<>(suggestedClaims);
    tempRefusedClaims.removeAll(consentedClaims);
    this.refusedClaims = unmodifiableSet(tempRefusedClaims);
  }

  public static Builder build() {
    return new BuilderImpl();
  }

  /**
   * @return Never <code>null</code>
   */
  public SessionId getSessionId() {
    return sessionId;
  }

  /**
   * @return Never <code>null</code>
   */
  public ZonedDateTime getCreationTimeStamp() {
    return creationTimeStamp;
  }

  /**
   * @return Never <code>null</code> but unmodifiable.
   */
  public Set<Claim> getConsentedClaims() {
    return consentedClaims;
  }

  /**
   * @return Never <code>null</code> but unmodifiable.
   */
  public Set<Claim> getRefusedClaims() {
    return refusedClaims;
  }

  public boolean isConsented() {
    return consented;
  }

  public boolean doRememberDecision() {
    return rememberDecision;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    ConsentingRequest that = (ConsentingRequest) o;
    return consented == that.consented &&
            rememberDecision == that.rememberDecision &&
            Objects.equals(sessionId, that.sessionId) &&
            Objects.equals(creationTimeStamp, that.creationTimeStamp) &&
            Objects.equals(consentedClaims, that.consentedClaims) &&
            Objects.equals(refusedClaims, that.refusedClaims);
  }

  @Override
  public int hashCode() {
    return Objects.hash(sessionId, creationTimeStamp, consented, consentedClaims, refusedClaims, rememberDecision);
  }

  @Override
  public String toString() {
    return format("sessionID '%1$s', consented '%2$s', claims '%3$s', refused '%4$s', remember '%5$s'",
            sessionId, consented, consentedClaims, refusedClaims, rememberDecision);
  }

  public interface Builder {

    Builder sessionId(String sessionId);

    Builder consented(boolean consenting);

    Builder consentedClaims(Set<Claim> consentedClaims);

    Builder rememberDecision(boolean rememberDecision);

    Builder suggestedClaims(Set<Claim> suggestedClaims);

    /**
     * @return Never <code>null</code>
     */
    ConsentingRequest build();

  }

  @NotThreadSafe
  public static final class BuilderImpl implements Builder {

    private final ZonedDateTime creationTimeStamp;
    private final Set<Claim> consentedClaims = new HashSet<>();
    private final Set<Claim> suggestedClaims = new HashSet<>();
    private String sessionId;
    private boolean consented = false;
    private boolean rememberDecision = false;

    public BuilderImpl() {
      this.creationTimeStamp = LocalDateTime.now().atZone(ZoneId.systemDefault());
    }

    @Override
    public Builder sessionId(final String sessionID) {
      this.sessionId = sessionID;
      return this;
    }

    @Override
    public Builder consented(final boolean consented) {
      this.consented = consented;
      return this;
    }

    @Override
    public Builder consentedClaims(final Set<Claim> consentedClaims) {
      if (consentedClaims != null) {
        this.consentedClaims.addAll(consentedClaims);
      }
      return this;
    }

    @Override
    public Builder suggestedClaims(final Set<Claim> suggestedClaims) {
      if (suggestedClaims != null) {
        this.suggestedClaims.addAll(suggestedClaims);
      }
      return this;
    }

    @Override
    public Builder rememberDecision(final boolean rememberDecision) {
      this.rememberDecision = rememberDecision;
      return this;
    }

    @Override
    public ConsentingRequest build() {
      return new ConsentingRequest(sessionId, consented, creationTimeStamp, consentedClaims, suggestedClaims,
              rememberDecision);
    }

  }

}
