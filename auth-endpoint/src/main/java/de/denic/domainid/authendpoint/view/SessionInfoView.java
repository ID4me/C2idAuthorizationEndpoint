/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.authendpoint.view;

import de.denic.domainid.authendpoint.model.SessionInfo;
import de.denic.domainid.connect2id.subjectsessionstore.entity.SubjectSession;
import org.apache.commons.lang3.Validate;

import javax.annotation.concurrent.Immutable;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.Collections.unmodifiableList;

@Immutable
public final class SessionInfoView extends AuthEndpointView {

  private static final Charset UTF_8 = Charset.forName("utf-8");

  private final SessionInfo sessionInfo;

  private final List<SubjectSession> subjectSessions;

  /**
   * @param sessionInfo     Required
   * @param subjectSessions Optional
   */
  public SessionInfoView(final SessionInfo sessionInfo, final Collection<SubjectSession> subjectSessions) {
    super("sessionInfo.ftl");
    Validate.notNull(sessionInfo, "Missing session info object");
    this.sessionInfo = sessionInfo;
    if (subjectSessions != null) {
      this.subjectSessions = unmodifiableList(new ArrayList<>(subjectSessions));
    } else {
      this.subjectSessions = emptyList();
    }
  }

  /**
   * @return Never <code>null</code>
   */
  public SessionInfo getSessionInfo() {
    return sessionInfo;
  }

  /**
   * @return Never <code>null</code> but unmodifiable!
   */
  public List<SubjectSession> getSubjectSessions() {
    return subjectSessions;
  }

}
