/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.authendpoint.resource;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.SharedMetricRegistries;
import com.codahale.metrics.annotation.Timed;
import de.denic.domainid.DomainId;
import de.denic.domainid.authendpoint.model.Claims;
import de.denic.domainid.authendpoint.model.Login;
import de.denic.domainid.authendpoint.model.OpenIdAuthenticationRequest;
import de.denic.domainid.authendpoint.model.RequestURI;
import de.denic.domainid.authendpoint.view.LoginView;
import de.denic.domainid.connect2id.authzsession.Connect2IdAuthorizationSessionAPI;
import de.denic.domainid.connect2id.authzsession.Connect2IdAuthorizationSessionAPI.InitiateSessionResponse;
import de.denic.domainid.connect2id.authzsession.ResponseType;
import de.denic.domainid.connect2id.authzsession.State;
import de.denic.domainid.connect2id.authzsession.entity.AuthenticationPromptResponse;
import de.denic.domainid.connect2id.authzstore.Connect2IdAuthorizationStoreAPI;
import de.denic.domainid.connect2id.authzstore.entity.OpenIdConnectAuthorization;
import de.denic.domainid.connect2id.clientregistration.OpenIdConnectClientRegistration;
import de.denic.domainid.connect2id.clientregistration.entity.OpenIdConnectClient;
import de.denic.domainid.connect2id.common.*;
import de.denic.domainid.connect2id.subjectsessionstore.Connect2IdSubjectSessionStoreAPI;
import de.denic.domainid.connect2id.subjectsessionstore.entity.SubjectSession;
import de.denic.domainid.connect2id.subjectsessionstore.entity.SubjectSessionOptionalData;
import de.denic.domainid.dbaccess.UserAuthenticationDAO;
import de.denic.domainid.openid.OpenIdAuthenticationRequestParamKeys;
import de.denic.domainid.subject.PseudonymSubject;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.concurrent.Immutable;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import static com.codahale.metrics.MetricRegistry.name;
import static de.denic.domainid.authendpoint.resource.SubjectSessionCookies.from;
import static de.denic.domainid.connect2id.common.Prompt.Value.select_account;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;
import static java.util.Optional.ofNullable;
import static javax.ws.rs.core.MediaType.APPLICATION_FORM_URLENCODED;
import static javax.ws.rs.core.MediaType.TEXT_HTML;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;

@Path("login")
@Consumes(APPLICATION_FORM_URLENCODED)
@Immutable
public final class OpenIdAuthenticationRequestResource implements OpenIdAuthenticationRequestParamKeys {

  private static final Logger LOG = LoggerFactory.getLogger(OpenIdAuthenticationRequestResource.class);
  private static final long HALF_A_SECOND_AS_MILLIS = Duration.ofMillis(500L).toMillis();
  private static final AtomicInteger PROMPT_FOR_SELECT_ACCOUNT_COUNTER = new AtomicInteger();
  private static final String TEXT_HTML_CHARSET_UTF8 = TEXT_HTML + ";charset=UTF-8";

  private final Connect2IdAuthorizationSessionAPI c2IdAuthorisationSession;
  private final Connect2IdSubjectSessionStoreAPI c2IdSubjectSessionStore;
  private final Connect2IdAuthorizationStoreAPI c2IdAuthorisationStore;
  private final UserAuthenticationDAO userAuthenticationDAO;
  private final OpenIdConnectClientRegistration openIdConnectClientRegistration;

  /**
   * @param c2IdAuthorisationSession        Required
   * @param c2IdSubjectSessionStore         Required
   * @param c2IdAuthorisationStore          Required
   * @param userAuthenticationDAO           Required
   * @param openIdConnectClientRegistration Required
   */
  public OpenIdAuthenticationRequestResource(final Connect2IdAuthorizationSessionAPI c2IdAuthorisationSession,
                                             final Connect2IdSubjectSessionStoreAPI c2IdSubjectSessionStore,
                                             final Connect2IdAuthorizationStoreAPI c2IdAuthorisationStore,
                                             final UserAuthenticationDAO userAuthenticationDAO,
                                             final OpenIdConnectClientRegistration openIdConnectClientRegistration) {
    Validate.notNull(c2IdAuthorisationSession, "Missing Connect2Id Authorisation Session API component");
    Validate.notNull(c2IdSubjectSessionStore, "Missing Connect2Id Subject Session Store API component");
    Validate.notNull(c2IdAuthorisationStore, "Missing Connect2Id Authorisation Store API component");
    Validate.notNull(userAuthenticationDAO, "Missing user authentication DAO");
    Validate.notNull(openIdConnectClientRegistration, "Missing OpenID Connect Client Registration component");
    this.userAuthenticationDAO = userAuthenticationDAO;
    this.c2IdAuthorisationSession = c2IdAuthorisationSession;
    this.c2IdSubjectSessionStore = c2IdSubjectSessionStore;
    this.c2IdAuthorisationStore = c2IdAuthorisationStore;
    this.openIdConnectClientRegistration = openIdConnectClientRegistration;
    final MetricRegistry metricRegistry = SharedMetricRegistries.tryGetDefault();
    if (metricRegistry != null) {
      metricRegistry.register(
              name(OpenIdAuthenticationRequestResource.class.getSimpleName(), "prompt_select_account", "count"),
              (Gauge<Integer>) PROMPT_FOR_SELECT_ACCOUNT_COUNTER::get);
    }
  }

  @GET
  @Produces(TEXT_HTML_CHARSET_UTF8)
  @Timed
  public Response viaGET(@Context final HttpHeaders httpHeaders,
                         @NotNull @QueryParam(RESPONSE_TYPE) final ResponseType responseType,
                         @NotNull @QueryParam(SCOPE) final Scope scope, @NotNull @QueryParam(CLIENT_ID) final ClientId clientId,
                         @NotNull @QueryParam(STATE) final State state, @NotNull @QueryParam(REDIRECT_URI) final RedirectURI redirectURI,
                         @QueryParam(LOGIN_HINT) final DomainId loginHint, @QueryParam(MAX_AGE) final MaxAge maxAge,
                         @QueryParam(NONCE) final de.denic.domainid.connect2id.common.Nonce nonce, @QueryParam(PROMPT) final Prompt prompt,
                         @QueryParam(CLAIMS) final Claims claims, @QueryParam(REQUEST_URI) final RequestURI requestURI,
                         @QueryParam(ID_TOKEN_HINT) final IdTokenHint idTokenHint, @QueryParam(DISPLAY) final Display
                                 display, @QueryParam(REQUEST) final Request request) {
    return viaPOST(httpHeaders, responseType, scope, clientId, state, redirectURI, loginHint, maxAge, nonce, prompt,
            claims, requestURI, idTokenHint, display, request);
  }

  @POST
  @Produces(TEXT_HTML_CHARSET_UTF8)
  @Timed
  public Response viaPOST(@Context final HttpHeaders httpHeaders,
                          @NotNull @FormParam(RESPONSE_TYPE) final ResponseType responseType, @NotNull @FormParam(SCOPE) final Scope scope,
                          @NotNull @FormParam(CLIENT_ID) final ClientId clientId, @NotNull @FormParam(STATE) final State state,
                          @NotNull @FormParam(REDIRECT_URI) final RedirectURI redirectURI, @FormParam(LOGIN_HINT) final DomainId loginHint,
                          @FormParam(MAX_AGE) final MaxAge maxAge, @FormParam(NONCE) final Nonce nonce,
                          @FormParam(PROMPT) final Prompt prompt, @FormParam(CLAIMS) final Claims claims,
                          @FormParam(REQUEST_URI) final RequestURI requestURI, @FormParam(ID_TOKEN_HINT) final IdTokenHint idTokenHint,
                          @FormParam(DISPLAY) final Display display, @QueryParam(REQUEST) final Request request) {
    return handle(OpenIdAuthenticationRequest.build().responseType(responseType).scope(scope).clientId(clientId)
                    .state(state).redirectURI(redirectURI).loginHint(loginHint).nonce(nonce).maxAge(maxAge).prompt(prompt)
                    .claims(claims).requestURI(requestURI).idTokenHint(idTokenHint).display(display).request(request)
                    .build(),
            httpHeaders);
  }

  private Response handle(final OpenIdAuthenticationRequest openIdAuthRequest, final HttpHeaders httpHeaders)
          throws WebApplicationException {
    final SubjectSessionCookies subjectSessionCookies = from(httpHeaders);
    updateMetricsApplying(openIdAuthRequest);
    LOG.info("Received OpenID authentication request: {} (cookies: {})", openIdAuthRequest, subjectSessionCookies);
    final Optional<Map<Claim, String>> optReasonsOfClaims = openIdAuthRequest.getClaims()
            .map(Claims::getReasonsOfClaims);
    final Optional<SubjectSessionId> optCurrentSubjectSessionId = subjectSessionCookies.getCurrentSessionId();
    try {
      final InitiateSessionResponse initiateSessionResponse = c2IdAuthorisationSession.initiateSession(
              openIdAuthRequest.toOpenIdAuthenticationRequestURI(), optCurrentSubjectSessionId.orElse(null),
              optReasonsOfClaims.orElse(null));
      final ClientId clientId = openIdAuthRequest.getClientId();
      final OpenIdConnectClient client = openIdConnectClientRegistration.getClientOf(clientId)
              .orElseThrow(() -> new WebApplicationException("Unknown client ID '" + clientId + "'", BAD_REQUEST));
      letConnect2IdServerDistributeSessionInfo();
      return initiateSessionResponse.dispatchedBy(new MyDispatcher(ofNullable(openIdAuthRequest.getLoginHint()), subjectSessionCookies,
              c2IdSubjectSessionStore, c2IdAuthorisationSession, c2IdAuthorisationStore, userAuthenticationDAO, client)).build();
    } catch (final Connect2IdException e) {
      throw new WebApplicationException(e.getMessage(), NOT_FOUND);
    }
  }

  private void letConnect2IdServerDistributeSessionInfo() {
    try {
      Thread.sleep(HALF_A_SECOND_AS_MILLIS);
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
    }
  }

  private void updateMetricsApplying(final OpenIdAuthenticationRequest openIdAuthRequest) {
    if (select_account.equals(openIdAuthRequest.getPrompt().map(Prompt::getEnumValue).orElse(null))) {
      PROMPT_FOR_SELECT_ACCOUNT_COUNTER.incrementAndGet();
    }
  }

  @Immutable
  static final class MyDispatcher extends ConsentPageSkippingDispatcherBase {

    private final Connect2IdSubjectSessionStoreAPI c2IdSubjectSessionStore;
    private final Connect2IdAuthorizationStoreAPI c2IdAuthorisationStore;
    private final OpenIdConnectClient client;
    private final Optional<DomainId> optLoginHint;

    /**
     * @param optLoginHint             Required
     * @param subjectSessionCookies    Required
     * @param c2IdSubjectSessionStore  Required
     * @param c2IdAuthorisationSession Required
     * @param c2IdAuthorisationStore   Required
     * @param userAuthenticationDAO    Required
     * @param client                   Required
     */
    MyDispatcher(final Optional<DomainId> optLoginHint, final SubjectSessionCookies subjectSessionCookies,
                 final Connect2IdSubjectSessionStoreAPI c2IdSubjectSessionStore,
                 final Connect2IdAuthorizationSessionAPI c2IdAuthorisationSession,
                 final Connect2IdAuthorizationStoreAPI c2IdAuthorisationStore,
                 final UserAuthenticationDAO userAuthenticationDAO,
                 final OpenIdConnectClient client) {
      super(c2IdAuthorisationSession, userAuthenticationDAO, subjectSessionCookies);
      Validate.notNull(c2IdSubjectSessionStore, "Missing C2ID Subject Session Store API component");
      Validate.notNull(c2IdAuthorisationStore, "Missing C2ID Authorisation Store API component");
      Validate.notNull(client, "Missing client");
      Validate.notNull(optLoginHint, "Missing optional login hint");
      this.c2IdSubjectSessionStore = c2IdSubjectSessionStore;
      this.c2IdAuthorisationStore = c2IdAuthorisationStore;
      this.optLoginHint = optLoginHint;
      this.client = client;
    }

    @Override
    public ResponseBuilder handle(final AuthenticationPromptResponse response) {
      final List<SubjectSession> subjectSessions;
      if (response.isSelectAccount()) {
        subjectSessions = c2IdSubjectSessionStore.sessionsOf(getSubjectSessionCookies().getAllSubjectSessionIds());
      } else {
        subjectSessions = emptyList();
      }
      return Response.ok(new LoginView(
              new Login(response.getConnect2IdAuthorisationSessionId(), optLoginHint.orElse(null),
                      subjectSessions), client));
    }

    @Override
    protected Set<Claim> loadRejectedClaimsOf(final PseudonymSubject subject, final ClientId clientId) {
      final Optional<OpenIdConnectAuthorization> optOpenIdConnectAuthorisation = c2IdAuthorisationStore
              .loadAuthorizationOf(subject, clientId);
      return optOpenIdConnectAuthorisation.map(OpenIdConnectAuthorization::getOptionalData)
              .map(SubjectSessionOptionalData::getRejectedClaims).orElse(emptySet());
    }

  }

}