/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.authendpoint.resource;

import com.codahale.metrics.annotation.Timed;
import de.denic.domainid.DomainId;
import de.denic.domainid.authendpoint.model.SwitchAccountRequest;
import de.denic.domainid.connect2id.authzsession.Connect2IdAuthorizationSessionAPI;
import de.denic.domainid.connect2id.authzsession.SessionId;
import de.denic.domainid.connect2id.authzsession.entity.SessionResponse;
import de.denic.domainid.connect2id.authzstore.Connect2IdAuthorizationStoreAPI;
import de.denic.domainid.connect2id.clientregistration.entity.OpenIdConnectClient;
import de.denic.domainid.connect2id.common.Claim;
import de.denic.domainid.connect2id.common.NotFoundException;
import de.denic.domainid.connect2id.common.SubjectSessionId;
import de.denic.domainid.connect2id.subjectsessionstore.Connect2IdSubjectSessionStoreAPI;
import de.denic.domainid.dbaccess.UserAuthenticationDAO;
import de.denic.domainid.dns.DomainIdDnsClient;
import de.denic.domainid.identityagent.IdentityAgentClient;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.concurrent.Immutable;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.Map;

import static de.denic.domainid.authendpoint.resource.SubjectSessionCookies.from;
import static java.util.Optional.ofNullable;
import static javax.ws.rs.core.MediaType.TEXT_HTML;
import static javax.ws.rs.core.Response.Status.GONE;

@Path("switch-account")
@Immutable
public final class SwitchAccountResource {

  private static final Logger LOG = LoggerFactory.getLogger(SwitchAccountResource.class);
  private static final Map<Claim, String> NO_REASONS_OF_CLAIMS = null;
  private static final String TEXT_HTML_CHARSET_UTF8 = TEXT_HTML + ";charset=UTF-8";

  private final Connect2IdAuthorizationSessionAPI c2IdAuthorisationSessionAPI;
  private final UserAuthenticationDAO userAuthenticationDAO;
  // private final DomainIdDnsClient domainIdDnsClient;
  // private final IdentityAgentClient identityAgentClient;
  private final Connect2IdAuthorizationStoreAPI c2IdAuthorisationStoreAPI;
  private final Connect2IdSubjectSessionStoreAPI c2IdSubjectSessionStoreAPI;

  /**
   * @param c2IdAuthorisationSessionAPI Required
   * @param userAuthenticationDAO       Required
   * @param domainIdDnsClient           Required
   * @param identityAgentClient         Required
   * @param c2IdAuthorisationStoreAPI   Required
   * @param c2IdSubjectSessionStoreAPI  Required
   */
  public SwitchAccountResource(final Connect2IdAuthorizationSessionAPI c2IdAuthorisationSessionAPI,
                               final UserAuthenticationDAO userAuthenticationDAO, final DomainIdDnsClient domainIdDnsClient,
                               final IdentityAgentClient identityAgentClient, final Connect2IdAuthorizationStoreAPI c2IdAuthorisationStoreAPI,
                               final Connect2IdSubjectSessionStoreAPI c2IdSubjectSessionStoreAPI) {
    Validate.notNull(c2IdAuthorisationSessionAPI, "Missing Connect2Id Authorisation Session API component");
    Validate.notNull(c2IdAuthorisationStoreAPI, "Missing Connect2Id Authorisation Store API component");
    Validate.notNull(userAuthenticationDAO, "Missing user authentication data access object");
    Validate.notNull(domainIdDnsClient, "Missing DomainID DNS client");
    Validate.notNull(identityAgentClient, "Missing Identity Agent client");
    Validate.notNull(c2IdSubjectSessionStoreAPI, "Missing C2ID Subject Session Store API component");
    this.c2IdAuthorisationSessionAPI = c2IdAuthorisationSessionAPI;
    this.userAuthenticationDAO = userAuthenticationDAO;
    // this.domainIdDnsClient = domainIdDnsClient;
    // this.identityAgentClient = identityAgentClient;
    this.c2IdAuthorisationStoreAPI = c2IdAuthorisationStoreAPI;
    this.c2IdSubjectSessionStoreAPI = c2IdSubjectSessionStoreAPI;
  }

  @POST
  @Produces(TEXT_HTML_CHARSET_UTF8)
  @Timed
  public Response viaPOST(@Context final HttpHeaders httpHeaders,
                          @NotNull @FormParam("sessionID") final SessionId sessionId,
                          @NotNull @FormParam("domainID") final DomainId domainId,
                          @NotNull @FormParam("subjectSessionID") final SubjectSessionId subjectSessionId) {
    try {
      return handle(SwitchAccountRequest.builder().domainId(domainId).sessionId(sessionId)
              .subjectSessionId(subjectSessionId).build(), from(httpHeaders));
    } catch (final NotFoundException e) {
      LOG.warn("Account switching fails cause Connect2Id Authorisation Session '{}' not available",
              sessionId, e);
      throw new WebApplicationException(Response.status(GONE).type(TEXT_HTML).entity(
              "<html><body><h2>Error: Account switching fails cause required backend resource not available any more.</h2></body></html>")
              .build());
    }
  }

  private Response handle(final SwitchAccountRequest request, final SubjectSessionCookies subjectSessionCookies) {
    LOG.info("Received data to switch account: {} (cookies: {})", request, subjectSessionCookies);
    final SessionResponse dataOfAuthSession = c2IdAuthorisationSessionAPI
            .getSession(request.getSessionId());
    final URI initialRequestURI = dataOfAuthSession.getInitialRequest().toURI();
    final OpenIdConnectClient client = null; // TODO: Fill in client data!
    return c2IdAuthorisationSessionAPI
            .initiateSession(initialRequestURI, request.getSubjectSessionId(), NO_REASONS_OF_CLAIMS)
            .dispatchedBy(new OpenIdAuthenticationRequestResource.MyDispatcher(ofNullable(request.getDomainId()),
                    subjectSessionCookies, c2IdSubjectSessionStoreAPI, c2IdAuthorisationSessionAPI, c2IdAuthorisationStoreAPI, userAuthenticationDAO, client))
            .build();
  }

}
