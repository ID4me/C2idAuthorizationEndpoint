/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.authendpoint.model;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.denic.domainid.connect2id.common.Claim;
import de.denic.domainid.connect2id.common.UrlEncodedValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.concurrent.Immutable;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import static java.util.Collections.unmodifiableMap;
import static org.apache.commons.lang3.StringUtils.trimToNull;

/**
 * Content of POST-parameter <a href="https://openid.net/specs/openid-connect-core-1_0.html#ClaimsParameter">"claims"
 * with an OpenID Authentication Request</a>.
 *
 * @see OpenIdAuthenticationRequest
 */
@Immutable
public final class Claims extends UrlEncodedValue {

  private static final Logger LOG = LoggerFactory.getLogger(Claims.class);
  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
  private static final String REASON = "reason";

  private final Map<Claim, String> reasonsOfClaims;

  public Claims(final String plainValue) {
    super(plainValue);
    reasonsOfClaims = unmodifiableMap(parseForReasonsOfClaims(plainValue));
  }

  private static Map<Claim, String> parseForReasonsOfClaims(final String plainValue) {
    final Map<Claim, String> reasonsOfClaims = new HashMap<>();
    try {
      final JsonNode claimGroupNode = OBJECT_MAPPER.readValue(plainValue, JsonNode.class);
      final List<Entry<String, JsonNode>> allClaimEntries = new LinkedList<>();
      claimGroupNode.fields().forEachRemaining(
              userInfo -> userInfo.getValue().fields().forEachRemaining(claimEntry -> allClaimEntries.add(claimEntry)));
      allClaimEntries.stream().filter(claimEntry -> claimEntry.getValue().has(REASON) && trimToNull(claimEntry.getValue().get
              (REASON).asText()) != null)
              .forEach(claimEntry -> reasonsOfClaims.put(new Claim(claimEntry.getKey()), claimEntry.getValue().get(REASON)
                      .asText()));
    } catch (final IOException e) {
      LOG.warn("Parsing as JSON failed: '{}'", plainValue, e);
    }
    return reasonsOfClaims;
  }

  /**
   * @return Never <code>null</code> but unmodifiable.
   */
  public Map<Claim, String> getReasonsOfClaims() {
    return reasonsOfClaims;
  }

}
