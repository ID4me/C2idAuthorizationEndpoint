/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.authendpoint.resource;

import de.denic.domainid.DomainId;
import de.denic.domainid.authendpoint.view.ConsentView;
import de.denic.domainid.connect2id.authzsession.Connect2IdAuthorizationSessionAPI;
import de.denic.domainid.connect2id.authzsession.ConsentedData;
import de.denic.domainid.connect2id.authzsession.entity.ConsentPromptResponse;
import de.denic.domainid.connect2id.authzsession.entity.ConsentPromptResponse.Claims;
import de.denic.domainid.connect2id.common.Claim;
import de.denic.domainid.connect2id.common.ClientId;
import de.denic.domainid.connect2id.subjectsessionstore.entity.SubjectSession;
import de.denic.domainid.dbaccess.UserAuthentication;
import de.denic.domainid.dbaccess.UserAuthenticationDAO;
import de.denic.domainid.subject.PseudonymSubject;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.concurrent.Immutable;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import java.util.List;
import java.util.Set;

@Immutable
abstract class ConsentPageSkippingDispatcherBase extends FinalResponseRedirectingDispatcherBase {

  private static final Logger LOG = LoggerFactory.getLogger(ConsentPageSkippingDispatcherBase.class);
  private static final boolean DO_REMEMBER_CONSENT = true;

  private final Connect2IdAuthorizationSessionAPI c2IdAuthorisationSessionAPI;
  private final SubjectSessionCookies subjectSessionCookies;
  private final UserAuthenticationDAO userAuthenticationDAO;

  /**
   * @param c2IdAuthorisationSessionAPI Required
   * @param userAuthenticationDAO       Required
   * @param subjectSessionCookies       Required
   */
  protected ConsentPageSkippingDispatcherBase(final Connect2IdAuthorizationSessionAPI c2IdAuthorisationSessionAPI,
                                              final UserAuthenticationDAO userAuthenticationDAO,
                                              final SubjectSessionCookies subjectSessionCookies) {
    super(subjectSessionCookies);
    Validate.notNull(c2IdAuthorisationSessionAPI, "Missing C2ID Auth. Session API component");
    Validate.notNull(subjectSessionCookies, "Missing subject session cookies");
    Validate.notNull(userAuthenticationDAO, "Missing user authentication DAO");
    this.userAuthenticationDAO = userAuthenticationDAO;
    this.subjectSessionCookies = subjectSessionCookies;
    this.c2IdAuthorisationSessionAPI = c2IdAuthorisationSessionAPI;
  }

  /**
   * @return Never <code>null</code>
   */
  public SubjectSessionCookies getSubjectSessionCookies() {
    return subjectSessionCookies;
  }

  private UserAuthentication filterForCurrentUserAuthentication(final ConsentPromptResponse response) {
    final SubjectSession subjectSession = response.getSubjectSession();
    final PseudonymSubject subject = subjectSession.getSubject();
    final List<UserAuthentication> userAuthenticationsOfSubject = userAuthenticationDAO.findUserAuthenticationsBy(subject);
    if (userAuthenticationsOfSubject.size() == 1) {
      return userAuthenticationsOfSubject.get(0);
    }

    final DomainId domainId = subjectSession.getOptionalData().getDomainId().orElseThrow(() -> new
            RuntimeException("Internal error: Consent prompt response's additional data section does NOT contain " +
            "required DomainID field: " + response));
    return userAuthenticationsOfSubject.stream().filter(userAuth -> userAuth.getUserName().equals(domainId))
            .findAny().orElseThrow(() -> new RuntimeException("Found no user authentication matching DomainID " +
                    "'" + subject + "' from active OpenID Connect Authz."));
  }

  @Override
  public final ResponseBuilder handle(final ConsentPromptResponse response) {
    final UserAuthentication userAuthentication = filterForCurrentUserAuthentication(response);
    if (!userAuthentication.isActive()) {
      return handle(c2IdAuthorisationSessionAPI.cancelSession(response.getConnect2IdAuthorisationSessionId()));
    }

    final Claims claims = response.getClaims();
    final Pair<Boolean, Set<Claim>> allClaimsToConsentNewlyAreRejectedOnes = allClaimsToConsentNewlyAreRejectedOnes(
            claims, response.getSubjectSession().getSubject(), response.getClient().getId());
    final ResponseBuilder result;
    if (allClaimsToConsentNewlyAreRejectedOnes.getLeft()) {
      LOG.info("Skipping consenting step (cause all claims to consent newly are rejected ones)");
      final ConsentedData consentedData = new ConsentedData(response.getScopes().getAll(),
              response.getClaims().getConsented().getAll(), allClaimsToConsentNewlyAreRejectedOnes.getRight());
      result = c2IdAuthorisationSessionAPI
              .provide(response.getConnect2IdAuthorisationSessionId(), userAuthentication.getUserName(), consentedData,
                      DO_REMEMBER_CONSENT).dispatchedBy(this);
    } else {
      result = Response.ok(new ConsentView(response, userAuthentication.getUserName()));
    }
    return result.cookie(subjectSessionCookies.cookiesOfNewActiveIdentity(response.getSubjectSession().getId(),
            response.getSubjectSession().getMinOfLifetimes()));
  }

  private Pair<Boolean, Set<Claim>> allClaimsToConsentNewlyAreRejectedOnes(final Claims claims, final PseudonymSubject subject,
                                                                           final ClientId clientId) {
    final List<Claim> allClaimsToConsentNewly = claims.getNew().getAll();
    final Set<Claim> rejectedClaims = loadRejectedClaimsOf(subject, clientId);
    LOG.info("Claims: {} - Rejected claims: {}", claims, rejectedClaims);
    return new ImmutablePair<>(rejectedClaims.containsAll(allClaimsToConsentNewly), rejectedClaims);
  }

  /**
   * @param subject  Required
   * @param clientId Required
   * @return Never <code>null</code>
   */
  protected abstract Set<Claim> loadRejectedClaimsOf(PseudonymSubject subject, ClientId clientId);

}
