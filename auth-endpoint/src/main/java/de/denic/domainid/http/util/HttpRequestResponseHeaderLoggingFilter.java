/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.http.util;

import static org.apache.commons.lang3.StringUtils.join;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.client.ClientResponseFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class HttpRequestResponseHeaderLoggingFilter implements ClientResponseFilter {

  private static final Logger LOG = LoggerFactory.getLogger(HttpRequestResponseHeaderLoggingFilter.class);

  @Override
  public void filter(final ClientRequestContext requestContext, final ClientResponseContext responseContext)
      throws IOException {
    if (LOG.isDebugEnabled()) {
      final List<String> logLines = new LinkedList<>();
      logLines.add("Request/response headers");
      logLines.add("> " + requestContext.getMethod() + " " + requestContext.getUri());
      requestContext.getStringHeaders().entrySet().stream()
          .forEach(entry -> logLines.add("> " + entry.getKey() + ": " + join(entry.getValue(), ",")));
      logLines.add("< " + responseContext.getStatusInfo());
      responseContext.getHeaders().entrySet().stream()
          .forEach(entry -> logLines.add("< " + entry.getKey() + ": " + join(entry.getValue(), ",")));
      LOG.debug(join(logLines, '\n'));
    }
  }

}
