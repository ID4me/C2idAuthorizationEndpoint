/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.identityagent.impl;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.denic.domainid.DomainId;
import de.denic.domainid.connect2id.common.ClientId;
import de.denic.domainid.subject.OpenIdSubject;

import javax.annotation.concurrent.Immutable;
import java.net.URI;
import java.time.ZonedDateTime;
import java.util.Objects;

import static java.time.format.DateTimeFormatter.ISO_OFFSET_DATE_TIME;

@Immutable
public final class AuthenticationRequest {

  private final DomainId usersDomainId;
  private final ClientId clientId;
  private final boolean successfullyAuthenticated;
  private final ZonedDateTime timestampOfAuthentication;
  private final URI issuerURI;
  private final OpenIdSubject subject;

  public AuthenticationRequest(final DomainId usersDomainId, final OpenIdSubject subject, final ClientId clientId,
                               final boolean successfullyAuthenticated, final ZonedDateTime timestampOfAuthentication, final URI issuerURI) {
    this.usersDomainId = usersDomainId;
    this.subject = subject;
    this.clientId = clientId;
    this.successfullyAuthenticated = successfullyAuthenticated;
    this.timestampOfAuthentication = timestampOfAuthentication;
    this.issuerURI = issuerURI;
  }

  @JsonProperty("sub")
  public String getSubject() {
    return (subject == null ? null : subject.getName());
  }

  @JsonProperty("id4me.identifier")
  public String getId4meIdentifier() {
    return usersDomainId.getName();
  }

  // TODO: For backward compatibility
  @JsonProperty("identifier")
  public String getIdentifier() {
    return getId4meIdentifier();
  }

  // TODO: For backward compatibility
  @JsonProperty("id4me")
  public String getDomainId() {
    return getId4meIdentifier();
  }

  @JsonProperty("iss")
  public String getIssuerURI() {
    return (issuerURI == null ? null : issuerURI.toASCIIString());
  }

  @JsonProperty("client_id")
  public String getClientId() {
    return clientId.getPlain();
  }

  @JsonProperty("success")
  public boolean isSuccessfullyAuthenticated() {
    return successfullyAuthenticated;
  }

  @JsonProperty("auth_time")
  public long getEpochTimeOfAuthentication() {
    return timestampOfAuthentication.toEpochSecond();
  }

  @JsonProperty("auth_time_rfc3339")
  public String getTimestampOfAuthentication() {
    return timestampOfAuthentication.format(ISO_OFFSET_DATE_TIME);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    AuthenticationRequest that = (AuthenticationRequest) o;
    return successfullyAuthenticated == that.successfullyAuthenticated &&
            Objects.equals(usersDomainId, that.usersDomainId) &&
            Objects.equals(clientId, that.clientId) &&
            Objects.equals(timestampOfAuthentication, that.timestampOfAuthentication) &&
            Objects.equals(issuerURI, that.issuerURI) &&
            Objects.equals(subject, that.subject);
  }

  @Override
  public int hashCode() {
    return Objects.hash(usersDomainId, clientId, successfullyAuthenticated, timestampOfAuthentication, issuerURI, subject);
  }

  @Override
  public String toString() {
    return "usersDomainId=" + usersDomainId + ", clientURI=" + clientId + ", success=" + successfullyAuthenticated
            + ", timestamp=" + timestampOfAuthentication;
  }

}
