/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.identityagent.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import de.denic.domainid.DomainId;
import de.denic.domainid.connect2id.common.ClientId;
import de.denic.domainid.identityagent.IdentityAgentClient;
import de.denic.domainid.identityagent.IdentityAgentClientException;
import de.denic.domainid.identityagent.RequestUUIDGenerator;
import de.denic.domainid.jwks.KeyID;
import de.denic.domainid.subject.OpenIdSubject;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.concurrent.Immutable;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.security.Key;
import java.time.Duration;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.UUID;

import static io.jsonwebtoken.SignatureAlgorithm.RS256;
import static javax.ws.rs.client.Entity.entity;
import static javax.ws.rs.core.Response.Status.Family.SUCCESSFUL;
import static org.apache.commons.io.IOUtils.readLines;
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;
import static org.apache.commons.lang3.StringUtils.join;

@Immutable
public final class IdentityAgentClientImpl implements IdentityAgentClient {

  private static final Logger LOG = LoggerFactory.getLogger(IdentityAgentClientImpl.class);
  private static final String KEY_ID_HEADER = "kid";
  private static final Charset UTF8 = Charset.forName("UTF8");
  private static final MediaType APPLICATION_JWT = MediaType.valueOf("application/jwt");
  private static final String CALLBACK_AUTHENTICATION_URI_PATH = "/callback/authentication";
  private static final ObjectWriter AUTHENTICATION_REQUEST_ENTITY_WRITER = new ObjectMapper()
          .writerFor(AuthenticationRequest.class);
  private static final RequestUUIDGenerator MY_INDIVIDUAL_REQUEST_UUID_GENERATOR = UUID::randomUUID;

  private final Client restClient;
  private final RequestUUIDGenerator individualRequestUUIDGenerator;
  private final Pair<KeyID, Key> signingKey;
  private final URI optIssuerURI;

  /**
   * @param restClient Required
   * @param signingKey Optional, if missing JWTs won't get signed
   * @param issuerURI  Optional
   * @see #IdentityAgentClientImpl(Client, Pair, URI, RequestUUIDGenerator)
   */
  public IdentityAgentClientImpl(final Client restClient, final Pair<KeyID, Key> signingKey, final URI issuerURI) {
    this(restClient, signingKey, issuerURI, null);
  }

  /**
   * @param restClient                     Required
   * @param signingKey                     Optional, if missing JWTs won't get signed
   * @param issuerURI                      Optional
   * @param individualRequestUUIDGenerator Optional
   */
  public IdentityAgentClientImpl(final Client restClient, final Pair<KeyID, Key> signingKey, final URI issuerURI,
                                 final RequestUUIDGenerator individualRequestUUIDGenerator) {
    Validate.notNull(restClient, "Missing REST client");
    this.restClient = restClient;
    this.signingKey = signingKey;
    this.optIssuerURI = issuerURI;
    this.individualRequestUUIDGenerator = defaultIfNull(individualRequestUUIDGenerator,
            MY_INDIVIDUAL_REQUEST_UUID_GENERATOR);
  }

  @Override
  public void authentication(final DomainId domainId, final OpenIdSubject subject, final URI identityAgentURI,
                             final ZonedDateTime timestampOfAuthentication, final boolean successfullyAuthenticated, final ClientId clientId)
          throws IdentityAgentClientException {
    Validate.notNull(domainId, "Missing domain ID");
    Validate.notNull(identityAgentURI, "Missing Identity Agent's URI");
    Validate.notNull(clientId, "Missing client URI");
    Validate.notNull(timestampOfAuthentication, "Missing timestamp of authentication");
    LOG.info("Executing authentication callback on Identity Agent of '{}'", domainId);
    final AuthenticationRequest requestEntity = new AuthenticationRequest(domainId, subject, clientId,
            successfullyAuthenticated, timestampOfAuthentication, optIssuerURI);
    final String plainJsonRequestBody;
    try {
      plainJsonRequestBody = AUTHENTICATION_REQUEST_ENTITY_WRITER.writeValueAsString(requestEntity);
    } catch (final IOException e) {
      throw new RuntimeException("Internal error marshalling " + requestEntity, e);
    }

    LOG.debug("Plain JSON payload sent to Identity Agent: {}", plainJsonRequestBody);
    final JwtBuilder jwtBuilder = Jwts.builder().setPayload(plainJsonRequestBody);
    if (signingKey != null) {
      jwtBuilder.signWith(RS256, signingKey.getValue());
      jwtBuilder.setHeaderParam(KEY_ID_HEADER, signingKey.getKey().getValue());
    }
    final String jwsRequestBody = jwtBuilder.compact();
    final URI usersIdentityAgentURI;
    try {
      usersIdentityAgentURI = new URI("https", identityAgentURI.getUserInfo(), identityAgentURI.getHost(),
              identityAgentURI.getPort(), identityAgentURI.getPath(), identityAgentURI.getQuery(),
              identityAgentURI.getFragment());
    } catch (final URISyntaxException e) {
      throw new RuntimeException("Internal error", e);
    }

    final WebTarget webTarget = restClient.target(usersIdentityAgentURI).path(CALLBACK_AUTHENTICATION_URI_PATH)
            .path(individualRequestUUIDGenerator.newUUID().toString());
    LOG.debug("==> {}: >{}< ({})", "PUT", jwsRequestBody, webTarget.getUri());
    final Instant startingAt = Instant.now();
    Response postResponse = null;
    try {
      postResponse = webTarget.request().put(entity(jwsRequestBody, APPLICATION_JWT));
      final Duration invocationDuration = Duration.between(startingAt, Instant.now());
      if (SUCCESSFUL.equals(postResponse.getStatusInfo().getFamily())) {
        LOG.debug("<== {} {} ({}, elapsed {})", postResponse.getStatus(), postResponse.getStatusInfo(), webTarget.getUri(),
                invocationDuration);
      } else {
        LOG.info("<== {} {} ({}, elapsed {})", postResponse.getStatus(), postResponse.getStatusInfo(), webTarget.getUri(),
                invocationDuration);
        try {
          LOG.info("<== BODY: {} (request body was '{}')", join(readLines((InputStream) postResponse.getEntity(),
                  UTF8), "<LB>"), jwsRequestBody);
        } catch (final IOException e) {
          LOG.info("Evaluating response body failed", e);
        }
      }
    } catch (final ProcessingException e) {
      throw new ConnectingIdentityAgentFailedException("Identity Agent " + usersIdentityAgentURI,
              (e.getCause() == null ? e : e.getCause()));
    } finally {
      if (postResponse != null) {
        postResponse.close();
      }
    }
  }

}
