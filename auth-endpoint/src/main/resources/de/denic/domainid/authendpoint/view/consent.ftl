<#ftl output_format="HTML" encoding="UTF-8">
<#-- @ftlvariable name="" type="de.denic.domainid.authendpoint.view.ConsentView" -->
<html>
	<#include "include/head.ftl">
	<body>
		<div class="w3-margin">
			<div class="w3-display-container" style="height:350px;">
				<div class="w3-display-topmiddle w3-card-4" style="width:350px">
					<#include "include/identityAuthorityHeader.ftl">
					<div class="w3-padding">
						<form action="/logout" method="post" accept-charset="utf-8" autocomplete="off" name="logoutForm">
							<input type="hidden" name="sessionID" value="${consent.connect2IdAuthorisationSessionId}"/>
							<p>Your current online identity: <b class="w3-text-inetid-purple">${domainId}</b> <button class="w3-button w3-border w3-round w3-small w3-inetid-grey" type="submit">Not me!</button></p>
						</form>
						<hr/>
						<p>Login at <b class="w3-text-inetid-purple">${consent.client.name}</b> 
<#if consent.client.logoUri??>
   						<img style="width:36px;height:auto;" src="${consent.client.logoUri}">
</#if>
<#if consent.client.termsOfServiceUri??>
							<a href="${consent.client.termsOfServiceUri}">ToS</a>
</#if>
<#if consent.client.policyUri??>
   						<a href="${consent.client.policyUri}">Policy</a>
</#if>
   					</p>
						<hr/>
						<form action="/consent" name="consentForm" method="post" accept-charset="utf-8" autocomplete="off">
<#assign reasonsOfClaims = consent.sessionData.reasonsOfClaimsValues />
<#assign consentedClaims = consent.claims.consented.essential + consent.claims.consented.voluntary />
<#assign essentialClaims = consent.claims.new.essential + consent.claims.consented.essential />
<#assign sortedClaims = claimsSortedAppropriately />
							<input type="hidden" name="suggestedClaims" value="${sortedClaims?join(",")}"/>
							<div class="w3-card-3 w3-padding">
<#list sortedClaims>
								<p>Following Data is requested:</p>
								<table class="w3-table w3-striped">
	<#items as claim>
									<tr>
		<#if consentedClaims?seq_contains(claim)>
										<td><input type="checkbox" name="claim" value="${claim}" checked></td>
		<#elseif essentialClaims?seq_contains(claim)>
										<td><input type="checkbox" name="claim" value="${claim}" checked></td>
		<#else>
										<td><input type="checkbox" name="claim" value="${claim}"></td>
		</#if>
										<td title="${getDescriptionOfClaim(claim)}">
		<#if essentialClaims?seq_contains(claim)>
											<b>${getNameOfClaim(claim)}*</b>
		<#else>
											${getNameOfClaim(claim)}
		</#if>
		<#if reasonsOfClaims?keys?seq_contains(claim)>
											<i class="w3-text-grey">(Reason: ${reasonsOfClaims[claim]})</i>
		</#if>
										</td>
									</tr>
	</#items>
								</table>
<#else>
								<p>No data has been requested.</p>
</#list>
								<p><input type="checkbox" name="remember" value="true">&nbsp;Remember for future logins with this client?</p>
							</div>
							<p>
								<button class="w3-button w3-border w3-round w3-inetid-gold" type="submit" name="consenting" value="true">OK</button>
								<button class="w3-button w3-border w3-round w3-inetid-grey" type="submit" name="consenting" value="false">Cancel</button>
							</p>
							<input type="hidden" name="sessionID" value="${consent.connect2IdAuthorisationSessionId}"/>
						</form>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
