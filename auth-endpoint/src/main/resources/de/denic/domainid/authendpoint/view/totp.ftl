<#ftl output_format="HTML" encoding="UTF-8">
<#-- @ftlvariable name="" type="de.denic.domainid.authendpoint.view.TOTPView" -->
<html>
	<#include "include/head.ftl">
	<body>
		<div class="w3-container">
			<div class="w3-display-container w3-margin-top">
				<div class="w3-display-topmiddle w3-card-4" style="width:350px">
					<#include "include/identityAuthorityHeader.ftl">
					<div class="w3-padding">
						<form action="/authenticate/totp" method="post" accept-charset="utf-8">
							<input type="hidden" name="sessionID" value="${sessionId}"/>
							<input type="hidden" name="domainID" value="${domainId}"/>
							<input type="hidden" name="passwordOK" value="${hexSHA256OfHexSHA256PasswordHash}"/>
							<p>
								<input class="w3-input" type="text" id="verificationCode" name="verificationCode" maxlength="8" placeholder="123456" autofocus="on" autocomplete="off"/>
								<label class="w3-tiny"> Open your authentication app and enter the numeric code here </label>
								<div class="w3-container w3-center">
									<button class="w3-button w3-inetid-grey" name="submit"> Authenticate </button>
								</div>
							</p>
						</form>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
