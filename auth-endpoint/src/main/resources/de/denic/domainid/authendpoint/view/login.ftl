<#ftl output_format="HTML" encoding="UTF-8">
<#-- @ftlvariable name="" type="de.denic.domainid.authendpoint.view.LoginView" -->
<html>
	<#include "include/head.ftl">
	<body>
		<div class="w3-container">
			<div class="w3-display-container w3-margin-top">
				<div class="w3-display-topmiddle w3-card-4" style="width:350px">
					<#include "include/identityAuthorityHeader.ftl">
					<div class="w3-padding">
						Login at <b class="w3-text-inetid-purple">${client.name!"ID " + client.id}</b>
<#if client.logoURI??>
						<img style="width:36px;height:auto;" src="${client.logoURI}">
</#if>
<#if client.termsOfServiceURI??>
						<a href="${client.termsOfServiceURI}">ToS</a>
</#if>
<#if client.policyURI??>
						<a href="${client.policyURI}">Policy</a>
</#if>
					</div>
<#list login.knownSessionsOfSubject as knownSession>
					<div class="w3-padding">
						<!--form action="switch-account" method="post" accept-charset="utf-8"-->
							<input type="hidden" name="subjectSessionID" value="${knownSession.id}">
							<input type="hidden" name="sessionID" value="${login.sessionId}"/>
							<input type="hidden" name="domainID" value="${knownSession.subject}"/>
							<p>
								<input class="w3-input" id="domainID" name="domainID" maxlength="40" value="${knownSession.subject}" disabled="true" type="text">
								<label class="w3-tiny"> ${idName} </label>
							</p>
							<p>
								<div class="w3-container w3-center">
									<button class="w3-button w3-petrol" name="submit" disabled="true"> Apply </button>
								</div>
							</p>
						<!--/form-->
					</div>
</#list>
					<div class="w3-padding">
						<form action="authenticate" method="post" accept-charset="utf-8">
							<input type="hidden" name="sessionID" value="${login.sessionId}"/>
							<p>
<#if login.domainId??>
								<input class="w3-input" type="text" id="domainID" name="domainID" maxlength="40" value="${login.domainId.name}" disabled="true">
								<input type="hidden" name="domainID" value="${login.domainId.name}"/>
<#else>
								<input class="w3-input" type="text" id="domainID" name="domainID" maxlength="40" placeholder="my.${idName}.example" autofocus="on">
</#if>
								<label class="w3-tiny"> ${idName} </label>
							</p>
							<p>
<#if login.domainId??>
								<input class="w3-input" type="password" id="password" name="password" maxlength="32" placeholder="secret" autofocus="on"/>
<#else>
								<input class="w3-input" type="password" id="password" name="password" maxlength="32" placeholder="secret"/>
</#if>
								<label class="w3-tiny"> Password </label>
								<div class="w3-container w3-center">
									<button class="w3-button w3-inetid-grey" name="submit"> Login </button>
								</div>
							</p>
						</form>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
