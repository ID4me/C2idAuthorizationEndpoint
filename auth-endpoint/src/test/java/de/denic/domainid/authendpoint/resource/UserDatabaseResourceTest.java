/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.authendpoint.resource;

import de.denic.domainid.DomainId;
import de.denic.domainid.connect2id.authzstore.Connect2IdAuthorizationStoreAPI;
import de.denic.domainid.dbaccess.UserAuthentication;
import de.denic.domainid.dbaccess.UserAuthenticationDAO;
import de.denic.domainid.subject.PseudonymSubject;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.junit.After;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.core.Response;
import java.net.URI;

import static de.denic.domainid.dbaccess.UserAuthentication.ACTIVE;
import static javax.ws.rs.client.Entity.entity;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
import static javax.ws.rs.core.Response.Status.CREATED;
import static javax.ws.rs.core.Response.Status.OK;
import static javax.ws.rs.core.UriBuilder.fromUri;
import static org.apache.commons.codec.digest.DigestUtils.sha256Hex;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.easymock.EasyMock.*;
import static org.junit.Assert.assertEquals;

public class UserDatabaseResourceTest {

  private static final String SOME_PASSWORD_VALUE = "somePassword";
  private static final String SOME_DOMAIN_ID_VALUE = "some.domain-id";
  private static final DomainId SOME_DOMAIN_ID = new DomainId(SOME_DOMAIN_ID_VALUE);
  private static final UserAuthentication SOME_USER_AUTHENTICATION = new UserAuthentication(SOME_DOMAIN_ID, sha256Hex(SOME_PASSWORD_VALUE), false, false,
      new PseudonymSubject("pseudonym." + SOME_DOMAIN_ID_VALUE), ACTIVE);
  private static final UserAuthenticationDAO USER_AUTHENTICATION_DAO_MOCK = createMock(UserAuthenticationDAO.class);
  private static final Connect2IdAuthorizationStoreAPI AUTH_STORE_API_MOCK = createMock(Connect2IdAuthorizationStoreAPI.class);
  @ClassRule
  public static final ResourceTestRule RESOURCES_UT = ResourceTestRule.builder()
      //.addProvider(new ViewMessageBodyWriter(new MetricRegistry(), singleton(new FreemarkerViewRenderer())))
      .addResource(new UserDatabaseResource(USER_AUTHENTICATION_DAO_MOCK, AUTH_STORE_API_MOCK)).build();

  @After
  public void tearDown() {
    resetMocks();
  }

  @Test
  public void putNewUser() {
    final URI targetURI = fromUri("/user/" + SOME_DOMAIN_ID_VALUE + "?password=" + SOME_PASSWORD_VALUE).build();
    expect(USER_AUTHENTICATION_DAO_MOCK.updateUserAuthentication(SOME_DOMAIN_ID, sha256Hex(SOME_PASSWORD_VALUE))).andReturn(0);
    expect(USER_AUTHENTICATION_DAO_MOCK.insertEndUserIdentity(SOME_DOMAIN_ID, sha256Hex(SOME_PASSWORD_VALUE))).andReturn(1);
    expect(USER_AUTHENTICATION_DAO_MOCK.findUserAuthenticationBy(SOME_DOMAIN_ID)).andReturn(SOME_USER_AUTHENTICATION);
    replayMocks();
    Response response = null;
    try {
      response = RESOURCES_UT.client().target(targetURI).request().put(entity(EMPTY, APPLICATION_JSON_TYPE));
      verifyMocks();
      assertEquals("Response status code", CREATED.getStatusCode(), response.getStatusInfo().getStatusCode());
      assertEquals("Response entity",
          "{\"userName\":\"some.domain-id\",\"passwordHexSHA256\":\"baae90bd064867ab28f034d6ed40ef14684012e4c0567181eaee3494a2358695\",\"pseudonymUserName\":\"pseudonym.some.domain-id\",\"twoFactorAuthEnabled\":false,\"agentPwResetEnabled\":false,\"active\":true}",
          response.readEntity(String.class));
    } finally {
      if (response != null)
        response.close();
    }
  }

  @Test
  public void putKnownUser() {
    final URI targetURI = fromUri("/user/" + SOME_DOMAIN_ID_VALUE + "?password=" + SOME_PASSWORD_VALUE).build();
    expect(USER_AUTHENTICATION_DAO_MOCK.updateUserAuthentication(SOME_DOMAIN_ID, sha256Hex(SOME_PASSWORD_VALUE))).andReturn(1);
    expect(USER_AUTHENTICATION_DAO_MOCK.findUserAuthenticationBy(SOME_DOMAIN_ID)).andReturn(SOME_USER_AUTHENTICATION);
    replayMocks();
    Response response = null;
    try {
      response = RESOURCES_UT.client().target(targetURI).request().put(entity(EMPTY, APPLICATION_JSON_TYPE));
      verifyMocks();
      assertEquals("Response status code", OK.getStatusCode(), response.getStatusInfo().getStatusCode());
      assertEquals("Response entity",
          "{\"userName\":\"some.domain-id\",\"passwordHexSHA256\":\"baae90bd064867ab28f034d6ed40ef14684012e4c0567181eaee3494a2358695\",\"pseudonymUserName\":\"pseudonym.some.domain-id\",\"twoFactorAuthEnabled\":false,\"agentPwResetEnabled\":false,\"active\":true}",
          response.readEntity(String.class));
    } finally {
      if (response != null)
        response.close();
    }
  }

  private void verifyMocks() {
    verify(USER_AUTHENTICATION_DAO_MOCK);
  }

  private void replayMocks() {
    replay(USER_AUTHENTICATION_DAO_MOCK);
  }

  private void resetMocks() {
    reset(USER_AUTHENTICATION_DAO_MOCK);
  }

}
