/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.authendpoint.resource;

import com.codahale.metrics.MetricRegistry;
import com.warrenstrange.googleauth.IGoogleAuthenticator;
import de.denic.domainid.DomainId;
import de.denic.domainid.connect2id.authzsession.Connect2IdAuthorizationSessionAPI;
import de.denic.domainid.connect2id.authzsession.Connect2IdAuthorizationSessionAPI.AnyResponseImpl;
import de.denic.domainid.connect2id.authzsession.ConsentedData;
import de.denic.domainid.connect2id.authzsession.ResponseType;
import de.denic.domainid.connect2id.authzsession.SessionId;
import de.denic.domainid.connect2id.authzsession.entity.AuthzSessionOptionalData;
import de.denic.domainid.connect2id.authzsession.entity.FinalResponse;
import de.denic.domainid.connect2id.authzsession.entity.FinalResponse.Parameters;
import de.denic.domainid.connect2id.authzsession.entity.SessionResponse;
import de.denic.domainid.connect2id.authzsession.entity.SessionResponse.OpenIdConnectAuthenticationRequest;
import de.denic.domainid.connect2id.authzstore.Connect2IdAuthorizationStoreAPI;
import de.denic.domainid.connect2id.authzstore.entity.OpenIdConnectAuthorization;
import de.denic.domainid.connect2id.common.*;
import de.denic.domainid.connect2id.common.NotFoundException.NotFoundEntity;
import de.denic.domainid.connect2id.subjectsessionstore.entity.SubjectSessionOptionalData;
import de.denic.domainid.dbaccess.UserAuthentication;
import de.denic.domainid.dbaccess.UserAuthenticationDAO;
import de.denic.domainid.dns.DomainIdDnsClient;
import de.denic.domainid.dns.LookupResult;
import de.denic.domainid.identityagent.IdentityAgentClient;
import de.denic.domainid.subject.OpenIdSubject;
import de.denic.domainid.subject.PseudonymSubject;
import io.dropwizard.testing.junit.ResourceTestRule;
import io.dropwizard.views.ViewMessageBodyWriter;
import io.dropwizard.views.freemarker.FreemarkerViewRenderer;
import org.junit.After;
import org.junit.ClassRule;
import org.junit.Test;

import javax.security.auth.Subject;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.Charset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static de.denic.domainid.connect2id.common.AuthenticationMethodReference.Value.otp;
import static de.denic.domainid.connect2id.common.AuthenticationMethodReference.Value.pwd;
import static de.denic.domainid.connect2id.common.AuthenticationMethodReference.ofMany;
import static de.denic.domainid.dbaccess.UserAuthentication.ACTIVE;
import static de.denic.domainid.dns.LookupResult.empty;
import static java.util.Collections.*;
import static javax.ws.rs.client.Entity.form;
import static javax.ws.rs.core.MediaType.TEXT_HTML_TYPE;
import static javax.ws.rs.core.Response.Status.*;
import static javax.ws.rs.core.UriBuilder.fromUri;
import static org.apache.commons.codec.digest.DigestUtils.sha256Hex;
import static org.apache.commons.io.IOUtils.readLines;
import static org.easymock.EasyMock.*;
import static org.glassfish.jersey.client.ClientProperties.FOLLOW_REDIRECTS;
import static org.junit.Assert.*;

/**
 * General hint regarding expected HTTP response codes: Redirection response code "303 SEE OTHER" required for security
 * reasons! See <a href="https://infsec.uni-trier.de/people/publications/paper/FettKuestersSchmitz-CCS-2016.pdf">A
 * Comprehensive Formal Security Analysis of OAuth 2.0</a>, chapter 3.1.
 */
public class AuthenticationResourceTest {

        private static final String NAME_OF_SESSION_COOKIE = "sub_sid";
        private static final Charset UTF8 = Charset.forName("utf8");
        private static final URI SOME_IDENTITY_AGENT_HOST_NAME = URI.create("identity.agent.hostname");
        private static final String SOME_SESSION_ID_VALUE = "someSessionID";
        private static final String SOME_PASSWORD_VALUE = "somePassword";
        private static final String SOME_DOMAIN_ID_VALUE = "some.domain-id";
        private static final PseudonymSubject SOME_PSEUDONYM_SUBJECT = new PseudonymSubject("pseudonym." + SOME_DOMAIN_ID_VALUE);
        private static final DomainId SOME_DOMAIN_ID = new DomainId(SOME_DOMAIN_ID_VALUE);
        private static final UserAuthentication SOME_USER_AUTHENTICATION = new UserAuthentication(SOME_DOMAIN_ID, sha256Hex(SOME_PASSWORD_VALUE), false, false,
                        SOME_PSEUDONYM_SUBJECT, ACTIVE);
        private static final SessionId SOME_SESSION_ID = new SessionId(SOME_SESSION_ID_VALUE);
        private static final Connect2IdAuthorizationSessionAPI AUTH_SESSION_API_MOCK = createMock(Connect2IdAuthorizationSessionAPI.class);
        private static final UserAuthenticationDAO USER_AUTHENTICATION_DAO_MOCK = createMock(UserAuthenticationDAO.class);
        private static final DomainIdDnsClient DOMAIN_ID_DNS_CLIENT_MOCK = createMock(DomainIdDnsClient.class);
        private static final IdentityAgentClient IDENTITY_AGENT_CLIENT_MOCK = createMock(IdentityAgentClient.class);
        private static final Connect2IdAuthorizationStoreAPI AUTH_STORE_API_MOCK = createMock(Connect2IdAuthorizationStoreAPI.class);
        private static final IGoogleAuthenticator GOOGLE_AUTHENTICATOR_MOCK = createMock(IGoogleAuthenticator.class);
        @ClassRule
        public static final ResourceTestRule RESOURCES_UT = ResourceTestRule.builder()
                        .addProvider(new ViewMessageBodyWriter(new MetricRegistry(), singleton(new FreemarkerViewRenderer())))
                        .addResource(new AuthenticationResource(AUTH_SESSION_API_MOCK, USER_AUTHENTICATION_DAO_MOCK, DOMAIN_ID_DNS_CLIENT_MOCK,
                                        IDENTITY_AGENT_CLIENT_MOCK, AUTH_STORE_API_MOCK, GOOGLE_AUTHENTICATOR_MOCK))
                        .build();
        private static final SubjectSessionId SOME_SUBJECT_SESSION_ID = new SubjectSessionId("subject-session-id");
        private static final ClientId SOME_CLIENT_ID = new ClientId("client-id");
        private static final SubjectSessionId NO_SUBJECT_SESSION_ID = null;
        private static final ResponseType SOME_RESPONSE_TYPE = new ResponseType("RESPONSE_TYPE");
        private static final RedirectURI SOME_REDIRECT_URI = new RedirectURI("SOME_REDIRECT_URI");
        private static final boolean TWO_FACTOR_AUTH_ENABLED = true;
        private static final boolean AGENT_PW_RESET_ENABLED = true;
        private static final boolean DO_REMEMBER_CONSENT = true;
        private static final AuthzSessionOptionalData NO_OPTIONAL_DATA = null;
        private static final MediaType TEXT_HTML_CHARSET_UTF8_TYPE = new MediaType("text", "html", "UTF-8");

        @After
        public void tearDown() {
                resetMocks();
        }

        @Test
        public void issueMismatchingCredentials() {
                final URI targetURI = fromUri("/authenticate").build();
                final Form form = new Form().param("domainID", SOME_DOMAIN_ID_VALUE).param("password", SOME_PASSWORD_VALUE).param("sessionID", SOME_SESSION_ID_VALUE);
                expect(USER_AUTHENTICATION_DAO_MOCK.findUserAuthenticationBy(SOME_DOMAIN_ID))
                                .andReturn(new UserAuthentication(SOME_DOMAIN_ID, sha256Hex("mismatching"), false, false, SOME_PSEUDONYM_SUBJECT, ACTIVE));
                expect(DOMAIN_ID_DNS_CLIENT_MOCK.lookupIdentityAgentHostNameOf(SOME_DOMAIN_ID)).andReturn(new LookupResult.Impl<>(true, SOME_IDENTITY_AGENT_HOST_NAME));
                expect(AUTH_SESSION_API_MOCK.getSession(SOME_SESSION_ID))
                                .andReturn(new SessionResponse(new OpenIdConnectAuthenticationRequest(SOME_RESPONSE_TYPE, SOME_CLIENT_ID, SOME_REDIRECT_URI),
                                                SOME_SUBJECT_SESSION_ID, NO_OPTIONAL_DATA));
                IDENTITY_AGENT_CLIENT_MOCK.authentication(eq(SOME_DOMAIN_ID), isNull(OpenIdSubject.class), eq(SOME_IDENTITY_AGENT_HOST_NAME),
                                anyObject(ZonedDateTime.class), eq(false), eq(SOME_CLIENT_ID));
                final URI errorRedirectingUri = URI.create("http://error-redirecting-uri.test");
                expect(AUTH_SESSION_API_MOCK.cancelSession(SOME_SESSION_ID))
                                .andReturn(new FinalResponse("response", "mode", NO_SUBJECT_SESSION_ID, new Parameters(errorRedirectingUri)));
                replayMocks();
                Response response = null;
                try {
                        response = RESOURCES_UT.client().target(targetURI).property(FOLLOW_REDIRECTS, false).request().post(form(form));
                        verifyMocks();
                        assertEquals("Response status code", SEE_OTHER.getStatusCode(), response.getStatusInfo().getStatusCode());
                        assertEquals("Redirection target URI", errorRedirectingUri, response.getLocation());
                        final NewCookie httpSessionCookie = response.getCookies().get(NAME_OF_SESSION_COOKIE);
                        assertNull("No HTTP session cookie", httpSessionCookie);
                } finally {
                        if (response != null)
                                response.close();
                }
        }

        @Test
        public void issueCorrectAuthenticationCredentials() throws IOException {
                final URI targetURI = fromUri("/authenticate").build();
                final Form form = new Form().param("domainID", SOME_DOMAIN_ID_VALUE).param("password", SOME_PASSWORD_VALUE).param("sessionID", SOME_SESSION_ID_VALUE);
                expect(AUTH_SESSION_API_MOCK.authenticate(eq(SOME_SESSION_ID), anyObject(Subject.class), eq(ofMany(pwd))))
                                .andReturn(new AnyResponseImpl("{ \"type\":\"consent\", \"sid\":\"" + SOME_SESSION_ID + "\", \"sub_session\": { \"sid\":\""
                                                + SOME_SUBJECT_SESSION_ID + "\", \"sub\":\"" + SOME_PSEUDONYM_SUBJECT + "\" }, \"client\": { \"client_id\":\""
                                                + SOME_CLIENT_ID + "\" }, \"claims\": { \"new\": { \"essential\":[\"claimNewlyToAccept\"] } } } }"));
                expect(USER_AUTHENTICATION_DAO_MOCK.findUserAuthenticationBy(SOME_DOMAIN_ID)).andReturn(SOME_USER_AUTHENTICATION);
                expect(DOMAIN_ID_DNS_CLIENT_MOCK.lookupIdentityAgentHostNameOf(SOME_DOMAIN_ID)).andReturn(new LookupResult.Impl<>(true, SOME_IDENTITY_AGENT_HOST_NAME));
                expect(AUTH_SESSION_API_MOCK.getSession(SOME_SESSION_ID))
                                .andReturn(new SessionResponse(new OpenIdConnectAuthenticationRequest(SOME_RESPONSE_TYPE, SOME_CLIENT_ID, SOME_REDIRECT_URI),
                                                SOME_SUBJECT_SESSION_ID, NO_OPTIONAL_DATA));
                expect(AUTH_STORE_API_MOCK.loadAuthorizationOf(SOME_PSEUDONYM_SUBJECT, SOME_CLIENT_ID))
                                .andReturn(Optional.of(new OpenIdConnectAuthorization(SOME_DOMAIN_ID_VALUE, SOME_CLIENT_ID, null, null, null, true, false, null, null,
                                                null, null, false, null, null, null, null, null, null, null, null, null, null, null)));
                IDENTITY_AGENT_CLIENT_MOCK.authentication(eq(SOME_DOMAIN_ID), eq(SOME_PSEUDONYM_SUBJECT), eq(SOME_IDENTITY_AGENT_HOST_NAME),
                                anyObject(ZonedDateTime.class), eq(true), eq(SOME_CLIENT_ID));
                expect(USER_AUTHENTICATION_DAO_MOCK.findUserAuthenticationsBy(SOME_PSEUDONYM_SUBJECT)).andReturn(singletonList(SOME_USER_AUTHENTICATION));
                replayMocks();
                {
                        postFormVerifyingMocksAndCheckingForConsentResponse(targetURI, form, OK, SOME_SUBJECT_SESSION_ID);
                }
        }

        @Test
        public void lookupOfIdentityAgentHostYieldsNoResult() throws IOException {
                final URI targetURI = fromUri("/authenticate").build();
                final Form form = new Form().param("domainID", SOME_DOMAIN_ID_VALUE).param("password", SOME_PASSWORD_VALUE).param("sessionID", SOME_SESSION_ID_VALUE);
                expect(USER_AUTHENTICATION_DAO_MOCK.findUserAuthenticationBy(SOME_DOMAIN_ID)).andReturn(SOME_USER_AUTHENTICATION);
                expect(AUTH_SESSION_API_MOCK.getSession(SOME_SESSION_ID))
                                .andReturn(new SessionResponse(new OpenIdConnectAuthenticationRequest(SOME_RESPONSE_TYPE, SOME_CLIENT_ID, SOME_REDIRECT_URI),
                                                SOME_SUBJECT_SESSION_ID, NO_OPTIONAL_DATA));
                expect(DOMAIN_ID_DNS_CLIENT_MOCK.lookupIdentityAgentHostNameOf(SOME_DOMAIN_ID)).andReturn(empty(true));
                expect(AUTH_STORE_API_MOCK.loadAuthorizationOf(SOME_PSEUDONYM_SUBJECT, SOME_CLIENT_ID))
                                .andReturn(Optional.of(new OpenIdConnectAuthorization(SOME_DOMAIN_ID_VALUE, SOME_CLIENT_ID, null, null, null, true, false, null, null,
                                                null, null, false, null, null, null, null, null, null, null, null, null, null, null)));
                expect(AUTH_SESSION_API_MOCK.authenticate(eq(SOME_SESSION_ID), anyObject(Subject.class), eq(ofMany(pwd))))
                                .andReturn(new AnyResponseImpl("{ \"type\":\"consent\", \"sid\":\"" + SOME_SESSION_ID + "\", \"sub_session\": { \"sid\":\""
                                                + SOME_SUBJECT_SESSION_ID + "\", \"sub\":\"" + SOME_PSEUDONYM_SUBJECT + "\" }, \"client\": { \"client_id\":\""
                                                + SOME_CLIENT_ID + "\" }, \"claims\": { \"new\": { \"essential\":[\"claimNewlyToAccept\"] } } } }"));
                expect(USER_AUTHENTICATION_DAO_MOCK.findUserAuthenticationsBy(SOME_PSEUDONYM_SUBJECT)).andReturn(singletonList(SOME_USER_AUTHENTICATION));
                replayMocks();
                {
                        postFormVerifyingMocksAndCheckingForConsentResponse(targetURI, form, OK, SOME_SUBJECT_SESSION_ID);
                }
        }

        @Test
        public void connect2IdAuthorisationSessionHasTimedOut() throws IOException {
                final URI targetURI = fromUri("/authenticate").build();
                final Form form = new Form().param("domainID", SOME_DOMAIN_ID_VALUE).param("password", SOME_PASSWORD_VALUE).param("sessionID", SOME_SESSION_ID_VALUE);
                expect(AUTH_SESSION_API_MOCK.authenticate(eq(SOME_SESSION_ID), anyObject(Subject.class), eq(ofMany(pwd))))
                                .andThrow(new NotFoundException(new NotFoundEntity("error", "description")));
                expect(USER_AUTHENTICATION_DAO_MOCK.findUserAuthenticationBy(SOME_DOMAIN_ID)).andReturn(SOME_USER_AUTHENTICATION);
                expect(DOMAIN_ID_DNS_CLIENT_MOCK.lookupIdentityAgentHostNameOf(SOME_DOMAIN_ID)).andReturn(new LookupResult.Impl<>(true, SOME_IDENTITY_AGENT_HOST_NAME));
                expect(AUTH_SESSION_API_MOCK.getSession(SOME_SESSION_ID))
                                .andReturn(new SessionResponse(new OpenIdConnectAuthenticationRequest(SOME_RESPONSE_TYPE, SOME_CLIENT_ID, SOME_REDIRECT_URI),
                                                SOME_SUBJECT_SESSION_ID, NO_OPTIONAL_DATA));
                expect(AUTH_STORE_API_MOCK.loadAuthorizationOf(SOME_PSEUDONYM_SUBJECT, SOME_CLIENT_ID))
                                .andReturn(Optional.of(new OpenIdConnectAuthorization(SOME_DOMAIN_ID_VALUE, SOME_CLIENT_ID, null, null, null, true, false, null, null,
                                                null, null, false, null, null, null, null, null, null, null, null, null, null, null)));
                IDENTITY_AGENT_CLIENT_MOCK.authentication(eq(SOME_DOMAIN_ID), eq(SOME_PSEUDONYM_SUBJECT), eq(SOME_IDENTITY_AGENT_HOST_NAME),
                                anyObject(ZonedDateTime.class), eq(true), eq(SOME_CLIENT_ID));
                replayMocks();
                {
                        postFormVerifyingMocksAndCheckingForConsentResponse(targetURI, form, GONE, SOME_SUBJECT_SESSION_ID);
                }
        }

        @Test
        public void oneEarlierAcceptedAndEarlierRejectedClaim() throws IOException {
                final Claim claimRejectedEarlier = new Claim("rejected_earlier");
                final Claim claimConsentedEarlier = new Claim("accepted_earlier");
                final URI targetURI = fromUri("/authenticate").build();
                final Form form = new Form().param("domainID", SOME_DOMAIN_ID_VALUE).param("password", SOME_PASSWORD_VALUE).param("sessionID", SOME_SESSION_ID_VALUE);
                expect(AUTH_SESSION_API_MOCK.getSession(SOME_SESSION_ID))
                                .andReturn(new SessionResponse(new OpenIdConnectAuthenticationRequest(SOME_RESPONSE_TYPE, SOME_CLIENT_ID, SOME_REDIRECT_URI),
                                                SOME_SUBJECT_SESSION_ID, NO_OPTIONAL_DATA));
                expect(USER_AUTHENTICATION_DAO_MOCK.findUserAuthenticationBy(SOME_DOMAIN_ID)).andReturn(SOME_USER_AUTHENTICATION);
                IDENTITY_AGENT_CLIENT_MOCK.authentication(eq(SOME_DOMAIN_ID), eq(SOME_PSEUDONYM_SUBJECT), eq(SOME_IDENTITY_AGENT_HOST_NAME),
                                anyObject(ZonedDateTime.class), eq(true), eq(SOME_CLIENT_ID));
                final SubjectSessionOptionalData authAdditionalData = new SubjectSessionOptionalData(SOME_DOMAIN_ID, singletonList(claimRejectedEarlier));
                expect(AUTH_STORE_API_MOCK.loadAuthorizationOf(SOME_PSEUDONYM_SUBJECT, SOME_CLIENT_ID))
                                .andReturn(Optional.of(new OpenIdConnectAuthorization(SOME_DOMAIN_ID_VALUE, SOME_CLIENT_ID, null, null, null, true, false, null, null,
                                                null, null, false, null, null, null, null, null, null, null, null, null, null, authAdditionalData)));
                expect(AUTH_SESSION_API_MOCK.authenticate(eq(SOME_SESSION_ID), anyObject(Subject.class), eq(ofMany(pwd)))).andReturn(
                                new AnyResponseImpl("{ \"type\":\"consent\", \"sid\":\"" + SOME_SESSION_ID + "\", \"sub_session\": { \"sid\":\"" + SOME_SUBJECT_SESSION_ID
                                                + "\", \"sub\":\"" + SOME_PSEUDONYM_SUBJECT + "\" }, \"client\": { \"client_id\":\"" + SOME_CLIENT_ID
                                                + "\" }, \"claims\": { \"consented\": { \"essential\":[\"" + claimConsentedEarlier.getPlain() + "\"] } } } }"));
                expect(DOMAIN_ID_DNS_CLIENT_MOCK.lookupIdentityAgentHostNameOf(SOME_DOMAIN_ID)).andReturn(new LookupResult.Impl<>(true, SOME_IDENTITY_AGENT_HOST_NAME));
                final RedirectURI redirectURI = new RedirectURI("http://example.com/some-redirect-uri");
                expect(AUTH_SESSION_API_MOCK.provide(SOME_SESSION_ID, SOME_DOMAIN_ID,
                                new ConsentedData(emptyList(), singleton(claimConsentedEarlier), singleton(claimRejectedEarlier)), DO_REMEMBER_CONSENT))
                                                .andReturn(new AnyResponseImpl(
                                                                "{ \"type\":\"response\", \"mode\":\"query\", \"parameters\": { \"uri\":\"" + redirectURI + "\" } }"));
                expect(USER_AUTHENTICATION_DAO_MOCK.findUserAuthenticationsBy(SOME_PSEUDONYM_SUBJECT)).andReturn(singletonList(SOME_USER_AUTHENTICATION));
                System.out.println("========================= STARTING ========================= " + currentMethodStackTrace());
                replayMocks();
                {
                        postFormVerifyingMocksAndCheckingForConsentResponse(targetURI, form, SEE_OTHER, SOME_SUBJECT_SESSION_ID);
                }
                System.out.println("========================= FINISHED =========================");
        }

        @Test
        public void oneNewlyToAcceptAndEarlierRejectedClaim() throws IOException {
                final Claim claimRejectedEarlier = new Claim("rejected_earlier");
                final Claim claimNewlyToAccept = new Claim("newly_accepted");
                final URI targetURI = fromUri("/authenticate").build();
                final Form form = new Form().param("domainID", SOME_DOMAIN_ID_VALUE).param("password", SOME_PASSWORD_VALUE).param("sessionID", SOME_SESSION_ID_VALUE);
                expect(AUTH_SESSION_API_MOCK.getSession(SOME_SESSION_ID))
                                .andReturn(new SessionResponse(new OpenIdConnectAuthenticationRequest(SOME_RESPONSE_TYPE, SOME_CLIENT_ID, SOME_REDIRECT_URI),
                                                SOME_SUBJECT_SESSION_ID, NO_OPTIONAL_DATA));
                expect(USER_AUTHENTICATION_DAO_MOCK.findUserAuthenticationBy(SOME_DOMAIN_ID)).andReturn(SOME_USER_AUTHENTICATION);
                IDENTITY_AGENT_CLIENT_MOCK.authentication(eq(SOME_DOMAIN_ID), eq(SOME_PSEUDONYM_SUBJECT), eq(SOME_IDENTITY_AGENT_HOST_NAME),
                                anyObject(ZonedDateTime.class), eq(true), eq(SOME_CLIENT_ID));
                final SubjectSessionOptionalData authAdditionalData = new SubjectSessionOptionalData(SOME_DOMAIN_ID, singletonList(claimRejectedEarlier));
                expect(AUTH_STORE_API_MOCK.loadAuthorizationOf(SOME_PSEUDONYM_SUBJECT, SOME_CLIENT_ID))
                                .andReturn(Optional.of(new OpenIdConnectAuthorization(SOME_DOMAIN_ID_VALUE, SOME_CLIENT_ID, null, null, null, true, false, null, null,
                                                null, null, false, null, null, null, null, null, null, null, null, null, null, authAdditionalData)));
                expect(AUTH_SESSION_API_MOCK.authenticate(eq(SOME_SESSION_ID), anyObject(Subject.class), eq(ofMany(pwd))))
                                .andReturn(new AnyResponseImpl("{ \"type\":\"consent\", \"sid\":\"" + SOME_SESSION_ID + "\", \"sub_session\": { \"sid\":\""
                                                + SOME_SUBJECT_SESSION_ID + "\", \"sub\":\"" + SOME_PSEUDONYM_SUBJECT + "\" }, \"client\": { \"client_id\":\""
                                                + SOME_CLIENT_ID + "\" }, \"claims\": { \"new\": { \"essential\":[\"" + claimNewlyToAccept.getPlain() + "\"] } } } }"));
                expect(DOMAIN_ID_DNS_CLIENT_MOCK.lookupIdentityAgentHostNameOf(SOME_DOMAIN_ID)).andReturn(new LookupResult.Impl<>(true, SOME_IDENTITY_AGENT_HOST_NAME));
                expect(USER_AUTHENTICATION_DAO_MOCK.findUserAuthenticationsBy(SOME_PSEUDONYM_SUBJECT)).andReturn(singletonList(SOME_USER_AUTHENTICATION));
                replayMocks();
                {
                        postFormVerifyingMocksAndCheckingForConsentResponse(targetURI, form, OK, SOME_SUBJECT_SESSION_ID);
                }
        }

        @Test
        public void issueCorrectAuthenticationYieldsToTOTPPage() throws IOException {
                final URI authTargetURI = fromUri("/authenticate").build();
                final Form authForm = new Form().param("domainID", SOME_DOMAIN_ID_VALUE).param("password", SOME_PASSWORD_VALUE).param("sessionID", SOME_SESSION_ID_VALUE);
                expect(AUTH_SESSION_API_MOCK.getSession(SOME_SESSION_ID))
                                .andReturn(new SessionResponse(new OpenIdConnectAuthenticationRequest(SOME_RESPONSE_TYPE, SOME_CLIENT_ID, SOME_REDIRECT_URI),
                                                SOME_SUBJECT_SESSION_ID, NO_OPTIONAL_DATA));
                expect(USER_AUTHENTICATION_DAO_MOCK.findUserAuthenticationBy(SOME_DOMAIN_ID)).andReturn(new UserAuthentication(SOME_DOMAIN_ID,
                                sha256Hex(SOME_PASSWORD_VALUE), TWO_FACTOR_AUTH_ENABLED, AGENT_PW_RESET_ENABLED, SOME_PSEUDONYM_SUBJECT, ACTIVE));
                final String googleAuthKey = "key";
                final int googleAuthVerificationCode = 42;
                replayMocks();
                {
                        postFormVerifyingMocksAndCheckingFor2FAResponse(authTargetURI, authForm, OK);
                }
                resetMocks();
                final URI totpTargetURI = fromUri("/authenticate/totp").build();
                final Form totpForm = new Form().param("domainID", SOME_DOMAIN_ID_VALUE).param("sessionID", SOME_SESSION_ID_VALUE)
                                .param("verificationCode", Integer.toString(googleAuthVerificationCode)).param("passwordOK", sha256Hex(sha256Hex(SOME_PASSWORD_VALUE)));
                expect(USER_AUTHENTICATION_DAO_MOCK.findUserAuthenticationBy(SOME_DOMAIN_ID)).andReturn(new UserAuthentication(SOME_DOMAIN_ID,
                                sha256Hex(SOME_PASSWORD_VALUE), TWO_FACTOR_AUTH_ENABLED, AGENT_PW_RESET_ENABLED, SOME_PSEUDONYM_SUBJECT, ACTIVE));
                expect(USER_AUTHENTICATION_DAO_MOCK.selectTOTPSharedSecretOf(SOME_DOMAIN_ID)).andReturn(googleAuthKey.toCharArray());
                expect(GOOGLE_AUTHENTICATOR_MOCK.authorize(googleAuthKey, googleAuthVerificationCode)).andReturn(true);
                expect(AUTH_SESSION_API_MOCK.getSession(SOME_SESSION_ID))
                                .andReturn(new SessionResponse(new OpenIdConnectAuthenticationRequest(SOME_RESPONSE_TYPE, SOME_CLIENT_ID, SOME_REDIRECT_URI),
                                                SOME_SUBJECT_SESSION_ID, NO_OPTIONAL_DATA));
                expect(DOMAIN_ID_DNS_CLIENT_MOCK.lookupIdentityAgentHostNameOf(SOME_DOMAIN_ID)).andReturn(new LookupResult.Impl<>(true, SOME_IDENTITY_AGENT_HOST_NAME));
                IDENTITY_AGENT_CLIENT_MOCK.authentication(eq(SOME_DOMAIN_ID), eq(SOME_PSEUDONYM_SUBJECT), eq(SOME_IDENTITY_AGENT_HOST_NAME),
                                anyObject(ZonedDateTime.class), eq(true), eq(SOME_CLIENT_ID));
                expect(AUTH_STORE_API_MOCK.loadAuthorizationOf(SOME_PSEUDONYM_SUBJECT, SOME_CLIENT_ID))
                                .andReturn(Optional.of(new OpenIdConnectAuthorization(SOME_DOMAIN_ID_VALUE, SOME_CLIENT_ID, null, null, null, true, false, null, null,
                                                null, null, false, null, null, null, null, null, null, null, null, null, null, null)));
                expect(AUTH_SESSION_API_MOCK.authenticate(eq(SOME_SESSION_ID), anyObject(Subject.class), eq(ofMany(pwd, otp))))
                                .andReturn(new AnyResponseImpl("{ \"type\":\"consent\", \"sid\":\"" + SOME_SESSION_ID + "\", \"sub_session\": { \"sid\":\""
                                                + SOME_SUBJECT_SESSION_ID + "\", \"sub\":\"" + SOME_PSEUDONYM_SUBJECT + "\" }, \"client\": { \"client_id\":\""
                                                + SOME_CLIENT_ID + "\" }, \"claims\": { \"new\": { \"essential\":[\"claimNewlyToAccept\"] } } } }"));
                expect(USER_AUTHENTICATION_DAO_MOCK.findUserAuthenticationsBy(SOME_PSEUDONYM_SUBJECT)).andReturn(singletonList(SOME_USER_AUTHENTICATION));
                replayMocks();
                {
                        postFormVerifyingMocksAndCheckingForConsentResponse(totpTargetURI, totpForm, OK, SOME_SUBJECT_SESSION_ID);
                }
        }

        private String currentMethodStackTrace() {
                return Thread.currentThread().getStackTrace()[2].getMethodName();
        }

        private void postFormVerifyingMocksAndCheckingFor2FAResponse(final URI targetURI, final Form formToPost, final Status expectedResponseStatus) throws IOException {
                Response response = null;
                try {
                        response = RESOURCES_UT.client().target(targetURI).property(FOLLOW_REDIRECTS, false).request().post(form(formToPost));
                        verifyMocks();
                        System.out.println("RESPONSE: " + response);
                        assertEquals("Response status code", expectedResponseStatus.getStatusCode(), response.getStatusInfo().getStatusCode());
                        final Map<String, NewCookie> httpSessionCookies = response.getCookies();
                        final List<String> responseBodyLines = readLines((InputStream) response.getEntity(), UTF8);
                        System.out.println("RESPONSE BODY: " + responseBodyLines);
                        assertEquals("Response media type", TEXT_HTML_CHARSET_UTF8_TYPE, response.getMediaType());
                        assertConnect2IdAuthSessionIdHasBeenSetAsHiddenField(SOME_SESSION_ID, responseBodyLines, 1);
                        assertForInputOfVerificationCode(responseBodyLines);
                        assertTrue("No new HTTP session cookie", httpSessionCookies.isEmpty());
                } finally {
                        if (response != null)
                                response.close();
                }
        }

        private void assertForInputOfVerificationCode(final List<String> responseBodyLines) {
                assertTrue("Input field for Google Authenticator Verification Code available", responseBodyLines.stream().anyMatch(line -> line.contains(
                                "<input class=\"w3-input\" type=\"text\" id=\"verificationCode\" name=\"verificationCode\" maxlength=\"8\" placeholder=\"123456\" autofocus=\"on\" autocomplete=\"off\"/>")));
        }

        private void postFormVerifyingMocksAndCheckingForConsentResponse(final URI targetURI, final Form formToPost, final Status expectedResponseStatus,
                        final SubjectSessionId optSubjectSessionId) throws IOException {
                Response response = null;
                try {
                        Invocation.Builder request = RESOURCES_UT.client().target(targetURI).property(FOLLOW_REDIRECTS, false).request();
                        if (optSubjectSessionId != null) {
                                final String sessionCookieName = SubjectSessionCookies.NAME_PREFIX + "001";
                                request = request.cookie(SubjectSessionCookies.NAME_OF_CURRENT, sessionCookieName).cookie(sessionCookieName,
                                                optSubjectSessionId.getPlain());
                        }
                        response = request.post(form(formToPost));
                        verifyMocks();
                        assertEquals("Response status code", expectedResponseStatus.getStatusCode(), response.getStatusInfo().getStatusCode());
                        final Map<String, NewCookie> httpSessionCookies = response.getCookies();
                        final List<String> responseBodyLines = readLines((InputStream) response.getEntity(), UTF8);
                        System.out.println("RESPONSE BODY: " + responseBodyLines);
                        if (!SEE_OTHER.equals(expectedResponseStatus)) {
                                assertEquals("Response media type", TEXT_HTML_CHARSET_UTF8_TYPE, response.getMediaType());
                        }
                        if (OK.equals(expectedResponseStatus)) {
                                assertConnect2IdAuthSessionIdHasBeenSetAsHiddenField(SOME_SESSION_ID, responseBodyLines, 2);
                                assertSubjectGetsNamed(SOME_DOMAIN_ID_VALUE, responseBodyLines);
                        }
                        if (OK.equals(expectedResponseStatus) || SEE_OTHER.equals(expectedResponseStatus)) {
                                final NewCookie currentSessionCookie = httpSessionCookies.get("sub_sid_current");
                                assertNotNull("Current session cookie available", currentSessionCookie);
                                assertEquals("Path of current session cookie", "/", currentSessionCookie.getPath());
                                final NewCookie sessionCookie = httpSessionCookies.get(currentSessionCookie.getValue());
                                assertNotNull("Session cookie available", sessionCookie);
                                assertEquals("Path of session cookie", "/", sessionCookie.getPath());
                                assertEquals("Session cookie value", SOME_SUBJECT_SESSION_ID.getPlain(), sessionCookie.getValue());
                        } else {
                                assertTrue("No HTTP session cookie", httpSessionCookies.isEmpty());
                        }
                } finally {
                        if (response != null)
                                response.close();
                }
        }

        private void assertSubjectGetsNamed(final String subject, final List<String> responseBodyLines) {
                assertTrue("Authenticated subject gets named", responseBodyLines.stream()
                                .anyMatch(line -> line.contains("Your current online identity: <b class=\"w3-text-inetid-purple\">" + subject)));
        }

        private void assertConnect2IdAuthSessionIdHasBeenSetAsHiddenField(final SessionId sessionId, final List<String> responseBodyLines, final int expectedCount) {
                assertEquals("Number of hidden field containing session ID", expectedCount, responseBodyLines.stream().peek(System.out::println)
                                .filter(line -> line.contains("<input type=\"hidden\" name=\"sessionID\" value=\"" + sessionId + "\"/>")).count());
        }

        private void verifyMocks() {
                verify(AUTH_SESSION_API_MOCK, USER_AUTHENTICATION_DAO_MOCK, DOMAIN_ID_DNS_CLIENT_MOCK, IDENTITY_AGENT_CLIENT_MOCK, AUTH_STORE_API_MOCK,
                                GOOGLE_AUTHENTICATOR_MOCK);
        }

        private void replayMocks() {
                replay(AUTH_SESSION_API_MOCK, USER_AUTHENTICATION_DAO_MOCK, DOMAIN_ID_DNS_CLIENT_MOCK, IDENTITY_AGENT_CLIENT_MOCK, AUTH_STORE_API_MOCK,
                                GOOGLE_AUTHENTICATOR_MOCK);
        }

        private void resetMocks() {
                reset(AUTH_SESSION_API_MOCK, USER_AUTHENTICATION_DAO_MOCK, DOMAIN_ID_DNS_CLIENT_MOCK, IDENTITY_AGENT_CLIENT_MOCK, AUTH_STORE_API_MOCK,
                                GOOGLE_AUTHENTICATOR_MOCK);
        }

}
