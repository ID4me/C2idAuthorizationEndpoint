/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.authendpoint.view;

import de.denic.domainid.DomainId;
import de.denic.domainid.connect2id.authzsession.SessionId;
import de.denic.domainid.connect2id.authzsession.entity.AuthzSessionOptionalData;
import de.denic.domainid.connect2id.authzsession.entity.ConsentPromptResponse;
import de.denic.domainid.connect2id.authzsession.entity.ConsentPromptResponse.Claims;
import de.denic.domainid.connect2id.authzsession.entity.ConsentPromptResponse.Client;
import de.denic.domainid.connect2id.authzsession.entity.ConsentPromptResponse.EssentialAndVoluntaryClaims;
import de.denic.domainid.connect2id.authzsession.entity.ConsentPromptResponse.Scopes;
import de.denic.domainid.connect2id.common.AuthenticationMethodReference;
import de.denic.domainid.connect2id.common.Claim;
import de.denic.domainid.connect2id.common.ClientId;
import de.denic.domainid.connect2id.common.SubjectSessionId;
import de.denic.domainid.connect2id.subjectsessionstore.entity.SubjectSession;
import de.denic.domainid.connect2id.subjectsessionstore.entity.SubjectSessionOptionalData;
import de.denic.domainid.subject.PseudonymSubject;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapperBuilder;
import freemarker.template.Template;
import freemarker.template.Version;
import org.junit.Before;
import org.junit.Test;

import java.io.StringWriter;
import java.io.Writer;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertTrue;

public class ConsentViewFreeMarkterTemplateTest {

  private static final DomainId UMLAUT_DOMAIN_ID = new DomainId("some.dömain-id");
  private static final Version FREEMARKER_INCOMPATIBLE_IMPROVEMENTS = new Version("2.3.23");
  private static final Collection<AuthenticationMethodReference> NO_AUTH_METHOD_REFERENCES = null;
  private static final SubjectSessionOptionalData NO_ADDITIONAL_DATA = null;
  private static final Map<String, String> NO_CLAIMS = null;

  private Writer targetWriter;
  private Template templateUnderTest;
  private ConsentPromptResponse entity;

  @Before
  public void setup() throws Exception {
    targetWriter = new StringWriter();
    final Configuration configuration = new Configuration(FREEMARKER_INCOMPATIBLE_IMPROVEMENTS);
    configuration.setClassForTemplateLoading(ConsentView.class, "");
    configuration.setObjectWrapper(new DefaultObjectWrapperBuilder(FREEMARKER_INCOMPATIBLE_IMPROVEMENTS).build());
    templateUnderTest = configuration.getTemplate("consent.ftl");
  }

  @Test
  public void testRenderingOfConsentPage() throws Exception {
    final Scopes scope = new Scopes(null, null);
    final String secondEssentialClaim = "newEssent2";
    final Claims claims = new Claims(
            new EssentialAndVoluntaryClaims(asList(new Claim("newVolunt1"), new Claim("newVolunt2")),
                    asList(new Claim("newEssent1"), new Claim(secondEssentialClaim))),
            new EssentialAndVoluntaryClaims(asList(new Claim("consentVolunt1"), new Claim("consentVolunt2")),
                    asList(new Claim("consentEssent1"), new Claim("consentEssent2"))));
    final SubjectSession subjectSession = new SubjectSession(new PseudonymSubject("test-subject"),
            new SubjectSessionId("subjectSessionID"), 0, 0, 0, 0, 0, NO_AUTH_METHOD_REFERENCES,
            NO_CLAIMS, NO_ADDITIONAL_DATA);
    final Client client = new Client(new ClientId("clientID"), null, null, null, null, null, null, null);
    final Map<Claim, String> reasonsOfClaims = new HashMap<>();
    reasonsOfClaims.put(new Claim(secondEssentialClaim), "Reason for claim " + secondEssentialClaim);
    entity = new ConsentPromptResponse("consent", new SessionId("sessionID"),
            subjectSession, client, scope, claims, new AuthzSessionOptionalData(reasonsOfClaims));
    {
      templateUnderTest.process(this, targetWriter);
      final String renderedOutput = targetWriter.toString();
      System.out.println(renderedOutput);
      assertTrue("Input element of one of the claims as expected",
              renderedOutput.contains("<input type=\"checkbox\" name=\"claim\" value=\"consentVolunt2\" checked>"));
      assertTrue("Client ID is shown as default value if no Client name is available", renderedOutput.contains("Login " +
              "at <b class=\"w3-text-inetid-purple\">ID clientID</b>"));
      assertTrue("Content-Type and Charset are set correctly in meta tag",
              renderedOutput.contains("<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">"));
    }
  }

  public Object getConsent() {
    return entity;
  }

  public List<Claim> getClaimsSortedAppropriately() {
    return entity.getClaims().getAll(); // Dummy: Unsorted
  }

  public String getNameOfClaim(final String claim) {
    return ConsentView.getNameOfClaim(claim);
  }

  public String getDescriptionOfClaim(final String claim) {
    return ConsentView.getDescriptionOfClaim(claim);
  }

  public String getIdName() {
    return "domaiID_iNetID_id4me";
  }

  public DomainId getDomainId() {
    return UMLAUT_DOMAIN_ID;
  }

}
