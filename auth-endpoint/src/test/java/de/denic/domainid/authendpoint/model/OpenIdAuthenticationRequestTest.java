/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.authendpoint.model;

import de.denic.domainid.DomainId;
import de.denic.domainid.connect2id.authzsession.ResponseType;
import de.denic.domainid.connect2id.authzsession.State;
import de.denic.domainid.connect2id.common.*;
import org.junit.Test;

import java.net.URI;

import static org.junit.Assert.assertNotNull;

public class OpenIdAuthenticationRequestTest {

  private static final Display NO_DISPLAY = null;
  private static final IdTokenHint NO_ID_TOKEN_HINT = null;
  private static final RequestURI NO_REQUEST_URI = null;
  private static final Claims NO_CLAIMS = null;
  private static final Prompt NO_PROMPT = null;
  private static final MaxAge NO_MAX_AGE = null;
  private static final Nonce NO_NONCE = null;
  private static final DomainId NO_LOGIN_HINT = null;
  private static final Request NO_REQUEST = null;

  @Test
  public void onlyMandatoryParamsToBuildOpenIdAuthRequestURI() {
    final ResponseType responseType = new ResponseType("RESPONSE_TYPE");
    final Scope scope = new Scope("SCOPE");
    final ClientId clientId = new ClientId("CLIENT_ID");
    final State state = new State("STATE");
    final RedirectURI redirectURI = new RedirectURI("http://some.redirect?uri=foo");
    final OpenIdAuthenticationRequest CUT = new OpenIdAuthenticationRequest(responseType, scope, clientId, state,
            redirectURI, NO_LOGIN_HINT, NO_NONCE, NO_MAX_AGE, NO_PROMPT, NO_CLAIMS, NO_REQUEST_URI, NO_ID_TOKEN_HINT,
            NO_DISPLAY, NO_REQUEST);
    final URI openIdAuthenticationRequestURI = CUT.toOpenIdAuthenticationRequestURI();
    System.out.println(openIdAuthenticationRequestURI);
    assertNotNull("Auth. Request URI", openIdAuthenticationRequestURI);
  }

}
