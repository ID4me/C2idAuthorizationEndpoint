/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.authendpoint.resource;

import de.denic.domainid.connect2id.common.SubjectSessionId;
import org.junit.Test;

import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.NewCookie;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static de.denic.domainid.authendpoint.resource.SubjectSessionCookies.NAME_OF_CURRENT;
import static java.util.Arrays.asList;
import static java.util.Arrays.spliterator;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.stream.StreamSupport.stream;
import static org.assertj.core.api.BDDAssertions.then;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class SubjectSessionCookiesTest {

  public static final boolean NOT_PARALLEL = false;
  private static final Duration SOME_AGE = Duration.ofMinutes(1L);

  @Test
  public void createFromValidCookie() {
    createWithCookieApplying("sub_sid_123", false, "foo");
    createWithCookieApplying("sub_sid_0", false, "foo");
    createWithCookieApplying("sub_sid_000", false, "foo");
    createWithCookieApplying("sub_sid_999", false, "foo");
    createWithCookieApplying("sub_sid_current", false, "sub_sid_000");
  }

  private void createWithCookieApplying(final String name, final boolean currentFlag, final String value) {
    final SubjectSessionCookies CUT = SubjectSessionCookies.from(singletonList(new Cookie(name, value)));
    assertEquals("Current", currentFlag, CUT.getCurrentSessionId().isPresent());
    System.out.println("toString() test: " + CUT.toString());
  }

  @Test
  public void createFromInvalidCookie() {
    expectingIllegalArgumentExceptionApplyingName("sid_123");
    expectingIllegalArgumentExceptionApplyingName("sub_sid_");
  }

  private void expectingIllegalArgumentExceptionApplyingName(final String name) {
    try {
      fail("Expecting IllegalArgumentException, but got: "
              + SubjectSessionCookies.from(singletonList(new Cookie(name, "foo"))));
    } catch (final IllegalArgumentException expected) {
      // EXPECTED
    }
  }

  @Test
  public void newCookiesOfActiveSessionFromEmptyOne() {
    final SubjectSessionCookies CUT = SubjectSessionCookies.from(emptyList());
    final NewCookie[] newCookies = CUT.cookiesOfNewActiveIdentity(new SubjectSessionId("actualSessionId"), SOME_AGE);
    final NewCookie currentSessionCookie = newCookies[0];
    final NewCookie newSessionCookie = newCookies[1];
    assertGeneralExpressions(newCookies, currentSessionCookie, newSessionCookie);
    assertEquals("New cookie name", "sub_sid_000", newSessionCookie.getName());
  }

  @Test
  public void newCookiesOfActiveSessionFromListWithFreePositionInTheMiddle() {
    final List<Cookie> currentCookies = asList(new Cookie("sub_sid_0", "foo0"), new Cookie("sub_sid_1", "foo1"),
            new Cookie("sub_sid_3", "foo3"));
    final SubjectSessionCookies CUT = SubjectSessionCookies.from(currentCookies);
    final NewCookie[] newCookies = CUT.cookiesOfNewActiveIdentity(new SubjectSessionId("actualSessionId"), SOME_AGE);
    final NewCookie currentSessionCookie = newCookies[0];
    final NewCookie newSessionCookie = newCookies[1];
    assertGeneralExpressions(newCookies, currentSessionCookie, newSessionCookie);
    assertEquals("New cookie name", "sub_sid_002", newSessionCookie.getName());
  }

  private void assertGeneralExpressions(final NewCookie[] newCookies, final NewCookie currentSessionCookie,
                                        final NewCookie newSessionCookie) {
    assertEquals("Count", 2, newCookies.length);
    assertEquals("Current cookie name", "sub_sid_current", currentSessionCookie.getName());
    assertEquals("Matching cookie reference", newSessionCookie.getName(), currentSessionCookie.getValue());
    assertEquals("Current cookie max age", SOME_AGE.getSeconds(), currentSessionCookie.getMaxAge());
    assertEquals("New cookie max age", SOME_AGE.getSeconds(), newSessionCookie.getMaxAge());
  }

  @Test
  public void cookiesToChangeWithLogout_IfNoCurrentSessionCookieAvailable() {
    final List<Cookie> currentCookies = asList(new Cookie("sub_sid_000", "foo0"), new Cookie("sub_sid_001", "foo1"),
            new Cookie("sub_sid_003", "foo3"));
    final SubjectSessionCookies CUT = SubjectSessionCookies.from(currentCookies);
    final NewCookie[] newCookies = CUT.cookiesToChangeWithLogout(SOME_AGE);
    System.out.println("New cookies: " + Arrays.toString(newCookies));
    then(newCookies.length).as("Amount of cookies to set new").isEqualTo(4);
    assertContainsCookieHaving(newCookies, "sub_sid_000", "", 0);
    assertContainsCookieHaving(newCookies, "sub_sid_001", "", 0);
    assertContainsCookieHaving(newCookies, "sub_sid_003", "", 0);
    assertContainsCookieHaving(newCookies, "sub_sid_current", "", 0);
  }

  private void assertContainsCookieHaving(final NewCookie[] newCookies, final String name, final String
          expectedValue, final int expectedMaxAge) {
    final Optional<NewCookie> optCookieOfName = stream(spliterator(newCookies), NOT_PARALLEL).filter(cookie -> name.equals(cookie.getName())).findAny();
    then(optCookieOfName.isPresent()).as("Found cookie of name '%s'", name).isTrue();
    final NewCookie cookieOfName = optCookieOfName.get();
    then(cookieOfName.getValue()).as("Value of cookie").isEqualTo(expectedValue);
    then(cookieOfName.getMaxAge()).as("Max-Age of cookie").isEqualTo(expectedMaxAge);
  }

  @Test
  public void cookiesToChangeWithLogout_IfCurrentSessionCookieTargetsUnknownSession() {
    final String cookieWithLowestNumber = "sub_sid_000";
    final List<Cookie> currentCookies = asList(new Cookie(cookieWithLowestNumber, "foo0"), new Cookie("sub_sid_001", "foo1"),
            new Cookie("sub_sid_003", "foo3"), new Cookie(NAME_OF_CURRENT, "sub_sid_002"));
    final SubjectSessionCookies CUT = SubjectSessionCookies.from(currentCookies);
    final NewCookie[] newCookies = CUT.cookiesToChangeWithLogout(SOME_AGE);
    System.out.println("New cookies: " + Arrays.toString(newCookies));
    then(newCookies.length).as("Amount of cookies to set new").isEqualTo(4);
    assertContainsCookieHaving(newCookies, "sub_sid_000", "", 0);
    assertContainsCookieHaving(newCookies, "sub_sid_001", "", 0);
    assertContainsCookieHaving(newCookies, "sub_sid_003", "", 0);
    assertContainsCookieHaving(newCookies, "sub_sid_current", "", 0);
  }

  @Test
  public void cookiesToChangeWithLogout_IfCurrentSessionCookieTargetsOneOfManyKnownSession() {
    final String cookieWithLowestNumber = "sub_sid_000";
    final String oldCurrentSessionCookieName = "sub_sid_001";
    final List<Cookie> currentCookies = asList(new Cookie(cookieWithLowestNumber, "foo0"), new Cookie(oldCurrentSessionCookieName, "foo1"),
            new Cookie("sub_sid_003", "foo3"), new Cookie(NAME_OF_CURRENT, oldCurrentSessionCookieName));
    final SubjectSessionCookies CUT = SubjectSessionCookies.from(currentCookies);
    final NewCookie[] newCookies = CUT.cookiesToChangeWithLogout(SOME_AGE);
    System.out.println("New cookies: " + Arrays.toString(newCookies));
    then(newCookies.length).as("Amount of cookies to set new").isEqualTo(4);
    assertContainsCookieHaving(newCookies, "sub_sid_000", "", 0);
    assertContainsCookieHaving(newCookies, "sub_sid_001", "", 0);
    assertContainsCookieHaving(newCookies, "sub_sid_003", "", 0);
    assertContainsCookieHaving(newCookies, "sub_sid_current", "", 0);
  }

  @Test
  public void cookiesToChangeWithLogout_IfCurrentSessionCookieTargetsSingleKnownSession() {
    final String oldCurrentSessionCookieName = "sub_sid_000";
    final List<Cookie> currentCookies = asList(new Cookie(oldCurrentSessionCookieName, "foo0"), new Cookie(NAME_OF_CURRENT, oldCurrentSessionCookieName));
    final SubjectSessionCookies CUT = SubjectSessionCookies.from(currentCookies);
    final NewCookie[] newCookies = CUT.cookiesToChangeWithLogout(SOME_AGE);
    System.out.println("New cookies: " + Arrays.toString(newCookies));
    then(newCookies.length).as("Amount of cookies to set new").isEqualTo(2);
    final NewCookie identityCookie = newCookies[0];
    then(identityCookie.getName()).as("Name of cookie to remove").isEqualTo(oldCurrentSessionCookieName);
    then(identityCookie.getMaxAge()).as("Age of cookie to remove").isEqualTo(0);
    final NewCookie currentSessionCookie = newCookies[1];
    then(currentSessionCookie.getName()).as("Name of new current session cookie").isEqualTo(NAME_OF_CURRENT);
    then(currentSessionCookie.getValue()).as("Value of new current session cookie").isEmpty();
    then(currentSessionCookie.getMaxAge()).as("Age of new current session cookie").isEqualTo(0);
  }

  @Test
  public void cookiesToChangeWithLogout_IfOnlyCurrentSessionCookieAvailable() {
    final String oldCurrentSessionCookieName = "sub_sid_001";
    final List<Cookie> currentCookies = singletonList(new Cookie(NAME_OF_CURRENT, oldCurrentSessionCookieName));
    final SubjectSessionCookies CUT = SubjectSessionCookies.from(currentCookies);
    final NewCookie[] newCookies = CUT.cookiesToChangeWithLogout(SOME_AGE);
    System.out.println("New cookies: " + Arrays.toString(newCookies));
    then(newCookies.length).as("Amount of cookies to set new").isEqualTo(1);
    assertContainsCookieHaving(newCookies, "sub_sid_current", "", 0);
  }

  @Test
  public void cookiesToChangeWithLogout_IfAbsolutelyNoCookieAvailable() {
    final SubjectSessionCookies CUT = SubjectSessionCookies.from(emptyList());
    final NewCookie[] newCookies = CUT.cookiesToChangeWithLogout(SOME_AGE);
    System.out.println("New cookies: " + Arrays.toString(newCookies));
    then(newCookies.length).as("Amount of cookies to set new").isEqualTo(1);
    assertContainsCookieHaving(newCookies, "sub_sid_current", "", 0);
  }

}
