/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.authendpoint.config;

import static org.junit.Assert.assertNull;

import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;

import org.junit.Before;
import org.junit.Test;

public class Base64KeyMaterialConfigTest {

  /**
   * The following base64-encoded keys have been generated applying this code: <code>
   * final KeyPair keyPair = io.jsonwebtoken.impl.crypto.RsaProvider.generateKeyPair();
   * final Encoder base64Encoder = java.util.Base64.getEncoder();
   * System.out.println(base64Encoder.encodeToString(keyPair.getPublic().getEncoded()));
   * System.out.println(base64Encoder.encodeToString(keyPair.getPrivate().getEncoded()));
   * </code>
   */
  private static final String SOME_BASE64_ENCODED_RSA_PRIVATE_KEY = "MIIJQgIBADANBgkqhkiG9w0BAQEFAASCCSwwggkoAgEAAoICAQCNKm3YtJVu3pTEjpDbDwIw7xNPR0MdK0QAsWu/ikNbcgB3q1qUECAHp5owFqycrG0QHU+Jb4fvbXATFNL21l1tcvls2R+OWQtQKA47PUC31JdMKm6v0F4tzNmGyz4MxGGfobO2BRfvrBuOf/FhOTOx9m2Kxt79mAAvrV2mUIZstEuJChB8jU60jrtPvuE4l4IgHQJRP6kt3b0K4MwRf2OU+6mUHRGW+vaCeq4kQiABRolR2O05g4s0tkVrC6PY+YKFxnpVq+WXrB1LaO+PdhkPRlp13HeIEZqSYQLn5zpbmwi2UqnW1FNnPUcB4eV/lETUqrvL26pt0diqQaMX7Qf/tJPmYoWy7eM4OUViWMPyDlF9cCwoyy/8Uq4I7em3gPNCe/S7mEtiZuNS3T/XhwbiEM7srejnj0nSYYzUkcfyZi0xt8+6uoEv4C+DLpytv70CcrRv6Z1YZEULoRSo2rM75xnmWvzQOkhZkRTjEhRUc/msXrVb4lMVgw/v8U9RIR21HHKwEUakbse74/MxAlsk3sx4866EUte6pmsaPQxcAmDrrxksNGph0qBty9KyIWpKOpzHG1epyMRakGdKlQNHMpGkOJTQIK/XoOct2ReQLvRkgLqgWIdAqiN+h1ExqEoPvb9diISx2ExG/sGHn9iMCzY/krgrd20r1+448OXf6wIDAQABAoICACEZH6Q9wwO5wFvNqX0TKcpbY8WkycGfEVSwRLkmK+DW+/ETBivNFBV3YJmB13rsmq26kZmBwveKS0rOCSDC5ijxWjYBOnrfud1TZvLQy8nOWKMNF+K1Ej/e1ena4XOQ3+rTuRUmYJRbYCKrMOdAyDQjuU9ejY4cA38qQNjr7Hi0VTmOWIs7foarKJPsB1aDf4kUBCUUrhAI+3f/eRlpKuoVj48gua0SrpWtOgNaVbK23R+JoyVnCE6VpfURR5tp9QZ0oN5RbYWAX2X89p5GbF2+extDU+tdZUaI5GuHNZGg6gwFmbi7E8sS/QEd+/dLxk+usFV97ZnOJ+TTeu/zLXqXejn28WRipOmJpReUQkGX1hwXqA9vcZ52pf+62n1KpLdH8klLb6N5RyI1o3hAFnzlG8f6ktTOI1apVEKk4hrrSad3p3AfpYSwhsKrDrXjAMut3SGJxU2oZ/7rDgUJ2vod9bqtLXBGsznlZn+sH7LsLu+pGWkZEBI3wOotKm2SfajZpl2YQNvPrt4Xk48wU929MHH4HI1XGXP7IivIoI4z+59CuShAlOs93WVZWOS2pAHB8MpHOOcSB1TymV9UqxbE6VGawc7fHZcj4pxqYuTcO7Tr8UD5mkYDQGIkSSVKZsYRmrJI1xOc3y6Dnnl4QkhVPyMBJES2jeDhVEvOHwfxAoIBAQDLxRUHja3g2zcU3LbNwv6EmQZN21zdkTHYBF7Uc594Zm/tLmV+j3Q0Lx1okMTxegarqpI2X5YE5aJcXek2OpXje7/tA4U7E1fohpMD81JTMrP0++e14/b24o7p+sWXpzEEAdP9mM2oGvwtRNtCaiW7rRb3TZK1RUIU5NCkBoekCNxbvBelJ2kplVD27qnCCLj4oyM6eJIgPw/3CvvfLjFujZBWlpRlwqi8Xo4Ddi02m9hvGiejt2WqSeKAmuF5B3GT3Zo017tZKgjsLFNz9IqYg3gldsd9WAMVUTUoOAIlsI5OnrNrR1okP/RMTDKkIuav4i+629zYQxL8IXYn9OJdAoIBAQCxWWgCpbrQCJFgaIQz1yuMQgNIf4bdCuIy4hHHMJXVAODf2ay86+WE/ri5mHu3l16vEYropTfNGKMY9c8MtC7OHuwn6bN/RLiDJd6TmPsqloYXewheyxVLkgNg4sY98uLakQwV4iL0vzsxW+y2HlpfGl9bl5jjEDyoqPlqVF7IGSfu8Q1EMls3KGRatsd+pY332frZhdLy8PscTAtk5IzL+SeX8e7pMSOQzH/w6H8xoie5ip28A1KQhvKl98LwsZKlZjG7LT5ZBlpcM7NtT9EfaVce4ryFB9zCqeERVzOmQFUlJMxO/1v4G7otOiuWUeiJ0azgkb0ZI+iwzArRBjbnAoIBAQCrjU4KIO6dmxFBgBxrGNp8ufRDwgXqB95l1jAcPweoLt3WWjRam92iPXRN32vfPPO2vREBs/GyR74sc2fvHt2oOjw4B7m4Ja3h9sJ5FfZeUxaMjzPhv33hFpZpSJXn+ntau0tFesrR134tgoXx4TnJSo1GQUndUOS0gHtjTJgPsuIKXU2u3oI73xjd+8pxsDRCJ9510KJ9gXooa1fn6pT1VTuKmpVvIrY6eISpR2oDSqupxeRPggyWlEOcMEjZQzoM2gmEqYdyJuCCLUNJIT05751MNTb4QQu/e7H8HUM5he4ZJKU7EURDtpAvWAtegn+i3hN/BrFkZPvrxyQZuaKFAoIBAGaX6eteJCukSTUUQSYcGz0QEwYlf8bvPNtinBY2rPaDg5L7QUoozKWbFRGIfElAb62m4EylBxrGXdNan2Qh99GVYdfVAdllz7dOeOu5W9LPMNL0gWsU4ETPOtUZWsULdOAreGpQXxhmZ7+uBcl01aKehvtdPzxbcDvBREeWdx4aL1eyapJA7zOBUkyH3/ALpYzztIGRQO75KMq90vmSnUCB6b2kXyjXHFPveTPLy4jS3VggMA9LfL/O0R/hrAvBLaww6iP1kFO/sRYVcn6R5c/eCF/9mkD6w2J5E5IMd/x8ysSp/ObWIkXtBE6VkBFtYLAZc4CVzmZ0t29PGhLBHZECggEAJ0khYvMzCXJK/MBbUp3YILMEbAECmDqEImBbvdfkAsp5ho03EpiACHkl2bR4IBTM2yRE4p0Szyt9F7z2s3oCaDv94pwy8XjAQ7tKwwC8ZQ9LNeFY32t3SVPCEK0J7VjRdVYyc8QSAHFflMmx/lEdvsnFqRsFA8pyHw6Gq5qMZQ0CFbA/0yx7vB5WGrDwAwPwVvQh5L3IDSbS23kFrG8yvyM3o7DZsLmiRLekF7ZPLJy8/ZlfvLTXO5ED/gbgT+WY4AQgR59vDlN6K5A2iaagyhKGzPGBSeJfQ/HGwBTl9CK4sAni52NsPghUe/8VltccuprEjZmSlRs1KhSfuVDJ3Q==";
  private static final String HASH_ALGORITHM_FOR_SIGNING = "SHA256";
  private static final String TEST_DATA_TO_SIGN_AND_VERIFY = "The quick brown fox jumps over the lazy dog";
  private static final String DUMMY_KEY_MATERIAL = "abcdefghijklmnopqrstuvwxyz0123456789";

  private Base64KeyMaterialConfig CUT;

  @Before
  public void setup() throws NoSuchAlgorithmException {
    CUT = new Base64KeyMaterialConfig();
    CUT.setKeyAlgorithm("RSA");
    assertNull("Precondition: No private key material set", CUT.getPrivateKey());
  }

  @Test(expected = InvalidKeySpecException.class)
  public void providingDummyPrivateKeyMaterialFails() throws InvalidKeySpecException {
    try {
      CUT.setBase64PrivateKey(DUMMY_KEY_MATERIAL);
    } catch (final InvalidKeySpecException e) {
      e.printStackTrace();
      throw e;
    }
  }

  @Test
  public void getKeyPairFromPrecalculatedBase64DataAndApplyThem() throws Exception {
    CUT.setBase64PrivateKey(SOME_BASE64_ENCODED_RSA_PRIVATE_KEY);
    final byte[] signedTestDataBytes;
    {
      final PrivateKey privateKey = CUT.getPrivateKey();
      final Signature signingSignature = Signature
          .getInstance(HASH_ALGORITHM_FOR_SIGNING + "with" + privateKey.getAlgorithm());
      signingSignature.initSign(privateKey);
      signingSignature.update(TEST_DATA_TO_SIGN_AND_VERIFY.getBytes("ASCII"));
      signedTestDataBytes = signingSignature.sign();
      System.out.println(
          "Base64-encoded " + HASH_ALGORITHM_FOR_SIGNING + "with" + privateKey.getAlgorithm() + " signature of '"
              + TEST_DATA_TO_SIGN_AND_VERIFY + "': " + Base64.getEncoder().encodeToString(signedTestDataBytes));
    }
  }

}
