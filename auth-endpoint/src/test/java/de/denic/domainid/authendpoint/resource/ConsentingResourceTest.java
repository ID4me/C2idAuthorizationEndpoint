/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.authendpoint.resource;

import com.codahale.metrics.MetricRegistry;
import de.denic.domainid.DomainId;
import de.denic.domainid.connect2id.authzsession.Connect2IdAuthorizationSessionAPI;
import de.denic.domainid.connect2id.authzsession.Connect2IdAuthorizationSessionAPI.AnyResponseImpl;
import de.denic.domainid.connect2id.authzsession.ConsentedData;
import de.denic.domainid.connect2id.authzsession.SessionId;
import de.denic.domainid.connect2id.common.AuthenticationMethodReference;
import de.denic.domainid.connect2id.common.Claim;
import de.denic.domainid.connect2id.common.SubjectSessionId;
import de.denic.domainid.connect2id.subjectsessionstore.entity.SubjectSessionOptionalData;
import de.denic.domainid.connect2id.subjectsessionstore.Connect2IdSubjectSessionStoreAPI;
import de.denic.domainid.connect2id.subjectsessionstore.entity.SubjectSession;
import de.denic.domainid.subject.PseudonymSubject;
import io.dropwizard.testing.junit.ResourceTestRule;
import io.dropwizard.views.ViewMessageBodyWriter;
import io.dropwizard.views.freemarker.FreemarkerViewRenderer;
import org.junit.After;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.io.IOException;
import java.net.URI;
import java.util.Collection;
import java.util.Map;

import static de.denic.domainid.connect2id.common.Scope.OPEN_ID;
import static java.util.Arrays.asList;
import static java.util.Collections.*;
import static javax.ws.rs.client.Entity.form;
import static javax.ws.rs.core.Response.Status.SEE_OTHER;
import static javax.ws.rs.core.UriBuilder.fromUri;
import static org.easymock.EasyMock.*;
import static org.glassfish.jersey.client.ClientProperties.FOLLOW_REDIRECTS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ConsentingResourceTest {

  private static final String SOME_SESSION_ID_VALUE = "someSessionID";
  private static final SessionId SOME_SESSION_ID = new SessionId(SOME_SESSION_ID_VALUE);
  private static final DomainId SOME_DOMAIN_ID = new DomainId("test.domain.id");
  private static final Connect2IdAuthorizationSessionAPI AUTH_SESSION_API_MOCK = createMock(
          Connect2IdAuthorizationSessionAPI.class);
  private static final Connect2IdSubjectSessionStoreAPI SUBJECT_SESSION_STORE_API_MOCK = createMock
          (Connect2IdSubjectSessionStoreAPI.class);
  @ClassRule
  public static final ResourceTestRule RESOURCES_UT = ResourceTestRule.builder()
          .addProvider(new ViewMessageBodyWriter(new MetricRegistry(), singleton(new FreemarkerViewRenderer())))
          .addResource(new ConsentingResource(AUTH_SESSION_API_MOCK, SUBJECT_SESSION_STORE_API_MOCK)).build();
  private static final String CLAIM1_VALUE = "claim1";
  private static final String CLAIM2_VALUE = "claim2";
  private static final String CLAIM3_VALUE = "claim3";
  private static final SubjectSessionId SUBJECT_SESSION_ID = new SubjectSessionId("subjectSessionCookieValue");
  private static final Map<String, String> NO_CLAIMS = null;
  private static final Collection<AuthenticationMethodReference> NO_AMR = null;
  private static final PseudonymSubject SOME_SUBJECT = new PseudonymSubject("pseudonymSubject");
  private static final SubjectSession TEST_SUBJECT_SESSION = new SubjectSession(SOME_SUBJECT, SUBJECT_SESSION_ID, 0, 0,
          0, 0, 0, NO_AMR, NO_CLAIMS, new SubjectSessionOptionalData(SOME_DOMAIN_ID));

  @After
  public void tearDown() {
    reset(AUTH_SESSION_API_MOCK, SUBJECT_SESSION_STORE_API_MOCK);
  }

  @Test
  public void commaSeparatedSuggestedClaims() throws IOException {
    final URI targetURI = fromUri("/consent").build();
    final Form form = new Form().param("consenting", "true").param("sessionID", SOME_SESSION_ID_VALUE)
            .param("domainID", SOME_DOMAIN_ID.getName()).param("claim", CLAIM3_VALUE).param("claim", CLAIM2_VALUE)
            .param("suggestedClaims", CLAIM3_VALUE + "," + CLAIM1_VALUE + "," + CLAIM2_VALUE).param("remember", "true");
    final ConsentedData consentedData = new ConsentedData(OPEN_ID,
            asList(new Claim(CLAIM3_VALUE), new Claim(CLAIM2_VALUE)), singletonList(new Claim(CLAIM1_VALUE)));
    final URI redirectURI = fromUri("http://example.com/some-redirect-uri").build();
    expect(AUTH_SESSION_API_MOCK.provide(SOME_SESSION_ID, SOME_DOMAIN_ID, consentedData, true)).andReturn(new AnyResponseImpl(
            "{ \"type\":\"response\", \"mode\":\"query\", \"parameters\": { \"uri\":\"" + redirectURI + "\" } }"));
    expect(SUBJECT_SESSION_STORE_API_MOCK.sessionOf(SUBJECT_SESSION_ID)).andReturn(TEST_SUBJECT_SESSION);
    replayMocks();
    {
      postFormToTargetUriVerifyingMocksAndCheckingResponse(targetURI, form, SEE_OTHER, redirectURI);
    }
  }

  @Test
  public void emptySuggestedClaims() throws IOException {
    final URI targetURI = fromUri("/consent").build();
    final Form form = new Form().param("consenting", "true").param("domainID", SOME_DOMAIN_ID.getName())
            .param("sessionID", SOME_SESSION_ID_VALUE).param("suggestedClaims", "").param("remember", "true");
    final ConsentedData consentedData = new ConsentedData(OPEN_ID, emptyList(), emptyList());
    final URI redirectURI = fromUri("http://example.com/some-redirect-uri").build();
    expect(AUTH_SESSION_API_MOCK.provide(SOME_SESSION_ID, SOME_DOMAIN_ID, consentedData, true)).andReturn(new
            AnyResponseImpl(
            "{ \"type\":\"response\", \"mode\":\"query\", \"parameters\": { \"uri\":\"" + redirectURI + "\" } }"));
    expect(SUBJECT_SESSION_STORE_API_MOCK.sessionOf(SUBJECT_SESSION_ID)).andReturn(TEST_SUBJECT_SESSION);
    replayMocks();
    {
      postFormToTargetUriVerifyingMocksAndCheckingResponse(targetURI, form, SEE_OTHER, redirectURI);
    }
  }

  @Test
  public void noSuggestedClaims() throws IOException {
    final URI targetURI = fromUri("/consent").build();
    final Form form = new Form().param("consenting", "true").param("domainID", SOME_DOMAIN_ID.getName()).
            param("sessionID", SOME_SESSION_ID_VALUE).param("remember", "true");
    final ConsentedData consentedData = new ConsentedData(OPEN_ID, emptyList(), emptyList());
    final URI redirectURI = fromUri("http://example.com/some-redirect-uri").build();
    expect(AUTH_SESSION_API_MOCK.provide(SOME_SESSION_ID, SOME_DOMAIN_ID, consentedData, true)).andReturn(new AnyResponseImpl(
            "{ \"type\":\"response\", \"mode\":\"query\", \"parameters\": { \"uri\":\"" + redirectURI + "\" } }"));
    expect(SUBJECT_SESSION_STORE_API_MOCK.sessionOf(SUBJECT_SESSION_ID)).andReturn(TEST_SUBJECT_SESSION);
    replayMocks();
    {
      postFormToTargetUriVerifyingMocksAndCheckingResponse(targetURI, form, SEE_OTHER, redirectURI);
    }
  }

  private void postFormToTargetUriVerifyingMocksAndCheckingResponse(final URI targetURI, final Form formToPost,
                                                                    final Status expectedResponseStatus, final URI expectedRedirectURI) {
    final Cookie sessionCookie = new Cookie("sub_sid_current", "sub_sid_000");
    final Cookie actualSessionCookie = new Cookie("sub_sid_000", SUBJECT_SESSION_ID.getPlain());
    Response response = null;
    try {
      response = RESOURCES_UT.client().target(targetURI).property(FOLLOW_REDIRECTS, false).request().
              cookie(sessionCookie).cookie(actualSessionCookie).post(form(formToPost));
      verifyMocks();
      assertEquals("Response status code", expectedResponseStatus.getStatusCode(),
              response.getStatusInfo().getStatusCode());
      assertTrue("No new cookies", response.getCookies().isEmpty());
    } finally {
      if (response != null) response.close();
    }
  }

  private void verifyMocks() {
    verify(AUTH_SESSION_API_MOCK, SUBJECT_SESSION_STORE_API_MOCK);
  }

  private void replayMocks() {
    replay(AUTH_SESSION_API_MOCK, SUBJECT_SESSION_STORE_API_MOCK);
  }

}
