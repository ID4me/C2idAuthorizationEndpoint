/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.authendpoint.config;

import static org.apache.commons.io.FileUtils.touch;
import static org.apache.commons.io.FileUtils.write;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PrivateKeyConfigTest {

  private static final Charset ASCII = Charset.forName("ASCII");
  private static final String JWK_SET_FILE_CONTENT = "{ \"keys\": [ " + "{ \"kty\":\"RSA\","
      + " \"d\":\"bmpuqB4PIhJcndRs_i0jOXKjyQzwBXXq2GuWxPEsgFBYx7fFdCuGifQiytMeSEW2OQFY6W7XaqJbXneYMmoI0qTwMQcD91FNX_vlR5he0dNlpZqqYsvVN3c_oT4ENoPUr4GF6L4Jz74gBOlVsE8rvw3MVqrfmbF543ONBJPUt3d1TjKwaZQlgPji-ycGg_P7K-dKxpyfQsC8xMmVmiAF4QQtnUa9vMgiChiO8-6VzGm2yWWyIUVRLxSohrbSNFhqF2zeWXePAw0_nzeZh3IDIMS5ABo92Pry4N3X-X7v_7nf8MGngK4duQ_1UkkLk-3u0I3tk_glsarDN0tYhzPwAQ\","
      + " \"e\":\"AQAB\"," + " \"use\":\"sig\"," + " \"kid\":\"CXup\","
      + " \"n\":\"hrwD-lc-IwzwidCANmy4qsiZk11yp9kHykOuP0yOnwi36VomYTQVEzZXgh2sDJpGgAutdQudgwLoV8tVSsTG9SQHgJjH9Pd_9V4Ab6PANyZNG6DSeiq1QfiFlEP6Obt0JbRB3W7X2vkxOVaNoWrYskZodxU2V0ogeVL_LkcCGAyNu2jdx3j0DjJatNVk7ystNxb9RfHhJGgpiIkO5S3QiSIVhbBKaJHcZHPF1vq9g0JMGuUCI-OTSVg6XBkTLEGw1C_R73WD_oVEBfdXbXnLukoLHBS11p3OxU7f4rfxA_f_72_UwmWGJnsqS3iahbms3FkvqoL9x_Vj3GhuJSf97Q\" }"
      + " ] }";

  private PrivateKeyConfig CUT;
  private File jwkSetTestFile;

  @Before
  public void setUp() throws Exception {
    CUT = new PrivateKeyConfig();
    assertFalse("Precondition: No key configured yet", CUT.isKeyConfigured());
  }

  @After
  public void tearDown() throws Exception {
    if (jwkSetTestFile != null) {
      assertTrue("Deleting test file", jwkSetTestFile.delete());
    }
  }

  @Test(expected = FileNotFoundException.class)
  public void missingJwkSetFile() throws IOException {
    CUT.setJwkSetFile("file-not-available");
    fail("Not yet implemented");
  }

  @Test
  public void emptyJwkSetFile() throws IOException {
    jwkSetTestFile = new File("empty-file");
    touch(jwkSetTestFile);
    CUT.setJwkSetFile(jwkSetTestFile.getAbsolutePath());
    assertFalse("No key configured", CUT.isKeyConfigured());
  }

  @Test
  public void validJwkSetFile() throws IOException {
    jwkSetTestFile = new File("valid-jwks-file");
    write(jwkSetTestFile, JWK_SET_FILE_CONTENT, ASCII);
    CUT.setJwkSetFile(jwkSetTestFile.getAbsolutePath());
    assertTrue("Some key configured", CUT.isKeyConfigured());
  }

}
