/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.authendpoint.resource;

import com.codahale.metrics.MetricRegistry;
import de.denic.domainid.authendpoint.view.ValidationErrorMessageHTMLBodyWriter;
import de.denic.domainid.connect2id.authzsession.Connect2IdAuthorizationSessionAPI;
import de.denic.domainid.connect2id.authzsession.Connect2IdAuthorizationSessionAPI.AnyResponseImpl;
import de.denic.domainid.connect2id.authzsession.Connect2IdAuthorizationSessionAPI.InitiateSessionResponse;
import de.denic.domainid.connect2id.authzsession.SessionId;
import de.denic.domainid.connect2id.authzstore.Connect2IdAuthorizationStoreAPI;
import de.denic.domainid.connect2id.clientregistration.OpenIdConnectClientRegistration;
import de.denic.domainid.connect2id.clientregistration.entity.OpenIdConnectClient;
import de.denic.domainid.connect2id.common.Claim;
import de.denic.domainid.connect2id.common.ClientId;
import de.denic.domainid.connect2id.common.SubjectSessionId;
import de.denic.domainid.connect2id.subjectsessionstore.entity.SubjectSessionOptionalData;
import de.denic.domainid.connect2id.subjectsessionstore.Connect2IdSubjectSessionStoreAPI;
import de.denic.domainid.connect2id.subjectsessionstore.entity.SubjectSession;
import de.denic.domainid.dbaccess.UserAuthenticationDAO;
import de.denic.domainid.subject.PseudonymSubject;
import io.dropwizard.testing.junit.ResourceTestRule;
import io.dropwizard.views.ViewMessageBodyWriter;
import io.dropwizard.views.freemarker.FreemarkerViewRenderer;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.asList;
import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static javax.ws.rs.client.Entity.form;
import static javax.ws.rs.core.MediaType.TEXT_HTML_TYPE;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.SEE_OTHER;
import static javax.ws.rs.core.UriBuilder.fromUri;
import static org.apache.commons.io.IOUtils.readLines;
import static org.apache.commons.lang3.StringUtils.join;
import static org.easymock.EasyMock.*;
import static org.glassfish.jersey.client.ClientProperties.FOLLOW_REDIRECTS;
import static org.junit.Assert.*;

public class OpenIdAuthenticationRequestResourceTest {

  private static final String SELECT_ACCOUNT = "select_account";
  private static final String SOME_REDIRECT_URI_VALUE = "someRedirectURI";
  private static final String SOME_STATE_VALUE = "someState";
  private static final String SOME_CLIENT_ID_VALUE = "someClientId";
  private static final ClientId SOME_CLIENT_ID = new ClientId("someClientId");
  private static final String SOME_SCOPE_VALUE = "someScope";
  private static final String SOME_RESPONSE_TYPE_VALUE = "someResponseType";
  private static final boolean DO_SELECT_ACCOUNT = true;
  private static final boolean NO_SELECT_ACCOUNT = !DO_SELECT_ACCOUNT;
  private static final String SOME_LOGIN_HINT = "test.domain-id";
  private static final String SOME_NONCE_VALUE = "someNonceValue";
  private static final String SOME_MAX_AGE_VALUE = "10000";
  private static final String SOME_ID_TOKEN_HINT = "someIdTokenHint";
  private static final String SOME_CLIENT_NAME = "Client Name";
  private static final SubjectSessionId NO_SUBJECT_SESSION_ID = null;
  private static final Map<Claim, String> NO_REASONS_OF_CLAIMS = null;
  private static final Charset UTF8 = Charset.forName("UTF8");
  private static final Connect2IdAuthorizationSessionAPI AUTH_SESSION_MOCK = createMock(
          Connect2IdAuthorizationSessionAPI.class);
  private static final Connect2IdSubjectSessionStoreAPI SUBJECT_SESSION_STORE_MOCK = createMock(
          Connect2IdSubjectSessionStoreAPI.class);
  private static final Connect2IdAuthorizationStoreAPI AUTH_STORE_MOCK = createMock(
          Connect2IdAuthorizationStoreAPI.class);
  private static final UserAuthenticationDAO USER_AUTHENTICATION_DAO_MOCK = createMock(
          UserAuthenticationDAO.class);
  private static final OpenIdConnectClientRegistration OPEN_ID_CONNECT_CLIENT_REGISTRATION_MOCK = createMock(
          OpenIdConnectClientRegistration.class);
  @ClassRule
  public static final ResourceTestRule RESOURCES_UT = ResourceTestRule.builder()
          .addProvider(new ViewMessageBodyWriter(new MetricRegistry(), singleton(new FreemarkerViewRenderer())))
          .addProvider(ValidationErrorMessageHTMLBodyWriter.class)
          .addResource(
                  new OpenIdAuthenticationRequestResource(AUTH_SESSION_MOCK, SUBJECT_SESSION_STORE_MOCK,
                          AUTH_STORE_MOCK, USER_AUTHENTICATION_DAO_MOCK, OPEN_ID_CONNECT_CLIENT_REGISTRATION_MOCK))
          .build();
  private static final SubjectSessionOptionalData NO_ADDITIONAL_DATA = null;
  private static final Map<String, String> NO_CLAIMS = null;
  private static final MediaType TEXT_HTML_CHARSET_UTF8_TYPE = new MediaType("text", "html", "UTF-8");

  private Response response;

  private static void closeQuietly(final Response response) {
    if (response != null) response.close();
  }

  @After
  public void tearDown() {
    reset(AUTH_SESSION_MOCK, SUBJECT_SESSION_STORE_MOCK, AUTH_STORE_MOCK, USER_AUTHENTICATION_DAO_MOCK, OPEN_ID_CONNECT_CLIENT_REGISTRATION_MOCK);
  }

  @Test
  public void issueOpenIdAuthenticationRequestViaGET() throws IOException {
    final URI targetURI = fromUri("/login?response_type={1}&scope={2}&client_id={3}&state={4}&redirect_uri={5}").build(
            SOME_RESPONSE_TYPE_VALUE, SOME_SCOPE_VALUE, SOME_CLIENT_ID_VALUE, SOME_STATE_VALUE, SOME_REDIRECT_URI_VALUE);
    final SessionId sessionId = new SessionId("session-id");
    final InitiateSessionResponse initiateSessionResponse = new AnyResponseImpl("{ \"type\":\"auth\", \"sid\":\""
            + sessionId + "\", \"display\":\"page\", \"select_account\":" + NO_SELECT_ACCOUNT + " }");
    expect(AUTH_SESSION_MOCK.initiateSession(URI.create(targetURI.getQuery()), NO_SUBJECT_SESSION_ID,
            NO_REASONS_OF_CLAIMS)).andReturn(initiateSessionResponse);
    expect(OPEN_ID_CONNECT_CLIENT_REGISTRATION_MOCK.getClientOf(SOME_CLIENT_ID)).andReturn(of(new OpenIdConnectClient
            (SOME_CLIENT_ID, SOME_CLIENT_NAME, null, null, null)));
    replayMocks();
    try {
      response = RESOURCES_UT.client().target(targetURI).request().get();
      verifyMocks();
      final List<String> responseBodyLines = checkStatusCodeAndMediaTypeProvidingBodyLines(sessionId, response);
      assertSomeLineContainsEditableDomainIdField(responseBodyLines);
    } finally {
      closeQuietly(response);
    }
  }

  @Test
  public void issueLoginHintWithOpenIdAuthenticationRequest() throws IOException {
    final URI targetURI = fromUri(
            "/login?response_type={1}&scope={2}&client_id={3}&state={4}&redirect_uri={5}&login_hint={6}").build(
            SOME_RESPONSE_TYPE_VALUE, SOME_SCOPE_VALUE, SOME_CLIENT_ID_VALUE, SOME_STATE_VALUE, SOME_REDIRECT_URI_VALUE,
            SOME_LOGIN_HINT);
    final SessionId sessionId = new SessionId("session-id");
    final InitiateSessionResponse initiateSessionResponse = new AnyResponseImpl("{ \"type\":\"auth\", \"sid\":\""
            + sessionId + "\", \"display\":\"page\", \"select_account\":" + NO_SELECT_ACCOUNT + " }");
    expect(AUTH_SESSION_MOCK.initiateSession(URI.create(targetURI.getQuery()), NO_SUBJECT_SESSION_ID,
            NO_REASONS_OF_CLAIMS)).andReturn(initiateSessionResponse);
    expect(OPEN_ID_CONNECT_CLIENT_REGISTRATION_MOCK.getClientOf(SOME_CLIENT_ID)).andReturn(of(new OpenIdConnectClient
            (SOME_CLIENT_ID, SOME_CLIENT_NAME, null, null, null)));
    replayMocks();
    try {
      response = RESOURCES_UT.client().target(targetURI).request().get();
      verifyMocks();
      final List<String> responseBodyLines = checkStatusCodeAndMediaTypeProvidingBodyLines(sessionId, response);
      assertSomeLineContains(responseBodyLines, "Field 'domainID' preset with login hint value",
              "<input class=\"w3-input\" type=\"text\" id=\"domainID\" name=\"domainID\" maxlength=\"40\" value=\""
                      + SOME_LOGIN_HINT + "\"");
    } finally {
      closeQuietly(response);
    }
  }

  @Test
  public void issueOpenIdAuthenticationRequestViaPOST() throws IOException {
    final URI targetURI = fromUri("/login").build();
    final Form form = new Form().param("response_type", SOME_RESPONSE_TYPE_VALUE).param("scope", SOME_SCOPE_VALUE)
            .param("client_id", SOME_CLIENT_ID_VALUE).param("state", SOME_STATE_VALUE)
            .param("redirect_uri", SOME_REDIRECT_URI_VALUE);
    final SessionId sessionId = new SessionId("session-id");
    final InitiateSessionResponse initiateSessionResponse = new AnyResponseImpl("{ \"type\":\"auth\", \"sid\":\""
            + sessionId + "\", \"display\":\"page\", \"select_account\":" + NO_SELECT_ACCOUNT + " }");
    expect(AUTH_SESSION_MOCK.initiateSession(
            fromUri("response_type={1}&scope={2}&client_id={3}&state={4}&redirect_uri={5}").build(SOME_RESPONSE_TYPE_VALUE,
                    SOME_SCOPE_VALUE, SOME_CLIENT_ID_VALUE, SOME_STATE_VALUE, SOME_REDIRECT_URI_VALUE),
            NO_SUBJECT_SESSION_ID, NO_REASONS_OF_CLAIMS)).andReturn(initiateSessionResponse);
    expect(OPEN_ID_CONNECT_CLIENT_REGISTRATION_MOCK.getClientOf(SOME_CLIENT_ID)).andReturn(of(new OpenIdConnectClient
            (SOME_CLIENT_ID, SOME_CLIENT_NAME, null, null, null)));
    replayMocks();
    try {
      response = RESOURCES_UT.client().target(targetURI).request().post(form(form));
      verifyMocks();
      final List<String> responseBodyLines = checkStatusCodeAndMediaTypeProvidingBodyLines(sessionId, response);
      assertSomeLineContainsEditableDomainIdField(responseBodyLines);
    } finally {
      closeQuietly(response);
    }
  }

  @Test
  public void issueOpenIdAuthenticationRequestWithNonce() throws IOException {
    final URI targetURI = fromUri("/login").build();
    final Form form = new Form().param("response_type", SOME_RESPONSE_TYPE_VALUE).param("scope", SOME_SCOPE_VALUE)
            .param("client_id", SOME_CLIENT_ID_VALUE).param("state", SOME_STATE_VALUE)
            .param("redirect_uri", SOME_REDIRECT_URI_VALUE).param("nonce", SOME_NONCE_VALUE);
    final SessionId sessionId = new SessionId("session-id");
    final InitiateSessionResponse initiateSessionResponse = new AnyResponseImpl("{ \"type\":\"auth\", \"sid\":\""
            + sessionId + "\", \"display\":\"page\", \"select_account\":" + NO_SELECT_ACCOUNT + " }");
    expect(AUTH_SESSION_MOCK.initiateSession(
            fromUri("response_type={1}&scope={2}&client_id={3}&state={4}&redirect_uri={5}&nonce={6}").build(
                    SOME_RESPONSE_TYPE_VALUE, SOME_SCOPE_VALUE, SOME_CLIENT_ID_VALUE, SOME_STATE_VALUE, SOME_REDIRECT_URI_VALUE,
                    SOME_NONCE_VALUE),
            NO_SUBJECT_SESSION_ID, NO_REASONS_OF_CLAIMS)).andReturn(initiateSessionResponse);
    expect(OPEN_ID_CONNECT_CLIENT_REGISTRATION_MOCK.getClientOf(SOME_CLIENT_ID)).andReturn(of(new OpenIdConnectClient
            (SOME_CLIENT_ID, SOME_CLIENT_NAME, null, null, null)));
    replayMocks();
    try {
      response = RESOURCES_UT.client().target(targetURI).request().post(form(form));
      verifyMocks();
      final List<String> responseBodyLines = checkStatusCodeAndMediaTypeProvidingBodyLines(sessionId, response);
      assertSomeLineContainsEditableDomainIdField(responseBodyLines);
    } finally {
      closeQuietly(response);
    }
  }

  @Test
  public void issueOpenIdAuthenticationRequestWithMaxAge() throws IOException {
    final URI targetURI = fromUri("/login").build();
    final Form form = new Form().param("response_type", SOME_RESPONSE_TYPE_VALUE).param("scope", SOME_SCOPE_VALUE)
            .param("client_id", SOME_CLIENT_ID_VALUE).param("state", SOME_STATE_VALUE)
            .param("redirect_uri", SOME_REDIRECT_URI_VALUE).param("max_age", SOME_MAX_AGE_VALUE);
    final SessionId sessionId = new SessionId("session-id");
    final InitiateSessionResponse initiateSessionResponse = new AnyResponseImpl("{ \"type\":\"auth\", \"sid\":\""
            + sessionId + "\", \"display\":\"page\", \"select_account\":" + NO_SELECT_ACCOUNT + " }");
    expect(AUTH_SESSION_MOCK.initiateSession(
            fromUri("response_type={1}&scope={2}&client_id={3}&state={4}&redirect_uri={5}&max_age={6}").build(
                    SOME_RESPONSE_TYPE_VALUE, SOME_SCOPE_VALUE, SOME_CLIENT_ID_VALUE, SOME_STATE_VALUE, SOME_REDIRECT_URI_VALUE,
                    SOME_MAX_AGE_VALUE),
            NO_SUBJECT_SESSION_ID, NO_REASONS_OF_CLAIMS)).andReturn(initiateSessionResponse);
    expect(OPEN_ID_CONNECT_CLIENT_REGISTRATION_MOCK.getClientOf(SOME_CLIENT_ID)).andReturn(of(new OpenIdConnectClient
            (SOME_CLIENT_ID, SOME_CLIENT_NAME, null, null, null)));
    replayMocks();
    try {
      response = RESOURCES_UT.client().target(targetURI).request().post(form(form));
      verifyMocks();
      final List<String> responseBodyLines = checkStatusCodeAndMediaTypeProvidingBodyLines(sessionId, response);
      assertSomeLineContainsEditableDomainIdField(responseBodyLines);
    } finally {
      closeQuietly(response);
    }
  }

  @Test
  public void issueOpenIdAuthenticationRequestHavingSpaceInScopeVariable() throws IOException {
    final String scopeWithSpaces = "scope with spaces";
    final URI targetURI = fromUri("/login").build();
    final Form form = new Form().param("response_type", SOME_RESPONSE_TYPE_VALUE).param("scope", scopeWithSpaces)
            .param("client_id", SOME_CLIENT_ID_VALUE).param("state", SOME_STATE_VALUE)
            .param("redirect_uri", SOME_REDIRECT_URI_VALUE);
    final SessionId sessionId = new SessionId("session-id");
    final InitiateSessionResponse initiateSessionResponse = new AnyResponseImpl("{ \"type\":\"auth\", \"sid\":\""
            + sessionId + "\", \"display\":\"page\", \"select_account\":" + NO_SELECT_ACCOUNT + " }");
    expect(AUTH_SESSION_MOCK.initiateSession(
            fromUri("response_type={1}&scope={2}&client_id={3}&state={4}&redirect_uri={5}").build(SOME_RESPONSE_TYPE_VALUE,
                    scopeWithSpaces.replace(' ', '+'), SOME_CLIENT_ID_VALUE, SOME_STATE_VALUE, SOME_REDIRECT_URI_VALUE),
            NO_SUBJECT_SESSION_ID, NO_REASONS_OF_CLAIMS)).andReturn(initiateSessionResponse);
    expect(OPEN_ID_CONNECT_CLIENT_REGISTRATION_MOCK.getClientOf(SOME_CLIENT_ID)).andReturn(of(new OpenIdConnectClient
            (SOME_CLIENT_ID, SOME_CLIENT_NAME, null, null, null)));
    replayMocks();
    try {
      response = RESOURCES_UT.client().target(targetURI).request().post(form(form));
      verifyMocks();
      final List<String> responseBodyLines = checkStatusCodeAndMediaTypeProvidingBodyLines(sessionId, response);
      assertSomeLineContainsEditableDomainIdField(responseBodyLines);
    } finally {
      closeQuietly(response);
    }
  }

  @Test
  public void issueOpenIdAuthenticationRequestWithPromptNONE() throws IOException {
    final URI targetURI = fromUri("/login").build();
    final Form form = new Form().param("response_type", SOME_RESPONSE_TYPE_VALUE).param("scope", SOME_SCOPE_VALUE)
            .param("client_id", SOME_CLIENT_ID_VALUE).param("state", SOME_STATE_VALUE)
            .param("redirect_uri", SOME_REDIRECT_URI_VALUE).param("prompt", "none");
    final InitiateSessionResponse initiateSessionResponse = new AnyResponseImpl(
            "{ \"type\":\"response\", \"mode\":\"query\", \"parameters\": { \"uri\":\"http://redirect-uri.test/error?error=login_required&error_description=Login+required&state="
                    + SOME_STATE_VALUE + "\" } }");
    expect(AUTH_SESSION_MOCK.initiateSession(
            fromUri("response_type={1}&scope={2}&client_id={3}&state={4}&redirect_uri={5}&prompt={6}").build(
                    SOME_RESPONSE_TYPE_VALUE, SOME_SCOPE_VALUE, SOME_CLIENT_ID_VALUE, SOME_STATE_VALUE, SOME_REDIRECT_URI_VALUE,
                    "none"),
            NO_SUBJECT_SESSION_ID, NO_REASONS_OF_CLAIMS)).andReturn(initiateSessionResponse);
    expect(OPEN_ID_CONNECT_CLIENT_REGISTRATION_MOCK.getClientOf(SOME_CLIENT_ID)).andReturn(of(new OpenIdConnectClient
            (SOME_CLIENT_ID, SOME_CLIENT_NAME, null, null, null)));
    replayMocks();
    try {
      response = RESOURCES_UT.client().target(targetURI).property(FOLLOW_REDIRECTS, false).request()
              .post(form(form));
      verifyMocks();
      assertEquals("Response status code", SEE_OTHER, response.getStatusInfo());
      final String responseBody = join(readLines((InputStream) response.getEntity(), "ASCII"), "");
      assertTrue("Empty response body", responseBody.isEmpty());
    } finally {
      closeQuietly(response);
    }
  }

  @Test
  public void issueOpenIdAuthenticationRequestWithIdTokenHint() throws IOException {
    final URI targetURI = fromUri("/login").build();
    final Form form = new Form().param("response_type", SOME_RESPONSE_TYPE_VALUE).param("scope", SOME_SCOPE_VALUE)
            .param("client_id", SOME_CLIENT_ID_VALUE).param("state", SOME_STATE_VALUE)
            .param("redirect_uri", SOME_REDIRECT_URI_VALUE).param("id_token_hint", SOME_ID_TOKEN_HINT);
    final InitiateSessionResponse initiateSessionResponse = new AnyResponseImpl(
            "{ \"type\":\"response\", \"mode\":\"query\", \"parameters\": { \"uri\":\"http://redirect-uri.test/error?error=login_required&error_description=Login+required&state="
                    + SOME_STATE_VALUE + "\" } }");
    expect(AUTH_SESSION_MOCK.initiateSession(
            fromUri("response_type={1}&scope={2}&client_id={3}&state={4}&redirect_uri={5}&id_token_hint={6}").build(
                    SOME_RESPONSE_TYPE_VALUE, SOME_SCOPE_VALUE, SOME_CLIENT_ID_VALUE, SOME_STATE_VALUE, SOME_REDIRECT_URI_VALUE,
                    SOME_ID_TOKEN_HINT),
            NO_SUBJECT_SESSION_ID, NO_REASONS_OF_CLAIMS)).andReturn(initiateSessionResponse);
    expect(OPEN_ID_CONNECT_CLIENT_REGISTRATION_MOCK.getClientOf(SOME_CLIENT_ID)).andReturn(of(new OpenIdConnectClient
            (SOME_CLIENT_ID, SOME_CLIENT_NAME, null, null, null)));
    replayMocks();
    try {
      response = RESOURCES_UT.client().target(targetURI).property(FOLLOW_REDIRECTS, false).request()
              .post(form(form));
      verifyMocks();
      assertEquals("Response status code", SEE_OTHER, response.getStatusInfo());
      final String responseBody = join(readLines((InputStream) response.getEntity(), "ASCII"), "");
      assertTrue("Empty response body", responseBody.isEmpty());
    } finally {
      closeQuietly(response);
    }
  }

  @Test
  public void issueOpenIdAuthenticationRequestTargettingUnknownClientID() throws IOException {
    final URI targetURI = fromUri("/login").build();
    final ClientId unknownClientId = new ClientId("mismatchingClientID");
    final Form form = new Form().param("response_type", SOME_RESPONSE_TYPE_VALUE).param("scope", SOME_SCOPE_VALUE)
            .param("client_id", unknownClientId.getPlain()).param("state", SOME_STATE_VALUE)
            .param("redirect_uri", SOME_REDIRECT_URI_VALUE);
    final InitiateSessionResponse initiateSessionResponse = new AnyResponseImpl(
            "{\"type\":\"error\",\"error\":\"invalid_request\",\"error_description\":\"No such client ID: "
                    + unknownClientId + "\"}");
    expect(AUTH_SESSION_MOCK.initiateSession(
            fromUri("response_type={1}&scope={2}&client_id={3}&state={4}&redirect_uri={5}").build(SOME_RESPONSE_TYPE_VALUE,
                    SOME_SCOPE_VALUE, unknownClientId, SOME_STATE_VALUE, SOME_REDIRECT_URI_VALUE),
            NO_SUBJECT_SESSION_ID, NO_REASONS_OF_CLAIMS)).andReturn(initiateSessionResponse);
    expect(OPEN_ID_CONNECT_CLIENT_REGISTRATION_MOCK.getClientOf(unknownClientId)).andReturn(empty());
    replayMocks();
    try {
      response = RESOURCES_UT.client().target(targetURI).request().post(form(form));
      verifyMocks();
      assertEquals("Response status code", BAD_REQUEST, response.getStatusInfo());
      System.out.println(IOUtils.toString((InputStream) response.getEntity(), UTF8));
    } finally {
      closeQuietly(response);
    }
  }

  @Test
  public void issueOpenIdAuthenticationRequestTargettingClientWithoutNameField() throws IOException {
    final URI targetURI = fromUri("/login?response_type={1}&scope={2}&client_id={3}&state={4}&redirect_uri={5}").build(
            SOME_RESPONSE_TYPE_VALUE, SOME_SCOPE_VALUE, SOME_CLIENT_ID_VALUE, SOME_STATE_VALUE, SOME_REDIRECT_URI_VALUE);
    final SessionId sessionId = new SessionId("session-id");
    final InitiateSessionResponse initiateSessionResponse = new AnyResponseImpl("{ \"type\":\"auth\", \"sid\":\""
            + sessionId + "\", \"display\":\"page\", \"select_account\":" + NO_SELECT_ACCOUNT + " }");
    expect(AUTH_SESSION_MOCK.initiateSession(URI.create(targetURI.getQuery()), NO_SUBJECT_SESSION_ID,
            NO_REASONS_OF_CLAIMS)).andReturn(initiateSessionResponse);
    expect(OPEN_ID_CONNECT_CLIENT_REGISTRATION_MOCK.getClientOf(SOME_CLIENT_ID)).andReturn(of(new OpenIdConnectClient
            (SOME_CLIENT_ID, null, null, null, null)));
    replayMocks();
    try {
      response = RESOURCES_UT.client().target(targetURI).request().get();
      verifyMocks();
      final List<String> responseBodyLines = checkStatusCodeAndMediaTypeProvidingBodyLines(sessionId, response);
      responseBodyLines.forEach(System.out::println);
      assertSomeLineContainsEditableDomainIdField(responseBodyLines);
    } finally {
      closeQuietly(response);
    }
  }

  @Test
  public void issueOpenIdAuthenticationRequestMissingResponseTypeParameter() throws IOException {
    final URI targetURI = fromUri("/login").build();
    final Form form = new Form().param("scope", SOME_SCOPE_VALUE).param("client_id", SOME_CLIENT_ID_VALUE)
            .param("state", SOME_STATE_VALUE).param("redirect_uri", SOME_REDIRECT_URI_VALUE);
    replayMocks();
    try {
      response = RESOURCES_UT.client().target(targetURI).request().post(form(form));
      verifyMocks();
      assertEquals("Response status code", BAD_REQUEST, response.getStatusInfo());
      System.out.println(IOUtils.toString((InputStream) response.getEntity(), UTF8));
    } finally {
      closeQuietly(response);
    }
  }

  @Test
  public void issueOpenIdAuthenticationRequestHavingSpaceInStateVariable() throws IOException {
    final String stateWithSpaces = "state with spaces";
    final URI targetURI = fromUri("/login").build();
    final Form form = new Form().param("response_type", SOME_RESPONSE_TYPE_VALUE).param("scope", SOME_SCOPE_VALUE)
            .param("client_id", SOME_CLIENT_ID_VALUE).param("state", stateWithSpaces)
            .param("redirect_uri", SOME_REDIRECT_URI_VALUE);
    final SessionId sessionId = new SessionId("session-id");
    final InitiateSessionResponse initiateSessionResponse = new AnyResponseImpl("{ \"type\":\"auth\", \"sid\":\""
            + sessionId + "\", \"display\":\"page\", \"select_account\":" + NO_SELECT_ACCOUNT + " }");
    expect(AUTH_SESSION_MOCK.initiateSession(
            fromUri("response_type={1}&scope={2}&client_id={3}&state={4}&redirect_uri={5}").build(SOME_RESPONSE_TYPE_VALUE,
                    SOME_SCOPE_VALUE, SOME_CLIENT_ID_VALUE, stateWithSpaces.replace(' ', '+'), SOME_REDIRECT_URI_VALUE),
            NO_SUBJECT_SESSION_ID, NO_REASONS_OF_CLAIMS)).andReturn(initiateSessionResponse);
    expect(OPEN_ID_CONNECT_CLIENT_REGISTRATION_MOCK.getClientOf(SOME_CLIENT_ID)).andReturn(of(new OpenIdConnectClient
            (SOME_CLIENT_ID, SOME_CLIENT_NAME, null, null, null)));
    replayMocks();
    try {
      response = RESOURCES_UT.client().target(targetURI).request().post(form(form));
      verifyMocks();
      final List<String> responseBodyLines = checkStatusCodeAndMediaTypeProvidingBodyLines(sessionId, response);
      assertSomeLineContainsEditableDomainIdField(responseBodyLines);
    } finally {
      closeQuietly(response);
    }
  }

  @Test
  public void issueOpenIdAuthenticationRequestWithSubjectSessionIdCookie() throws IOException {
    final URI targetURI = fromUri("/login").build();
    final Form form = new Form().param("response_type", SOME_RESPONSE_TYPE_VALUE).param("scope", SOME_SCOPE_VALUE)
            .param("client_id", SOME_CLIENT_ID_VALUE).param("state", SOME_STATE_VALUE)
            .param("redirect_uri", SOME_REDIRECT_URI_VALUE);
    final SessionId sessionId = new SessionId("session-id");
    final InitiateSessionResponse initiateSessionResponse = new AnyResponseImpl("{ \"type\":\"auth\", \"sid\":\""
            + sessionId + "\", \"display\":\"page\", \"select_account\":" + NO_SELECT_ACCOUNT + " }");
    final SubjectSessionId subjectSessionId = new SubjectSessionId("SUBJECT_SESSION_ID");
    expect(AUTH_SESSION_MOCK.initiateSession(
            fromUri("response_type={1}&scope={2}&client_id={3}&state={4}&redirect_uri={5}").build(SOME_RESPONSE_TYPE_VALUE,
                    SOME_SCOPE_VALUE, SOME_CLIENT_ID_VALUE, SOME_STATE_VALUE, SOME_REDIRECT_URI_VALUE),
            subjectSessionId, NO_REASONS_OF_CLAIMS)).andReturn(initiateSessionResponse);
    expect(OPEN_ID_CONNECT_CLIENT_REGISTRATION_MOCK.getClientOf(SOME_CLIENT_ID)).andReturn(of(new OpenIdConnectClient
            (SOME_CLIENT_ID, SOME_CLIENT_NAME, null, null, null)));
    replayMocks();
    try {
      response = RESOURCES_UT.client().target(targetURI).request()
              .cookie("sub_sid_current", "sub_sid_042").cookie("sub_sid_042", subjectSessionId.getPlain()).post(form
                      (form));
      verifyMocks();
      assertNoNewCookies(response);
      final List<String> responseBodyLines = checkStatusCodeAndMediaTypeProvidingBodyLines(sessionId, response);
      assertSomeLineContainsEditableDomainIdField(responseBodyLines);
    } finally {
      closeQuietly(response);
    }
  }

  @Test
  public void selectAccountWithMultipleSubjectSessionIdCookies() throws IOException {
    final URI targetURI = fromUri("/login").build();
    final Form form = new Form().param("response_type", SOME_RESPONSE_TYPE_VALUE).param("scope", SOME_SCOPE_VALUE)
            .param("client_id", SOME_CLIENT_ID_VALUE).param("state", SOME_STATE_VALUE)
            .param("redirect_uri", SOME_REDIRECT_URI_VALUE).param("prompt", SELECT_ACCOUNT);
    final SessionId sessionId = new SessionId("session-id");
    final InitiateSessionResponse initiateSessionResponse = new AnyResponseImpl("{ \"type\":\"auth\", \"sid\":\""
            + sessionId + "\", \"display\":\"page\", \"" + SELECT_ACCOUNT + "\":" + DO_SELECT_ACCOUNT + " }");
    final SubjectSessionId subjectSessionId = new SubjectSessionId("SUBJECT_SESSION_ID");
    final SubjectSessionId otherSubjSessionId = new SubjectSessionId("otherSubjectSessionID");
    expect(AUTH_SESSION_MOCK.initiateSession(
            fromUri("response_type={1}&scope={2}&client_id={3}&state={4}&redirect_uri={5}&prompt={6}").build(
                    SOME_RESPONSE_TYPE_VALUE, SOME_SCOPE_VALUE, SOME_CLIENT_ID_VALUE, SOME_STATE_VALUE, SOME_REDIRECT_URI_VALUE,
                    SELECT_ACCOUNT),
            subjectSessionId, NO_REASONS_OF_CLAIMS)).andReturn(initiateSessionResponse);
    expect(SUBJECT_SESSION_STORE_MOCK.sessionsOf(asList(otherSubjSessionId, subjectSessionId))).andReturn(null);
    expect(OPEN_ID_CONNECT_CLIENT_REGISTRATION_MOCK.getClientOf(SOME_CLIENT_ID)).andReturn(of(new OpenIdConnectClient
            (SOME_CLIENT_ID, SOME_CLIENT_NAME, null, null, null)));
    replayMocks();
    try {
      response = RESOURCES_UT.client().target(targetURI).request()
              .cookie("sub_sid_current", "sub_sid_042").cookie("sub_sid_042", subjectSessionId.getPlain())
              .cookie("sub_sid_024", otherSubjSessionId.getPlain()).post(form(form));
      verifyMocks();
      assertNoNewCookies(response);
      final List<String> responseBodyLines = checkStatusCodeAndMediaTypeProvidingBodyLines(sessionId, response);
      assertSomeLineContainsEditableDomainIdField(responseBodyLines);
    } finally {
      closeQuietly(response);
    }
  }

  @Test
  public void promptForSelectAccountButNoCurrentSessions() throws IOException {
    final URI targetURI = fromUri("/login").build();
    final Form form = new Form().param("response_type", SOME_RESPONSE_TYPE_VALUE).param("scope", SOME_SCOPE_VALUE)
            .param("client_id", SOME_CLIENT_ID_VALUE).param("state", SOME_STATE_VALUE)
            .param("redirect_uri", SOME_REDIRECT_URI_VALUE).param("prompt", SELECT_ACCOUNT);
    final SessionId sessionId = new SessionId("session-id");
    final InitiateSessionResponse initiateSessionResponse = new AnyResponseImpl("{ \"type\":\"auth\", \"sid\":\""
            + sessionId + "\", \"display\":\"page\", \"select_account\":" + DO_SELECT_ACCOUNT + " }");
    final SubjectSessionId subjectSessionId = new SubjectSessionId("SUBJECT_SESSION_ID");
    final SubjectSessionId otherSubjSessionId = new SubjectSessionId("otherSubjectSessionID");
    expect(AUTH_SESSION_MOCK.initiateSession(
            fromUri("response_type={1}&scope={2}&client_id={3}&state={4}&redirect_uri={5}&prompt={6}").build(
                    SOME_RESPONSE_TYPE_VALUE, SOME_SCOPE_VALUE, SOME_CLIENT_ID_VALUE, SOME_STATE_VALUE, SOME_REDIRECT_URI_VALUE,
                    SELECT_ACCOUNT),
            subjectSessionId, NO_REASONS_OF_CLAIMS)).andReturn(initiateSessionResponse);
    expect(SUBJECT_SESSION_STORE_MOCK.sessionsOf(asList(otherSubjSessionId, subjectSessionId))).andReturn(null);
    expect(OPEN_ID_CONNECT_CLIENT_REGISTRATION_MOCK.getClientOf(SOME_CLIENT_ID)).andReturn(of(new OpenIdConnectClient
            (SOME_CLIENT_ID, SOME_CLIENT_NAME, null, null, null)));
    replayMocks();
    try {
      response = RESOURCES_UT.client().target(targetURI).request()
              .cookie("sub_sid_current", "sub_sid_042").cookie("sub_sid_042", subjectSessionId.getPlain())
              .cookie("sub_sid_24", otherSubjSessionId.getPlain()).post(form(form));
      verifyMocks();
      assertNoNewCookies(response);
      final List<String> responseBodyLines = checkStatusCodeAndMediaTypeProvidingBodyLines(sessionId, response);
      assertNoLineContains(responseBodyLines, "Hidden field to POST known Subject Session ID",
              "<input type=\"hidden\" name=\"subjectSessionID\" value=\"" + subjectSessionId + "\">");
      assertSomeLineContainsEditableDomainIdField(responseBodyLines);
    } finally {
      closeQuietly(response);
    }
  }

  @Test
  public void promptForSelectAccountWithOneCurrentSession() throws IOException {
    final URI targetURI = fromUri("/login").build();
    final Form form = new Form().param("response_type", SOME_RESPONSE_TYPE_VALUE).param("scope", SOME_SCOPE_VALUE)
            .param("client_id", SOME_CLIENT_ID_VALUE).param("state", SOME_STATE_VALUE)
            .param("redirect_uri", SOME_REDIRECT_URI_VALUE).param("prompt", SELECT_ACCOUNT);
    final SessionId sessionId = new SessionId("session-id");
    final InitiateSessionResponse initiateSessionResponse = new AnyResponseImpl("{ \"type\":\"auth\", \"sid\":\""
            + sessionId + "\", \"display\":\"page\", \"select_account\":" + DO_SELECT_ACCOUNT + " }");
    final SubjectSessionId subjectSessionId = new SubjectSessionId("SUBJECT_SESSION_ID");
    final SubjectSessionId otherSubjSessionId = new SubjectSessionId("otherSubjectSessionID");
    expect(AUTH_SESSION_MOCK.initiateSession(
            fromUri("response_type={1}&scope={2}&client_id={3}&state={4}&redirect_uri={5}&prompt={6}").build(
                    SOME_RESPONSE_TYPE_VALUE, SOME_SCOPE_VALUE, SOME_CLIENT_ID_VALUE, SOME_STATE_VALUE, SOME_REDIRECT_URI_VALUE,
                    SELECT_ACCOUNT),
            subjectSessionId, NO_REASONS_OF_CLAIMS)).andReturn(initiateSessionResponse);
    final SubjectSession knownSubjectSession = new SubjectSession(new PseudonymSubject(SOME_LOGIN_HINT), subjectSessionId,
            1_000_000L, 1_500_000L, 120, 60, 30, null, NO_CLAIMS, NO_ADDITIONAL_DATA);
    expect(SUBJECT_SESSION_STORE_MOCK.sessionsOf(asList(otherSubjSessionId, subjectSessionId)))
            .andReturn(singletonList(knownSubjectSession));
    expect(OPEN_ID_CONNECT_CLIENT_REGISTRATION_MOCK.getClientOf(SOME_CLIENT_ID)).andReturn(of(new OpenIdConnectClient
            (SOME_CLIENT_ID, SOME_CLIENT_NAME, null, null, null)));
    replayMocks();
    try {
      response = RESOURCES_UT.client().target(targetURI).request()
              .cookie("sub_sid_current", "sub_sid_042").cookie("sub_sid_042", subjectSessionId.getPlain())
              .cookie("sub_sid_24", otherSubjSessionId.getPlain()).post(form(form));
      verifyMocks();
      assertNoNewCookies(response);
      final List<String> responseBodyLines = checkStatusCodeAndMediaTypeProvidingBodyLines(sessionId, response);
      responseBodyLines.forEach(System.out::println);
      assertSomeLineContains(responseBodyLines, "Hidden field to POST known Subject Session ID",
              "<input type=\"hidden\" name=\"subjectSessionID\" value=\"" + subjectSessionId + "\">");
      assertSomeLineContainsEditableDomainIdField(responseBodyLines);
    } finally {
      closeQuietly(response);
    }
  }

  private void assertNoNewCookies(final Response response) {
    assertTrue("No new cookies", response.getCookies().isEmpty());
  }

  private void assertSomeLineContainsEditableDomainIdField(final List<String> responseBodyLines) {
    assertSomeLineContains(responseBodyLines, "Editable field 'domainID'",
            "<input class=\"w3-input\" type=\"text\" id=\"domainID\" name=\"domainID\" maxlength=\"40\" " +
                    "placeholder=\"my.ID4me.example\" autofocus=\"on\"");
  }

  private List<String> checkStatusCodeAndMediaTypeProvidingBodyLines(final SessionId sessionId, final Response response)
          throws IOException {
    assertEquals("Response status code", 200, response.getStatusInfo().getStatusCode());
    assertEquals("Response media type", TEXT_HTML_CHARSET_UTF8_TYPE, response.getMediaType());
    final List<String> responseBodyLines = readLines((InputStream) response.getEntity(), "ASCII");
    assertSomeLineContains(responseBodyLines, "Hidden field containing session ID",
            "<input type=\"hidden\" name=\"sessionID\" value=\"" + sessionId + "\"/>");
    return responseBodyLines;
  }

  private void assertSomeLineContains(final List<String> responseEntityLines, final String description,
                                      final String containedContent) {
    assertTrue(description,
            responseEntityLines.stream().anyMatch(line -> line.contains(containedContent)));
  }

  private void assertNoLineContains(final List<String> responseEntityLines, final String description,
                                    final String containedContent) {
    assertFalse(description,
            responseEntityLines.stream().anyMatch(line -> line.contains(containedContent)));
  }

  private void verifyMocks() {
    verify(AUTH_SESSION_MOCK, SUBJECT_SESSION_STORE_MOCK, AUTH_STORE_MOCK, USER_AUTHENTICATION_DAO_MOCK, OPEN_ID_CONNECT_CLIENT_REGISTRATION_MOCK);
  }

  private void replayMocks() {
    replay(AUTH_SESSION_MOCK, SUBJECT_SESSION_STORE_MOCK, AUTH_STORE_MOCK, USER_AUTHENTICATION_DAO_MOCK, OPEN_ID_CONNECT_CLIENT_REGISTRATION_MOCK);
  }

}
