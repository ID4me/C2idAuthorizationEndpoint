/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.identityagent.impl;

import de.denic.domainid.DomainId;
import de.denic.domainid.connect2id.common.ClientId;
import de.denic.domainid.subject.OpenIdSubject;
import de.denic.domainid.subject.PseudonymSubject;
import org.junit.Test;

import java.net.URI;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import static org.junit.Assert.assertEquals;

public class AuthenticationRequestTest {

  @Test
  public void testAuthenticationTimestampSerialisation() {
    final DomainId usersDomainId = new DomainId("domain-id.test");
    final OpenIdSubject subject = new PseudonymSubject("foo");
    final ClientId clientId = new ClientId("client-id-value");
    final boolean successfullyAuthenticated = true;
    final ZonedDateTime timestampOfAuthentication = ZonedDateTime.of(2017, 7, 11, 11, 47, 53, 0, ZoneId.of("UTC")); // parse("2017-07-11T11:47:53Z");
    final URI issuerURI = URI.create("http://some-issuer-uri.test");
    {
      final AuthenticationRequest CUT = new AuthenticationRequest(usersDomainId, subject, clientId,
              successfullyAuthenticated, timestampOfAuthentication, issuerURI);
      assertEquals("Serialized authentication timestamp", "2017-07-11T11:47:53Z", CUT.getTimestampOfAuthentication());
    }
  }

}
