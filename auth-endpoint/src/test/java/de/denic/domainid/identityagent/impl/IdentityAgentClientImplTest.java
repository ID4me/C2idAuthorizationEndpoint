/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.identityagent.impl;

import de.denic.domainid.DomainId;
import de.denic.domainid.authendpoint.config.Base64KeyMaterialConfigTest;
import de.denic.domainid.connect2id.common.ClientId;
import de.denic.domainid.identityagent.IdentityAgentClient;
import de.denic.domainid.identityagent.RequestUUIDGenerator;
import de.denic.domainid.jwks.KeyID;
import de.denic.domainid.subject.OpenIdSubject;
import de.denic.domainid.subject.PseudonymSubject;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.Key;
import java.time.ZonedDateTime;
import java.util.UUID;

import static java.time.ZonedDateTime.parse;
import static javax.ws.rs.core.Response.Status.OK;
import static org.easymock.EasyMock.*;

public class IdentityAgentClientImplTest {

  /**
   * @see Base64KeyMaterialConfigTest
   */
  private static final ClientId SOME_CLIENT_ID = new ClientId("some-client-id");
  private static final boolean SUCCESSFUL_AUTHENTICATION = true;
  private static final ZonedDateTime TIMESTAMP_OF_AUTHENTICATION = parse("2017-04-11T09:34:10Z");
  // bastians-xm1.office.denic.de | denic.de
  private static final URI IDENTITY_AGENT_URI = URI.create("//identityagent.de");
  private static final DomainId DOMAIN_ID = new DomainId("test-domain.id");
  private static final UUID UU_ID = UUID.randomUUID();
  private static final URI SOME_ISSUER_URI = URI.create("some-issuer-uri");
  private static final Pair<KeyID, Key> NO_SIGNING_KEY = null;
  private static final OpenIdSubject SUBJECT = new PseudonymSubject("foo");

  private IdentityAgentClient CUT;
  private RequestUUIDGenerator requestUUIDGeneratorMock;
  private Client restClientMock;
  private WebTarget webTargetMock;
  private Builder requestBuilderMock;
  private Response responseMock;

  @Before
  public void setup() {
    requestUUIDGeneratorMock = createMock(RequestUUIDGenerator.class);
    restClientMock = createMock(Client.class);
    webTargetMock = createMock(WebTarget.class);
    requestBuilderMock = createMock(Builder.class);
    responseMock = createMock(Response.class);
  }

  @Test
  public void mockedTest() throws URISyntaxException {
    expect(restClientMock.target(
            new URI("https", IDENTITY_AGENT_URI.getUserInfo(), IDENTITY_AGENT_URI.getHost(), IDENTITY_AGENT_URI.getPort(),
                    IDENTITY_AGENT_URI.getPath(), IDENTITY_AGENT_URI.getQuery(), IDENTITY_AGENT_URI.getFragment())))
            .andReturn(webTargetMock);
    expect(webTargetMock.path("/callback/authentication")).andReturn(webTargetMock);
    expect(requestUUIDGeneratorMock.newUUID()).andReturn(UU_ID);
    expect(webTargetMock.path(UU_ID.toString())).andReturn(webTargetMock);
    expect(webTargetMock.getUri()).andReturn(URI.create("dummy-uri")).times(2);
    expect(webTargetMock.request()).andReturn(requestBuilderMock);
    expect(requestBuilderMock.put(anyObject())).andReturn(responseMock);
    expect(responseMock.getStatus()).andReturn(200);
    expect(responseMock.getStatusInfo()).andReturn(OK).times(2);
    responseMock.close();
    replay(requestUUIDGeneratorMock, restClientMock, webTargetMock, requestBuilderMock, responseMock);
    {
      CUT = new IdentityAgentClientImpl(restClientMock, NO_SIGNING_KEY, SOME_ISSUER_URI, requestUUIDGeneratorMock);
      CUT.authentication(DOMAIN_ID, SUBJECT, IDENTITY_AGENT_URI, TIMESTAMP_OF_AUTHENTICATION, SUCCESSFUL_AUTHENTICATION,
              SOME_CLIENT_ID);
      verify(requestUUIDGeneratorMock, restClientMock, webTargetMock, requestBuilderMock, responseMock);
    }
  }

}
