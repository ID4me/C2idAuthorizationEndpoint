#!/bin/bash

_fullscriptpath="$(readlink -f ${BASH_SOURCE[0]})"
BASEDIR="$(dirname $_fullscriptpath)"

DOCKERREGISTRY=$DOCKER_REGISTRY
DOCKERREGISTRYPATH=test
IMAGENAME=auth-endpoint

KUBECOMMAND="$KUBECTL --kubeconfig=$KUBECONFIG --namespace=$1"
VERSION=$2

sed -i "s#__DOCKER_REGISTRY__#${DOCKERREGISTRY}#" auth-endpoint-dc.yaml
sed -i "s#__IMAGE_VERSION__#${VERSION}#" auth-endpoint-dc.yaml
$KUBECOMMAND apply -f auth-endpoint-dc.yaml --record
$KUBECOMMAND rollout status deployment/auth-endpoint --watch
