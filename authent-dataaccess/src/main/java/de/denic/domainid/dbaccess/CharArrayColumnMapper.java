/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.dbaccess;

import org.jdbi.v3.core.mapper.ColumnMapper;
import org.jdbi.v3.core.statement.StatementContext;

import javax.annotation.concurrent.Immutable;
import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.Reader;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.apache.commons.io.IOUtils.copy;

@Immutable
public final class CharArrayColumnMapper implements ColumnMapper<char[]> {

  public static char[] toCharArray(final Reader characterReader) {
    if (characterReader == null) {
      return null;
    }

    final CharArrayWriter targetWriter = new CharArrayWriter();
    try {
      copy(characterReader, targetWriter);
    } catch (IOException e) {
      throw new RuntimeException("Reading character data from DB result set failed", e);
    }

    return getResultWiping(targetWriter);
  }

  private static char[] getResultWiping(final CharArrayWriter targetWriter) {
    final char[] result = targetWriter.toCharArray();
    targetWriter.reset();
    try {
      targetWriter.write(new char[result.length]);
    } catch (IOException e) {
      throw new RuntimeException("Wiping character data from writer failed", e);
    }

    return result;
  }

  @Override
  public char[] map(final ResultSet resultSet, final int columnNumber, final StatementContext context) throws SQLException {
    return toCharArray(resultSet.getCharacterStream(columnNumber));
  }

  @Override
  public char[] map(final ResultSet resultSet, final String columnLabel, final StatementContext context) throws SQLException {
    return toCharArray(resultSet.getCharacterStream(columnLabel));
  }

}
