/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.dbaccess;

import de.denic.domainid.DomainId;
import de.denic.domainid.subject.PseudonymSubject;
import org.apache.commons.lang3.Validate;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import javax.annotation.concurrent.Immutable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

/**
 * Instances does NOT hold DB field <code>totp_shared_secret</code> for security reasons: Instances will live longer
 * than recommended to operate on this secret!
 */
@Immutable
public final class UserAuthentication {

  public static final boolean ACTIVE = true;
  public static final boolean DEACTIVATED = false;

  private final DomainId userName;
  private final String passwordHexSHA256;
  private final PseudonymSubject pseudonymUserName;
  private final boolean twoFactorAuthEnabled, agentPwResetEnabled, active;

  /**
   * @param userName             Required
   * @param passwordHexSHA256    Required
   * @param twoFactorAuthEnabled Required
   * @param pseudonymUserName    Required
   * @param active               Required
   */
  public UserAuthentication(final DomainId userName, final String passwordHexSHA256, final boolean twoFactorAuthEnabled, final boolean agentPwResetEnabled,
      final PseudonymSubject pseudonymUserName, final boolean active) {
    Validate.notNull(userName, "Missing user name");
    Validate.notNull(pseudonymUserName, "Missing pseudonym user name");
    this.userName = userName;
    this.pseudonymUserName = pseudonymUserName;
    this.passwordHexSHA256 = passwordHexSHA256;
    this.twoFactorAuthEnabled = twoFactorAuthEnabled;
    this.agentPwResetEnabled = agentPwResetEnabled;
    this.active = active;
  }

  /**
   * @return Never <code>null</code> nor empty
   */
  public DomainId getUserName() {
    return userName;
  }

  /**
   * @return Never <code>null</code> nor empty
   */
  public String getPasswordHexSHA256() {
    return passwordHexSHA256;
  }

  public boolean isTwoFactorAuthEnabled() {
    return twoFactorAuthEnabled;
  }

  public boolean isAgentPwResetEnabled() {
    return agentPwResetEnabled;
  }

  /**
   * @return Never <code>null</code> nor empty
   */
  public PseudonymSubject getPseudonymUserName() {
    return pseudonymUserName;
  }

  public boolean isActive() {
    return active;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;
    UserAuthentication that = (UserAuthentication) o;
    return twoFactorAuthEnabled == that.twoFactorAuthEnabled && agentPwResetEnabled == that.agentPwResetEnabled && active == that.active
        && Objects.equals(userName, that.userName) && Objects.equals(passwordHexSHA256, that.passwordHexSHA256)
        && Objects.equals(pseudonymUserName, that.pseudonymUserName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(userName, passwordHexSHA256, pseudonymUserName, twoFactorAuthEnabled, active);
  }

  /**
   */
  @Override
  public String toString() {
    return (active ? "'" : "INACTIVE '") + userName + "', password hash " + passwordHexSHA256 + ", 2FA enabled? " + isTwoFactorAuthEnabled() + ", pseudonym '"
        + pseudonymUserName + "'";
  }

  public static class Mapper implements RowMapper<UserAuthentication> {

    @Override
    public UserAuthentication map(final ResultSet resultSet, final StatementContext context) throws SQLException {
      final boolean twoFactorAuthEnabled = (resultSet.getObject("totp_shared_secret_length") != null);
      return new UserAuthentication(new DomainId(resultSet.getString("user_name")), resultSet.getString("password_hex_sha256"), twoFactorAuthEnabled,
          resultSet.getBoolean("agent_pw_reset_enabled"), new PseudonymSubject(resultSet.getString("pseudonym_user_name")), resultSet.getBoolean("active"));
    }

  }
}
