/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.dbaccess;

import de.denic.domainid.DomainId;
import de.denic.domainid.subject.PseudonymSubject;
import org.apache.commons.codec.digest.DigestUtils;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.internal.util.jdbc.DriverDataSource;
import org.jdbi.v3.sqlobject.SingleValue;
import org.jdbi.v3.sqlobject.config.RegisterColumnMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.statement.UseRowMapper;

import javax.sql.DataSource;
import java.util.List;

import static org.apache.commons.codec.binary.Base64.encodeBase64String;
import static org.apache.commons.lang3.RandomStringUtils.random;

/**
 * Implemented applying <a href="http://jdbi.org/jdbi2/sql_object_overview/">JDBI SQL Object API</a>.
 */
public interface UserAuthenticationDAO {

        DigestUtils SHA_384_DIGEST = new DigestUtils(DigestUtils.getSha384Digest());

        default void migrateDatabaseIfNecessaryApplying(final String driverClass, final String dbUrl, final String dbUser, final String dbUserPassword) {
                migrateDatabaseIfNecessaryApplying(new DriverDataSource(getClass().getClassLoader(), driverClass, dbUrl, dbUser, dbUserPassword));
        }

        /**
         * @param dataSource Required
         */
        default void migrateDatabaseIfNecessaryApplying(final DataSource dataSource) {
                final Flyway flyway = new Flyway();
                flyway.setDataSource(dataSource);
                flyway.migrate();
        }

        /**
         * @param userName            Required
         * @param passwordAsHexSHA256 Required
         * @return Count of INSERTs
         */
        default int insertEndUserIdentity(final DomainId userName, final String passwordAsHexSHA256) {
                final String pseudonymUserName = encodeBase64String(SHA_384_DIGEST.digest(userName + random(32)));
                return insertEndUserIdentity(userName, passwordAsHexSHA256, pseudonymUserName);
        }

        /**
         * @param userName            Required
         * @param passwordAsHexSHA256 Required
         * @param pseudonymUserName   Required
         * @return Count of INSERTs
         */
        @SqlUpdate("INSERT INTO \"public\".\"user_authentication\" (user_name, password_hex_sha256, active, pseudonym_user_name) "
                        + "VALUES (:domainId.name, :passwordHash, true, :pseudonymUserName)")
        int insertEndUserIdentity(@BindBean("domainId") DomainId userName, @Bind("passwordHash") String passwordAsHexSHA256,
                        @Bind("pseudonymUserName") String pseudonymUserName);

        /**
         * @see #insertEndUserIdentity(DomainId, String)
         */
        @SqlUpdate("UPDATE \"public\".\"user_authentication\" SET password_hex_sha256 = :passwordHash WHERE user_name = :domainId.name")
        int updateUserAuthentication(@BindBean("domainId") DomainId userName, @Bind("passwordHash") String passwordAsHexSHA256);

        /**
         * @param userName         Required
         * @param totpSharedSecret Optional
         */
        @SqlUpdate("UPDATE \"public\".\"user_authentication\" SET totp_shared_secret = :totpSharedSecret WHERE user_name = :domainId.name")
        int updateTOTPSharedSecret(@BindBean("domainId") DomainId userName, @Bind("totpSharedSecret") String totpSharedSecret);

        /**
         * @param userName         Required
         * @param totpSharedSecret Optional
         */
        @SqlUpdate("UPDATE \"public\".\"user_authentication\" SET agent_pw_reset_enabled = :agentPwResetEnabled WHERE user_name = :domainId.name")
        int updateAgentPwResetEnabled(@BindBean("domainId") DomainId userName, @Bind("agentPwResetEnabled") boolean agentPwResetEnabled);

        /**
         * @param userName Required
         * @return Optional
         */
        @SqlQuery("SELECT totp_shared_secret FROM \"public\".\"user_authentication\" WHERE user_name = :domainId.name")
        @RegisterColumnMapper(CharArrayColumnMapper.class)
        @SingleValue
        char[] selectTOTPSharedSecretOf(@BindBean("domainId") DomainId userName);

        /**
         * @param userName Required
         */
        @SqlQuery("SELECT active FROM \"public\".\"user_authentication\" WHERE user_name = :domainId.name")
        boolean selectActiveOf(@BindBean("domainId") DomainId userName);

        /**
         * @param userName Required
         */
        @SqlUpdate("UPDATE \"public\".\"user_authentication\" SET active = :active WHERE user_name = :domainId.name")
        int updateActive(@BindBean("domainId") DomainId userName, @Bind("active") boolean active);

        @SqlUpdate("DELETE FROM \"public\".\"user_authentication\" WHERE user_name = :domainId.name")
        int removeByUserName(@BindBean("domainId") DomainId userName);

        @SqlQuery("SELECT user_name, password_hex_sha256, active, agent_pw_reset_enabled, pseudonym_user_name, {fn length(totp_shared_secret)} AS totp_shared_secret_length FROM "
                        + "\"public\".\"user_authentication\" WHERE user_name = :domainId.name")
        @UseRowMapper(UserAuthentication.Mapper.class)
        UserAuthentication findUserAuthenticationBy(@BindBean("domainId") DomainId userName);

        @SqlQuery("SELECT user_name, password_hex_sha256, active, agent_pw_reset_enabled, pseudonym_user_name, {fn length(totp_shared_secret)} AS totp_shared_secret_length FROM "
                        + "\"public\".\"user_authentication\" WHERE pseudonym_user_name = :pseudonym.name")
        @UseRowMapper(UserAuthentication.Mapper.class)
        List<UserAuthentication> findUserAuthenticationsBy(@BindBean("pseudonym") PseudonymSubject subject);

        @SqlQuery("SELECT user_name FROM \"public\".\"user_authentication\" ORDER BY user_name")
        @RegisterColumnMapper(DomainIdColumnMapper.class)
        List<DomainId> findAllUserNames();

        /**
         * @param firstUserName  Required
         * @param secondUserName Required
         * @return Changed rows count
         */
        @SqlUpdate("UPDATE \"public\".\"user_authentication\" SET pseudonym_user_name = "
                        + "(SELECT pseudonym_user_name from \"public\".\"user_authentication\" WHERE user_name = :firstDomainId.name) "
                        + "WHERE user_name = :secondDomainId.name")
        int updatePseudonymUserNameOfSecondUserToTheOneOfFirstUser(@BindBean("firstDomainId") DomainId firstUserName,
                        @BindBean("secondDomainId") DomainId secondUserName);

        /**
         * Mainly for metric purposes.
         */
        @SqlQuery("SELECT count(*) FROM \"public\".\"user_authentication\"")
        int userAuthenticationCount();

        /**
         * Mainly for metric purposes.
         */
        @SqlQuery("SELECT count(*) FROM \"public\".\"user_authentication\" WHERE totp_shared_secret IS NOT NULL")
        int userAuthenticationWithTOTPCount();

}
