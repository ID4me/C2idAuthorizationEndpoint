/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package db.migration;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.Validate;
import org.flywaydb.core.api.migration.jdbc.JdbcMigration;

import javax.annotation.concurrent.Immutable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.apache.commons.codec.binary.Base64.encodeBase64String;
import static org.apache.commons.lang3.ArrayUtils.isEmpty;
import static org.apache.commons.lang3.ArrayUtils.removeAllOccurences;
import static org.apache.commons.lang3.RandomStringUtils.random;

@Immutable
public final class V4__Add_Column_pseudonymUserName implements JdbcMigration {

  private final DigestUtils sha384Digest = new DigestUtils(DigestUtils.getSha384Digest());

  @Override
  public void migrate(final Connection connection) throws SQLException {
    Validate.notNull(connection, "Missing database connection");
    final boolean initialAutoCommit = connection.getAutoCommit();
    if (initialAutoCommit) {
      connection.setAutoCommit(false);
    }
    try {
      addColumnAsNullable(connection);
      migrateData(connection);
      changeColumnToMandatoryAndUnique(connection);
      connection.commit();
    } catch (SQLException e) {
      connection.rollback();
      throw e;
    } finally {
      if (initialAutoCommit) {
        connection.setAutoCommit(true);
      }
    }
  }

  private void changeColumnToMandatoryAndUnique(final Connection connection) throws SQLException {
    try (final PreparedStatement statement =
                 connection.prepareStatement("ALTER TABLE \"public\".\"user_authentication\" " +
                         "ALTER COLUMN pseudonym_user_name SET NOT NULL")) {
      statement.execute();
    }
    try (final PreparedStatement statement =
                 connection.prepareStatement("ALTER TABLE \"public\".\"user_authentication\" " +
                         "ADD CONSTRAINT unique_pseudonym_user_name UNIQUE (pseudonym_user_name)")) {
      statement.execute();
    }
  }

  private void migrateData(final Connection connection) throws SQLException {
    try (final PreparedStatement batchUpdateStatement = connection.prepareStatement("UPDATE \"public\".\"user_authentication\" " +
            "SET pseudonym_user_name = ? WHERE user_name = ?")) {
      try (final PreparedStatement selectStatement =
                   connection.prepareStatement("SELECT user_name FROM \"public\".\"user_authentication\"")) {
        try (final ResultSet resultSet = selectStatement.executeQuery()) {
          while (resultSet.next()) {
            final String userName = resultSet.getString("user_name");
            final String pseudonymUserName = encodeBase64String(sha384Digest.digest(userName + random(32)));
            batchUpdateStatement.setString(1,
                    pseudonymUserName);
            batchUpdateStatement.setString(2, userName);
            batchUpdateStatement.addBatch();
          }
        }
      }
      final int[] batchUpdates = batchUpdateStatement.executeBatch();
      Validate.validState(isEmpty(removeAllOccurences(batchUpdates, 1)),
              "Batch updates modify single row each: {}", batchUpdates);
    }
  }

  private void addColumnAsNullable(final Connection connection) throws SQLException {
    try (final PreparedStatement statement =
                 connection.prepareStatement("ALTER TABLE \"public\".\"user_authentication\" " +
                         "ADD COLUMN pseudonym_user_name varchar(64) NULL")) {
      statement.execute();
    }
  }

}
