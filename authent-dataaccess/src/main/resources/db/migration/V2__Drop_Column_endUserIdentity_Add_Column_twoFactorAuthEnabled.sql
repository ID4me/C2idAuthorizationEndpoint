ALTER TABLE "public"."user_authentication"
   DROP COLUMN end_user_identity;

ALTER TABLE "public"."user_authentication"
   ADD COLUMN two_factor_auth_enabled BOOL DEFAULT false NOT NULL;