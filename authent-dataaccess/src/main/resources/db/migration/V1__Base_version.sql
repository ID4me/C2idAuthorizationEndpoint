CREATE TABLE "public"."user_authentication" (
   user_name varchar(40) PRIMARY KEY NOT NULL,
   password_hex_sha256 varchar(64) NOT NULL,
   end_user_identity varchar(256) NOT NULL
);

/*
For staging stage ONLY:
INSERT INTO "public"."user_authentication" (user_name,password_hex_sha256,end_user_identity) VALUES
('test.denic.de','59157b9445829b0269a15a3b77900442db1a2af9a5be33b9f350357ef2a3f231','test.denic.de');
*/
