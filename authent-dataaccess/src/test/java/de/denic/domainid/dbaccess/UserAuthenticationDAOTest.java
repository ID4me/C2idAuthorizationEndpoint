/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.dbaccess;

import de.denic.domainid.DomainId;
import org.h2.jdbcx.JdbcDataSource;
import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.sqlobject.SqlObjectPlugin;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

import static de.denic.domainid.dbaccess.UserAuthentication.DEACTIVATED;
import static org.apache.commons.lang3.StringUtils.reverse;
import static org.junit.Assert.*;

public class UserAuthenticationDAOTest {

  // See here: https://stackoverflow.com/questions/5225700/can-i-have-h2-autocreate-a-schema-in-an-in-memory-database
  private static final String TEST_DATABASE_URL = "jdbc:h2:mem:test;" +
          "INIT=CREATE SCHEMA IF NOT EXISTS \"public\"\\;SET SCHEMA \"public\"";

  private static Handle HANDLE;
  private static UserAuthenticationDAO CUT;

  @BeforeClass
  public static void setUpClass() {
    final JdbcDataSource dataSource = new JdbcDataSource();
    dataSource.setURL(TEST_DATABASE_URL);
    final Jdbi jdbi = Jdbi.create(dataSource);
    jdbi.installPlugin(new SqlObjectPlugin());
    HANDLE = jdbi.open();
    CUT = HANDLE.attach(UserAuthenticationDAO.class);
    CUT.migrateDatabaseIfNecessaryApplying(dataSource);
  }

  @AfterClass
  public static void tearDownClass() {
    HANDLE.close();
  }

  @Before
  public void setUp() {
    clearContentOfDatabaseTables();
  }

  @Test
  public void addOneUserIdentity_LoadAllAgain_DeleteThisOne_AgainLoadThemAll() {
    final DomainId userName = new DomainId("userName");
    final String passwordAsHexSHA256 = "passwordSHA256HexHash";
    {
      assertEquals("Setup: Single inserted dataset", 1, CUT.insertEndUserIdentity(userName, passwordAsHexSHA256));
    }
    {
      final List<DomainId> allUserNames = CUT.findAllUserNames();
      assertEquals("Single user name available", 1, allUserNames.size());
      assertEquals("Correct user name", userName, allUserNames.get(0));
    }
    {
      final int count = CUT.removeByUserName(userName);
      assertEquals("Count of removed entities", 1, count);
      clearContentOfDatabaseTables();
    }
  }

  @Test
  public void findUserAuthenticationWithKnownAndUnknownDomainId() {
    final DomainId userName = new DomainId("domain.id");
    final String passwordAsHexSHA256 = "passwordSHA256HexHash";
    assertEquals("Setup: Single inserted dataset", 1, CUT.insertEndUserIdentity(userName, passwordAsHexSHA256));
    {
      final UserAuthentication userAuthWithMatchingDomainId = CUT.findUserAuthenticationBy(userName);
      assertNotNull("Found matching user authentication", userAuthWithMatchingDomainId);
      assertEquals("User authentication's user name", userName, userAuthWithMatchingDomainId.getUserName());
      assertEquals("User authentication's pw hash", passwordAsHexSHA256,
              userAuthWithMatchingDomainId.getPasswordHexSHA256());
    }
    {
      assertNull("No user authentication found", CUT.findUserAuthenticationBy(new DomainId(reverse(userName.getName
              ()))));
    }
  }

  @Test
  public void addOneUserIdentity_UpdateIt_AndCheckUpdatedValue() {
    final DomainId userName = new DomainId("userName");
    final String passwordAsHexSHA256 = "passwordSHA256HexHash";
    {
      assertEquals("Setup: Single inserted dataset", 1, CUT.insertEndUserIdentity(userName, passwordAsHexSHA256));
    }
    final String changedPasswordAsHexSHA256 = reverse(passwordAsHexSHA256);
    {
      assertEquals("Count of updated entities", 1, CUT.updateUserAuthentication(userName, changedPasswordAsHexSHA256));
      assertEquals("Updated password hash", changedPasswordAsHexSHA256,
              CUT.findUserAuthenticationBy(userName).getPasswordHexSHA256());
    }
    {
      assertEquals("No updates with unknown user name", 0,
              CUT.updateUserAuthentication(new DomainId("UNKNOWN_USER_NAME"), changedPasswordAsHexSHA256));
    }
  }

  @Test
  public void read_AndWrite_TOTPSharedSecret() {
    final DomainId userName = new DomainId("userName");
    final String passwordAsHexSHA256 = "passwordSHA256HexHash";
    {
      assertEquals("Single inserted dataset", 1, CUT.insertEndUserIdentity(userName, passwordAsHexSHA256));
      assertFalse("2FA enabled", CUT.findUserAuthenticationBy(userName).isTwoFactorAuthEnabled());
    }
    final String sharedSecret = "shared-secret";
    {
      assertEquals("Count of updated entities", 1, CUT.updateTOTPSharedSecret(userName, sharedSecret));
      assertEquals("Count of updated entities, 2nd time", 1, CUT.updateTOTPSharedSecret(userName, sharedSecret));
    }
    {
      assertArrayEquals("Shared secret read from DB", sharedSecret.toCharArray(), CUT.selectTOTPSharedSecretOf(userName));
      assertTrue("2FA enabled", CUT.findUserAuthenticationBy(userName).isTwoFactorAuthEnabled());
    }
    { // Deleting shared secret:
      assertEquals("Count of updated entities", 1, CUT.updateTOTPSharedSecret(userName, null));
      assertEquals("Count of updated entities, 2nd time", 1, CUT.updateTOTPSharedSecret(userName, null));
    }
    {
      assertNull("Shared secret read from DB", CUT.selectTOTPSharedSecretOf(userName));
      assertFalse("2FA enabled", CUT.findUserAuthenticationBy(userName).isTwoFactorAuthEnabled());
    }
  }

  @Test
  public void addUserIdentity_Read_Update_Read_ActiveFlag() {
    final DomainId userName = new DomainId("userName");
    final String passwordAsHexSHA256 = "passwordSHA256HexHash";
    {
      assertEquals("Setup: Single inserted dataset", 1, CUT.insertEndUserIdentity(userName, passwordAsHexSHA256));
    }
    {
      assertTrue("Active per default", CUT.selectActiveOf(userName));
      assertTrue("Activated also within UserAuthentication", CUT.findUserAuthenticationBy(userName).isActive());
    }
    {
      assertEquals("Count of updated rows", 1,
              CUT.updateActive(userName, DEACTIVATED));
      assertFalse("Deactivated after update", CUT.selectActiveOf(userName));
      assertFalse("Deactivated also within UserAuthentication", CUT.findUserAuthenticationBy(userName).isActive());
    }
  }

  private void clearContentOfDatabaseTables() {
    HANDLE.execute("DELETE FROM \"public\".\"user_authentication\"");
    assertTrue("Empty database", CUT.findAllUserNames().isEmpty());
  }

}
