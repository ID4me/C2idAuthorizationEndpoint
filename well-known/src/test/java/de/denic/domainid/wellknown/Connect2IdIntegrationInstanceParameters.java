/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.wellknown;

import java.net.MalformedURLException;
import java.net.URL;

import static org.apache.commons.lang3.SystemUtils.USER_DIR;

/**
 * Just to hold all parameters to target integrations tests against public Connect2Id instance interface
 * running on <code>auth.freedom-id.de</code> on one place.
 */
public class Connect2IdIntegrationInstanceParameters {

  public static final URL PUBLIC_CONNECT2ID_URL;
  private static final String JVM_SSL_TRUST_STORE_CONFIG = "javax.net.ssl.trustStore";
  private static final String JVM_SSL_TRUST_STORE_PASSWORD_CONFIG = "javax.net.ssl.trustStorePassword";
  private static final String TRUST_STORE_FILE_PASSWORD = "changeme";
  private static final String PUBLIC_CONNECT2ID_URL_VALUE = "https://auth.freedom-id.de";
  private static final String LOCAL_PATH_TO_DOMAINID_CA_TRUST_STORE = USER_DIR
          + "/src/test/resources/trusted-certificates.jks";

  static {
    try {
      PUBLIC_CONNECT2ID_URL = new URL(PUBLIC_CONNECT2ID_URL_VALUE);
    } catch (final MalformedURLException e) {
      throw new ExceptionInInitializerError(e);
    }
  }

  public static void setSystemsSSLTrustStoreConfig() {
    System.out.println(
            "### Setting System property: " + JVM_SSL_TRUST_STORE_CONFIG + ":" + LOCAL_PATH_TO_DOMAINID_CA_TRUST_STORE);
    System.setProperty(JVM_SSL_TRUST_STORE_CONFIG, LOCAL_PATH_TO_DOMAINID_CA_TRUST_STORE);
    System.setProperty(JVM_SSL_TRUST_STORE_PASSWORD_CONFIG, TRUST_STORE_FILE_PASSWORD);
  }

  public static void resetSystemsSSLTrustStoreConfig() {
    System.out.println("### Clearing System property " + JVM_SSL_TRUST_STORE_CONFIG);
    System.clearProperty(JVM_SSL_TRUST_STORE_CONFIG);
  }

}
