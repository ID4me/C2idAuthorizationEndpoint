/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.wellknown.openid;

import static de.denic.domainid.wellknown.Connect2IdIntegrationInstanceParameters.PUBLIC_CONNECT2ID_URL;
import static de.denic.domainid.wellknown.Connect2IdIntegrationInstanceParameters.resetSystemsSSLTrustStoreConfig;
import static de.denic.domainid.wellknown.Connect2IdIntegrationInstanceParameters.setSystemsSSLTrustStoreConfig;
import static org.junit.Assert.assertNotNull;

import javax.ws.rs.client.Client;

import org.glassfish.jersey.client.JerseyClientBuilder;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import de.denic.domainid.wellknown.HttpRequestResponseHeaderLoggingFilter;

public class OpenIdConfigurationEndpointImplTest {

  @BeforeClass
  public static void setupClass() {
    setSystemsSSLTrustStoreConfig();
  }

  @AfterClass
  public static void tearDownClass() {
    resetSystemsSSLTrustStoreConfig();
  }

  @Test
  public void integrationTest() throws Exception {
    final Client restClientMock = new JerseyClientBuilder().register(HttpRequestResponseHeaderLoggingFilter.class)
        .build();
    final OpenIdConfigurationClient CUT = new OpenIdConfigurationClient.Impl(restClientMock);
    {
      final OpenIdConfiguration openIdConfig = CUT.getFromBaseURI(PUBLIC_CONNECT2ID_URL.toURI());
      System.out.println("OpenID config loaded from " + PUBLIC_CONNECT2ID_URL + ": " + openIdConfig);
      assertNotNull("Got issuer from OpenId configuration", openIdConfig.getIssuer());
      assertNotNull("Got JWK-set URI from OpenId configuration", openIdConfig.getJwkSetURI());
    }
  }

}
