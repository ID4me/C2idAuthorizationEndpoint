/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.wellknown.openid;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.concurrent.Immutable;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.time.Duration;
import java.time.Instant;

import static java.lang.Boolean.TRUE;
import static java.time.Instant.now;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.glassfish.jersey.client.ClientProperties.FOLLOW_REDIRECTS;

public interface OpenIdConfigurationClient {

  /**
   * @param baseURI Required
   * @return Never <code>null</code>
   */
  OpenIdConfiguration getFromBaseURI(URI baseURI) throws OpenIdConfigurationClientException;

  @Immutable
  final class Impl implements OpenIdConfigurationClient {

    private static final Logger LOG = LoggerFactory.getLogger(Impl.class);
    private static final String WELL_KNOWN_OPENID_CONFIGURATION_PATH = "/.well-known/openid-configuration";
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private static final ObjectReader OPENID_CONFIGURATION_READER = OBJECT_MAPPER.readerFor(OpenIdConfiguration.class);

    private final Client restClient;

    /**
     * @param restClient Required
     */
    public Impl(final Client restClient) {
      Validate.notNull(restClient, "Missing REST client");
      this.restClient = restClient;
    }

    @Override
    public OpenIdConfiguration getFromBaseURI(final URI baseURI) throws OpenIdConfigurationClientException {
      Validate.notNull(baseURI, "Missing URI");
      Validate.isTrue(baseURI.isAbsolute(), "URI '%1s' is not absolute", baseURI);
      final WebTarget target = restClient.target(baseURI).path(WELL_KNOWN_OPENID_CONFIGURATION_PATH).property
              (FOLLOW_REDIRECTS, TRUE);
      LOG.info("==> {}: >{}< ({})", "GET", EMPTY, target.getUri());
      final Instant startingAt = now();
      Response openIdConfigurationResponse = null;
      try {
        openIdConfigurationResponse = target.request().accept(APPLICATION_JSON_TYPE).get();
        final Duration invocationDuration = Duration.between(startingAt, now());
        LOG.info("<== {} {} ({}, elapsed {})", openIdConfigurationResponse.getStatus(),
                openIdConfigurationResponse.getStatusInfo(), target.getUri(), invocationDuration);
        try (final JsonParser openIdConfigurationJSONParser = new JsonFactory()
                .createParser((InputStream) openIdConfigurationResponse.getEntity())) {
          return OPENID_CONFIGURATION_READER.readValue(openIdConfigurationJSONParser);
        }
      } catch (final IOException | ProcessingException e) {
        throw new OpenIdConfigurationClientException(
                "Requesting '" + target.getUri() + "' failed: " + e.getMessage(), e);
      } finally {
        if (openIdConfigurationResponse != null) openIdConfigurationResponse.close();
      }
    }

  }

}
