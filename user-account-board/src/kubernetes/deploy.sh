#!/bin/bash

_fullscriptpath="$(readlink -f ${BASH_SOURCE[0]})"
BASEDIR="$(dirname $_fullscriptpath)"

DOCKERREGISTRY=$DOCKER_REGISTRY
DOCKERREGISTRYPATH=test
IMAGENAME=user-account-board

KUBECOMMAND="$KUBECTL --kubeconfig=$KUBECONFIG --namespace=$1"
VERSION=$2

sed -i "s#__DOCKER_REGISTRY__#${DOCKERREGISTRY}#" user-account-board-dc.yaml
sed -i "s#__IMAGE_VERSION__#${VERSION}#" user-account-board-dc.yaml
$KUBECOMMAND apply -f user-account-board-dc.yaml --record
$KUBECOMMAND rollout status deployment/user-account-board --watch
