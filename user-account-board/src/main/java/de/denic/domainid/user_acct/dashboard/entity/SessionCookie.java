/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.user_acct.dashboard.entity;

import de.denic.domainid.DomainId;
import org.apache.commons.lang3.Validate;

import javax.annotation.concurrent.Immutable;
import javax.ws.rs.CookieParam;
import java.util.Objects;

import static org.apache.commons.codec.digest.DigestUtils.sha256Hex;
import static org.apache.commons.lang3.StringUtils.isEmpty;

/**
 * By intention this type has <strong>NO</strong> knowledge about the salt value itself!
 */
@Immutable
public final class SessionCookie {

  private final DomainId domainId;
  private final String saltedHashValue;
  private final int hashCode;

  /**
   * @param domainId        Required
   * @param saltedHashValue Required
   */
  public SessionCookie(final DomainId domainId, final String saltedHashValue) {
    Validate.notNull(domainId, "Missing Domain ID");
    Validate.notEmpty(saltedHashValue, "Missing hash value");
    this.domainId = domainId;
    this.saltedHashValue = saltedHashValue;
    this.hashCode = calcHashCode();
  }

  /**
   * @param domainId       Required
   * @param plainSaltValue Required to be not empty
   * @return Never <code>null</code>
   */
  private static String saltedHashValueOf(final DomainId domainId, final String plainSaltValue) {
    Validate.notNull(domainId, "Missing domain ID");
    Validate.notEmpty(plainSaltValue, "Missing salt");
    return sha256Hex(plainSaltValue + domainId.getName());
  }

  /**
   * @param value Optional
   * @return Optional
   * @see CookieParam
   */
  public static SessionCookie fromString(final String value) {
    if (isEmpty(value)) {
      return null;
    }

    final String[] domainIdAndHashValue = value.split("\\:");
    Validate.isTrue(domainIdAndHashValue.length == 2, "Invalid value: '%s'", value);
    return new SessionCookie(new DomainId(domainIdAndHashValue[0]), domainIdAndHashValue[1]);
  }

  /**
   * @param domainId       Required
   * @param plainSaltValue Required to be not empty
   * @return Never <code>null</code>
   */
  public static SessionCookie from(final DomainId domainId, final String plainSaltValue) {
    return new SessionCookie(domainId, saltedHashValueOf(domainId, plainSaltValue));
  }

  /**
   * @return Never <code>null</code>
   */
  public DomainId getDomainId() {
    return domainId;
  }

  /**
   * @param proposedSalt Optional
   */
  public boolean matchesApplyingSalt(final String proposedSalt) {
    if (isEmpty(proposedSalt)) {
      return false;
    }

    return saltedHashValue.equals(saltedHashValueOf(domainId, proposedSalt));
  }

  /**
   * @return Never <code>null</code> nor empty.
   */
  public String asCookieValue() {
    return domainId.getAceEncodedName() + ":" + saltedHashValue;
  }

  private int calcHashCode() {
    return Objects.hash(domainId, saltedHashValue);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    SessionCookie that = (SessionCookie) o;
    return Objects.equals(domainId, that.domainId) &&
            Objects.equals(saltedHashValue, that.saltedHashValue);
  }

  @Override
  public int hashCode() {
    return hashCode;
  }

  @Override
  public String toString() {
    return asCookieValue();
  }

}
