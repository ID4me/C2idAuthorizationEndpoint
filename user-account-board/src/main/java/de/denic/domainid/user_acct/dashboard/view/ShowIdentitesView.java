/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.user_acct.dashboard.view;

import de.denic.domainid.DomainId;
import de.denic.domainid.dbaccess.UserAuthentication;
import io.dropwizard.views.View;
import jdk.nashorn.internal.ir.annotations.Immutable;
import org.apache.commons.lang3.Validate;

import java.util.List;

import static java.util.Collections.unmodifiableList;
import static java.util.stream.Collectors.toList;

@Immutable
public class ShowIdentitesView extends ViewWithDomainId {

  private final UserAuthentication actualAuthentication;
  private final List<UserAuthentication> bundledAuthentications;

  /**
   * @param domainId               Required
   * @param bundledAuthentications Required to contain at least the actual one with provided {@link DomainId}.
   * @see View#View(String)
   */
  public ShowIdentitesView(final DomainId domainId, final List<UserAuthentication> bundledAuthentications) {
    super("showIdentities.ftl", domainId);
    Validate.notEmpty(bundledAuthentications, "Missing at least actual user authentication '%1s'", domainId);
    actualAuthentication = bundledAuthentications.stream().filter(authentication -> authentication.getUserName().equals
            (domainId)).findFirst().orElseThrow(() -> new IllegalArgumentException("Missing actual user " +
            "authentication '" + domainId + "' within " + bundledAuthentications));
    this.bundledAuthentications = unmodifiableList(bundledAuthentications.stream().filter(userAuthentication -> !userAuthentication.equals
            (actualAuthentication)).collect(toList()));
  }

  /**
   * @return Never <code>null</code> but unmodifiable.
   */
  public List<UserAuthentication> getBundledAuthentications() {
    return bundledAuthentications;
  }

  /**
   * @return Never <code>null</code>
   */
  public UserAuthentication getActualAuthentication() {
    return actualAuthentication;
  }

}
