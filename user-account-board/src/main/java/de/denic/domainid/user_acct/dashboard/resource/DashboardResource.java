/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.user_acct.dashboard.resource;

import com.codahale.metrics.annotation.Metered;
import com.codahale.metrics.annotation.Metric;
import com.warrenstrange.googleauth.GoogleAuthenticatorKey;
import com.warrenstrange.googleauth.IGoogleAuthenticator;
import de.denic.domainid.DomainId;
import de.denic.domainid.connect2id.authzstore.Connect2IdAuthorizationStoreAPI;
import de.denic.domainid.connect2id.authzstore.entity.ModifiableOpenIdConnectAuthorisation;
import de.denic.domainid.connect2id.authzstore.entity.OpenIdConnectAuthorization;
import de.denic.domainid.connect2id.clientregistration.OpenIdConnectClientRegistration;
import de.denic.domainid.connect2id.clientregistration.entity.OpenIdConnectClient;
import de.denic.domainid.connect2id.common.Claim;
import de.denic.domainid.connect2id.common.ClientId;
import de.denic.domainid.dbaccess.UserAuthentication;
import de.denic.domainid.dbaccess.UserAuthenticationDAO;
import de.denic.domainid.dns.DomainIdDnsClient;
import de.denic.domainid.dns.LookupResult;
import de.denic.domainid.subject.PseudonymSubject;
import de.denic.domainid.user_acct.dashboard.entity.SessionCookie;
import de.denic.domainid.user_acct.dashboard.service.SessionDataStore;
import de.denic.domainid.user_acct.dashboard.service.impl.SessionDataInMemoryStore;
import de.denic.domainid.user_acct.dashboard.view.*;
import io.dropwizard.views.View;
import net.glxn.qrgen.QRCode;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.concurrent.Immutable;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.core.Context;
import java.io.ByteArrayOutputStream;
import java.net.URI;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

import static com.warrenstrange.googleauth.GoogleAuthenticatorQRGenerator.getOtpAuthTotpURL;
import static de.denic.domainid.dbaccess.UserAuthentication.ACTIVE;
import static de.denic.domainid.dbaccess.UserAuthentication.DEACTIVATED;
import static java.lang.Integer.parseUnsignedInt;
import static java.util.Arrays.fill;
import static java.util.Arrays.stream;
import static java.util.Locale.ENGLISH;
import static net.glxn.qrgen.image.ImageType.PNG;
import static org.apache.commons.codec.binary.Base64.encodeBase64String;
import static org.apache.commons.codec.digest.DigestUtils.sha256Hex;
import static org.apache.commons.lang3.StringUtils.isEmpty;

@Path(ResourceBase.DASHBOARD_PATH)
@Immutable
public class DashboardResource extends ResourceBase {

  private static final Logger LOG = LoggerFactory.getLogger(DashboardResource.class);
  private static final String GOOGLE_AUTHENTICATOR_ISSUER = "DENIC eG id4me";
  private static final String NO_BASE64_ENCODED_QR_CODE = null;
  private static final String NO_SHARED_SECRET = null;
  private static final int DUMMY_VALUE = -1;

  private final Connect2IdAuthorizationStoreAPI connect2IdAuthorisationStoreAPI;
  private final OpenIdConnectClientRegistration clientRegistrationEndpoint;
  private final SessionDataStore sessionDataStore = SessionDataInMemoryStore.getInstance();
  private final IGoogleAuthenticator googleAuthenticator;

  /**
   * @param userAuthenticationDAO           Required
   * @param dnsClient                       Required
   * @param restClient                      Required
   * @param connect2IdAuthorisationStoreAPI Required
   * @param clientRegistrationEndpoint      Required
   */
  public DashboardResource(final UserAuthenticationDAO userAuthenticationDAO, final DomainIdDnsClient dnsClient,
                           final Client restClient, final Connect2IdAuthorizationStoreAPI connect2IdAuthorisationStoreAPI,
                           final OpenIdConnectClientRegistration clientRegistrationEndpoint, final IGoogleAuthenticator googleAuthenticator) {
    super(userAuthenticationDAO, dnsClient, restClient);
    Validate.notNull(connect2IdAuthorisationStoreAPI, "Missing Connect2ID Authorization Store API client");
    Validate.notNull(clientRegistrationEndpoint, "Missing OpenID Client registration endpoint");
    Validate.notNull(googleAuthenticator, "Missing Google Authenticator component");
    this.connect2IdAuthorisationStoreAPI = connect2IdAuthorisationStoreAPI;
    this.clientRegistrationEndpoint = clientRegistrationEndpoint;
    this.googleAuthenticator = googleAuthenticator;
  }

  @GET
  @Produces(TEXT_HTML_CHARSET_UTF8)
  public View indexPage() {
    return doIndexPage();
  }

  @POST
  @Produces(TEXT_HTML_CHARSET_UTF8)
  public View askFor(@CookieParam(SESSION_COOKIE) final SessionCookie sessionCookie,
                     @FormParam("domainID") final String domainIdValue, @Context final HttpServletResponse response) {
    return ifDomainIdValueIsMissingShowIndexPageElse(domainIdValue,
            userAuthentication -> askFor(userAuthentication, sessionCookie, response));
  }

  @POST
  @Path("/login")
  @Produces(TEXT_HTML_CHARSET_UTF8)
  @Metered
  public View login(@FormParam("domainID") final String domainIdValue,
                    @FormParam("loginOrForgot") final String loginOfForgot, @FormParam("currentPassword") final String password,
                    @Context final HttpServletResponse response) {
    return ifDomainIdValueIsMissingShowIndexPageElse(domainIdValue,
            domainId -> doLogin(domainId, loginOfForgot, password, response));
  }

  /**
   * @param domainId Required
   * @param password Optional
   * @param response Required
   */
  private View doLogin(final DomainId domainId, final String loginOfForgot, final String password,
                       @Context final HttpServletResponse response) {
    LOG.info("Somebody wants to operate on '{}': '{}'", domainId, loginOfForgot);
    if (oneIsMissing(domainId)) {
      return indexPage();
    }

    if (oneIsMissing(loginOfForgot)) {
      return askFor(domainId, response);
    }

    switch (loginOfForgot.toLowerCase(ENGLISH)) {
      case "login":
        return loginWith(domainId, password, response);

      case "forgot":
        return initRegistrationOf(domainId);

      default:
        return askFor(domainId, response);
    }
  }

  /**
   * @param domainId Required
   * @param password Optional
   * @param response Required
   */
  private View loginWith(final DomainId domainId, final String password, final HttpServletResponse response) {
    Validate.notNull(domainId, "Missing Domain ID");
    Validate.notNull(response, "Missing response object");
    if (oneIsMissing(password)) {
      return askFor(domainId, response);
    }

    final UserAuthentication userAuthentication = getUserAuthenticationDAO().findUserAuthenticationBy(domainId);
    if (oneIsMissing(userAuthentication)) {
      LOG.info("Going to init registration of DomainID '{}'", userAuthentication);
      return initRegistrationOf(domainId);
    }

    final LookupResult<URI> identityAgentLookup = getDnsClient().lookupIdentityAgentHostNameOf(domainId);
    final String sha256HexOfPassword = sha256Hex(password);
    if (!sha256HexOfPassword.equals(userAuthentication.getPasswordHexSHA256())) {
      LOG.info("Mismatching credentials of DomainID '{}'", domainId);
      return new LoginToDomainIdAccountView(domainId, identityAgentLookup);
    }

    LOG.info("Matching credentials of DomainID '{}'", domainId);
    if (userAuthentication.isTwoFactorAuthEnabled()) {
      return showTwoFactorAuthenticationView(domainId);
    }

    response.addCookie(newCookieWith(domainId));
    return newDomainIdAccountNavigationView(userAuthentication, identityAgentLookup);
  }

  @POST
  @Path("/login/totp")
  @Produces(TEXT_HTML_CHARSET_UTF8)
  @Metered
  public View loginViaTOTP(@FormParam("domainID") final String domainIdValue, @FormParam("loginOrForgot") final String loginOfForgot,
                           @FormParam("verificationCode") final String verificationCode, @Context final
                           HttpServletResponse response) {
    return ifDomainIdValueIsMissingShowIndexPageElse(domainIdValue,
            domainId -> doLoginViaTOTP(domainId, loginOfForgot, verificationCode, response));
  }

  private View doLoginViaTOTP(final DomainId domainId, final String loginOfForgot, final String verificationCodeValue,
                              final HttpServletResponse response) {
    Validate.notNull(domainId, "Missing Domain ID");
    Validate.notNull(response, "Missing response object");
    final int verificationCode;
    try {
      verificationCode = parseUnsignedInt(verificationCodeValue);
    } catch (final NumberFormatException e) {
      return showTwoFactorAuthenticationView(domainId);
    }

    if (oneIsMissing(loginOfForgot)) {
      return askFor(domainId, response);
    }

    final UserAuthentication userAuthentication = getUserAuthenticationDAO().findUserAuthenticationBy(domainId);
    if (oneIsMissing(userAuthentication)) {
      LOG.info("Going to init registration of '{}'", domainId);
      return initRegistrationOf(domainId);
    }

    LOG.info("Received TOTP verification code: {} (domain ID: {})", verificationCode, domainId);
    final char[] sharedSecret = getUserAuthenticationDAO().selectTOTPSharedSecretOf(domainId);
    final boolean totpAuthorized = googleAuthenticator.authorize(new String(sharedSecret), verificationCode);
    fill(sharedSecret, ' '); // Wipe out shared secret from memory
    if (!totpAuthorized) {
      return showTwoFactorAuthenticationView(domainId);
    }

    final LookupResult<URI> identityAgentLookup = getDnsClient().lookupIdentityAgentHostNameOf(domainId);
    response.addCookie(newCookieWith(domainId));
    return newDomainIdAccountNavigationView(userAuthentication, identityAgentLookup);
  }

  private View showTwoFactorAuthenticationView(final DomainId domainId) {
    return new TwoFactorAuthenticationView(domainId);
  }

  @POST
  @Path("/logout")
  @Produces(TEXT_HTML_CHARSET_UTF8)
  @Metered
  public View logout(@CookieParam(SESSION_COOKIE) final SessionCookie sessionCookie,
                     @Context final HttpServletResponse response) {
    return ifSessionCookieIsNotValidShowIndexPageElse(sessionCookie, response,
            domainId -> doLogout(sessionCookie, response));
  }

  private View doLogout(final SessionCookie sessionCookie, final HttpServletResponse response) {
    sessionDataStore.clearSessionDataOf(sessionCookie);
    response.addCookie(invalidated(sessionCookie));
    return indexPage();
  }

  @GET
  @Path("/manageActivationStatus")
  @Produces(TEXT_HTML_CHARSET_UTF8)
  public View manageActivationStatus(@CookieParam(SESSION_COOKIE) final SessionCookie sessionCookie,
                                     @Context final HttpServletResponse response) {
    return ifSessionCookieIsNotValidShowIndexPageElse(sessionCookie, response, userAuthentication ->
            (userAuthentication.isActive() ? new PrepareDeactivationView(userAuthentication.getUserName()) : new
                    PrepareActivationView(userAuthentication.getUserName())));
  }

  @POST
  @Path("/activation")
  @Produces(TEXT_HTML_CHARSET_UTF8)
  public View activate(@CookieParam(SESSION_COOKIE) final SessionCookie sessionCookie,
                       @Context final HttpServletResponse response) {
    return ifSessionCookieIsNotValidShowIndexPageElse(sessionCookie, response, this::doActivate);
  }

  private View doActivate(final UserAuthentication userAuthentication) {
    LOG.info("Activation of '{}'", userAuthentication);
    getUserAuthenticationDAO().updateActive(userAuthentication.getUserName(), ACTIVE);
    return newDomainIdAccountNavigationView(userAuthentication);
  }

  @POST
  @Path("/deactivation")
  @Produces(TEXT_HTML_CHARSET_UTF8)
  public View deactivate(@CookieParam(SESSION_COOKIE) final SessionCookie sessionCookie,
                         @FormParam("consented") final boolean consented,
                         @Context final HttpServletResponse response) {
    return ifSessionCookieIsNotValidShowIndexPageElse(sessionCookie, response, userAuthentication -> doDeactivate(userAuthentication,
            consented));
  }

  private View doDeactivate(final UserAuthentication userAuthentication, final boolean consented) {
    LOG.info("Deactivation of DomainID '{}' consented? {}", userAuthentication, consented);
    if (!consented) {
      return newDomainIdAccountNavigationView(userAuthentication);
    }

    getUserAuthenticationDAO().updateActive(userAuthentication.getUserName(), DEACTIVATED);
    final PseudonymSubject pseudonymSubject = userAuthentication.getPseudonymUserName();
    connect2IdAuthorisationStoreAPI.revokeAllAuthorizationsOf(pseudonymSubject);
    return new SuccessfulDeactivationView(userAuthentication.getUserName());
  }

  @POST
  @Path("/showDomainIdAccountNavigationView")
  @Produces(TEXT_HTML_CHARSET_UTF8)
  public View showDomainIdAccountNavigationViewViaPost(@CookieParam(SESSION_COOKIE) final SessionCookie sessionCookie,
                                                       @Context final HttpServletResponse response) {
    return showDomainIdAccountNavigationViewViaGet(sessionCookie, response);
  }

  @GET
  @Path("/showDomainIdAccountNavigationView")
  @Produces(TEXT_HTML_CHARSET_UTF8)
  public View showDomainIdAccountNavigationViewViaGet(@CookieParam(SESSION_COOKIE) final SessionCookie sessionCookie,
                                                      @Context final HttpServletResponse response) {
    return ifSessionCookieIsNotValidShowIndexPageElse(sessionCookie, response,
            this::newDomainIdAccountNavigationView);
  }

  @POST
  @Path("/manageTwoFactorAuth")
  @Produces(TEXT_HTML_CHARSET_UTF8)
  public View manageTwoFactorAuthentication(@CookieParam(SESSION_COOKIE) final SessionCookie sessionCookie,
                                            @Context final HttpServletResponse response) {
    return ifSessionCookieIsNotValidShowIndexPageElse(sessionCookie, response, userAuthentication -> new ManageTwoFactorAuthenticationView(userAuthentication, NO_BASE64_ENCODED_QR_CODE, NO_SHARED_SECRET));
  }

  @POST
  @Path("/changeTwoFactorAuth")
  @Produces(TEXT_HTML_CHARSET_UTF8)
  @Metric
  public View changeTwoFactorAuthentication(@CookieParam(SESSION_COOKIE) final SessionCookie sessionCookie,
                                            @FormParam("operation") final Change2FAOperation operation,
                                            @FormParam("verificationCode") final String verificationCode,
                                            @FormParam("sharedSecret") final String sharedSecret, @Context final HttpServletResponse response) {
    return ifSessionCookieIsNotValidShowIndexPageElse(sessionCookie, response, userAuthentication ->
            doChangeTwoFactorAuthentication(userAuthentication, operation, sharedSecret, verificationCode));
  }

  private View doChangeTwoFactorAuthentication(final UserAuthentication userAuthentication, final Change2FAOperation operation,
                                               final String receivedSharedSecret, final String verificationCodeValue) {
    int verificationCode;
    try {
      verificationCode = (isEmpty(verificationCodeValue) ? DUMMY_VALUE : parseUnsignedInt(verificationCodeValue));
    } catch (final NumberFormatException e) {
      verificationCode = DUMMY_VALUE;
    }
    final String base64EncodedQRCode;
    final String sharedSecret;
    switch (operation) {
      case enable:
        final GoogleAuthenticatorKey googleAuthKey = googleAuthenticator.createCredentials();
        sharedSecret = googleAuthKey.getKey();
        base64EncodedQRCode = calculateBase64EncodeQRCode(userAuthentication.getUserName(), googleAuthKey);
        break;
      case activate:
        final boolean totpAuthorized = googleAuthenticator.authorize(receivedSharedSecret, verificationCode);
        if (totpAuthorized) {
          sharedSecret = receivedSharedSecret;
          updateTOTPSharedSecretOf(userAuthentication.getUserName(), receivedSharedSecret);
        } else {
          sharedSecret = NO_SHARED_SECRET;
        }
        base64EncodedQRCode = NO_BASE64_ENCODED_QR_CODE;
        break;
      case disable:
        updateTOTPSharedSecretOf(userAuthentication.getUserName(), NO_SHARED_SECRET);
        base64EncodedQRCode = NO_BASE64_ENCODED_QR_CODE;
        sharedSecret = NO_SHARED_SECRET;
        break;
      default:
        throw new RuntimeException("Internal error: Unknown operation '" + operation + "'");
    }
    return new ManageTwoFactorAuthenticationView(userAuthentication, base64EncodedQRCode, sharedSecret);
  }

  private void updateTOTPSharedSecretOf(final DomainId domainId, final String sharedSecret) {
    final int updatedRows = getUserAuthenticationDAO().updateTOTPSharedSecret(domainId, sharedSecret);
    if (updatedRows != 1) {
      throw new RuntimeException("Internal error: Updating database entry '" + domainId + "' expected single update, but " +
              "got " + updatedRows);
    }
  }

  private String calculateBase64EncodeQRCode(final DomainId domainId, final GoogleAuthenticatorKey googleAuthKey) {
    final String totpAuthURL = getOtpAuthTotpURL(GOOGLE_AUTHENTICATOR_ISSUER, domainId.getName(), googleAuthKey);
    final ByteArrayOutputStream qrCodeStream = QRCode.from(totpAuthURL).to(PNG).withSize(200, 200).stream();
    return encodeBase64String(qrCodeStream.toByteArray());
  }

  @POST
  @Path("/changePasswordView")
  @Produces(TEXT_HTML_CHARSET_UTF8)
  public View changePasswordView(@CookieParam(SESSION_COOKIE) final SessionCookie sessionCookie,
                                 @Context final HttpServletResponse response) {
    return ifSessionCookieIsNotValidShowIndexPageElse(sessionCookie, response, userAuthentication ->
            new ChangeDomainIdCredentialsView(userAuthentication.getUserName()));
  }

  @POST
  @Path("/showClaimsPerClientView")
  @Produces(TEXT_HTML_CHARSET_UTF8)
  public View showClaimsPerClientView(@CookieParam(SESSION_COOKIE) final SessionCookie sessionCookie,
                                      @Context final HttpServletResponse response) {
    return ifSessionCookieIsNotValidShowIndexPageElse(sessionCookie, response,
            this::showClaimsPerClientView);
  }

  /**
   * @param userAuthentication Required
   * @return Never <code>null</code>
   */
  private View showClaimsPerClientView(final UserAuthentication userAuthentication) {
    final Map<ClientId, OpenIdConnectAuthorization> allAuthorisationsOfDomainId = connect2IdAuthorisationStoreAPI
            .loadAllAuthorizationsOf(userAuthentication.getPseudonymUserName());
    final Map<OpenIdConnectClient, OpenIdConnectAuthorization> allAuthorisationsOfDomainIdPerClient = new HashMap<>(
            allAuthorisationsOfDomainId.size());
    allAuthorisationsOfDomainId.forEach((key, value) -> {
      final Optional<OpenIdConnectClient> optClient = clientRegistrationEndpoint.getClientOf(key);
      if (optClient.isPresent()) {
        allAuthorisationsOfDomainIdPerClient.put(optClient.get(), value);
      }
    });
    return new ShowClaimsPerClientView(userAuthentication.getUserName(), allAuthorisationsOfDomainIdPerClient);
  }

  @POST
  @Path("/change")
  @Produces(TEXT_HTML_CHARSET_UTF8)
  public View changePassword(@CookieParam(SESSION_COOKIE) final SessionCookie sessionCookie,
                             @FormParam("currentPassword") final String currentPassword, @FormParam("newPassword") final String newPassword,
                             @FormParam("passwordConfirmation") final String passwordConfirmation,
                             @Context final HttpServletResponse response) {
    return ifSessionCookieIsNotValidShowIndexPageElse(sessionCookie, response,
            userAuthentication -> changePassword(userAuthentication, currentPassword, newPassword, passwordConfirmation, response));
  }

  /**
   * @param userAuthentication Required
   * @return Never <code>null</code>
   */
  private View changePassword(final UserAuthentication userAuthentication, final String currentPassword, final String newPassword,
                              final String passwordConfirmation, final HttpServletResponse response) {
    Validate.notNull(userAuthentication, "Missing Domain ID");
    LOG.info("Somebody wants to change '{}'", userAuthentication);
    if (oneIsMissing(currentPassword, newPassword, passwordConfirmation)) {
      return askFor(userAuthentication.getUserName(), response);
    }

    final String sha256HexOfCurrentPassword = sha256Hex(currentPassword);
    if (!sha256HexOfCurrentPassword.equals(userAuthentication.getPasswordHexSHA256())) {
      LOG.info("Mismatching credentials of '{}'", userAuthentication);
      return askFor(userAuthentication.getUserName(), response);
    }

    if (!newPassword.equals(passwordConfirmation)) {
      return askFor(userAuthentication.getUserName(), response);
    }

    final String sha256HexOfNewPassword = sha256Hex(newPassword);
    final int updateCounter = getUserAuthenticationDAO().updateUserAuthentication(userAuthentication.getUserName(),
            sha256HexOfNewPassword);
    if (updateCounter < 1) {
      LOG.info("DomainID '{}' unknown", userAuthentication);
      return askFor(userAuthentication.getUserName(), response);
    }

    LOG.info("Credentials of DomainID '{}' successfully changed", userAuthentication);
    return new CredentialsChangedSuccessfullyView(userAuthentication.getUserName());
  }

  @POST
  @Path("/changeClaimsPerClient/{clientID}")
  @Produces(TEXT_HTML_CHARSET_UTF8)
  public View changeClaimsPerClient(@CookieParam(SESSION_COOKIE) final SessionCookie sessionCookie, @PathParam("clientID") final ClientId clientId,
                                    @FormParam("claims") final Set<Claim> checkedClaims, @Context final HttpServletResponse response) {
    return ifSessionCookieIsNotValidShowIndexPageElse(sessionCookie, response,
            userAuthentication -> doChangeClaimsPerClient(userAuthentication, clientId, checkedClaims));
  }

  private View doChangeClaimsPerClient(final UserAuthentication userAuthentication, final ClientId clientId,
                                       final Set<Claim> checkedClaims) {
    LOG.info("Changing checked claims of DomainID '{}' with ClientID '{}' to {}", userAuthentication, clientId, checkedClaims);
    final Optional<OpenIdConnectAuthorization> optCurrentAuthorisation = connect2IdAuthorisationStoreAPI
            .loadAuthorizationOf(userAuthentication.getPseudonymUserName(), clientId);
    if (optCurrentAuthorisation.isPresent()) {
      final ModifiableOpenIdConnectAuthorisation modifiableAuthorisation = optCurrentAuthorisation.get().modifiable();
      modifiableAuthorisation.getConsentedClaims().retainAll(checkedClaims);
      modifiableAuthorisation.getSavedConsentedClaims().retainAll(checkedClaims);
      final OpenIdConnectAuthorization changedAuth = modifiableAuthorisation.unmodifiable();
      connect2IdAuthorisationStoreAPI.changeAuthorization(changedAuth);
      dirtyHackSleepToLetC2IdDistributeChangesInHisCluster();
    }
    return showClaimsPerClientView(userAuthentication);
  }

  @POST
  @Path("/changeClaimsPerClient/{clientID}/revoke")
  @Produces(TEXT_HTML_CHARSET_UTF8)
  public View revokeClient(@CookieParam(SESSION_COOKIE) final SessionCookie sessionCookie,
                           @PathParam("clientID") final ClientId clientId, @Context final HttpServletResponse response) {
    return ifSessionCookieIsNotValidShowIndexPageElse(sessionCookie, response,
            userAuthentication -> doRevokeClient(userAuthentication, clientId));
  }

  private View doRevokeClient(final UserAuthentication userAuthentication, final ClientId clientId) {
    LOG.info("Deleting client with ID '{}' of '{}' completely", clientId, userAuthentication);
    final Optional<OpenIdConnectAuthorization> optCurrentAuthorisation = connect2IdAuthorisationStoreAPI
            .loadAuthorizationOf(userAuthentication.getPseudonymUserName(), clientId);
    if (optCurrentAuthorisation.isPresent()) {
      connect2IdAuthorisationStoreAPI.revokeAuthorizationOf(userAuthentication.getPseudonymUserName(), clientId);
      dirtyHackSleepToLetC2IdDistributeChangesInHisCluster();
    }
    return showClaimsPerClientView(userAuthentication);
  }

  private void dirtyHackSleepToLetC2IdDistributeChangesInHisCluster() {
    // TODO: Dirty hack wegen Zeitverzoegerung bei der Verteilung der Daten im C2ID-Cluster!
    try {
      Thread.sleep(1000L);
    } catch (final InterruptedException e) {
      Thread.currentThread().interrupt();
    }
  }

  private boolean oneIsMissing(final Object... values) {
    return stream(values).anyMatch(value -> (value instanceof CharSequence ? isEmpty((CharSequence) value) : value == null));
  }

  /**
   * @param domainIdValue Optional
   * @param operation     Required
   * @return Never <code>null</code>
   */
  private View ifDomainIdValueIsMissingShowIndexPageElse(final String domainIdValue,
                                                         final Function<DomainId, View> operation) {
    Validate.notNull(operation, "Missing operation");
    LOG.info("Got request targetting following DomainID: {}", domainIdValue);
    final Optional<DomainId> domainId = DomainId.from(domainIdValue);
    return domainId.isPresent() ? operation.apply(domainId.get()) : indexPage();
  }

  /**
   * @param domainId Required
   * @return Never <code>null</code>
   */
  private Cookie newCookieWith(final DomainId domainId) {
    Validate.notNull(domainId, "Missing domain ID");
    final Pair<SessionCookie, Duration> sessionData = sessionDataStore.newSessionDataWith(domainId);
    return createCookieApplying(sessionData.getLeft().asCookieValue(), sessionData.getRight().getSeconds());
  }

}
