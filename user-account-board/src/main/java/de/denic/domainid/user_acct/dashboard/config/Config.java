/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.user_acct.dashboard.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.denic.domainid.connect2id.config.Connect2IdAuthorisationStoreAPIConfig;
import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;

import javax.annotation.concurrent.ThreadSafe;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;

@ThreadSafe
public final class Config extends Configuration {

  @Valid
  @NotNull
  @JsonProperty("userCredentialDatasource")
  private final DataSourceFactory dataSourceFactory = new DataSourceFactory();

  @Valid
  @NotNull
  @JsonProperty("dns")
  private final DnsServerConfig dnsServerConfig = new DnsServerConfig();

  @Valid
  @NotNull
  @JsonProperty("connect2Id")
  private final Connect2IdAuthorisationStoreAPIConfig connect2IdAuthorisationStoreAPIConfig = new Connect2IdAuthorisationStoreAPIConfig();

  @Valid
  @NotNull
  @JsonProperty("openIdConnect")
  private final OpenIdConnectClientRegistrationConfig openIdConnectClientRegistrationConfig = new
          OpenIdConnectClientRegistrationConfig();

  /**
   * @return Never <code>null</code>.
   */
  public DataSourceFactory getDataSourceFactory() {
    return dataSourceFactory;
  }

  public Map<String, Map<String, String>> getViewRendererConfiguration() {
    final Map<String, String> freeMarkerConfiguration = new HashMap<>();
    freeMarkerConfiguration.put("strict_syntax", "yes");
    final Map<String, Map<String, String>> viewRendererConfiguration = new HashMap<>();
    viewRendererConfiguration.put(".ftl", freeMarkerConfiguration);
    return viewRendererConfiguration;
  }

  /**
   * @return Never <code>null</code>.
   */
  public Connect2IdAuthorisationStoreAPIConfig getC2IdAuthorisationStoreConfig() {
    return connect2IdAuthorisationStoreAPIConfig;
  }

  /**
   * @return Never <code>null</code>.
   */
  public DnsServerConfig getDnsServerConfig() {
    return dnsServerConfig;
  }

  /**
   * @return Never <code>null</code>.
   */
  public OpenIdConnectClientRegistrationConfig getOpenIdConnectClientRegistrationConfig() {
    return openIdConnectClientRegistrationConfig;
  }

}
