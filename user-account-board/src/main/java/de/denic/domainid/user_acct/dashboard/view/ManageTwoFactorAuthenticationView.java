/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.user_acct.dashboard.view;

import de.denic.domainid.dbaccess.UserAuthentication;
import org.apache.commons.lang3.Validate;

import javax.annotation.concurrent.Immutable;

import static org.apache.commons.lang3.StringUtils.isEmpty;

@Immutable
public final class ManageTwoFactorAuthenticationView extends ViewWithDomainId {

  private final UserAuthentication userAuthentication;
  private final String base64EncodedQRCode, sharedSecret;

  /**
   * @param userAuthentication  Required
   * @param base64EncodedQRCode Optional
   * @param sharedSecret        Required if <code>base64EncodedQRCode</code> is available.
   */
  public ManageTwoFactorAuthenticationView(final UserAuthentication userAuthentication,
                                           final String base64EncodedQRCode, final String sharedSecret) {
    super("manageTwoFactorAuth.ftl", userAuthentication.getUserName());
    Validate.notNull(userAuthentication, "Missing user authentication");
    if (!isEmpty(base64EncodedQRCode)) {
      Validate.notEmpty(sharedSecret, "Missing shared secret value");
    }
    this.userAuthentication = userAuthentication;
    this.base64EncodedQRCode = base64EncodedQRCode;
    this.sharedSecret = sharedSecret;
  }

  /**
   * @return Never <code>null</code>
   */
  public UserAuthentication getUserAuthentication() {
    return userAuthentication;
  }

  /**
   * @return Optional
   */
  public String getBase64EncodedQRCode() {
    return base64EncodedQRCode;
  }

  /**
   * @return Optional
   */
  public String getSharedSecret() {
    return sharedSecret;
  }

}
