/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.user_acct.dashboard;

import com.warrenstrange.googleauth.GoogleAuthenticator;
import de.denic.domainid.connect2id.authzstore.Connect2IdAuthorizationStoreAPI;
import de.denic.domainid.connect2id.clientregistration.OpenIdConnectClientRegistration;
import de.denic.domainid.connect2id.config.Connect2IdAuthorisationStoreAPIConfig;
import de.denic.domainid.connect2id.healthcheck.Connect2IdServerHealthCheck;
import de.denic.domainid.dbaccess.UserAuthenticationDAO;
import de.denic.domainid.dns.DomainIdDnsClient;
import de.denic.domainid.dns.impl.DomainIdDnsClientDnsJavaBasedImpl;
import de.denic.domainid.user_acct.dashboard.config.Config;
import de.denic.domainid.user_acct.dashboard.metric.Version;
import de.denic.domainid.user_acct.dashboard.resource.DashboardResource;
import de.denic.domainid.user_acct.dashboard.resource.ManageIdentitiesResource;
import de.denic.domainid.wellknown.openid.OpenIdConfiguration;
import de.denic.domainid.wellknown.openid.OpenIdConfigurationClient;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.jersey.setup.JerseyEnvironment;
import io.dropwizard.lifecycle.Managed;
import io.dropwizard.lifecycle.setup.LifecycleEnvironment;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.ViewBundle;
import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.dropwizard.DropwizardExports;
import io.prometheus.client.exporter.MetricsServlet;
import org.jdbi.v3.core.Jdbi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xbill.DNS.Resolver;
import org.xbill.DNS.SimpleResolver;

import javax.annotation.concurrent.Immutable;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;

import static org.apache.commons.lang3.SystemUtils.*;

@Immutable
public final class App extends Application<Config> implements Managed {

  private static final Logger LOG = LoggerFactory.getLogger(App.class);

  public static void main(final String[] args) throws Exception {
    new App().run(args);
  }

  @Override
  public String getName() {
    return "User Account Dashboard version " + App.class.getPackage().getImplementationVersion();
  }

  @Override
  public void run(final Config configuration, final Environment environment) throws Exception {
    LOG.info("Running on JRE version {}, JVM implementation version {}", JAVA_RUNTIME_VERSION, JAVA_VM_VERSION);
    final CollectorRegistry collectorRegistry = CollectorRegistry.defaultRegistry;
    collectorRegistry.register(new DropwizardExports(environment.metrics()));
    final MetricsServlet metricServlet = new MetricsServlet(collectorRegistry);
    environment.admin().addServlet("prometheusMetrics", metricServlet).addMapping("/prometheusMetrics");
    final LifecycleEnvironment lifecycleEnvironment = environment.lifecycle();
    lifecycleEnvironment.manage(this);
    lifecycleEnvironment.manage(new Version());
    final DataSourceFactory dataSourceFactory = configuration.getDataSourceFactory();
    final Jdbi jdbi = new JdbiFactory().build(environment, dataSourceFactory, "authenticationDB");
    LOG.info("Using user authentication database {}", dataSourceFactory.getUrl());
    final UserAuthenticationDAO userAuthenticationDAO = jdbi.onDemand(UserAuthenticationDAO.class);
    userAuthenticationDAO.migrateDatabaseIfNecessaryApplying(dataSourceFactory.getDriverClass(), dataSourceFactory
            .getUrl(), dataSourceFactory.getUser(), dataSourceFactory.getPassword());
    final Resolver googleResolver = new SimpleResolver(configuration.getDnsServerConfig().getAddress().getHostAddress());
    final DomainIdDnsClient dnsClient = new DomainIdDnsClientDnsJavaBasedImpl(googleResolver);
    final Client restClient = ClientBuilder.newClient();
    final Connect2IdAuthorisationStoreAPIConfig c2idConfig = configuration.getC2IdAuthorisationStoreConfig();
    final Connect2IdAuthorizationStoreAPI connect2IdAuthorisationStoreAPI = new Connect2IdAuthorizationStoreAPI.Impl(
            c2idConfig.getAuthorisationStoreAPIUrl(), c2idConfig.getAuthorisationStoreAPIBearerAccessToken(), restClient);
    final OpenIdConnectClientRegistration clientRegistrationEndpoint = new OpenIdConnectClientRegistration.Impl(
            c2idConfig.getBaseURL(), c2idConfig.getHealthCheckBearerAccessToken(), restClient);
    final JerseyEnvironment jerseyEnvironment = environment.jersey();
    // jerseyEnvironment.register(ValidationErrorMessageHTMLBodyWriter.class);
    jerseyEnvironment.register(new DashboardResource(userAuthenticationDAO, dnsClient, restClient,
            connect2IdAuthorisationStoreAPI, clientRegistrationEndpoint, new GoogleAuthenticator()));
    jerseyEnvironment.register(new ManageIdentitiesResource(userAuthenticationDAO, dnsClient, restClient,
            configuration.getOpenIdConnectClientRegistrationConfig().getClientRegistrationSecret(),
            requestTokenEndpointUriFrom(configuration.getC2IdAuthorisationStoreConfig().getBaseURL(), restClient)));
    environment.healthChecks().register("connect2IdServer",
            new Connect2IdServerHealthCheck(restClient, configuration.getC2IdAuthorisationStoreConfig()));
  }

  private URI requestTokenEndpointUriFrom(final URL connect2IdURL, final Client restClient) throws URISyntaxException {
    final OpenIdConfiguration openIdConfiguration = new OpenIdConfigurationClient.Impl(restClient).getFromBaseURI(connect2IdURL.toURI());
    return openIdConfiguration.getTokenEndpoint();
  }

  @Override
  public void initialize(final Bootstrap<Config> bootstrap) {
    bootstrap.addBundle(new ViewBundle<Config>() {
      @Override
      public Map<String, Map<String, String>> getViewConfiguration(final Config config) {
        return config.getViewRendererConfiguration();
      }
    });
    // Enable config variable substitution with environment variables
    bootstrap.setConfigurationSourceProvider(new SubstitutingSourceProvider(bootstrap.getConfigurationSourceProvider(),
            new EnvironmentVariableSubstitutor(false)));
    bootstrap.addBundle(new AssetsBundle("/assets/style", "/style"));
  }

  @Override
  public void start() {
    // Not interested in
  }

  @Override
  public void stop() {
    LOG.info("STOPPING");
  }

}
