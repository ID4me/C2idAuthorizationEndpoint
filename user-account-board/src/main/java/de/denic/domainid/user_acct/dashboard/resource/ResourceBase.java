/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.user_acct.dashboard.resource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import de.denic.domainid.DomainId;
import de.denic.domainid.dbaccess.UserAuthentication;
import de.denic.domainid.dbaccess.UserAuthenticationDAO;
import de.denic.domainid.dns.DomainIdDnsClient;
import de.denic.domainid.dns.LookupResult;
import de.denic.domainid.user_acct.dashboard.entity.InitDomainIdResponseEntity;
import de.denic.domainid.user_acct.dashboard.entity.SessionCookie;
import de.denic.domainid.user_acct.dashboard.service.SessionDataStore;
import de.denic.domainid.user_acct.dashboard.service.impl.SessionDataInMemoryStore;
import de.denic.domainid.user_acct.dashboard.view.*;
import io.dropwizard.views.View;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.concurrent.Immutable;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URI;
import java.time.Duration;
import java.time.Instant;
import java.util.Optional;
import java.util.function.Function;

import static java.time.Instant.now;
import static javax.ws.rs.client.Entity.json;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.TEXT_HTML;
import static javax.ws.rs.core.Response.Status.Family.SUCCESSFUL;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.defaultIfEmpty;

@Immutable
abstract class ResourceBase {

  protected static final String DASHBOARD_PATH = "/dashboard";
  protected static final String SESSION_COOKIE = "SESSIONID";
  protected static final String TEXT_HTML_CHARSET_UTF8 = TEXT_HTML + ";charset=utf-8";

  private static final Logger LOG = LoggerFactory.getLogger(ResourceBase.class);
  private static final long REMOVE_COOKIE = 0L;
  private static final SessionCookie NO_SESSION_COOKIE = null;
  private static final URI BASE_INET_ID_INIT_URI = URI.create("https://auth.freedom-id.de/init/start");
  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
  private static final ObjectReader INIT_DOMAIN_ID_RESPONSE_ENTITY_READER = OBJECT_MAPPER
          .readerFor(InitDomainIdResponseEntity.class);

  private final SessionDataStore sessionDataStore = SessionDataInMemoryStore.getInstance();
  private final UserAuthenticationDAO userAuthenticationDAO;
  private final DomainIdDnsClient dnsClient;
  private final Client restClient;

  protected ResourceBase(final UserAuthenticationDAO userAuthenticationDAO, final DomainIdDnsClient dnsClient,
                         final Client restClient) {
    Validate.notNull(userAuthenticationDAO, "Missing user authentication data access object");
    Validate.notNull(dnsClient, "Missing DNS client component");
    Validate.notNull(restClient, "Missing REST client component");
    this.userAuthenticationDAO = userAuthenticationDAO;
    this.dnsClient = dnsClient;
    this.restClient = restClient;
  }

  protected final View doIndexPage() {
    return new AskForDomainIdView();
  }

  /**
   * @param sessionCookie Optional
   * @param response      Required
   * @param operation     Required
   * @return Never <code>null</code>
   */
  protected final View ifSessionCookieIsNotValidShowIndexPageElse(final SessionCookie sessionCookie,
                                                                  final HttpServletResponse response, final Function<UserAuthentication, View> operation) {
    Validate.notNull(operation, "Missing operation");
    LOG.info("Got request with following session cookie: {}", sessionCookie);
    if (sessionCookie == null) {
      return doIndexPage();
    }

    final DomainId domainId = sessionCookie.getDomainId();
    final String cookieValue = sessionCookie.asCookieValue();
    final Optional<Duration> optSessionData = sessionDataStore.sessionDataOf(sessionCookie);
    if (optSessionData.isPresent()) {
      response.addCookie(createCookieApplying(cookieValue, optSessionData.get().getSeconds()));
      final UserAuthentication userAuthentication = userAuthenticationDAO.findUserAuthenticationBy(domainId);
      return operation.apply(userAuthentication);
    }

    response.addCookie(invalidated(sessionCookie));
    return askFor(domainId, response);
  }

  /**
   * @return Never <code>null</code>
   */
  protected final UserAuthenticationDAO getUserAuthenticationDAO() {
    return userAuthenticationDAO;
  }

  /**
   * @return Never <code>null</code>
   */
  protected final DomainIdDnsClient getDnsClient() {
    return dnsClient;
  }

  /**
   * @param value Required to be not empty
   * @return Never <code>null</code>
   */
  protected final Cookie createCookieApplying(final String value, final long maxAge) {
    Validate.notEmpty(value, "Missing cookie value");
    final Cookie cookie = new Cookie(SESSION_COOKIE, value);
    cookie.setMaxAge((int) maxAge);
    cookie.setComment("iNetID-Dashboard-Session");
    cookie.setHttpOnly(true);
    cookie.setSecure(false);
    cookie.setPath(DASHBOARD_PATH);
    return cookie;
  }

  /**
   * @param sessionCookie Required
   * @return Never <code>null</code>
   */
  protected final Cookie invalidated(final SessionCookie sessionCookie) {
    Validate.notNull(sessionCookie, "Missing session cookie");
    return createCookieApplying(sessionCookie.asCookieValue(), REMOVE_COOKIE);
  }

  /**
   * @param domainId Required
   * @param response Required
   */
  protected final View askFor(final DomainId domainId, final HttpServletResponse response) {
    return askFor(domainId, NO_SESSION_COOKIE, response);
  }

  /**
   * @param domainId      Required
   * @param sessionCookie Optional
   */
  protected final View askFor(final DomainId domainId, final SessionCookie sessionCookie, final HttpServletResponse
          response) {
    Validate.notNull(domainId, "Missing domain ID");
    LOG.info("Somebody asks for '{}' (session cookie '{}')", domainId, sessionCookie);
    final UserAuthentication userAuthentication = getUserAuthenticationDAO().findUserAuthenticationBy(domainId);
    if (userAuthentication == null) {
      LOG.info("User authentication NOT available. So going to init registration.");
      return initRegistrationOf(domainId);
    }

    LOG.info("User authentication available for DomainID '{}'. So going to login to account.", domainId);
    final LookupResult<URI> identityAgentHostName = dnsClient.lookupIdentityAgentHostNameOf(domainId);
    final Optional<Duration> optSessionMaxAge = sessionDataStore.sessionDataOf(sessionCookie);
    if (optSessionMaxAge.isPresent()) {
      response.addCookie(createCookieApplying(sessionCookie.asCookieValue(), optSessionMaxAge.get().getSeconds()));
      return newDomainIdAccountNavigationView(userAuthentication);
    }

    return new LoginToDomainIdAccountView(domainId, identityAgentHostName);
  }

  /**
   * @param domainId Required
   */
  protected final View initRegistrationOf(final DomainId domainId) {
    Validate.notNull(domainId, "Missing DomainID");
    final WebTarget targetURI = restClient.target(BASE_INET_ID_INIT_URI).path(domainId.getName());
    final String requestPayload = EMPTY;
    final Invocation initDomainIdInvocation = targetURI.request(APPLICATION_JSON).buildPut(json(requestPayload));
    final Pair<Response, String> invocationResult = invoke(initDomainIdInvocation, targetURI.getUri(), "PUT",
            requestPayload);
    final Response.StatusType responseStatusInfo = invocationResult.getLeft().getStatusInfo();
    InitDomainIdResponseEntity initDomainIdResponseEntity;
    try {
      initDomainIdResponseEntity = INIT_DOMAIN_ID_RESPONSE_ENTITY_READER.readValue(invocationResult.getRight());
    } catch (final IOException e) {
      throw new RuntimeException("Internal error", e);
    }

    if (SUCCESSFUL.equals(responseStatusInfo.getFamily())) {
      LOG.info("Initializing registration of DomainID '{}' succeeded", domainId);
      return new SuccessfullyInitializedDomainIdView(domainId, initDomainIdResponseEntity);
    }

    LOG.info("Initializing registration of DomainID '{}' failed", domainId);
    return new InitializingDomainIdFailedView(domainId, initDomainIdResponseEntity);
  }

  /**
   * @return Never <code>null</code>
   * @see #newDomainIdAccountNavigationView(UserAuthentication, LookupResult)
   */
  protected final View newDomainIdAccountNavigationView(final UserAuthentication userAuthentication) {
    return newDomainIdAccountNavigationView(userAuthentication,
            getDnsClient().lookupIdentityAgentHostNameOf(userAuthentication.getUserName()));
  }

  /**
   * @return Never <code>null</code>
   * @see DomainIdAccountNavigationView#DomainIdAccountNavigationView(DomainId, boolean, LookupResult)
   */
  protected final View newDomainIdAccountNavigationView(final UserAuthentication userAuthentication,
                                                        final LookupResult<URI> identityAgentLookup) {
    return new DomainIdAccountNavigationView(userAuthentication.getUserName(),
            userAuthentication.isActive(), identityAgentLookup);
  }

  /**
   * @param invocation                        Required
   * @param targetUriForLoggingOnly           Required, for logging purposes only
   * @param httpMethodForLoggingOnly          Required, for logging purposes only
   * @param optionalRequestBodyForLoggingOnly Optional, for logging purposes only
   * @return Never <code>null</code>, contains {@link Response} instance as well as response's body as {@link String}.
   */
  private Pair<Response, String> invoke(final Invocation invocation, final URI targetUriForLoggingOnly,
                                        final String httpMethodForLoggingOnly, final String optionalRequestBodyForLoggingOnly) {
    Validate.notNull(invocation, "Missing invocation");
    Validate.notNull(httpMethodForLoggingOnly, "Missing HTTP method");
    Validate.notNull(targetUriForLoggingOnly, "Missing target URI");
    final String requestBody = defaultIfEmpty(optionalRequestBodyForLoggingOnly, "");
    LOG.info("==> {}: >{}< ({})", httpMethodForLoggingOnly, requestBody, targetUriForLoggingOnly);
    final Instant startingAt = Instant.now();
    final Response invocationResponse = invocation.invoke();
    // {
    // final SSLSessionContext serverSessionContext = restApiClient.getSslContext().getServerSessionContext();
    // final Enumeration<byte[]> sslSessionIDs = serverSessionContext.getIds();
    // System.out.println("===> Session IDs: " + sslSessionIDs);
    // try {
    // while (sslSessionIDs.hasMoreElements()) {
    // final byte[] sslSessionID = sslSessionIDs.nextElement();
    // System.out.println("===> Session ID: " + sslSessionID);
    // final SSLSession sslSession = serverSessionContext.getSession(sslSessionID);
    // X509Certificate[] peerCertificateChain;
    // peerCertificateChain = sslSession.getPeerCertificateChain();
    // for (int i = 0; i < peerCertificateChain.length; i++) {
    // final X509Certificate x509Certificate = peerCertificateChain[i];
    // System.out.println("===> Checking validity of: " + x509Certificate);
    // x509Certificate.checkValidity();
    // }
    // }
    // } catch (final SSLPeerUnverifiedException | CertificateExpiredException | CertificateNotYetValidException e) {
    // e.printStackTrace();
    // }
    // }
    final Duration invocationDuration = Duration.between(startingAt, now());
    final String responseEntity = invocationResponse.readEntity(String.class);
    LOG.info("<== {} {}: >{}< ({}, elapsed {})", invocationResponse.getStatus(), invocationResponse.getStatusInfo(),
            responseEntity, targetUriForLoggingOnly, invocationDuration);
    return new ImmutablePair<>(invocationResponse, responseEntity);
  }

}
