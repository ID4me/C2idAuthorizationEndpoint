/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.user_acct.dashboard.resource;

import de.denic.domainid.DomainId;
import de.denic.domainid.connect2id.common.ClientId;
import de.denic.domainid.connect2id.tokenendpoint.TokenEndpointClient;
import de.denic.domainid.connect2id.tokenendpoint.TokenEndpointResponseEntity;
import de.denic.domainid.dbaccess.UserAuthentication;
import de.denic.domainid.dbaccess.UserAuthenticationDAO;
import de.denic.domainid.dns.DomainIdDnsClient;
import de.denic.domainid.dns.LookupResult;
import de.denic.domainid.user_acct.dashboard.entity.SessionCookie;
import de.denic.domainid.user_acct.dashboard.view.ShowIdentitesView;
import io.dropwizard.views.View;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.concurrent.Immutable;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import java.lang.reflect.Method;
import java.net.URI;

import static javax.ws.rs.core.Response.seeOther;
import static org.apache.commons.lang3.StringUtils.isAllEmpty;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

@Path(ManageIdentitiesResource.PATH)
@Immutable
public class ManageIdentitiesResource extends ResourceBase {

  static final String PATH = DASHBOARD_PATH + "/manageIdentities";
  private static final Logger LOG = LoggerFactory.getLogger(ManageIdentitiesResource.class);
  private static final ClientId DASHBOARD_CLIENT_ID = new ClientId("denic-id4me-dashboard");
  private static final Method AUTHENTICATION_REDIRECT_TARGET_METHOD;

  static {
    try {
      AUTHENTICATION_REDIRECT_TARGET_METHOD = ManageIdentitiesResource.class.getMethod("authenticationCallback",
              SessionCookie.class, String.class, String.class, String.class, String.class, HttpServletResponse.class,
              UriInfo.class);
    } catch (NoSuchMethodException e) {
      throw new ExceptionInInitializerError(e.toString());
    }
  }

  private final TokenEndpointClient tokenEndpointClient;
  private final String clientSecret;
  private final URI tokenEndpointURI;

  /**
   * @param userAuthenticationDAO Required
   * @param dnsClient             Required
   * @param restClient            Required
   * @param clientSecret          Required
   * @param tokenEndpointURI      Required
   */
  public ManageIdentitiesResource(final UserAuthenticationDAO userAuthenticationDAO, final DomainIdDnsClient
          dnsClient, final Client restClient, final String clientSecret, final URI tokenEndpointURI) {
    super(userAuthenticationDAO, dnsClient, restClient);
    Validate.notNull(tokenEndpointURI, "Missing token endpoint URI");
    Validate.notEmpty(clientSecret, "Missing client secret");
    this.tokenEndpointURI = tokenEndpointURI;
    this.clientSecret = clientSecret;
    tokenEndpointClient = new TokenEndpointClient.Impl(restClient);
  }

  @GET
  @Produces(TEXT_HTML_CHARSET_UTF8)
  public View showIdentities(@CookieParam(SESSION_COOKIE) final SessionCookie sessionCookie,
                             @Context final HttpServletResponse response) {
    return ifSessionCookieIsNotValidShowIndexPageElse(sessionCookie, response, userAuthentication ->
            new ShowIdentitesView(userAuthentication.getUserName(),
                    getUserAuthenticationDAO().findUserAuthenticationsBy(userAuthentication.getPseudonymUserName())));
  }

  @POST
  @Path("/bundle")
  @Produces(TEXT_HTML_CHARSET_UTF8)
  public View bundleAnotherIdentity(@CookieParam(SESSION_COOKIE) final SessionCookie sessionCookie,
                                    @FormParam("domainIdToBundle") final String domainIdToBundleValue,
                                    @FormParam("consented") final boolean consented,
                                    @Context final HttpServletResponse response,
                                    @Context final UriInfo uriInfo) {
    return ifSessionCookieIsNotValidShowIndexPageElse(sessionCookie, response, userAuthentication ->
            doBundleAnotherIdentity(userAuthentication, new DomainId(domainIdToBundleValue), consented, uriInfo));
  }

  private View doBundleAnotherIdentity(final UserAuthentication userAuthentication, final DomainId domainIdToBundle,
                                       final boolean consented, final UriInfo uriInfo) {
    if (!consented || userAuthentication.getUserName().equals(domainIdToBundle)) {
      return newDomainIdAccountNavigationView(userAuthentication);
    }

    if (!checkIdentityAuthorityAndStatusOf(domainIdToBundle)) {
      return newDomainIdAccountNavigationView(userAuthentication);
    }

    final URI redirectURI = UriBuilder.fromUri("https://auth.freedom-id.de/login?" +
            "client_id={DASHBOARD_CLIENT_ID}&" +
            "redirect_uri={MY_REDIRECT_URI}&" +
            "response_type=code&" +
            "scope=openid&" +
            "state={DOMAIN_ID_TO_BUNDLE}&" +
            "login_hint={DOMAIN_ID_TO_BUNDLE}").build(DASHBOARD_CLIENT_ID,
            buildCallbackUriFrom(uriInfo), domainIdToBundle.getName(),
            domainIdToBundle.getName());
    throw new WebApplicationException(seeOther(redirectURI).build());
  }

  private boolean checkIdentityAuthorityAndStatusOf(final DomainId domainIdToBundle) {
    final LookupResult<Pair<URI, URI>> lookupResult = getDnsClient().lookupIdentityAuthAndAgentHostNameOf(domainIdToBundle);
    if (lookupResult.isEmpty()) {
      LOG.warn("Bundling of domain ID {} fails: DNS lookup of it's Identity Authority provides no result", domainIdToBundle);
      return false;
    }

    final String identityAuthorityHostOfDomainIdToBundle = lookupResult.get().getKey().getHost();
    if (!tokenEndpointURI.getHost().equals(identityAuthorityHostOfDomainIdToBundle)) {
      LOG.warn("Bundling of domain ID {} fails: It's Identity Authority '{}' does not match our's '{}'",
              domainIdToBundle, identityAuthorityHostOfDomainIdToBundle, tokenEndpointURI.getHost());
      return false;
    }

    final boolean domainIdToBundleIsActive = getUserAuthenticationDAO().selectActiveOf(domainIdToBundle);
    if (!domainIdToBundleIsActive) {
      LOG.warn("Bundling of domain ID {} fails cause it is INACTIVE", domainIdToBundle);
    }
    return domainIdToBundleIsActive;
  }

  private URI buildCallbackUriFrom(UriInfo uriInfo) {
    return uriInfo.getBaseUriBuilder().path(ManageIdentitiesResource.class).path(AUTHENTICATION_REDIRECT_TARGET_METHOD).build();
  }

  /**
   * CAUTION: If changing name of signature of this method, modify referenced constant accordingly:
   *
   * @see #AUTHENTICATION_REDIRECT_TARGET_METHOD
   */
  @GET
  @Path("/callback")
  @Produces(TEXT_HTML_CHARSET_UTF8)
  public View authenticationCallback(@CookieParam(SESSION_COOKIE) final SessionCookie sessionCookie,
                                     @QueryParam("code") final String authenticationCode,
                                     @QueryParam("state") final String state,
                                     @QueryParam("error") final String error,
                                     @QueryParam("error_description") final String errorDescription,
                                     @Context final HttpServletResponse response,
                                     @Context final UriInfo uriInfo) {
    return ifSessionCookieIsNotValidShowIndexPageElse(sessionCookie, response, userAuthentication ->
            doAuthenticationCallback(userAuthentication, authenticationCode, state, error, errorDescription,
                    response, uriInfo, sessionCookie));
  }

  private View doAuthenticationCallback(final UserAuthentication userAuthentication, final String authenticationCode, final String
          state, final String error, final String errorDescription, final HttpServletResponse response, final UriInfo uriInfo, final SessionCookie sessionCookie) {
    LOG.info("authenticationCode = [" + authenticationCode + "], state = [" + state + "], error = [" + error + "], errorDescription = [" + errorDescription + "]");
    if (authenticationErrorOccured(error, errorDescription)) {
      return newDomainIdAccountNavigationView(userAuthentication);
    }

    if (!checkAuthenticationCode(authenticationCode, uriInfo)) {
      return newDomainIdAccountNavigationView(userAuthentication);
    }

    final DomainId domainIdToBundle = new DomainId(state);
    bundleSecondDomainIdWithFirstOne(userAuthentication.getUserName(), domainIdToBundle);
    return showIdentities(sessionCookie, response);
  }

  private boolean authenticationErrorOccured(final String error, final String errorDescription) {
    return !isAllEmpty(error, errorDescription);
  }

  private void bundleSecondDomainIdWithFirstOne(final DomainId firstDomainId, final DomainId secondDomainId) {
    getUserAuthenticationDAO().updatePseudonymUserNameOfSecondUserToTheOneOfFirstUser(firstDomainId, secondDomainId);
  }

  private boolean checkAuthenticationCode(final String authenticationCode, final UriInfo uriInfo) {
    final TokenEndpointResponseEntity responseEntity = tokenEndpointClient.exchange(tokenEndpointURI, DASHBOARD_CLIENT_ID, clientSecret,
            buildCallbackUriFrom(uriInfo), authenticationCode);
    return isNotEmpty(responseEntity.getAccessToken());
  }

}
