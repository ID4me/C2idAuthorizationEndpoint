/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.user_acct.dashboard.view;

import de.denic.domainid.DomainId;
import de.denic.domainid.dns.LookupResult;
import org.apache.commons.lang3.Validate;

import javax.annotation.concurrent.Immutable;
import java.net.URI;

@Immutable
public final class DomainIdAccountNavigationView extends ViewWithDomainId {

  private final LookupResult<URI> identityAgentLookup;
  private final boolean active;

  /**
   * @param domainId            Required
   * @param active              Required
   * @param identityAgentLookup Required
   */
  public DomainIdAccountNavigationView(final DomainId domainId, final boolean active,
                                       final LookupResult<URI> identityAgentLookup) {
    super("domainIdAccountNavigation.ftl", domainId);
    Validate.notNull(identityAgentLookup, "Missing lookup result of Identity Agent");
    this.identityAgentLookup = identityAgentLookup;
    this.active = active;
  }

  /**
   * @return Never <code>null</code>.
   */
  public LookupResult<URI> getIdentityAgentLookup() {
    return identityAgentLookup;
  }

  public boolean isActive() {
    return active;
  }

}
