/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.user_acct.dashboard.view;

import de.denic.domainid.DomainId;
import de.denic.domainid.connect2id.authzstore.entity.OpenIdConnectAuthorization;
import de.denic.domainid.connect2id.clientregistration.entity.OpenIdConnectClient;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import javax.annotation.concurrent.Immutable;
import java.util.*;

import static java.util.Collections.emptyList;
import static java.util.Collections.unmodifiableList;
import static java.util.ResourceBundle.getBundle;
import static java.util.stream.Collectors.toCollection;

@Immutable
public final class ShowClaimsPerClientView extends ViewWithDomainId {

  private static final ResourceBundle CLAIM_NAMES_RESOURCE_BUNDLE = getBundle("claimNamesResourceBundle");
  private static final ResourceBundle CLAIM_DESCRIPTIONS_RESOURCE_BUNDLE = getBundle("claimDescriptionsResourceBundle");
  private static final Comparator<Pair<OpenIdConnectClient, ?>> OPEN_ID_CONNECT_CLIENT_COMPARATOR = new Comparator<Pair<OpenIdConnectClient, ?>>() {

    @Override
    public int compare(final Pair<OpenIdConnectClient, ?> pair1, final Pair<OpenIdConnectClient, ?> pair2) {
      final String name1 = pair1.getKey().getName();
      final String name2 = pair2.getKey().getName();
      final int result;
      if (name1 != null) {
        if (name2 != null) {
          result = name1.compareTo(name2);
        } else {
          result = -1;
        }
      } else {
        if (name2 != null) {
          result = 1;
        } else {
          result = pair1.getKey().getId().compareTo(pair2.getKey().getId());
        }
      }
      return result;
    }

  };

  private final List<Pair<OpenIdConnectClient, OpenIdConnectAuthorization>> clientAuthorisationPairs;

  /**
   * @param domainId                             Required
   * @param allAuthorisationsOfDomainIdPerClient Optional
   */
  public ShowClaimsPerClientView(final DomainId domainId,
                                 final Map<OpenIdConnectClient, OpenIdConnectAuthorization> allAuthorisationsOfDomainIdPerClient) {
    super("showClaimsPerClient.ftl", domainId);
    final List<Pair<OpenIdConnectClient, OpenIdConnectAuthorization>> modifiableList = (allAuthorisationsOfDomainIdPerClient == null
            ? emptyList()
            : allAuthorisationsOfDomainIdPerClient.entrySet().stream()
            .map(entry -> new ImmutablePair<>(entry.getKey(), entry.getValue()))
            .collect(toCollection(() -> new ArrayList<>(allAuthorisationsOfDomainIdPerClient.size()))));
    modifiableList.sort(OPEN_ID_CONNECT_CLIENT_COMPARATOR);
    this.clientAuthorisationPairs = unmodifiableList(modifiableList);
  }

  /**
   * @return Never <code>null</code>, but unmodifiable.
   */
  public List<Pair<OpenIdConnectClient, OpenIdConnectAuthorization>> getClientAuthorisationPairs() {
    return clientAuthorisationPairs;
  }

  /**
   * @param claim Required to be not empty.
   * @return Never <code>null</code> nor empty.
   */
  public String getNameOfClaim(final String claim) {
    Validate.notEmpty(claim, "Missing claim");
    try {
      return CLAIM_NAMES_RESOURCE_BUNDLE.getString(claim);
    } catch (MissingResourceException e) {
      return claim;
    }
  }

  /**
   * @param claim Required to be not empty.
   * @return Never <code>null</code> nor empty.
   */
  public String getDescriptionOfClaim(final String claim) {
    Validate.notEmpty(claim, "Missing claim");
    try {
      return CLAIM_DESCRIPTIONS_RESOURCE_BUNDLE.getString(claim);
    } catch (MissingResourceException e) {
      return "Description of " + claim;
    }
  }

}
