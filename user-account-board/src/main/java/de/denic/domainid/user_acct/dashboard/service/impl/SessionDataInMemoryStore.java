/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.user_acct.dashboard.service.impl;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.Metric;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.SharedMetricRegistries;
import de.denic.domainid.DomainId;
import de.denic.domainid.user_acct.dashboard.entity.SessionCookie;
import de.denic.domainid.user_acct.dashboard.service.SessionDataStore;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.text.RandomStringGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.concurrent.GuardedBy;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.ThreadSafe;
import java.security.SecureRandom;
import java.time.Duration;
import java.time.Instant;
import java.util.*;

import static com.codahale.metrics.MetricRegistry.name;
import static java.time.Instant.now;
import static java.time.temporal.ChronoUnit.MINUTES;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.apache.commons.text.CharacterPredicates.DIGITS;
import static org.apache.commons.text.CharacterPredicates.LETTERS;

@ThreadSafe
public final class SessionDataInMemoryStore implements SessionDataStore {

  private static final Logger LOG = LoggerFactory.getLogger(SessionDataInMemoryStore.class);
  private static final SessionDataStore INSTANCE = new SessionDataInMemoryStore();
  private static final Random SALT_GENERATOR_RANDOM = new SecureRandom();
  private static final RandomStringGenerator SALT_GENERATOR = new RandomStringGenerator.Builder().withinRange('0', 'z')
          .usingRandom(SALT_GENERATOR_RANDOM::nextInt).filteredBy(LETTERS, DIGITS).build();
  private static final Duration SESSION_COOKIE_LIFETIME = Duration.of(10L, MINUTES);

  @GuardedBy("sessionDataOfDomainIds")
  private final Map<DomainId, SessionData> sessionDataOfDomainIds = new HashMap<>();

  private SessionDataInMemoryStore() {
    if (SharedMetricRegistries.names().isEmpty()) {
      LOG.warn("Found NO Metric Registry, so registering session counter metric NOT possible!");
    } else {
      final MetricRegistry metricRegistry = SharedMetricRegistries.getDefault();
      final Metric metric = new Gauge<Integer>() {

        @Override
        public Integer getValue() {
          final Instant now = Instant.now();
          synchronized (sessionDataOfDomainIds) {
            sessionDataOfDomainIds.values().removeIf(sessionData -> sessionData.expiredBefore(now));
            return sessionDataOfDomainIds.size();
          }
        }

      };
      metricRegistry.register(
              name("user-account-board", SessionDataInMemoryStore.class.getSimpleName(), "session", "count"), metric);
    }
  }

  /**
   * @return Never <code>null</code>
   */
  public static SessionDataStore getInstance() {
    return INSTANCE;
  }

  @Override
  public Pair<SessionCookie, Duration> newSessionDataWith(final DomainId domainId) {
    final String salt = SALT_GENERATOR.generate(32);
    final Duration sessionCookieLifetime = SESSION_COOKIE_LIFETIME;
    final SessionData newSessionData = new SessionData(salt, now().plus(sessionCookieLifetime));
    final SessionData oldSessionData;
    synchronized (sessionDataOfDomainIds) {
      oldSessionData = sessionDataOfDomainIds.put(domainId, newSessionData);
    }
    LOG.info("New session data of {}: '{}' (substitutes: '{}')", domainId, newSessionData, oldSessionData);
    return new ImmutablePair<>(SessionCookie.from(domainId, salt), sessionCookieLifetime);
  }

  @Override
  public Optional<Duration> sessionDataOf(final SessionCookie sessionCookie) {
    if (sessionCookie == null) {
      return empty();
    }

    final DomainId domainId = sessionCookie.getDomainId();
    SessionData sessionData;
    Optional<Duration> result = empty();
    synchronized (sessionDataOfDomainIds) {
      sessionData = sessionDataOfDomainIds.get(domainId);
      if (sessionData == null) {
        // Nothing to do here
      } else if (sessionData.isExpired()) {
        sessionDataOfDomainIds.remove(domainId);
      } else if (!sessionData.hasMismatchingHashWith(sessionCookie)) {
        sessionData = sessionData.withExtendExpiration(SESSION_COOKIE_LIFETIME);
        sessionDataOfDomainIds.put(domainId, sessionData);
        result = of(SESSION_COOKIE_LIFETIME);
      }
    }
    LOG.info("Cookie '{}' matches session data '{}'", sessionCookie, sessionData);
    return result;
  }

  @Override
  public void clearSessionDataOf(final SessionCookie sessionCookie) {
    Validate.notNull(sessionCookie, "Missing session cookie");
    synchronized (sessionDataOfDomainIds) {
      sessionDataOfDomainIds.remove(sessionCookie.getDomainId());
    }
  }

  @Immutable
  private static final class SessionData {

    private final String salt;
    private final Instant expirationInstant;

    /**
     * @param salt              Required to be not empty
     * @param expirationInstant Required to lie in the future
     */
    public SessionData(final String salt, final Instant expirationInstant) {
      Validate.notEmpty(salt, "Missing salt");
      Validate.notNull(expirationInstant, "Missing expiration instant");
      Validate.isTrue(expirationInstant.isAfter(now()), "Expiration instant not in the future: %s", expirationInstant);
      this.salt = salt;
      this.expirationInstant = expirationInstant;
    }

    /**
     * @param sessionCookie Required
     */
    public boolean hasMismatchingHashWith(final SessionCookie sessionCookie) {
      Validate.notNull(sessionCookie, "Missing session cookie");
      return !sessionCookie.matchesApplyingSalt(salt);
    }

    /**
     * Does <strong>NOT</strong> modify current instance but creates a new one!
     *
     * @param extension Required
     * @return Never <code>null</code>
     */
    public SessionData withExtendExpiration(final Duration extension) {
      Validate.notNull(extension, "Missing extension");
      return new SessionData(this.salt, now().plus(extension));
    }

    /**
     * @param instant Required
     */
    public boolean expiredBefore(final Instant instant) {
      Validate.notNull(instant, "Missing instant");
      return expirationInstant.isBefore(instant);
    }

    public boolean isExpired() {
      return expiredBefore(now());
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      SessionData that = (SessionData) o;
      return Objects.equals(salt, that.salt) &&
              Objects.equals(expirationInstant, that.expirationInstant);
    }

    @Override
    public int hashCode() {
      return Objects.hash(salt, expirationInstant);
    }

    @Override
    public String toString() {
      return "expires " + expirationInstant;
    }

  }

}
