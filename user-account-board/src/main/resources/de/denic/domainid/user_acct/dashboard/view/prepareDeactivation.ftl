<#ftl output_format="HTML" encoding="UTF-8">
<#-- @ftlvariable name="" type="de.denic.domainid.user_acct.dashboard.view.PrepareDeactivationView" -->
<html>
	<#include "include/head.ftl">
	<body class="w3-container">
		<div class="w3-display-container w3-margin-top">
			<div class="w3-display-topmiddle w3-card-4" style="width:450px">
				<#include "include/identityAuthorityHeader.ftl">
				<div class="w3-padding">
					<form action="/dashboard/deactivation" method="POST" accept-charset="utf-8" style="margin-bottom:0px;">
						<p>
							<input class="w3-input" type="text" id="domainID" name="domainID" maxlength="40" value="${domainId}" disabled="true">
							<label class="w3-tiny"> Your ${idName} </label>
						</p>
						<p>
							Here you may <em>deactivate</em>/<em>disable</em> your ${idName}! Kind of an <em>emergency shut-off</em>.
						</p>
						<p>
							If you proceed,
							<ul>
								<li><em>all</em> the authorizations you give to Relying Parties/Shops will get revoked, so issued access/refresh tokens will get invalidated.</li>
							</ul>
						</p>
						<p>
							This will have some effects, for example:
							<ul>
								<li>Authentication on Relying Party's/Shop's websites using your ${idName} won't be able anymore.</li>
								<li>Relying Parties/Shops won't have access to your personal data anymore by querying appropriate endpoints with the access tokens issued in the past.</li>
							</ul>
						</p>
						<p>
							This might be a useful step, e.g. if at least one of the following is true:
							<ul>
								<li>Your ${idName} identity has been captured by some malicious party.</li>
							</ul>
						</p>
						<p>
							<input class="w3-button w3-inetid-gold w3-padding-small w3-small" type="checkbox" name="consented" value="true"> YES, I definitely want to deactivate my ${idName}! </input>
						</p>
						<p>
							<button class="w3-button w3-inetid-gold w3-padding-small w3-small" type="submit" name="operation" value="deactivate"> Deactivate! </button>
						</p>
					</form>
					<p></p>
					<div class="w3-container w3-center">
						<form method="GET" accept-charset="utf-8">
							<button class="w3-button w3-border w3-round w3-inetid-gold" type="submit" formaction="/dashboard/showDomainIdAccountNavigationView" accesskey="b">BACK</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
