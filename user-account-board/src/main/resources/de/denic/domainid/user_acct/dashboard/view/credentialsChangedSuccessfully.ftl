<#ftl output_format="HTML" encoding="UTF-8">
<#-- @ftlvariable name="" type="de.denic.domainid.user_acct.dashboard.view.CredentialsChangedSuccessfullyView" -->
<html>
	<#include "include/head.ftl">
	<body class="w3-container">
		<div class="w3-display-container w3-margin-top" style="height:300px;">
			<form method="GET" accept-charset="utf-8">
				<div class="w3-display-topmiddle w3-card-4" style="width:350px">
					<#include "include/identityAuthorityHeader.ftl">
					<div class="w3-padding">
						<p>Credentials of your ${idName} <i>${domainId}</i> has been updated successfully!</p>
						<p>
							<div class="w3-container w3-center">
								<button class="w3-button w3-border w3-round w3-inetid-grey" type="submit" formaction="/dashboard/showDomainIdAccountNavigationView" accesskey="b">BACK</button>
							</div>
						</p>
					</div>
				</div>
			</form>
		</div>
	</body>
</html>
