<#ftl output_format="HTML" encoding="UTF-8">
<#-- @ftlvariable name="" type="de.denic.domainid.user_acct.dashboard.view.ChangeDomainIdCredentialsView" -->
<html>
	<#include "include/head.ftl">
	<body class="w3-container">
		<div class="w3-display-container w3-margin-top" style="height:300px;">
			<form action="/dashboard/change" method="POST" accept-charset="utf-8">
				<div class="w3-display-topmiddle w3-card-4" style="width:350px">
					<#include "include/identityAuthorityHeader.ftl">
					<div class="w3-padding">
						<p> 
							<input class="w3-input" type="text" id="domainID" name="domainID" maxlength="40" value="${domainId}" disabled="true">
							<label class="w3-tiny"> Your ${idName} </label>
						</p>
						<p>
							<input class="w3-input" type="password" id="currentPassword" name="currentPassword" maxlength="32" placeholder="Actual password" autofocus="on" accesskey="p"/>
							<label class="w3-tiny"> Your actual password </label>
						</p>
						<p>
							<input class="w3-input" type="password" id="newPassword" name="newPassword" maxlength="32" placeholder="New password" accesskey="n"/>
							<label class="w3-tiny"> Choose your new password </label>
						</p>
						<p>
							<input class="w3-input" type="password" id="passwordConfirmation" name="passwordConfirmation" maxlength="32" placeholder="Retype password" accesskey="r"/>
							<label class="w3-tiny"> Confirm your new password </label>
							<div class="w3-container w3-center">
								<input class="w3-button w3-inetid-gold" type="submit" name="submit" value="Change" accesskey="c">
							</div>
						</p>
						<p>
							<div class="w3-container w3-center">
								<button class="w3-button w3-border w3-round w3-inetid-grey" type="submit" formaction="/dashboard/showDomainIdAccountNavigationView" accesskey="b">BACK</button>
							</div>
						</p>
					</div>
				</div>
			</form>
		</div>
	</body>
</html>
