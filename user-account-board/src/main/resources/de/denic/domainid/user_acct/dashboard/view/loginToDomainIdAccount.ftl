<#ftl output_format="HTML" encoding="UTF-8">
<#-- @ftlvariable name="" type="de.denic.domainid.user_acct.dashboard.view.LoginToDomainIdAccountView" -->
<html>
	<#include "include/head.ftl">
	<body class="w3-container">
		<div class="w3-display-container w3-margin-top" style="height:300px;">
			<div class="w3-display-topmiddle w3-card-4" style="width:350px">
				<#include "include/identityAuthorityHeader.ftl">
				<div class="w3-padding">
					<form action="/dashboard/login" method="POST" accept-charset="utf-8">
						<p>
							<input class="w3-input" type="text" id="domainID" name="domainID" maxlength="40" value="${domainId}" disabled="true">
							<input type="hidden" name="domainID" value="${domainId}"/>
							<label class="w3-tiny"> Your ${idName} </label>
						</p>
						<p>
							<input class="w3-input" type="password" id="currentPassword" name="currentPassword" maxlength="32" placeholder="Actual password" autofocus="on" accesskey="p"/>
							<label class="w3-tiny"> Your actual password </label>
							<div class="w3-container w3-center">
								<button class="w3-button w3-border w3-round w3-inetid-gold" type="submit" id="login" value="login" name="loginOrForgot" accesskey="l">Login</button>
								<button class="w3-button w3-border w3-round w3-inetid-grey" type="submit" id="forgot" value="forgot" name="loginOrForgot" accesskey="f">Forgot my password</button>
							</div>
						</p>
						<p>
							DNS-based information regarding your ${idName} <i>${domainId}</i>:
						</p>
						<p>
							<ul>
<#if identityAgentLookup.empty>
								<li>Identity Agent host: STILL UNKNOWN</li>
<#else>
								<li>Identity Agent host: <i>${identityAgentLookup.get().getHost()}${identityAgentLookup.get().getPath()}</i></li>
</#if>
								<li>These data have ${identityAgentLookup.validated?then("successfully", "NOT")} been validated via DNSSEC!</li>
							</ul>
						</p>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>
