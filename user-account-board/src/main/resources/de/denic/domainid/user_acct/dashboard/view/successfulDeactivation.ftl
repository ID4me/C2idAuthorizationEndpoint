<#ftl output_format="HTML" encoding="UTF-8">
<#-- @ftlvariable name="" type="de.denic.domainid.user_acct.dashboard.view.SuccessfulDeactivationView" -->
<html>
	<#include "include/head.ftl">
	<body class="w3-container">
		<div class="w3-display-container w3-margin-top">
			<div class="w3-display-topmiddle w3-card-4" style="width:350px">
				<#include "include/identityAuthorityHeader.ftl">
				<div class="w3-padding">
					<p>
						<input class="w3-input" type="text" id="domainID" name="domainID" maxlength="40" value="${domainId}" disabled="true">
						<label class="w3-tiny"> Your ${idName} </label>
					</p>
					<p>
						Your ${idName} has been deactivated successfully!
					</p>
					<p></p>
					<div class="w3-container w3-center">
						<form method="GET" accept-charset="utf-8">
							<button class="w3-button w3-border w3-round w3-inetid-gold" type="submit" formaction="/dashboard/showDomainIdAccountNavigationView" accesskey="b">BACK</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
