<#ftl output_format="HTML" encoding="UTF-8">
<#-- @ftlvariable name="" type="de.denic.domainid.user_acct.dashboard.view.ManageTwoFactorAuthenticationView" -->
<html>
	<#include "include/head.ftl">
	<body class="w3-container">
		<div class="w3-display-container w3-margin-top">
			<div class="w3-display-topmiddle w3-card-4" style="width:350px">
				<#include "include/identityAuthorityHeader.ftl">
				<div class="w3-padding">
					<form action="/dashboard/changeTwoFactorAuth" method="POST" accept-charset="utf-8" style="margin-bottom:0px;">
						<p>
							<input class="w3-input" type="text" id="domainID" name="domainID" maxlength="40" value="${domainId}" disabled="true">
							<label class="w3-tiny"> Your ${idName} </label>
						</p>
						<p>
							Two-Factor Authentication is currently <em>${userAuthentication.twoFactorAuthEnabled?string("enabled", "disabled")}</em>.
						</p>
<#if base64EncodedQRCode??>
						<p class="w3-center">
							<input type="hidden" id="sharedSecret" name="sharedSecret" value="${sharedSecret}">
							<img height="200" width="200" src="data:image/png;base64,${base64EncodedQRCode}" alt="TOTP shared secret ${sharedSecret}">
							<br>
							<label class="w3-tiny">Scan this QR code with your authentication app.</label>
						</p>
						<p>
							<input class="w3-input" type="text" id="verificationCode" name="verificationCode" maxlength="8" placeholder="123456" autofocus="on" autocomplete="off" accesskey="v"/>
							<label class="w3-tiny">Open your authentication app and enter the numeric code here.</label>
						</p>
</#if>
<#if userAuthentication.twoFactorAuthEnabled>
						<p>
							<button class="w3-button w3-inetid-gold w3-padding-small w3-small" type="submit" name="operation" value="disable"> Disable </button>
						</p>
<#else>
	<#if base64EncodedQRCode??>
						<p>
							<button class="w3-button w3-inetid-gold w3-padding-small w3-small" type="submit" name="operation" value="activate"> Activate </button>
						</p>
	<#else>
						<p>
							<button class="w3-button w3-inetid-gold w3-padding-small w3-small" type="submit" name="operation" value="enable"> Enable </button>
						</p>
	</#if>
</#if>
					</form>
					<p></p>
					<div class="w3-container w3-center">
						<form method="GET" accept-charset="utf-8">
							<button class="w3-button w3-border w3-round w3-inetid-gold" type="submit" formaction="/dashboard/showDomainIdAccountNavigationView" accesskey="b">BACK</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
