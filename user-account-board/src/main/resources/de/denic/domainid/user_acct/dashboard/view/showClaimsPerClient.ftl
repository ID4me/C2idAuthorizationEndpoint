<#ftl output_format="HTML" encoding="UTF-8">
<#-- @ftlvariable name="" type="de.denic.domainid.user_acct.dashboard.view.DomainIdAccountNavigationView" -->
<html>
	<#include "include/head.ftl">
	<body class="w3-container">
		<div class="w3-display-container w3-margin-top">
			<div class="w3-display-topmiddle w3-card-4" style="width:480px">
				<#include "include/identityAuthorityHeader.ftl">
				<div class="w3-padding">
					<p>
						<input class="w3-input" type="text" id="domainID" name="domainID" maxlength="40" value="${domainId}" disabled="true">
						<label class="w3-tiny"> Your ${idName} </label>
					</p>
<#list clientAuthorisationPairs>
					<p>
						Manage who accesses your data:
					</p>
					<table class="w3-table w3-bordered">
						<thead>
							<tr class="w3-light-grey">
								<th>Client/Relying Party</th>
								<th>Has access to</th>
								<th>Permanently revoked access to</th>
							</tr>
						</thead>
						<tbody>
	<#items as clientAuthorisationPair>
							<tr>
								<td>
									<p>
		<#if clientAuthorisationPair.key.logoURI??>
	    							<img style="height:24px;width:auto" src="${clientAuthorisationPair.key.logoURI}">
		</#if>
		<#if clientAuthorisationPair.key.name??>
										${clientAuthorisationPair.key.name}
		<#else>
										ID <i>${clientAuthorisationPair.key.id}</i>
		</#if>
									</p>
									<form action="/dashboard/changeClaimsPerClient/${clientAuthorisationPair.key.id}/revoke" method="POST" accept-charset="utf-8" style="margin-bottom:0px;">
										<button class="w3-button w3-inetid-gold w3-padding-small w3-tiny" type="submit" name="submit" accesskey="r">Revoke access completely</button>
									</form>
								</td>
								<td>
		<#list clientAuthorisationPair.value.allConsentedClaims>
									<form action="/dashboard/changeClaimsPerClient/${clientAuthorisationPair.key.id}" method="POST" accept-charset="utf-8" style="margin-bottom:0px;">
										<fieldset>
											<ul class="w3-ul w3-small">
			<#items as claim>
												<li class="w3-padding-tiny" title="${getDescriptionOfClaim(claim)}">
													<label>
														<input type="checkbox" id="${claim}" name="claims" value="${claim}" checked="checked">
														${getNameOfClaim(claim)}
													</label>
												</li> 
			</#items>
												<li class="w3-padding-tiny">
													<button class="w3-button w3-inetid-gold w3-padding-small" type="submit" name="submit" accesskey="s"> Save </button>
												</li> 
											</ul>
										</fieldset>
									</form>
		<#else>
									<fieldset class="w3-card-4">
										<ul class="w3-ul w3-small">
											<li class="w3-padding-tiny">
												No claims yet.
											</li>
										</ul>
									</fieldset>
		</#list>
								</td>
								<td>
		<#if (clientAuthorisationPair.value.additionalData.rejectedClaimsValue)?has_content>
			<#list clientAuthorisationPair.value.additionalData.rejectedClaimsValue>
									<fieldset>
										<ul class="w3-ul w3-small">
				<#items as rejectedClaim>
											<li class="w3-padding-tiny" title="${getDescriptionOfClaim(rejectedClaim)}">
												<label>
													<!--input type="checkbox" id="${rejectedClaim}" name="rejectedClaims" value="${rejectedClaim}" checked="checked"-->
													${getNameOfClaim(rejectedClaim)}
												</label>
											</li> 
				</#items>
											<li class="w3-padding-tiny">
												<button class="w3-button w3-inetid-gold w3-padding-small" type="submit" name="submit" accesskey="s" disabled="true"> Save </button>
											</li> 
										</ul>
									</fieldset>
			</#list>
		<#else>
									<fieldset class="w3-card-4">
										<ul class="w3-ul w3-small">
											<li class="w3-padding-tiny">
												No rejected claims yet.
											</li>
										</ul>
									</fieldset>
		</#if>
								</td>
							</tr>
	</#items>
						</tbody>
					</table>
<#else>
					You have not been logged in to any Client/Relying Party yet.
</#list>
					<p></p>
					<div class="w3-container w3-center">
						<form method="GET" accept-charset="utf-8">
							<button class="w3-button w3-border w3-round w3-inetid-gold" type="submit" formaction="/dashboard/showDomainIdAccountNavigationView" accesskey="b">BACK</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
