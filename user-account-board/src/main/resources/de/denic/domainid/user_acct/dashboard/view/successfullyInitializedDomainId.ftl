<#ftl output_format="HTML" encoding="UTF-8">
<#-- @ftlvariable name="" type="de.denic.domainid.user_acct.dashboard.view.SuccessfullyInitializedDomainIdView" -->
<html>
	<#include "include/head.ftl">
	<body class="w3-container">
		<div class="w3-display-container w3-margin-top" style="height:300px;">
			<div class="w3-display-topmiddle w3-card-4" style="width:350px">
				<#include "include/identityAuthorityHeader.ftl">
				<div class="w3-padding">
					<p>Process to register your ${idName} <i>${domainId}</i> has been launched successfully!</p>
					<p>Please check your mail account <i>${responseEntity.parameters['emailaddress']}</i> for a message describing further steps.</p>
				</div>
			</div>
		</div>
	</body>
</html>
