<#ftl output_format="HTML" encoding="UTF-8">
<#-- @ftlvariable name="" type="de.denic.domainid.user_acct.dashboard.view.ShowIdentitesView" -->
<html>
	<#include "include/head.ftl">
	<body class="w3-container">
		<div class="w3-display-container w3-margin-top">
			<div class="w3-display-topmiddle w3-card-4" style="width:480px">
				<#include "include/identityAuthorityHeader.ftl">
				<div class="w3-padding">
					<p>
						<input class="w3-input" type="text" id="domainID" name="domainID" maxlength="40" value="${domainId}" disabled="true">
						<label class="w3-tiny"> Your current ${idName} </label>
					</p>
<#list bundledAuthentications>
					<h4 class="w3-center">Currently bundled with following identities:</h4>
					<table class="w3-table w3-bordered">
						<thead>
							<tr class="w3-light-grey">
								<th>${idName}</th>
								<th>Two-Factor authenticated?</th>
								<th>Active?</th>
							</tr>
						</thead>
						<tbody>
	<#items as bundledAuthentication>
							<tr>
								<td title="Subject: ${bundledAuthentication.pseudonymUserName}">
									<p>
										${bundledAuthentication.userName}
									</p>
								</td>
								<td>
									<p>
										${bundledAuthentication.twoFactorAuthEnabled?then("YES", "NO")}
									</p>
								</td>
								<td>
									<p>
										${bundledAuthentication.active?then("YES", "NO")}
									</p>
								</td>
							</tr>
	</#items>
						</tbody>
					</table>
<#else>
					<p>
						You currently have no further bundled ${idName} identities.
					</p>
</#list>
					<h4 class="w3-center">Bundling with another ${idName} identity</h4>
					<div class="w3-center">
						Bundling another ${idName} identity with your current one
						<br>
						'<i>${domainId}</i>'
						<br>
						requires this other identity
			      </div>
					<ul>
						<li>to be authenticated by DENIC's Identity Authority (the one you're currently interacting with);</li>
						<li>to be active (cause you have to authenticate yourself as owner of this other identity).</li>
					</ul>
      			<div class="w3-center">
						<form method="POST" action="/dashboard/manageIdentities/bundle" accept-charset="utf-8" >
							<input type="hidden" id="domainID" name="domainID" maxlength="40" value="${domainId}">
							<p>
								<input class="w3-input" type="text" id="domainIdToBundle" name="domainIdToBundle" maxlength="40" placeholder="another.${idName}.example" autofocus="on">
								<label class="w3-tiny"> Your other ${idName} identity to bundle </label>
							</p>
							<p>
								<input class="w3-button w3-inetid-gold w3-padding-small w3-small" type="checkbox" name="consented" value="true"> YES, I definitely want to bundle my ${idName}! </input>
							</p>
							<p>
								<button class="w3-button w3-border w3-round w3-inetid-gold" type="submit" accesskey="a">START by authentication</button>
							</p>
						</form>
					</div>
					<div class="w3-container w3-center">
						<form method="GET" accept-charset="utf-8">
							<button class="w3-button w3-border w3-round w3-inetid-gold" type="submit" formaction="/dashboard/showDomainIdAccountNavigationView" accesskey="b">BACK</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
