<#ftl output_format="HTML" encoding="UTF-8">
<#-- @ftlvariable name="" type="de.denic.domainid.user_acct.dashboard.view.TwoFactorAuthenticationView" -->
<html>
	<#include "include/head.ftl">
	<body class="w3-container">
		<div class="w3-display-container w3-margin-top" style="height:300px;">
			<div class="w3-display-topmiddle w3-card-4" style="width:350px">
				<#include "include/identityAuthorityHeader.ftl">
				<div class="w3-padding">
					<form action="/dashboard/login/totp" method="POST" accept-charset="utf-8">
						<p>
							<input class="w3-input" type="text" id="domainID" name="domainID" maxlength="40" value="${domainId}" disabled="true">
							<input type="hidden" name="domainID" value="${domainId}"/>
							<label class="w3-tiny"> Your ${idName} </label>
						</p>
						<p>
							<input class="w3-input" type="text" id="verificationCode" name="verificationCode" maxlength="8" placeholder="123456" autofocus="on" autocomplete="off" accesskey="v"/>
							<label class="w3-tiny"> Open your authentication app and enter the numeric code here </label>
							<div class="w3-container w3-center">
								<button class="w3-button w3-border w3-round w3-inetid-gold" type="submit" value="login" name="loginOrForgot" accesskey="l">Login</button>
							</div>
						</p>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>
