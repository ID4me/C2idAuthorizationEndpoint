<#ftl output_format="HTML" encoding="UTF-8">
<#-- @ftlvariable name="" type="de.denic.domainid.user_acct.dashboard.view.InitializingDomainIdFailedView" -->
<html>
	<#include "include/head.ftl">
	<body class="w3-container">
		<div class="w3-display-container w3-margin-top" style="height:300px;">
			<div class="w3-display-topmiddle w3-card-4" style="width:350px">
				<#include "include/identityAuthorityHeader.ftl">
				<div class="w3-padding">
					<p>Launching process to register your ${idName} <i>${domainId}</i> has failed:</p>
					<p>${responseEntity.message}</p>
				</div>
			</div>
		</div>
	</body>
</html>
