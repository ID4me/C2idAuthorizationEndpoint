<#ftl output_format="HTML" encoding="UTF-8">
<#-- @ftlvariable name="" type="de.denic.domainid.user_acct.dashboard.view.AskForDomainIdView" -->
<html>
	<#include "include/head.ftl">
	<body class="w3-container">
		<div class="w3-display-container w3-margin-top" style="height:300px;">
			<form action="/dashboard" method="POST" accept-charset="utf-8">
				<div class="w3-display-topmiddle w3-card-4" style="width:350px">
					<#include "include/identityAuthorityHeader.ftl">
					<div class="w3-padding">
						<p>
							<input class="w3-input" type="text" id="domainID" name="domainID" maxlength="40" placeholder="your.${idName}.example" autofocus="on">
							<label class="w3-tiny"> Your ${idName}? </label>
						</p>
						<p>
							<div class="w3-container w3-center">
								<input class="w3-button w3-inetid-gold" type="submit" id="submit" name="submit" value="Next">
							</div>
						</p>
					</div>
				</div>
			</form>
		</div>
	</body>
</html>
