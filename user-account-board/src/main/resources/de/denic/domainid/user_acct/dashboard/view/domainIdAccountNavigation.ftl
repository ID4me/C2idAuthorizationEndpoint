<#ftl output_format="HTML" encoding="UTF-8">
<#-- @ftlvariable name="" type="de.denic.domainid.user_acct.dashboard.view.DomainIdAccountNavigationView" -->
<html>
	<#include "include/head.ftl">
	<body class="w3-container">
		<div class="w3-display-container w3-margin-top">
			<div class="w3-display-topmiddle w3-card-4" style="width:400px">
				<#include "include/identityAuthorityHeader.ftl">
				<div class="w3-padding">
					<form method="POST" accept-charset="utf-8">
						<p>
							<input class="w3-input" type="text" id="domainID" maxlength="40" value="${domainId}" disabled="true">
							<label class="w3-tiny"> Your ${idName} </label>
						</p>
						<p>
							Current state:
<#if active>
							<span style='color:green'>ACTIVE</span>
<#else>
							<span style='color:red'>DEACTIVATED</span>
</#if>
						</p>
						Manage your ${idName} account by:
						<p>
							<ul>
								<li>
									<button class="w3-button w3-border w3-round w3-inetid-gold" type="submit" id="manageActivationStatus" formaction="/dashboard/manageActivationStatus" formmethod="GET" accesskey="a">Activation status of your ${idName}</button>
								</li>
								<li>
									<button class="w3-button w3-border w3-round w3-inetid-gold" type="submit" id="changePasswordView" formaction="/dashboard/changePasswordView" formmethod="POST" accesskey="c">Change your credentials</button>
								</li>
								<li>
									<button class="w3-button w3-border w3-round w3-inetid-gold" type="submit" id="manage2FAView" formaction="/dashboard/manageTwoFactorAuth" formmethod="POST" accesskey="m">Two Factor Authentication</button>
								</li>
								<li>
									<button class="w3-button w3-border w3-round w3-inetid-gold" type="submit" id="showClaimsPerClientView" formaction="/dashboard/showClaimsPerClientView" formmethod="POST" accesskey="s">Who accesses your data</button>
								</li>
								<li>
									<button class="w3-button w3-border w3-round w3-inetid-gold" type="submit" id="bundleIdentifiers" formaction="/dashboard/manageIdentities" formmethod="GET" accesskey="b">Manage your ${idName} identities</button>
								</li>
							</ul>
						</p>
						<p>
							<ul>
								<li>
									<button class="w3-button w3-border w3-round w3-inetid-grey" type="submit" id="logout" formaction="/dashboard/logout" formmethod="POST" accesskey="l">Logout</button>
								</li>
							</ul>
						</p>
						<p>
							DNS-based information:
						</p>
						<p>
							<ul>
<#if identityAgentLookup.empty>
								<li>Identity Agent host: <span style='color:red'>NOT FOUND</span></li>
<#else>
								<li>Identity Agent host: <i>${identityAgentLookup.get().getHost()}${identityAgentLookup.get().getPath()}</i></li>
</#if>
								<li>These data have ${identityAgentLookup.validated?then("successfully", "NOT")} been validated via DNSSEC!</li>
							</ul>
						</p>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>
