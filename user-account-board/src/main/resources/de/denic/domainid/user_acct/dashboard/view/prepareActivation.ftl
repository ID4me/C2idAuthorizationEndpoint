<#ftl output_format="HTML" encoding="UTF-8">
<#-- @ftlvariable name="" type="de.denic.domainid.user_acct.dashboard.view.PrepareDeactivationView" -->
<html>
	<#include "include/head.ftl">
	<body class="w3-container">
		<div class="w3-display-container w3-margin-top">
			<div class="w3-display-topmiddle w3-card-4" style="width:450px">
				<#include "include/identityAuthorityHeader.ftl">
				<div class="w3-padding">
					<form action="/dashboard/activation" method="POST" accept-charset="utf-8" style="margin-bottom:0px;">
						<p>
							<input class="w3-input" type="text" id="domainID" name="domainID" maxlength="40" value="${domainId}" disabled="true">
							<label class="w3-tiny"> Your ${idName} </label>
						</p>
						<p>
							Here you may <em>re-activate</em>/<em>re-enable</em> your ${idName}!
						</p>
						<p>
							This will have some effects, for example:
							<ul>
								<li>Authentication on Relying Party's/Shop's websites using your ${idName} is made possible again.</li>
							</ul>
						</p>
						<p>
							<button class="w3-button w3-inetid-gold w3-padding-small w3-small" type="submit" name="operation"> Activate again! </button>
						</p>
					</form>
					<p></p>
					<div class="w3-container w3-center">
						<form method="GET" accept-charset="utf-8">
							<button class="w3-button w3-border w3-round w3-inetid-gold" type="submit" formaction="/dashboard/showDomainIdAccountNavigationView" accesskey="b">BACK</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
