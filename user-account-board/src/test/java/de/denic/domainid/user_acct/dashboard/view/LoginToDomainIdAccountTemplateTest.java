/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.user_acct.dashboard.view;

import de.denic.domainid.DomainId;
import de.denic.domainid.dns.LookupResult;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapperBuilder;
import freemarker.template.Template;
import freemarker.template.Version;
import org.junit.Before;
import org.junit.Test;

import java.io.StringWriter;
import java.io.Writer;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertTrue;

public class LoginToDomainIdAccountTemplateTest {

  private static final URI TEST_IDENTITY_AGENT_HOST = URI.create("//identity.agent.test");
  private static final DomainId TEST_DOMAIN_ID = new DomainId("test.domain.id");
  private static final Version FREEMARKER_INCOMPATIBLE_IMPROVEMENTS = new Version("2.3.23");

  private Writer targetWriter;
  private Template templateUnderTest;

  @Before
  public void setup() throws Exception {
    targetWriter = new StringWriter();
    final Configuration configuration = new Configuration(FREEMARKER_INCOMPATIBLE_IMPROVEMENTS);
    configuration.setClassForTemplateLoading(LoginToDomainIdAccountView.class, "");
    configuration.setObjectWrapper(new DefaultObjectWrapperBuilder(FREEMARKER_INCOMPATIBLE_IMPROVEMENTS).build());
    templateUnderTest = configuration.getTemplate("loginToDomainIdAccount.ftl");
  }

  @Test
  public void testRenderingWithEmptyIdentityAgentData() throws Exception {
    final LookupResult<URI> identityAgentLookup = LookupResult.empty(true);
    final LoginToDomainIdAccountView view = new LoginToDomainIdAccountView(TEST_DOMAIN_ID, identityAgentLookup);
    final Map<String, Object> root = new HashMap<>();
    root.put("domainId", view.getDomainId());
    root.put("identityAgentLookup", view.getIdentityAgentLookup());
    root.put("idName", "DomainIdOrINetIdOrId4Me");
    {
      templateUnderTest.process(root, targetWriter);
      final String renderedOutput = targetWriter.toString();
      System.out.println(renderedOutput);
      assertTrue("Identity Agent host has not been found",
              renderedOutput.contains("<li>Identity Agent host: STILL UNKNOWN</li>"));
    }
  }

  @Test
  public void testRenderingWithIdentityAgentData() throws Exception {
    final LookupResult<URI> identityAgentLookup = new LookupResult.Impl<>(true, TEST_IDENTITY_AGENT_HOST);
    final LoginToDomainIdAccountView view = new LoginToDomainIdAccountView(TEST_DOMAIN_ID, identityAgentLookup);
    final Map<String, Object> root = new HashMap<>();
    root.put("domainId", view.getDomainId());
    root.put("identityAgentLookup", view.getIdentityAgentLookup());
    root.put("idName", "DomainIdOrINetIdOrId4Me");
    {
      templateUnderTest.process(root, targetWriter);
      final String renderedOutput = targetWriter.toString();
      System.out.println(renderedOutput);
      assertTrue("Identity Agent host has not been found",
              renderedOutput.contains("<li>Identity Agent host: <i>identity.agent.test</i></li>"));
    }
  }

}
