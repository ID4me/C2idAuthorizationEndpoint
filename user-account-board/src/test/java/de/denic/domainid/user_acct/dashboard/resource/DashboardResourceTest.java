/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.user_acct.dashboard.resource;

import com.warrenstrange.googleauth.IGoogleAuthenticator;
import de.denic.domainid.DomainId;
import de.denic.domainid.connect2id.authzstore.Connect2IdAuthorizationStoreAPI;
import de.denic.domainid.connect2id.clientregistration.OpenIdConnectClientRegistration;
import de.denic.domainid.dbaccess.UserAuthenticationDAO;
import de.denic.domainid.dns.DomainIdDnsClient;
import de.denic.domainid.user_acct.dashboard.entity.SessionCookie;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URI;

import static javax.ws.rs.core.Response.Status.CREATED;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static org.easymock.EasyMock.*;
import static org.junit.Assert.assertNotNull;

public class DashboardResourceTest {

  private static final DomainId DOMAIN_ID = new DomainId("some.unit.test");
  private static final SessionCookie NO_COOKIE = null;
  private static final HttpServletResponse NO_RESPONSE = null;

  private DashboardResource CUT;
  private UserAuthenticationDAO userAuthenticationDAOMock;
  private DomainIdDnsClient dnsClientMock;
  private Client restClientMock;
  private WebTarget webTargetMock;
  private Builder builderMock;
  private Response responseMock;
  private Invocation invocationMock;
  private Connect2IdAuthorizationStoreAPI connect2IdAuthorisationStoreMock;
  private OpenIdConnectClientRegistration clientRegistrationEndpointMock;
  private IGoogleAuthenticator googleAuthenticatorMock;

  @Before
  public void setUp() throws Exception {
    userAuthenticationDAOMock = createMock(UserAuthenticationDAO.class);
    dnsClientMock = createMock(DomainIdDnsClient.class);
    restClientMock = createMock(Client.class);
    webTargetMock = createMock(WebTarget.class);
    builderMock = createMock(Builder.class);
    responseMock = createMock(Response.class);
    invocationMock = createMock(Invocation.class);
    connect2IdAuthorisationStoreMock = createMock(Connect2IdAuthorizationStoreAPI.class);
    clientRegistrationEndpointMock = createMock(OpenIdConnectClientRegistration.class);
    googleAuthenticatorMock = createMock(IGoogleAuthenticator.class);
    CUT = new DashboardResource(userAuthenticationDAOMock, dnsClientMock, restClientMock,
            connect2IdAuthorisationStoreMock, clientRegistrationEndpointMock, googleAuthenticatorMock);
  }

  @After
  public void tearDown() throws Exception {
    reset(userAuthenticationDAOMock, dnsClientMock, restClientMock, webTargetMock, builderMock, responseMock,
            invocationMock, connect2IdAuthorisationStoreMock, clientRegistrationEndpointMock, googleAuthenticatorMock);
  }

  @Test
  public void initializingCallReturnsCREATED() throws IOException {
    expect(userAuthenticationDAOMock.findUserAuthenticationBy(DOMAIN_ID)).andReturn(null);
    expect(restClientMock.target(URI.create("https://auth.freedom-id.de/init/start"))).andReturn(webTargetMock);
    expect(webTargetMock.path(DOMAIN_ID.getName())).andReturn(webTargetMock);
    expect(webTargetMock.request("application/json")).andReturn(builderMock);
    expect(builderMock.buildPut(anyObject())).andReturn(invocationMock);
    expect(webTargetMock.getUri()).andReturn(URI.create("https://auth.freedom-id.de/init/start/" + DOMAIN_ID));
    expect(invocationMock.invoke()).andReturn(responseMock);
    final String responseBody = "{\"message\":\"Test message\",\"params\":{\"domainid\":\"some.unit.test\",\"emailaddress\":\"some@unit.test\"}}";
    expect(responseMock.readEntity(String.class)).andReturn(responseBody);
    expect(responseMock.getStatusInfo()).andReturn(CREATED).times(2);
    expect(responseMock.getStatus()).andReturn(CREATED.getStatusCode());
    replayMocks();
    {
      assertNotNull("Received view", CUT.askFor(NO_COOKIE, DOMAIN_ID.getName(), NO_RESPONSE));
      verifyMocks();
    }
  }

  @Test
  public void initializingCallReturnsNOT_FOUND() throws IOException {
    expect(userAuthenticationDAOMock.findUserAuthenticationBy(DOMAIN_ID)).andReturn(null);
    expect(restClientMock.target(URI.create("https://auth.freedom-id.de/init/start"))).andReturn(webTargetMock);
    expect(webTargetMock.path(DOMAIN_ID.getName())).andReturn(webTargetMock);
    expect(webTargetMock.request("application/json")).andReturn(builderMock);
    expect(builderMock.buildPut(anyObject())).andReturn(invocationMock);
    expect(webTargetMock.getUri()).andReturn(URI.create("https://auth.freedom-id.de/init/start/" + DOMAIN_ID));
    expect(invocationMock.invoke()).andReturn(responseMock);
    final String responseBody = "{\"message\":\"Some problem occured\",\"params\":{\"domainid\":\"some.unit.test\",\"emailaddress\":\"some@unit.test\"}}";
    expect(responseMock.readEntity(String.class)).andReturn(responseBody);
    expect(responseMock.getStatusInfo()).andReturn(NOT_FOUND).times(2);
    expect(responseMock.getStatus()).andReturn(NOT_FOUND.getStatusCode());
    replayMocks();
    {
      assertNotNull("Received view", CUT.askFor(NO_COOKIE, DOMAIN_ID.getName(), NO_RESPONSE));
      verifyMocks();
    }
  }

  private void verifyMocks() {
    verify(userAuthenticationDAOMock, dnsClientMock, restClientMock, webTargetMock, builderMock, responseMock,
            invocationMock, connect2IdAuthorisationStoreMock, clientRegistrationEndpointMock, googleAuthenticatorMock);
  }

  private void replayMocks() {
    replay(userAuthenticationDAOMock, dnsClientMock, restClientMock, webTargetMock, builderMock, responseMock,
            invocationMock, connect2IdAuthorisationStoreMock, clientRegistrationEndpointMock, googleAuthenticatorMock);
  }

}
