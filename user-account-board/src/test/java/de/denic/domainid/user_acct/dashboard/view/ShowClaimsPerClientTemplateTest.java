/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.user_acct.dashboard.view;

import de.denic.domainid.DomainId;
import de.denic.domainid.connect2id.authzstore.entity.OpenIdConnectAuthorization;
import de.denic.domainid.connect2id.clientregistration.entity.OpenIdConnectClient;
import de.denic.domainid.connect2id.subjectsessionstore.entity.SubjectSessionOptionalData;
import de.denic.domainid.connect2id.common.Claim;
import de.denic.domainid.connect2id.common.ClientId;
import de.denic.domainid.connect2id.common.Scope;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapperBuilder;
import freemarker.template.Template;
import freemarker.template.Version;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Before;
import org.junit.Test;

import java.io.StringWriter;
import java.io.Writer;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertTrue;

/**
 * Mimics {@link ShowClaimsPerClientView} as data model of Freemarker template!
 */
public class ShowClaimsPerClientTemplateTest {

  private static final Version FREEMARKER_INCOMPATIBLE_IMPROVEMENTS = new Version("2.3.23");
  private static final String NO_NAME = null;

  private Writer targetWriter;
  private Template templateUnderTest;
  private DomainId domainId;
  private List<Pair<OpenIdConnectClient, OpenIdConnectAuthorization>> clientAuthorisationPairs;

  @Before
  public void setup() throws Exception {
    targetWriter = new StringWriter();
    final Configuration configuration = new Configuration(FREEMARKER_INCOMPATIBLE_IMPROVEMENTS);
    configuration.setClassForTemplateLoading(ShowClaimsPerClientView.class, "");
    configuration.setObjectWrapper(new DefaultObjectWrapperBuilder(FREEMARKER_INCOMPATIBLE_IMPROVEMENTS).build());
    templateUnderTest = configuration.getTemplate("showClaimsPerClient.ftl");
  }

  @Test
  public void testRendering() throws Exception {
    final DomainId domainId = new DomainId("test.domain.id");
    final Map<OpenIdConnectClient, OpenIdConnectAuthorization> allAuthorisations = new HashMap<>();
    final ClientId clientId = new ClientId("client-id");
    final String nameOfClient = "client-name";
    final OpenIdConnectClient client = new OpenIdConnectClient(clientId, nameOfClient, URI.create("client-uri"), URI
            .create("terms-of-service-uri"), URI.create("policy-uri"));
    allAuthorisations.put(client,
            new OpenIdConnectAuthorization(domainId.getName(), clientId, asList(new Scope("auth-scope1")), null, null,
                    true, null, null, null, null, null, null, null, null, null, null, null, singletonList(new Claim("cons_claim1")),
                    asList(new Claim("saved_cons_claim1"), new Claim("saved_cons_claim2")), null, null, null, null));
    final ShowClaimsPerClientView view = new ShowClaimsPerClientView(domainId, allAuthorisations);
    this.domainId = view.getDomainId();
    this.clientAuthorisationPairs = view.getClientAuthorisationPairs();
    {
      templateUnderTest.process(this, targetWriter);
      final String renderedOutput = targetWriter.toString();
      System.out.println(renderedOutput);
      assertTrue("Name of Client is shown", renderedOutput.contains("\t" + nameOfClient));
    }
  }

  @Test
  public void testRenderingWithClientMissingName() throws Exception {
    final DomainId domainId = new DomainId("test.domain.id");
    final Map<OpenIdConnectClient, OpenIdConnectAuthorization> allAuthorisations = new HashMap<>();
    final ClientId clientId = new ClientId("client-id");
    final OpenIdConnectClient client = new OpenIdConnectClient(clientId, NO_NAME, URI.create("client-uri"), URI
            .create("terms-of-service-uri"), URI.create("policy-uri"));
    allAuthorisations.put(client,
            new OpenIdConnectAuthorization(domainId.getName(), clientId, asList(new Scope("auth-scope1")), null, null,
                    true, null, null, null, null, null, null, null, null, null, null, null, singletonList(new Claim("cons_claim1")),
                    asList(new Claim("saved_cons_claim1"), new Claim("saved_cons_claim2")), null, null, null, null));
    final ShowClaimsPerClientView view = new ShowClaimsPerClientView(domainId, allAuthorisations);
    this.domainId = view.getDomainId();
    this.clientAuthorisationPairs = view.getClientAuthorisationPairs();
    {
      templateUnderTest.process(this, targetWriter);
      final String renderedOutput = targetWriter.toString();
      System.out.println(renderedOutput);
      assertTrue("ID of Client is shown", renderedOutput.contains("\t" + "ID <i>" + clientId.getPlain() + "</i>"));
    }
  }

  @Test
  public void testRenderingWithAdditionalDataButNoRejectedClaims() throws Exception {
    final DomainId domainId = new DomainId("test.domain.id");
    final Map<OpenIdConnectClient, OpenIdConnectAuthorization> allAuthorisations = new HashMap<>();
    final ClientId clientId = new ClientId("client-id");
    final OpenIdConnectClient client = new OpenIdConnectClient(clientId, NO_NAME, URI.create("client-uri"), URI
            .create("terms-of-service-uri"), URI.create("policy-uri"));
    allAuthorisations.put(client,
            new OpenIdConnectAuthorization(domainId.getName(), clientId, asList(new Scope("auth-scope1")), null, null,
                    true, null, null, null, null, null, null, null, null, null, null, null, singletonList(new Claim
                    ("cons_claim1")),
                    asList(new Claim("saved_cons_claim1"), new Claim("saved_cons_claim2")), null, null, null,
                    new SubjectSessionOptionalData(domainId, null)));
    final ShowClaimsPerClientView view = new ShowClaimsPerClientView(domainId, allAuthorisations);
    this.domainId = view.getDomainId();
    this.clientAuthorisationPairs = view.getClientAuthorisationPairs();
    {
      templateUnderTest.process(this, targetWriter);
      final String renderedOutput = targetWriter.toString();
      System.out.println(renderedOutput);
      assertTrue("ID of Client is shown", renderedOutput.contains("\t" + "ID <i>" + clientId.getPlain() + "</i>"));
    }
  }

  /**
   * Mimics {@link ShowClaimsPerClientView#getDomainId()}
   */
  public DomainId getDomainId() {
    return domainId;
  }

  /**
   * Mimics {@link ShowClaimsPerClientView#getClientAuthorisationPairs()}
   */
  public List<Pair<OpenIdConnectClient, OpenIdConnectAuthorization>> getClientAuthorisationPairs() {
    return clientAuthorisationPairs;
  }

  /**
   * Mimics {@link ShowClaimsPerClientView#getNameOfClaim(String)}
   */
  public String getNameOfClaim(final String claim) {
    return "Name of " + claim;
  }

  /**
   * Mimics {@link ShowClaimsPerClientView#getDescriptionOfClaim(String)}
   */
  public String getDescriptionOfClaim(final String claim) {
    return "Description of " + claim;
  }

  /**
   * Mimics {@link DashboardView#getIdName()}
   */
  public String getIdName() {
    return "IdName";
  }

}
