/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.user_acct.dashboard.entity;

import static org.apache.commons.codec.digest.DigestUtils.sha256Hex;
import static org.junit.Assert.assertFalse;

import org.junit.Test;

public class SessionCookieTest {

  @Test
  public void testApplyingEmptySalts() {
    final SessionCookie CUT = SessionCookie.fromString("domainId:dummyHash");
    assertFalse("Empty salt", CUT.matchesApplyingSalt(""));
    assertFalse("NULL salt", CUT.matchesApplyingSalt(null));
  }

  @Test
  public void testApplyingMismatchingSalt() {
    final String domainId = "domainId";
    final String saltedHash = sha256Hex("salt" + domainId);
    final SessionCookie CUT = SessionCookie.fromString(domainId + ":" + saltedHash);
    assertFalse("Mismatching salt", CUT.matchesApplyingSalt("mismatchingSalt"));
  }

  @Test
  public void testApplyingMatchingSalt() {
    final String domainId = "domainId";
    final String salt = "salt";
    final String saltedHash = sha256Hex(salt + domainId);
    final SessionCookie CUT = SessionCookie.fromString(domainId + ":" + saltedHash);
    assertFalse("Matching salt", CUT.matchesApplyingSalt(salt));
  }

}
