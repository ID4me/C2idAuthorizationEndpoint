#!groovy
pipeline {
	agent any
	options {
		buildDiscarder(logRotator(numToKeepStr: '24'))
		disableConcurrentBuilds()
	}
	environment {
		VERSION = sh (script: 'date --utc +%y%m%d.%H%M --date="@$(git show --no-patch --format=%ct)"', returnStdout: true).trim()
		// we use the registry from global jenkins var.
		//DOCKER_REGISTRY = 'kube-registry.freedom-id.de'
		DOCKER_REGISTRY_PATH = 'test'
		// Already available within Jenkins
		//KUBECONFIG = '/var/jenkins_home/kubectl --kubeconfig=/var/jenkins_home/secrets/jenkins-kubectl.conf'
	}
	triggers {
		pollSCM('TZ=Europe/Berlin\n' + 'H/5 8-18 * * 1-5')
	}
	stages {

		stage('Checkout') {
			steps {
				checkout([$class: 'GitSCM',
						branches: [[name: '*/master']],
						doGenerateSubmoduleConfigurations: false,
						extensions: [],
						submoduleCfg: [],
						userRemoteConfigs: [[credentialsId: 'domainid-bot-ssh-key', url: 'git@gitlab.com:ID4me/C2idAuthorizationEndpoint.git']]])
			}
		}

		stage('Build') {
			steps {
				timestamps {
					timeout(time: 12, unit: 'MINUTES') {
						sh "./gradlew -Pversion=${VERSION} --no-daemon --stacktrace clean build"
					}
				}
			}
			post {
				always {
					junit '*/build/test-results/test/*.xml'
					jacoco classPattern: '*/build/classes/java',
							execPattern: '*/build/jacoco/*.exec',
							sourcePattern: '*/src/*/java'
					findbugs canComputeNew: false,
							defaultEncoding: 'ISO-8859-1',
							healthy: '0',
							pattern: '*/build/reports/spotbugs/*.xml',
							unHealthy: '3'
					pmd canComputeNew: false,
							defaultEncoding: 'ISO-8859-1',
							healthy: '0',
							pattern: '*/build/reports/pmd/*.xml',
							unHealthy: '3'
				}
			}
		}

		stage('Archiving artifacts and publishing reports') {
			steps {
//				parallel (
//					'Archive artifacts': {
						archiveArtifacts artifacts: '*/build/distributions/*.tar, */build/libs/*-all.jar, identity-agent/build/libs/identity-agent.jar, dns-lookup-ms/build/libs/dns-lookup-ms.jar', onlyIfSuccessful: 
true
//					},
				/*
					'Connect2Id Client JavaDoc': {
						publishHTML([allowMissing: true,
									alwaysLinkToLastBuild: false,
									keepAll: false,
									reportDir: 'connect2id/build/docs/javadoc',
									reportFiles: 'index.html',
									reportName: 'Connect2Id Client JavaDoc',
									reportTitles: 'Connect2Id Client JavaDoc'])
					},
				*/
				/*
					'Build profile': {
						publishHTML([allowMissing: true,
									alwaysLinkToLastBuild: false,
									keepAll: false,
									reportDir: 'master/build/reports/profile',
									reportFiles: 'profile-*.html',
									reportName: 'Gradle Build Profile',
									reportTitles: 'Gradle Build Profile'])
					}
				*/
//				)
			}
		}

		stage('Build and push Docker image') {
			steps {
				parallel (
					'auth-endpoint': {
						dir('./auth-endpoint') {
							timestamps {
								timeout(time: 60, unit: 'SECONDS') {
									sh "./docker-build.sh \"${DOCKER_REGISTRY}\" \"${DOCKER_REGISTRY_PATH}\" \"${VERSION}\""
								}
							}
						}
					},
					'dns-lookup-ms': {
						dir('./dns-lookup-ms') {
							timestamps {
								timeout(time: 60, unit: 'SECONDS') {
									sh "./docker-build.sh \"${DOCKER_REGISTRY}\" \"${DOCKER_REGISTRY_PATH}\" \"${VERSION}\""
								}
							}
						}
					},
					'user-account-board': {
						dir('./user-account-board') {
							timestamps {
								timeout(time: 60, unit: 'SECONDS') {
									sh "./docker-build.sh \"${DOCKER_REGISTRY}\" \"${DOCKER_REGISTRY_PATH}\" \"${VERSION}\""
								}
							}
						}
					},
					'identity-agent': {
						dir('./identity-agent') {
							timestamps {
								timeout(time: 60, unit: 'SECONDS') {
									sh "./docker-build.sh \"${DOCKER_REGISTRY}\" \"${DOCKER_REGISTRY_PATH}\" \"${VERSION}\""
								}
							}
						}
					}
				)
			}
		}

		stage('Cluster deployment to staging namespace') {
			steps {
				parallel (
					'auth-endpoint': {
						dir('./auth-endpoint/src/kubernetes') {
							timestamps {
								timeout(time: 6, unit: 'MINUTES') {
									sh "./deploy.sh staging ${VERSION}"
								}
							}
						}
					},
					'user-account-board': {
						dir('./user-account-board/src/kubernetes') {
							timestamps {
								timeout(time: 6, unit: 'MINUTES') {
									sh "./deploy.sh staging ${VERSION}"
								}
							}
						}
					},
					'dns-lookup-microservice': {
						dir('./dns-lookup-ms/src/kubernetes') {
							timestamps {
								timeout(time: 6, unit: 'MINUTES') {
									sh "./deploy.sh staging ${VERSION}"
								}
							}
						}
					},
					'identity-agent': {
						dir('./identity-agent/src/kubernetes') {
							timestamps {
								timeout(time: 6, unit: 'MINUTES') {
									sh "./deploy.sh staging ${VERSION}"
								}
							}
						}
					}
				)
			}
		}

		stage('Integration Tests') {
			steps {
				timestamps {
					timeout(time: 3, unit: 'MINUTES') {
						sh "./gradlew -Pversion=${VERSION} \$($KUBESECRETS -n staging get c2id-secrets-vars -o gradle) --no-daemon intTest"
					}
				}
			}
			post {
				always {
					junit '*/build/test-results/intTest/*.xml'
				}
			}
		}

		stage('Cluster deployment to default namespace') {
			steps {
				parallel (
					'auth-endpoint': {
						dir('./auth-endpoint/src/kubernetes') {
							timestamps {
								timeout(time: 8, unit: 'MINUTES') {
									sh "./deploy.sh default ${VERSION}"
								}
							}
						}
					},
					'user-account-board': {
						dir('./user-account-board/src/kubernetes') {
							timestamps {
								timeout(time: 8, unit: 'MINUTES') {
									sh "./deploy.sh default ${VERSION}"
								}
							}
						}
					},
					'dns-lookup-microservice': {
						dir('./dns-lookup-ms/src/kubernetes') {
							timestamps {
								timeout(time: 8, unit: 'MINUTES') {
									sh "./deploy.sh default ${VERSION}"
								}
							}
						}
					},
					'identity-agent': {
						dir('./identity-agent/src/kubernetes') {
							timestamps {
								timeout(time: 8, unit: 'MINUTES') {
									sh "./deploy.sh default ${VERSION}"
								}
							}
						}
					}
				)
			}
		}

	}
}
