/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.authzsession.entity;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class DataOfSessionResponseTest {

  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
  private static final ObjectReader CUT_READER = OBJECT_MAPPER.readerFor(SessionResponse.class);
  private static final ObjectWriter CUT_WRITER = OBJECT_MAPPER.writerFor(SessionResponse.class);

  @Test
  public void deserializeSerializeRoundTripOfMinimalInstance() throws IOException {
    final String jsonInput = "{\"auth_req\":{\"response_type\":\"responseType\",\"client_id\":\"ClientID\",\"redirect_uri\":\"RedirectURI\"},\"sub_sid\":\"SubjectSessionID\"}";
    final SessionResponse CUT = CUT_READER.readValue(jsonInput);
    assertEquals("JSON", "{\"auth_req\":{\"response_type\":\"responseType\",\"client_id\":\"ClientID\"," +
            "\"redirect_uri\":\"RedirectURI\"},\"sub_sid\":\"SubjectSessionID\",\"data\":{}}", CUT_WRITER
            .writeValueAsString(CUT));
  }

  @Test
  public void deserializeSerializeRoundTripOfFullFledgedInstance() throws IOException {
    final String jsonInput = "{\"auth_req\":{\"response_type\":\"responseType\",\"client_id\":\"ClientID\",\"redirect_uri\":\"RedirectURI\",\"scope\":[\"Scope\"],\"state\":\"State\",\"nonce\":\"NONCE\",\"display\":\"touch\",\"prompt\":[\"consent\"],\"ui_locales\":[\"de\",\"en\"],"
            + "\"claims\":{\"userinfo\":{\"nickname\":null,\"given_name\":{\"essential\":true},\"http://example.claim/foo\":null},\"id_token\":{\"auth_time\":{\"essential\":true},\"acr\":{\"values\":[\"urn:mace:incommon:iap:silver\"]}}}},"
            + "\"sub_sid\":\"SubjectSessionID\"}";
    final SessionResponse CUT = CUT_READER.readValue(jsonInput);
    assertEquals("JSON", "{\"auth_req\":{\"response_type\":\"responseType\",\"client_id\":\"ClientID\",\"redirect_uri\":\"RedirectURI\",\"scope\":[\"Scope\"],\"state\":\"State\",\"nonce\":\"NONCE\",\"display\":\"touch\",\"prompt\":[\"consent\"],\"ui_locales\":[\"de\",\"en\"],"
            + "\"claims\":{\"userinfo\":{\"nickname\":null,\"given_name\":{\"essential\":true},\"http://example.claim/foo\":null},\"id_token\":{\"auth_time\":{\"essential\":true},\"acr\":{\"values\":[\"urn:mace:incommon:iap:silver\"]}}}},"
            + "\"sub_sid\":\"SubjectSessionID\",\"data\":{}}", CUT_WRITER.writeValueAsString(CUT));
    final String expectedInitialRequestUriQuery = "response_type=responseType&client_id=ClientID&redirect_uri=RedirectURI&scope=Scope&state=State&nonce=NONCE&display=touch&prompt=consent&ui_locales=de+en&claims=%7B%22userinfo%22%3A%7B%22nickname%22%3Anull%2C%22given%5Fname%22%3A%7B%22essential%22%3Atrue%7D%2C%22http%3A%2F%2Fexample%2Eclaim%2Ffoo%22%3Anull%7D%2C%22id%5Ftoken%22%3A%7B%22auth%5Ftime%22%3A%7B%22essential%22%3Atrue%7D%2C%22acr%22%3A%7B%22values%22%3A%5B%22urn%3Amace%3Aincommon%3Aiap%3Asilver%22%5D%7D%7D%7D";
    assertEquals("Initial request URI", expectedInitialRequestUriQuery,
            CUT.getInitialRequest().toURI().toString());
  }

}
