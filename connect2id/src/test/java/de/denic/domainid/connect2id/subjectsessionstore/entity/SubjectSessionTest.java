/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.subjectsessionstore.entity;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.denic.domainid.DomainId;
import de.denic.domainid.connect2id.common.AuthenticationMethodReference;
import de.denic.domainid.connect2id.common.SubjectSessionId;
import de.denic.domainid.subject.PseudonymSubject;
import org.junit.Test;

import java.io.IOException;
import java.time.Duration;
import java.util.Map;
import java.util.Set;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SubjectSessionTest {

  private static final long DUMMY_TIMESTAMP = 42L;
  private static final SubjectSessionId NO_SESSION_ID = null;
  private static final Duration VERY_LONG_DURATION = Duration.ofDays(1_000L * 365L); // 1.000 years
  private static final SubjectSessionOptionalData NO_ADDITIONAL_DATA = null;
  private static final Map<String, String> NO_CLAIMS = null;

  @Test
  public void withNegativeDurations() {
    final SubjectSession CUT = new SubjectSession(new PseudonymSubject("subject"), NO_SESSION_ID, DUMMY_TIMESTAMP,
            DUMMY_TIMESTAMP, -1, -1, -1, null, NO_CLAIMS, NO_ADDITIONAL_DATA);
    assertTrue("Nearly 'unlimited' auth. lifetime", CUT.getAuthenticationLifetime().compareTo(VERY_LONG_DURATION) > 0);
    assertTrue("Nearly 'unlimited' max idle", CUT.getMaxIdle().compareTo(VERY_LONG_DURATION) > 0);
    assertTrue("Nearly 'unlimited' max lifetime", CUT.getMaxLifetime().compareTo(VERY_LONG_DURATION) > 0);
  }

  @Test
  public void deserializeAuthenticationMethodReferenceValues() throws IOException {
    final SubjectSession CUT = new ObjectMapper().readerFor(SubjectSession.class).readValue(
            "{\"creation_time\":1508854645, \"sub\":\"test-user\", \"max_idle\":1440, \"auth_time\":1508854645, \"max_life\":20160, \"auth_life\":10080, \"amr\":[\"pwd\", \"otp\"]}");
    final Set<AuthenticationMethodReference> authenticationMethodReferences = CUT.getAuthenticationMethodReferences();
    assertEquals("Count of AMRs", 2, authenticationMethodReferences.size());
    assertTrue("AMRs", authenticationMethodReferences
            .containsAll(asList(new AuthenticationMethodReference("otp"), new AuthenticationMethodReference("pwd"))));
  }

  @Test
  public void deserializeAdditionalData() throws IOException {
    final SubjectSession CUT = new ObjectMapper().readerFor(SubjectSession.class).readValue(
            "{\"creation_time\":1508854645, \"sub\":\"test-user\", \"max_idle\":1440, \"auth_time\":1508854645, " +
                    "\"max_life\":20160, \"auth_life\":10080, \"data\":{\"identifier\":\"test.domain.id\"}}");
    assertEquals("Additional data domain ID", new DomainId("test.domain.id"), CUT.getOptionalData().getDomainId().orElse(null));
  }

  @Test
  public void deserializeClaims() throws IOException {
    final SubjectSession CUT = new ObjectMapper().readerFor(SubjectSession.class).readValue(
            "{\"creation_time\":1508854645, \"sub\":\"test-user\", \"max_idle\":1440, \"auth_time\":1508854645, " +
                    "\"max_life\":20160, \"auth_life\":10080, \"claims\":{\"id4me.identifier\":\"test.domain.id\"}}");
    assertEquals("ID4me identifier claim", "test.domain.id", CUT.getClaims().get("id4me.identifier"));
  }

}
