/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.authzsession.entity;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.denic.domainid.connect2id.common.Claim;

public class AuthzSessionOptionalDataTest {

  @Test
  public void jsonSerializationOfEmptySessionData() throws IOException {
    final AuthzSessionOptionalData CUT = new AuthzSessionOptionalData();
    assertEquals("JSON serialization", "{}", new ObjectMapper().writeValueAsString(CUT));
  }

  @Test
  public void jsonSerializationOfSessionData() throws IOException {
    final Map<Claim, String> reasonsOfClaims = new HashMap<>();
    reasonsOfClaims.put(new Claim("foo"), "foo reason");
    final AuthzSessionOptionalData CUT = new AuthzSessionOptionalData(reasonsOfClaims);
    assertEquals("JSON serialization", "{\"reasons_of_claims\":{\"foo\":\"foo reason\"}}",
        new ObjectMapper().writeValueAsString(CUT));
  }

}
