/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.common;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Arrays;

import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;

public class UrlEncodedValueTest {

  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
  private static final ObjectReader CUT_READER = OBJECT_MAPPER.readerFor(MyUrlEncodedValue.class);
  private static final ObjectWriter CUT_WRITER = OBJECT_MAPPER.writerFor(MyUrlEncodedValue.class);

  @Test
  public void jsonSerialisation() throws IOException {
    final MyUrlEncodedValue CUT = new MyUrlEncodedValue("foo");
    assertEquals("JSON serialisation", "\"foo\"", CUT_WRITER.writeValueAsString(CUT));
  }

  @Test
  public void jsonDeserialisation() throws IOException {
    final MyUrlEncodedValue CUT = new MyUrlEncodedValue("bar");
    assertEquals("JSON deserialisation", CUT, CUT_READER.readValue("\"bar\""));
  }

  @Test
  public void testWithPlainValueHavingSpace() {
    verifyApplying("a z", "a+z", "a%20z");
  }

  @Test
  public void testWithSomeURLValue() {
    final String plain = "http://example.com/foo/bar?withSpace=a z&withQuestionMark=a?z&withSemicolon=a;z&withPercent=a%z";
    final String expectedEncoded = plain.replace("%", "%25").replace("/", "%2F").replace("?", "%3F").replace("&", "%26")
        .replace(":", "%3A").replace(";", "%3B").replace("=", "%3D");
    verifyApplying(plain, expectedEncoded.replace(" ", "+"), expectedEncoded.replace(" ", "%20"));
  }

  private void verifyApplying(final String plainValue, final String... expectedEncoded) {
    final UrlEncodedValue CUT = new MyUrlEncodedValue(plainValue);
    assertEquals("Plain value", plainValue, CUT.getPlain());
    assertTrue("Encoded value: Exping one of " + Arrays.toString(expectedEncoded) + ", but got " + CUT.getEncoded(),
        asList(expectedEncoded).contains(CUT.getEncoded()));
    assertEquals("toString() provides encoded value", CUT.getEncoded(), CUT.toString());
  }

  private static class MyUrlEncodedValue extends UrlEncodedValue {

    public MyUrlEncodedValue(final String plainValue) {
      super(plainValue);
    }

  }

}
