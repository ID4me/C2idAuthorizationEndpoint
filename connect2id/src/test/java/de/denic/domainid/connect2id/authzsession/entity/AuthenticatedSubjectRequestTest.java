/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.authzsession.entity;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.denic.domainid.DomainId;
import de.denic.domainid.connect2id.common.AuthenticationMethodReference;
import de.denic.domainid.subject.PseudonymSubject;
import org.junit.Test;

import javax.security.auth.Subject;
import java.io.IOException;
import java.util.Collection;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

public class AuthenticatedSubjectRequestTest {

  @Test
  public void jsonSerializationWithoutAuthenticationMethods() throws IOException {
    final Subject subject = new Subject();
    subject.getPrincipals().add(new DomainId("test.domain.id"));
    subject.getPrincipals().add(new PseudonymSubject("pseudonym.subject"));
    final AuthenticatedSubjectRequest CUT = new AuthenticatedSubjectRequest(subject);
    assertEquals("JSON serialization", "{\"sub\":\"pseudonym.subject\",\"data\":{\"identifier\":\"test.domain.id\"},\"claims\":{\"id4me.identifier\":\"test.domain.id\"}}",
            new ObjectMapper().writeValueAsString(CUT));
  }

  @Test
  public void jsonSerializationWithAuthenticationMethods() throws IOException {
    final Subject subject = new Subject();
    subject.getPrincipals().add(new DomainId("test.domain.id"));
    subject.getPrincipals().add(new PseudonymSubject("pseudonym.subject"));
    final Collection<AuthenticationMethodReference> authMethodRefs = asList(new AuthenticationMethodReference("iris")
            , new AuthenticationMethodReference("face"));
    final AuthenticatedSubjectRequest CUT = new AuthenticatedSubjectRequest(subject, authMethodRefs);
    assertEquals("JSON serialization", "{\"sub\":\"pseudonym.subject\",\"data\":{\"identifier\":\"test.domain.id\"},\"claims\":{\"id4me.identifier\":\"test.domain.id\"},\"amr\":[\"face\",\"iris\"]}",
            new ObjectMapper().writeValueAsString(CUT));
  }

}