/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.common;

import static java.time.temporal.ChronoUnit.MINUTES;
import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.time.Duration;

import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;

public class UrlEncodedDurationValueTest {

  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
  private static final ObjectReader CUT_READER = OBJECT_MAPPER.readerFor(MyMinuteDurationValue.class);
  private static final ObjectWriter CUT_WRITER = OBJECT_MAPPER.writerFor(MyMinuteDurationValue.class);

  @Test
  public void properDuration() throws IOException {
    final MyMinuteDurationValue CUT = new MyMinuteDurationValue("12");
    assertEquals("Parsed duration", Duration.ofMinutes(12L), CUT.getDuration());
  }

  @Test
  public void jsonSerialisation() throws IOException {
    final MyMinuteDurationValue CUT = new MyMinuteDurationValue("24");
    assertEquals("JSON serialisation", "\"24\"", CUT_WRITER.writeValueAsString(CUT));
  }

  @Test
  public void jsonDeserialisation() throws IOException {
    final MyMinuteDurationValue CUT = new MyMinuteDurationValue("42");
    assertEquals("JSON deserialisation", CUT, CUT_READER.readValue("\"42\""));
  }

  private static class MyMinuteDurationValue extends UrlEncodedDurationValue {

    public MyMinuteDurationValue(final String plainValue) {
      super(plainValue, MINUTES);
    }

  }

}
