/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.authzsession.entity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import de.denic.domainid.DomainId;
import de.denic.domainid.connect2id.authzsession.ConsentedData;
import de.denic.domainid.connect2id.common.Claim;
import de.denic.domainid.connect2id.common.Scope;
import org.junit.Test;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertTrue;

public class ConsentedDataSubmissionRequestTest {

  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
  private static final ObjectWriter CONSENTED_DATA_SUBMISSION_REQUEST_ENTITY_WRITER = OBJECT_MAPPER
          .writerFor(ConsentedDataSubmissionRequest.class);
  private static final DomainId SOME_DOMAIN_ID = new DomainId("test.domain.id");

  @Test
  public void testJsonMarshallingOfScopesAndClaims() throws JsonProcessingException {
    final ConsentedData consentedData = new ConsentedData(asList(new Scope("scope1"), new Scope("scope2")),
            asList(new Claim("claim1"), new Claim("claim2")), asList(new Claim("rejected1"), new Claim("rejected2")));
    final ConsentedDataSubmissionRequest CUT = new ConsentedDataSubmissionRequest(SOME_DOMAIN_ID, consentedData,
            false);
    final String jsonEncoded = CONSENTED_DATA_SUBMISSION_REQUEST_ENTITY_WRITER.writeValueAsString(CUT);
    assertTrue("JSON marshalling contains scope element", jsonEncoded.contains("\"scope\":[\"scope1\",\"scope2\"]")
            || jsonEncoded.contains("\"scope\":[\"scope2\",\"scope1\"]"));
    assertTrue("JSON marshalling contains claims element", jsonEncoded.contains("\"claims\":[\"claim1\",\"claim2\"]")
            || jsonEncoded.contains("\"claims\":[\"claim2\",\"claim1\"]"));
  }

}
