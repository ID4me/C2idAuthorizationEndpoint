/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.authzsession.entity;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.denic.domainid.connect2id.common.Claim;
import de.denic.domainid.connect2id.common.SubjectSessionId;

public class InitiateSessionRequestTest {

  private static final SubjectSessionId NO_SUBJECT_SESSION_ID = null;
  private static final Map<Claim, String> NO_REASONS_OF_CLAIMS = null;

  @Test
  public void jsonSerializationWithQueryUriOnly() throws IOException {
    final InitiateSessionRequest CUT = new InitiateSessionRequest(URI.create("test-uri"),
        NO_SUBJECT_SESSION_ID, NO_REASONS_OF_CLAIMS);
    final String receivedJSON = new ObjectMapper().writerFor(InitiateSessionRequest.class)
        .writeValueAsString(CUT);
    final String expectedJSON = "{\"query\":\"test-uri\"}";
    assertEquals("JSON output", expectedJSON, receivedJSON);
  }

  @Test
  public void jsonSerializationWithAllFieldsSet() throws IOException {
    final Map<Claim, String> reasonsOfClaims = new HashMap<>();
    reasonsOfClaims.put(new Claim("claim_a"), "reason-of-claim-a");
    reasonsOfClaims.put(new Claim("b_claim"), "b-reason-of-claim");
    final InitiateSessionRequest CUT = new InitiateSessionRequest(URI.create("test-uri"),
        new SubjectSessionId("subject-session-id"), reasonsOfClaims);
    final String receivedJSON = new ObjectMapper().writerFor(InitiateSessionRequest.class)
        .writeValueAsString(CUT);
    final List<String> expectedJsonElements = asList("\"query\":\"test-uri\"", "\"data\":{\"reasons_of_claims\":{",
        "\"b_claim\":\"b-reason-of-claim\"", "\"claim_a\":\"reason-of-claim-a\"", "\"sub_sid\":\"subject-session-id\"");
    expectedJsonElements.stream()
        .forEach(expectedJson -> assertTrue("Contains '" + expectedJson + "'", receivedJSON.contains(expectedJson)));
  }

}
