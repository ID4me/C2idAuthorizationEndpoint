/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.common;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import de.denic.domainid.DomainId;
import de.denic.domainid.connect2id.subjectsessionstore.entity.SubjectSessionOptionalData;
import org.junit.Test;

import java.io.IOException;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

public class SubjectSessionOptionalDataTest {

  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
  private static final ObjectReader CUT_READER = OBJECT_MAPPER.readerFor(SubjectSessionOptionalData.class);
  private static final ObjectWriter CUT_WRITER = OBJECT_MAPPER.writerFor(SubjectSessionOptionalData.class);
  private static final DomainId SOME_DOMAIN_ID = new DomainId("test.domain.id");

  @Test
  public void jsonSerialisationOfMissingRejectedClaims() throws IOException {
    final SubjectSessionOptionalData CUT = new SubjectSessionOptionalData(SOME_DOMAIN_ID, null);
    assertEquals("JSON serialisation", "{\"identifier\":\"test.domain.id\"}", CUT_WRITER.writeValueAsString(CUT));
  }

  @Test
  public void jsonSerialisation() throws IOException {
    final SubjectSessionOptionalData CUT = new SubjectSessionOptionalData(
            SOME_DOMAIN_ID, asList(new Claim("claim1"), new Claim("claim2")));
    assertEquals("JSON serialisation", "{\"identifier\":\"test.domain.id\",\"rejected_claims\":[\"claim2\",\"claim1\"]}",
            CUT_WRITER.writeValueAsString(CUT));
  }

  @Test
  public void jsonDeserialisationOfIgnorableFieldsOnly() throws IOException {
    final SubjectSessionOptionalData CUT = new SubjectSessionOptionalData(SOME_DOMAIN_ID, null);
    assertEquals("JSON deserialisation", CUT,
            CUT_READER.readValue("{ \"identifier\":\"test.domain.id\", \"foo\":\"bar\", \"dummy_timestamp\":\"2017-10-15T11:30:22.22Z\" }"));
  }

  @Test
  public void jsonDeserialisation() throws IOException {
    final SubjectSessionOptionalData CUT = new SubjectSessionOptionalData(
            SOME_DOMAIN_ID, asList(new Claim("claim1"), new Claim("claim2")));
    assertEquals("JSON deserialisation", CUT,
            CUT_READER.readValue("{ \"identifier\":\"test.domain.id\", \"rejected_claims\":[\"claim1\",\"claim2\"]}"));
  }

}
