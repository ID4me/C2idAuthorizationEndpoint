/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.common;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Test;

public class UrlEncodedIntegerValueTest {

  @Test
  public void testWithNumericPlainValue() {
    verifyApplying("1000", "1000");
  }

  @Test(expected = IllegalArgumentException.class)
  public void testWithNegativeNumericPlainValue() {
    verifyApplying("-1000");
  }

  private void verifyApplying(final String plainValue, final String... expectedEncoded) {
    final UrlEncodedValue CUT = new TestClass(plainValue);
    assertEquals("Plain value", plainValue, CUT.getPlain());
    assertTrue("Encoded value: Expecting one of " + Arrays.toString(expectedEncoded) + ", but got " + CUT.getEncoded(),
        asList(expectedEncoded).contains(CUT.getEncoded()));
    assertEquals("toString() provides encoded value", CUT.getEncoded(), CUT.toString());
  }

  private static class TestClass extends UrlEncodedIntegerValue {

    public TestClass(final String value) {
      super(value);
    }
  }

}
