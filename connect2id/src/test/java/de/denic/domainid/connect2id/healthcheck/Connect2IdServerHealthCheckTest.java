/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.healthcheck;

import static javax.ws.rs.core.Response.Status.Family.SERVER_ERROR;
import static javax.ws.rs.core.Response.Status.Family.SUCCESSFUL;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.StatusType;

import org.junit.Before;
import org.junit.Test;

import com.codahale.metrics.health.HealthCheck;
import com.codahale.metrics.health.HealthCheck.Result;

import de.denic.domainid.connect2id.common.AccessToken;
import de.denic.domainid.connect2id.config.Connect2IdBaseConfig;

public class Connect2IdServerHealthCheckTest {

  private static final AccessToken SOME_BEARER_ACCESS_TOKEN = new AccessToken("AccessTokenValue");
  private static final String SOME_HEALTH_CHECK_PATH = "/health-check";
  private static final URL SOME_BASE_URL;
  static {
    try {
      SOME_BASE_URL = new URL("http://base.uri");
    } catch (final MalformedURLException e) {
      throw new ExceptionInInitializerError(e);
    }
  }

  private HealthCheck CUT;
  private Client restApiClientMock;
  private WebTarget webTargetMock;
  private Builder builderMock;
  private Response responseMock;
  private StatusType statusTypeMock;

  @Before
  public void setUp() throws Exception {
    restApiClientMock = createMock(Client.class);
    webTargetMock = createMock(WebTarget.class);
    builderMock = createMock(Builder.class);
    responseMock = createMock(Response.class);
    statusTypeMock = createMock(StatusType.class);
    final Connect2IdBaseConfig connect2IdConfig = new Connect2IdBaseConfig();
    connect2IdConfig.setHealthCheckBearerAccessToken(SOME_BEARER_ACCESS_TOKEN);
    connect2IdConfig.setBaseURL(SOME_BASE_URL);
    connect2IdConfig.setHealthCheckPath(SOME_HEALTH_CHECK_PATH);
    CUT = new Connect2IdServerHealthCheck(restApiClientMock, connect2IdConfig);
  }

  @Test
  public void healthCheckRespondsWithFamilySERVER_ERROR() throws URISyntaxException, MalformedURLException {
    prepareMocksToExpectRequest();
    expect(statusTypeMock.getFamily()).andReturn(SERVER_ERROR);
    expect(statusTypeMock.getStatusCode()).andReturn(500);
    expect(statusTypeMock.getReasonPhrase()).andReturn("Server Error");
    responseMock.close();
    replayMocks();
    {
      final Result result = CUT.execute();
      System.out.println(result);
      verifyMocks();
      assertFalse("Not healthy", result.isHealthy());
    }
  }

  @Test
  public void healthCheckRespondsWithFamilySUCCESSFUL() throws URISyntaxException, MalformedURLException {
    prepareMocksToExpectRequest();
    expect(statusTypeMock.getFamily()).andReturn(SUCCESSFUL);
    responseMock.close();
    replayMocks();
    {
      final Result result = CUT.execute();
      verifyMocks();
      assertTrue("Healthy", result.isHealthy());
    }
  }

  private void prepareMocksToExpectRequest() throws URISyntaxException, MalformedURLException {
    expect(restApiClientMock.target(new URL(SOME_BASE_URL, SOME_HEALTH_CHECK_PATH).toURI())).andReturn(webTargetMock);
    expect(webTargetMock.request()).andReturn(builderMock);
    expect(builderMock.header("Authorization", SOME_BEARER_ACCESS_TOKEN.withPrefix("Bearer"))).andReturn(builderMock);
    expect(builderMock.get()).andReturn(responseMock);
    expect(responseMock.getStatusInfo()).andReturn(statusTypeMock);
  }

  private void verifyMocks() {
    verify(restApiClientMock, webTargetMock, builderMock, responseMock, statusTypeMock);
  }

  private void replayMocks() {
    replay(restApiClientMock, webTargetMock, builderMock, responseMock, statusTypeMock);
  }

}
