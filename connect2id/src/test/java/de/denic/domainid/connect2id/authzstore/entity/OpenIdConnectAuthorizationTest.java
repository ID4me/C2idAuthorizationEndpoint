/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.authzstore.entity;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import de.denic.domainid.DomainId;
import de.denic.domainid.connect2id.subjectsessionstore.entity.SubjectSessionOptionalData;
import de.denic.domainid.connect2id.common.Claim;
import de.denic.domainid.connect2id.common.ClientId;
import de.denic.domainid.connect2id.common.Scope;
import org.junit.Test;

import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Locale;

import static java.util.Arrays.asList;
import static java.util.Locale.*;
import static org.junit.Assert.*;

public class OpenIdConnectAuthorizationTest {

  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
  private static final ObjectWriter OBJECT_WRITER = OBJECT_MAPPER.writerFor(OpenIdConnectAuthorization.class);
  private static final ObjectReader OBJECT_READER = OBJECT_MAPPER.readerFor(OpenIdConnectAuthorization.class);
  private static final SubjectSessionOptionalData ADDITIONAL_DATA = new SubjectSessionOptionalData(
          new DomainId("test.domain.id"), asList(new Claim("rejectedClaim1"), new Claim("rejectedClaim2")));

  @Test
  public void testMarshallingRoundtrip() throws IOException {
    final String subject = "subject";
    final ClientId clientId = new ClientId("clientID");
    final List<Scope> authorisationScopes = asList(new Scope("authScope1"), new Scope("authScope2"));
    final List<Scope> savedScopes = asList(new Scope("savedScope1"), new Scope("savedScope2"));
    final URI redirectionURI = URI.create("http://redirection.uri");
    final Boolean longLived = true;
    final Boolean issueRefreshToken = false;
    final String refreshToken = "refreshToken";
    final Long refreshTokenLifetimeSecs = 3600L;
    final Long accessTokenLifetimeSecs = 7200L;
    final String accessTokenEncoding = "accessTokenEncoding";
    final Boolean accessTokenEncryption = true;
    final String issuer = "issuer";
    final Long issuedAt = 7654321L;
    final Long updatedAt = 1234567L;
    final String authorisationAudience = "authorisationAudience";
    final String openIdConnectIdToken = "openIdConnectIdToken";
    final List<Claim> consentedClaims = asList(new Claim("consClaim1"), new Claim("consClaim2"));
    final List<Claim> savedConsentedClaims = asList(new Claim("savedConsClaim1"), new Claim("savedConsClaim2"));
    final List<Locale> preferredClaimLocales = asList(GERMAN, ENGLISH, FRENCH);
    final String pkceCodeChallenge = "pkceCodeChallenge";
    final String pkceCodeChallengeMethod = "pkceCodeChallengeMethod";
    final OpenIdConnectAuthorization CUT = new OpenIdConnectAuthorization(subject, clientId,
            authorisationScopes, savedScopes, redirectionURI, longLived, issueRefreshToken, refreshToken,
            refreshTokenLifetimeSecs, accessTokenLifetimeSecs, accessTokenEncoding, accessTokenEncryption, issuer, issuedAt,
            updatedAt, authorisationAudience, openIdConnectIdToken, consentedClaims, savedConsentedClaims,
            preferredClaimLocales, pkceCodeChallenge, pkceCodeChallengeMethod, ADDITIONAL_DATA);
    {
      final String valueAsString = OBJECT_WRITER.writeValueAsString(CUT);
      System.out.println("JSON: " + valueAsString);
      final Object reReadCUT = OBJECT_READER.readValue(valueAsString);
      System.out.println("Expecting: " + CUT);
      System.out.println("Received : " + reReadCUT);
      assertEquals("Marshalling roundtrip", CUT, reReadCUT);
    }
  }

  @Test
  public void marshallingIgnoresNullLifetime() throws IOException {
    final String subject = "subject";
    final ClientId clientId = new ClientId("clientID");
    final List<Scope> authorisationScopes = asList(new Scope("authScope1"), new Scope("authScope2"));
    final List<Scope> savedScopes = asList(new Scope("savedScope1"), new Scope("savedScope2"));
    final URI redirectionURI = URI.create("http://redirection.uri");
    final Boolean longLived = true;
    final Boolean issueRefreshToken = false;
    final String refreshToken = "refreshToken";
    final Long refreshTokenLifetimeSecs = null;
    final Long accessTokenLifetimeSecs = 7200L;
    final String accessTokenEncoding = "accessTokenEncoding";
    final Boolean accessTokenEncryption = true;
    final String issuer = "issuer";
    final Long issuedAt = 7654321L;
    final Long updatedAt = 1234567L;
    final String authorisationAudience = "authorisationAudience";
    final String openIdConnectIdToken = "openIdConnectIdToken";
    final List<Claim> consentedClaims = asList(new Claim("consClaim1"), new Claim("consClaim2"));
    final List<Claim> savedConsentedClaims = asList(new Claim("savedConsClaim1"), new Claim("savedConsClaim2"));
    final List<Locale> preferredClaimLocales = asList(GERMAN, ENGLISH, FRENCH);
    final String pkceCodeChallenge = "pkceCodeChallenge";
    final String pkceCodeChallengeMethod = "pkceCodeChallengeMethod";
    final OpenIdConnectAuthorization CUT = new OpenIdConnectAuthorization(subject, clientId,
            authorisationScopes, savedScopes, redirectionURI, longLived, issueRefreshToken, refreshToken,
            refreshTokenLifetimeSecs, accessTokenLifetimeSecs, accessTokenEncoding, accessTokenEncryption, issuer, issuedAt,
            updatedAt, authorisationAudience, openIdConnectIdToken, consentedClaims, savedConsentedClaims,
            preferredClaimLocales, pkceCodeChallenge, pkceCodeChallengeMethod, ADDITIONAL_DATA);
    {
      final String valueAsString = OBJECT_WRITER.writeValueAsString(CUT);
      System.out.println(valueAsString);
      assertFalse("No rtl element", valueAsString.contains("\"rtl\""));
      assertTrue("dat element available", valueAsString.contains("\"dat\""));
    }
  }

}
