/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.subjectsessionstore;

import de.denic.domainid.DomainId;
import de.denic.domainid.connect2id.Connect2IdIntegrationInstanceParameters;
import de.denic.domainid.connect2id.authzsession.Connect2IdAuthorizationSessionAPI;
import de.denic.domainid.connect2id.authzsession.ProvideSessionIdDispatcher;
import de.denic.domainid.connect2id.authzsession.ProvideSubjectSessionIdDispatcher;
import de.denic.domainid.connect2id.authzsession.SessionId;
import de.denic.domainid.connect2id.common.*;
import de.denic.domainid.subject.PseudonymSubject;
import org.apache.commons.text.RandomStringGenerator;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.security.auth.Subject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import java.net.URI;
import java.security.Principal;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static de.denic.domainid.connect2id.common.AuthenticationMethodReference.Value.pwd;
import static de.denic.domainid.connect2id.common.AuthenticationMethodReference.ofMany;
import static java.util.Arrays.asList;
import static java.util.Collections.emptySet;
import static org.apache.commons.lang3.StringUtils.reverse;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

public class Connect2IdSubjectSessionStoreAPITest extends Connect2IdIntegrationInstanceParameters {

  private static final RandomStringGenerator RANDOM_STATE_GENERATOR = new RandomStringGenerator.Builder()
          .withinRange('a', 'z').build();
  private static final Set<?> NO_PRIVATE_CREDENTIALS = emptySet();
  private static final Set<?> NO_PUBLIC_CREDENTIALS = emptySet();
  private static final String SOME_PSEUDONYM_USER_ID = "pseudonym.test-user";
  private static final Principal SOME_USER_ID_PRINCIPAL = new PseudonymSubject(SOME_PSEUDONYM_USER_ID);
  private static final DomainId SOME_DOMAIN_ID = new DomainId("test.domain.id");
  private static final Set<? extends Principal> SOME_PRINCIPALS = new HashSet<>(asList(SOME_DOMAIN_ID, SOME_USER_ID_PRINCIPAL));
  private static final boolean READ_ONLY = true;
  private static final Subject TEST_SUBJECT = new Subject(READ_ONLY, SOME_PRINCIPALS, NO_PUBLIC_CREDENTIALS,
          NO_PRIVATE_CREDENTIALS);
  private static final SubjectSessionId NO_SUBJECT_SESSION_ID = null;
  private static final Map<Claim, String> NO_REASONS_OF_CLAIMS = null;

  private static Client restApiClient;
  private Connect2IdSubjectSessionStoreAPI CUT;

  @BeforeClass
  public static void setUpBeforeClass() {
    setSystemsSSLTrustStoreConfig();
    safelyUnregisterIntegrationTestRelyingParty();
    registerIntegrationTestRelyingParty();
    restApiClient = ClientBuilder.newClient();
  }

  @AfterClass
  public static void tearDownAfterClass() {
    if (restApiClient != null) {
      restApiClient.close();
    }
    safelyUnregisterIntegrationTestRelyingParty();
    resetSystemsSSLTrustStoreConfig();
  }

  @Before
  public void setUp() {
    CUT = new Connect2IdSubjectSessionStoreAPI.Impl(CONNECT2ID_SUBJECT_SESSION_STORE_URL,
            getSessionStoreApiAccessToken(), restApiClient);
  }

  @Test
  public void mismatchingBearerAccessToken() {
    final AccessToken mismatchingBearerAccessToken = new AccessToken(reverse(getAuthSessionApiAccessToken().getValue()));
    final Connect2IdSubjectSessionStoreAPI misconfiguredCUT = new Connect2IdSubjectSessionStoreAPI.Impl(
            CONNECT2ID_SUBJECT_SESSION_STORE_URL, mismatchingBearerAccessToken, restApiClient);
    final SubjectSessionId randomSessionId = new SubjectSessionId(RANDOM_STATE_GENERATOR.generate(12));
    try {
      fail("Expecting UnauthorizedException, but got: " + misconfiguredCUT.sessionOf(randomSessionId));
    } catch (final UnauthorizedException expected) {
      expected.printStackTrace();
    }
  }

  @Test
  public void getDataOfUnknownSubjectSession() {
    final SubjectSessionId unknownSessionId = new SubjectSessionId(RANDOM_STATE_GENERATOR.generate(12));
    try {
      fail("Expecting NotFoundException, but got: " + CUT.sessionOf(unknownSessionId));
    } catch (final NotFoundException expected) {
      // EXPECTED
    }
  }

  @Test
  public void cancellingUnknownSubjectSession() {
    final SubjectSessionId unknownSessionId = new SubjectSessionId(RANDOM_STATE_GENERATOR.generate(12));
    assertFalse("Empty response ", CUT.cancelSession(unknownSessionId).isPresent());
  }

  @Test
  public void getDataOfKnownSubjectSession() {
    final Connect2IdAuthorizationSessionAPI authSessionAPI = new Connect2IdAuthorizationSessionAPI.Impl(
            CONNECT2ID_AUTHORISATION_SESSION_URL, getAuthSessionApiAccessToken(), restApiClient);
    final String randomState = RANDOM_STATE_GENERATOR.generate(12);
    final URI openIdAuthenticationRequestQuery = URI.create("response_type=code&scope=openid&client_id="
            + INTEGRATION_TEST_CLIENT_ID + "&state=" + randomState + "&redirect_uri=" + ENCODED_REDIRECT_URI_VALUE);
    final SessionId authApiSessionId = authSessionAPI
            .initiateSession(openIdAuthenticationRequestQuery, NO_SUBJECT_SESSION_ID, NO_REASONS_OF_CLAIMS)
            .dispatchedBy(new ProvideSessionIdDispatcher());
    final SubjectSessionId subjectSessionId = authSessionAPI.authenticate(authApiSessionId, TEST_SUBJECT, ofMany(pwd)).dispatchedBy(new ProvideSubjectSessionIdDispatcher());
    try {
      System.out.println(CUT.sessionOf(subjectSessionId));
    } finally {
      try {
        authSessionAPI.cancelSession(authApiSessionId);
      } catch (final NotFoundException possible) {
        // Intentionally left empty
      }
    }
  }

}
