/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.clientregistration;

import de.denic.domainid.connect2id.Connect2IdIntegrationInstanceParameters;
import de.denic.domainid.connect2id.clientregistration.entity.OpenIdClientMetadata;
import de.denic.domainid.connect2id.clientregistration.entity.OpenIdConnectClient;
import de.denic.domainid.connect2id.common.ClientId;
import de.denic.domainid.connect2id.common.NotFoundException;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import java.net.URI;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.apache.commons.lang3.StringUtils.repeat;
import static org.apache.commons.lang3.StringUtils.reverse;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

public class OpenIdConnectClientRegistrationTest extends Connect2IdIntegrationInstanceParameters {

  private static final ClientId UNKNOWN_CLIENT_ID = new ClientId("unknown");

  private static Client restApiClient;
  private OpenIdConnectClientRegistration CUT;

  @BeforeClass
  public static void setUpClass() {
    setSystemsSSLTrustStoreConfig();
    restApiClient = ClientBuilder.newClient();
  }

  @AfterClass
  public static void tearDownClass() {
    if (restApiClient != null) {
      restApiClient.close();
    }
    resetSystemsSSLTrustStoreConfig();
  }

  @Before
  public void setUp() {
    CUT = new OpenIdConnectClientRegistration.Impl(CONNECT2ID_URL, getClientRegistrationApiAccessToken(), restApiClient);
  }

  @Test
  public void loadUnknownClient() {
    assertFalse("No client with unknown ID", CUT.getClientOf(UNKNOWN_CLIENT_ID).isPresent());
  }

  @Test
  public void loadingAllClientsWorks() {
    final List<OpenIdConnectClient> allClients = CUT.getAllClients();
    System.out.println("=== Clients loaded: " + allClients.size() + " ===");
    allClients.forEach(client -> System.out.println("- " + client));
  }

  @Test(expected = NotFoundException.class)
  public void deleteUnknownClient() {
    CUT.deleteClientWith(UNKNOWN_CLIENT_ID);
  }

  @Test
  public void registerAndDeleteClient() {
    final ClientId preferredClientId = new ClientId("unit-test-rp-2");
    final String preferredClientSecret = reverse(repeat(preferredClientId.getPlain(), 3));
    final List<URI> redirectURIs = singletonList(URI.create("https://foo.bar"));
    final String clientName = "Client Name";
    final OpenIdClientMetadata clientMetadata = new OpenIdClientMetadata(redirectURIs, preferredClientId, clientName,
            preferredClientSecret);
    {
      final OpenIdConnectClient registeredClient = CUT.registerClient(clientMetadata);
      assertNotNull("Data of registered client", registeredClient);
      CUT.getClientOf(new ClientId("unit-test-rp"));
      CUT.deleteClientWith(registeredClient.getId());
    }
  }

}
