/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.authzsession;

import de.denic.domainid.DomainId;
import de.denic.domainid.connect2id.Connect2IdIntegrationInstanceParameters;
import de.denic.domainid.connect2id.HttpRequestResponseHeaderLoggingFilter;
import de.denic.domainid.connect2id.authzsession.Connect2IdAuthorizationSessionAPI.InitiateSessionResponse;
import de.denic.domainid.connect2id.authzsession.entity.AuthenticationPromptResponse;
import de.denic.domainid.connect2id.authzsession.entity.ConsentPromptResponse;
import de.denic.domainid.connect2id.authzsession.entity.FinalResponse;
import de.denic.domainid.connect2id.authzstore.Connect2IdAuthorizationStoreAPI;
import de.denic.domainid.connect2id.common.*;
import de.denic.domainid.connect2id.tokenendpoint.TokenEndpointClient;
import de.denic.domainid.connect2id.tokenendpoint.TokenEndpointResponseEntity;
import de.denic.domainid.jwks.JWKSetClient;
import de.denic.domainid.jwks.JWKSetParser;
import de.denic.domainid.subject.PseudonymSubject;
import de.denic.domainid.wellknown.openid.OpenIdConfigurationClient;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import org.apache.commons.text.RandomStringGenerator;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.junit.*;

import javax.security.auth.Subject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.Principal;
import java.security.PublicKey;
import java.time.Duration;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static de.denic.domainid.connect2id.authzsession.ConsentedData.NO_CONSENTED_DATA;
import static de.denic.domainid.connect2id.common.AuthenticationMethodReference.Value.pwd;
import static de.denic.domainid.connect2id.common.AuthenticationMethodReference.ofMany;
import static de.denic.domainid.connect2id.common.Scope.OPEN_ID;
import static java.net.URLEncoder.encode;
import static java.util.Arrays.asList;
import static java.util.Collections.*;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.reverse;
import static org.junit.Assert.*;

public class Connect2IdAuthorizationSessionAPIUpdateWithScopeAndClaimsTest
        extends Connect2IdIntegrationInstanceParameters {

  private static final boolean REMEMBER_CONSENT = true;
  private static final boolean DO_NOT_REMEMBER_CONSENT = !REMEMBER_CONSENT;
  private static final RandomStringGenerator RANDOM_STATE_GENERATOR = new RandomStringGenerator.Builder()
          .withinRange('a', 'z').build();
  private static final Set<?> NO_PRIVATE_CREDENTIALS = emptySet();
  private static final Set<?> NO_PUBLIC_CREDENTIALS = emptySet();
  private static final String SOME_PSEUDONYM_USER_ID = "pseudonym.test-user";
  private static final Principal SOME_USER_ID_PRINCIPAL = new PseudonymSubject(SOME_PSEUDONYM_USER_ID);
  private static final DomainId SOME_DOMAIN_ID = new DomainId("test.domain.id");
  private static final Set<? extends Principal> SOME_PRINCIPALS = new HashSet<>(asList(SOME_DOMAIN_ID,
          SOME_USER_ID_PRINCIPAL));
  private static final boolean READ_ONLY = true;
  private static final Subject TEST_SUBJECT = new Subject(READ_ONLY, SOME_PRINCIPALS, NO_PUBLIC_CREDENTIALS,
          NO_PRIVATE_CREDENTIALS);
  private static final Set<Claim> NO_REJECTED_CLAIMS = null;
  private static final ConsentedData OPENID_SCOPE = new ConsentedData(OPEN_ID, null, NO_REJECTED_CLAIMS);
  private static final ConsentedData OPENID_SCOPE_AND_NAME_CLAIM = new ConsentedData(OPEN_ID, singletonList(new Claim("name")),
          NO_REJECTED_CLAIMS);
  private static final ConsentedData OPENID_SCOPE_AND_UNKNOWN_CLAIM = new ConsentedData(OPEN_ID,
          singletonList(new Claim("foobar")), NO_REJECTED_CLAIMS);
  private static final SubjectSessionId NO_SUBJECT_SESSION_ID = null;
  private static final Map<Claim, String> NO_REASONS_OF_CLAIMS = null;
  private static final String NO_LOGIN_HINT = null;

  private final String randomState = RANDOM_STATE_GENERATOR.generate(16);
  private Client restApiClient;
  private Connect2IdAuthorizationSessionAPI CUT;
  private SessionId sessionId;

  @BeforeClass
  public static void setUpClass() {
    setSystemsSSLTrustStoreConfig();
    safelyUnregisterIntegrationTestRelyingParty();
    registerIntegrationTestRelyingParty();
  }

  @AfterClass
  public static void tearDownClass() {
    System.out.println("##### TEAR DOWN CLASS #####");
    safelyUnregisterIntegrationTestRelyingParty();
    resetSystemsSSLTrustStoreConfig();
  }

  private static void sleepForTwoSeconds() throws InterruptedException {
    System.out.println(">>> WAITING 2 SECONDS <<<");
    Thread.sleep(Duration.ofSeconds(2L).toMillis());
  }

  private static SessionId initiateSessionWithClaimNameAndAuthenticate(final Connect2IdAuthorizationSessionAPI CUT,
                                                                       final String randomState, final String optLoginHint, final SubjectSessionId optSubjectSessionId)
          throws UnsupportedEncodingException {
    final InitiateSessionResponse initiateSessionResponse = initiateSessionWithClaimName(CUT, randomState, optLoginHint,
            optSubjectSessionId);
    final SessionId sessionId = initiateSessionResponse
            .dispatchedBy(new ProvideAuthenticationPromptResponseDispatcher()).getConnect2IdAuthorisationSessionId();
    CUT.authenticate(sessionId, TEST_SUBJECT, ofMany(pwd)).dispatchedBy(new ProvideConsentPromptDispatcher());
    return sessionId;
  }

  private static InitiateSessionResponse initiateSessionWithClaimName(final Connect2IdAuthorizationSessionAPI CUT,
                                                                      final String randomState, final String optLoginHint, final SubjectSessionId optSubjectSessionId)
          throws UnsupportedEncodingException {
    return CUT
            .initiateSession(
                    URI.create("response_type=code&scope=openid&client_id=" + INTEGRATION_TEST_CLIENT_ID + "&state=" +
                            randomState
                            + "&redirect_uri=" + ENCODED_REDIRECT_URI_VALUE + "&"
                            + encode("claims={\"userinfo\":{\"name\":{\"essential\":true}}}", "ASCII")
                            + (isEmpty(optLoginHint) ? "" : "&login_hint=" + optLoginHint)),
                    optSubjectSessionId, NO_REASONS_OF_CLAIMS);
  }

  private static InitiateSessionResponse initiateSessionWithNoClaims(final Connect2IdAuthorizationSessionAPI CUT,
                                                                     final ClientId clientId, final String randomState, final String optLoginHint,
                                                                     final SubjectSessionId optSubjectSessionId) {
    return CUT.initiateSession(
            URI.create("response_type=code&scope=openid&client_id=" + clientId + "&state=" + randomState + "&redirect_uri="
                    + ENCODED_REDIRECT_URI_VALUE + (isEmpty(optLoginHint) ? "" : "&login_hint=" + optLoginHint)),
            optSubjectSessionId, NO_REASONS_OF_CLAIMS);
  }

  private static void assertResponseEntityContainsStateAndSomeAccessCode(final FinalResponse responseEntity,
                                                                         final String expectedState) {
    final URI redirectionURI = responseEntity.getParameters().getUri();
    final String redirectionUriQuery = redirectionURI.getQuery();
    assertTrue("Redirection URI's query contains auth token", redirectionUriQuery.contains("code="));
    assertTrue("Redirection URI's query contains provided state variable",
            redirectionUriQuery.contains("state=" + expectedState));
  }

  @Before
  public void setUp() {
    restApiClient = ClientBuilder.newClient();
    restApiClient.register(HttpRequestResponseHeaderLoggingFilter.class);
    revokeAllLongLivingAuthorisationsOf(SOME_PSEUDONYM_USER_ID);
    CUT = new Connect2IdAuthorizationSessionAPI.Impl(CONNECT2ID_AUTHORISATION_SESSION_URL,
            getAuthSessionApiAccessToken(), restApiClient);
  }

  private void revokeAllLongLivingAuthorisationsOf(final String subject) {
    new Connect2IdAuthorizationStoreAPI.Impl(CONNECT2ID_AUTHORISATION_STORE_URL, getSessionStoreApiAccessToken(),
            restApiClient).revokeAllAuthorizationsOf(() -> subject);
  }

  @After
  public void tearDown() {
    System.out.println("##### TEAR DOWN #####");
    try {
      if (CUT != null && sessionId != null) {
        try {
          CUT.cancelSession(sessionId);
        } catch (final NotFoundException irrelevant) {
          // IRRELEVANT
        }
      }
    } finally {
      if (restApiClient != null) {
        restApiClient.close();
      }
    }
  }

  @Test
  public void updatingSessionApplyingMismatchingSessionID() throws Exception {
    sessionId = initiateSessionWithClaimNameAndAuthenticate(CUT, randomState, NO_LOGIN_HINT, NO_SUBJECT_SESSION_ID);
    try {
      final SessionId mismatchingSessionId = new SessionId(reverse(sessionId.getValue()));
      fail("Expecting NotFoundException, but got: " + CUT.provide(mismatchingSessionId, SOME_DOMAIN_ID,
              NO_CONSENTED_DATA,
              false));
    } catch (final NotFoundException expected) {
      expected.printStackTrace(System.out);
    }
  }

  @Test
  public void updatingSessionWithUnknownClaim() throws Exception {
    sessionId = initiateSessionWithClaimNameAndAuthenticate(CUT, randomState, NO_LOGIN_HINT, NO_SUBJECT_SESSION_ID);
    final FinalResponse responseEntity = CUT.provide(sessionId, SOME_DOMAIN_ID, OPENID_SCOPE_AND_UNKNOWN_CLAIM, false)
            .dispatchedBy(new ProvideFinalResponseDispatcher());
    assertResponseEntityContainsStateAndSomeAccessCode(responseEntity, randomState);
  }

  @Test
  public void workflowWithTwoLongLivedAuthorisationSkippingConsentStepWithSecondOne() throws Exception {
    final SubjectSessionId subjectSessionId;
    {
      final AuthenticationPromptResponse expectedAuthenticationPromptEntity = initiateSessionWithClaimName(CUT,
              randomState, NO_LOGIN_HINT, NO_SUBJECT_SESSION_ID)
              .dispatchedBy(new ProvideAuthenticationPromptResponseDispatcher());
      sessionId = expectedAuthenticationPromptEntity.getConnect2IdAuthorisationSessionId();
      final ConsentPromptResponse expectedConsentPromptEntity = CUT.authenticate(sessionId, TEST_SUBJECT, ofMany(pwd)).dispatchedBy(new ProvideConsentPromptDispatcher());
      subjectSessionId = expectedConsentPromptEntity.getSubjectSession().getId();
      final FinalResponse expectedFinalResponseEntity = CUT
              .provide(sessionId, SOME_DOMAIN_ID, OPENID_SCOPE_AND_NAME_CLAIM, REMEMBER_CONSENT)
              .dispatchedBy(new ProvideFinalResponseDispatcher());
      final URI redirectionURI = expectedFinalResponseEntity.getParameters().getUri();
      final URI plainRedirectionURI = new URI(redirectionURI.getScheme(), redirectionURI.getUserInfo(),
              redirectionURI.getHost(), redirectionURI.getPort(), redirectionURI.getPath(), null, null);
      final Optional<String> optionalAccessCode = URLEncodedUtils.parse(redirectionURI.getQuery(), UTF8).stream()
              .filter(nameValuePair -> "code".equals(nameValuePair.getName()))
              .map(NameValuePair::getValue).findFirst();
      exchangeCodeForAccessTokenAtTokenEndpoint(optionalAccessCode.get(), INTEGRATION_TEST_CLIENT_ID, INTEGRATION_TEST_CLIENT_SECRET,
              plainRedirectionURI);
    }
    sleepForTwoSeconds();
    {
      final String secondRandomState = RANDOM_STATE_GENERATOR.generate(16);
      final FinalResponse immediatelyReceivedFinalResponseSkippingAuthenticationAsWellAsConsent = initiateSessionWithClaimName(
              CUT, secondRandomState, NO_LOGIN_HINT, subjectSessionId).dispatchedBy(new ProvideFinalResponseDispatcher());
      assertResponseEntityContainsStateAndSomeAccessCode(
              immediatelyReceivedFinalResponseSkippingAuthenticationAsWellAsConsent, secondRandomState);
    }
  }

  @Test
  public void workflowWithTwoLongLivedAuthorisationSkippingConsentStepWithSecondOneApplyingLoginHint()
          throws Exception {
    final String loginHint = SOME_PSEUDONYM_USER_ID;
    final SubjectSessionId subjectSessionId;
    {
      final AuthenticationPromptResponse expectedAuthenticationPromptEntity = initiateSessionWithClaimName(CUT,
              randomState, loginHint, NO_SUBJECT_SESSION_ID)
              .dispatchedBy(new ProvideAuthenticationPromptResponseDispatcher());
      sessionId = expectedAuthenticationPromptEntity.getConnect2IdAuthorisationSessionId();
      final ConsentPromptResponse expectedConsentPromptEntity = CUT.authenticate(sessionId, TEST_SUBJECT, ofMany(pwd)).dispatchedBy(new ProvideConsentPromptDispatcher());
      subjectSessionId = expectedConsentPromptEntity.getSubjectSession().getId();
      final FinalResponse expectedFinalResponseEntity = CUT
              .provide(sessionId, SOME_DOMAIN_ID, OPENID_SCOPE_AND_NAME_CLAIM, REMEMBER_CONSENT)
              .dispatchedBy(new ProvideFinalResponseDispatcher());
      final URI redirectionURI = expectedFinalResponseEntity.getParameters().getUri();
      final URI plainRedirectionURI = new URI(redirectionURI.getScheme(), redirectionURI.getUserInfo(),
              redirectionURI.getHost(), redirectionURI.getPort(), redirectionURI.getPath(), null, null);
      final Optional<String> optionalAccessCode = URLEncodedUtils.parse(redirectionURI.getQuery(), UTF8).stream()
              .filter(nameValuePair -> "code".equals(nameValuePair.getName()))
              .map(NameValuePair::getValue).findFirst();
      exchangeCodeForAccessTokenAtTokenEndpoint(optionalAccessCode.get(), INTEGRATION_TEST_CLIENT_ID, INTEGRATION_TEST_CLIENT_SECRET,
              plainRedirectionURI);
    }
    sleepForTwoSeconds();
    {
      final String secondRandomState = RANDOM_STATE_GENERATOR.generate(16);
      final FinalResponse immediatelyReceivedFinalResponseSkippingAuthenticationAsWellAsConsent = initiateSessionWithClaimName(
              CUT, secondRandomState, loginHint, subjectSessionId).dispatchedBy(new ProvideFinalResponseDispatcher());
      assertResponseEntityContainsStateAndSomeAccessCode(
              immediatelyReceivedFinalResponseSkippingAuthenticationAsWellAsConsent, secondRandomState);
    }
  }

  @Test
  public void workflowWithTwoShortLivedAuthorisationsGoingBothThroughConsentStep() throws Exception {
    sessionId = initiateSessionWithClaimNameAndAuthenticate(CUT, randomState, NO_LOGIN_HINT, NO_SUBJECT_SESSION_ID);
    {
      final FinalResponse responseEntity = CUT.provide(sessionId, SOME_DOMAIN_ID, OPENID_SCOPE_AND_NAME_CLAIM, DO_NOT_REMEMBER_CONSENT)
              .dispatchedBy(new ProvideFinalResponseDispatcher());
      final URI redirectionURI = responseEntity.getParameters().getUri();
      final URI plainRedirectionURI = new URI(redirectionURI.getScheme(), redirectionURI.getUserInfo(),
              redirectionURI.getHost(), redirectionURI.getPort(), redirectionURI.getPath(), null, null);
      final Optional<String> optionalAccessCode = URLEncodedUtils.parse(redirectionURI.getQuery(), UTF8).stream()
              .filter(nameValuePair -> "code".equals(nameValuePair.getName()))
              .map(NameValuePair::getValue).findFirst();
      exchangeCodeForAccessTokenAtTokenEndpoint(optionalAccessCode.get(), INTEGRATION_TEST_CLIENT_ID, INTEGRATION_TEST_CLIENT_SECRET,
              plainRedirectionURI);
    }
    sleepForTwoSeconds();
    final String secondRandomState = RANDOM_STATE_GENERATOR.generate(16);
    {
      // Reauthententication required cause Session Cookie is ignored!
      final SessionId secondSessionId = initiateSessionWithClaimName(CUT, secondRandomState, NO_LOGIN_HINT,
              NO_SUBJECT_SESSION_ID).dispatchedBy(new ProvideAuthenticationPromptResponseDispatcher())
              .getConnect2IdAuthorisationSessionId();
      final ConsentPromptResponse secondlyReceivedConsentPrompt = CUT.authenticate(secondSessionId, TEST_SUBJECT, singleton
              (new AuthenticationMethodReference("pwd"))).dispatchedBy(new ProvideConsentPromptDispatcher());
      assertNotNull("Consent prompt received a second time", secondlyReceivedConsentPrompt);
      final FinalResponse finalResponseEntity = CUT
              .provide(secondSessionId, SOME_DOMAIN_ID, OPENID_SCOPE_AND_NAME_CLAIM, DO_NOT_REMEMBER_CONSENT)
              .dispatchedBy(new ProvideFinalResponseDispatcher());
      assertResponseEntityContainsStateAndSomeAccessCode(finalResponseEntity, secondRandomState);
    }
  }

  @Ignore // TODO: Still ignored!
  @Test
  public void workflowWithSecondAuthorisationStepApplyingAlreadyEstablishedSubjectSessionCookie_BUT_DifferentLoginHint_OnSameShop()
          throws Exception {
    final SubjectSessionId subjectSessionId;
    {
      final String initialLoginHint = SOME_PSEUDONYM_USER_ID;
      final AuthenticationPromptResponse expectedAuthenticationPromptEntity = initiateSessionWithNoClaims(CUT,
              INTEGRATION_TEST_CLIENT_ID, randomState, initialLoginHint, NO_SUBJECT_SESSION_ID)
              .dispatchedBy(new ProvideAuthenticationPromptResponseDispatcher());
      sessionId = expectedAuthenticationPromptEntity.getConnect2IdAuthorisationSessionId();
      final ConsentPromptResponse expectedConsentPromptEntity = CUT.authenticate(sessionId, TEST_SUBJECT, ofMany(pwd)).dispatchedBy(new ProvideConsentPromptDispatcher());
      subjectSessionId = expectedConsentPromptEntity.getSubjectSession().getId();
      final FinalResponse expectedFinalResponseEntity = CUT.provide(sessionId, SOME_DOMAIN_ID, OPENID_SCOPE, REMEMBER_CONSENT)
              .dispatchedBy(new ProvideFinalResponseDispatcher());
      final URI redirectionURI = expectedFinalResponseEntity.getParameters().getUri();
      final URI plainRedirectionURI = new URI(redirectionURI.getScheme(), redirectionURI.getUserInfo(),
              redirectionURI.getHost(), redirectionURI.getPort(), redirectionURI.getPath(), null, null);
      final Optional<String> optionalAccessCode = URLEncodedUtils.parse(redirectionURI.getQuery(), UTF8).stream()
              .filter(nameValuePair -> "code".equals(nameValuePair.getName()))
              .map(NameValuePair::getValue).findFirst();
      final Jws<Claims> accessTokenJws = exchangeCodeForAccessTokenAtTokenEndpoint(optionalAccessCode.get(),
              INTEGRATION_TEST_CLIENT_ID, INTEGRATION_TEST_CLIENT_SECRET, plainRedirectionURI);
      final String subject = accessTokenJws.getBody().getSubject();
      assertEquals("Subject from Access Token", SOME_PSEUDONYM_USER_ID, subject);
    }
    sleepForTwoSeconds();
    {
      final String otherLoginHint = reverse(SOME_PSEUDONYM_USER_ID);
      revokeAllLongLivingAuthorisationsOf(otherLoginHint);
      sleepForTwoSeconds();
      final String secondRandomState = RANDOM_STATE_GENERATOR.generate(16);
      final FinalResponse immediatelyReceivedFinalResponse = initiateSessionWithNoClaims(CUT, INTEGRATION_TEST_CLIENT_ID,
              secondRandomState, otherLoginHint, subjectSessionId).dispatchedBy(new ProvideFinalResponseDispatcher());
      final URI redirectionURI = immediatelyReceivedFinalResponse.getParameters().getUri();
      final URI plainRedirectionURI = new URI(redirectionURI.getScheme(), redirectionURI.getUserInfo(),
              redirectionURI.getHost(), redirectionURI.getPort(), redirectionURI.getPath(), null, null);
      final Optional<String> optionalAccessCode = URLEncodedUtils.parse(redirectionURI.getQuery(), UTF8).stream()
              .filter(nameValuePair -> "code".equals(nameValuePair.getName()))
              .map(NameValuePair::getValue).findFirst();
      final Jws<Claims> accessTokenJws = exchangeCodeForAccessTokenAtTokenEndpoint(optionalAccessCode.get(),
              INTEGRATION_TEST_CLIENT_ID, INTEGRATION_TEST_CLIENT_SECRET, plainRedirectionURI);
      final String subject = accessTokenJws.getBody().getSubject();
      assertEquals("Subject from Access Token", otherLoginHint, subject);
    }
  }

  @Ignore // TODO: Still ignored!
  @Test
  public void workflowWithSecondAuthorisationStepApplyingAlreadyEstablishedSubjectSessionCookie_BUT_DifferentLoginHint_OnSecondShop()
          throws Exception {
    final SubjectSessionId subjectSessionId;
    {
      final String initialLoginHint = SOME_PSEUDONYM_USER_ID;
      final AuthenticationPromptResponse expectedAuthenticationPromptEntity = initiateSessionWithNoClaims(CUT,
              INTEGRATION_TEST_CLIENT_ID, randomState, initialLoginHint, NO_SUBJECT_SESSION_ID)
              .dispatchedBy(new ProvideAuthenticationPromptResponseDispatcher());
      sessionId = expectedAuthenticationPromptEntity.getConnect2IdAuthorisationSessionId();
      final ConsentPromptResponse expectedConsentPromptEntity = CUT.authenticate(sessionId, TEST_SUBJECT, ofMany(pwd)).dispatchedBy(new ProvideConsentPromptDispatcher());
      subjectSessionId = expectedConsentPromptEntity.getSubjectSession().getId();
      final FinalResponse expectedFinalResponseEntity = CUT.provide(sessionId, SOME_DOMAIN_ID, OPENID_SCOPE, REMEMBER_CONSENT)
              .dispatchedBy(new ProvideFinalResponseDispatcher());
      final URI redirectionURI = expectedFinalResponseEntity.getParameters().getUri();
      final URI plainRedirectionURI = new URI(redirectionURI.getScheme(), redirectionURI.getUserInfo(),
              redirectionURI.getHost(), redirectionURI.getPort(), redirectionURI.getPath(), null, null);
      final Optional<String> optionalAccessCode = URLEncodedUtils.parse(redirectionURI.getQuery(), UTF8).stream()
              .filter(nameValuePair -> "code".equals(nameValuePair.getName()))
              .map(NameValuePair::getValue).findFirst();
      final Jws<Claims> accessTokenJws = exchangeCodeForAccessTokenAtTokenEndpoint(optionalAccessCode.get(),
              INTEGRATION_TEST_CLIENT_ID, INTEGRATION_TEST_CLIENT_SECRET, plainRedirectionURI);
      final String subject = accessTokenJws.getBody().getSubject();
      assertEquals("Subject from Access Token", SOME_PSEUDONYM_USER_ID, subject);
    }
    sleepForTwoSeconds();
    {
      final String otherLoginHint = reverse(SOME_PSEUDONYM_USER_ID);
      revokeAllLongLivingAuthorisationsOf(otherLoginHint);
      sleepForTwoSeconds();
      final String secondRandomState = RANDOM_STATE_GENERATOR.generate(16);
      final ConsentPromptResponse expectedConsentPromptEntity = initiateSessionWithNoClaims(CUT,
              INTEGRATION_TEST_CLIENT_ID, secondRandomState, otherLoginHint, subjectSessionId)
              .dispatchedBy(new ProvideConsentPromptDispatcher());
      final SessionId secondSessionId = expectedConsentPromptEntity.getConnect2IdAuthorisationSessionId();
      final SubjectSessionId secondSubjectSessionId = expectedConsentPromptEntity.getSubjectSession().getId();
      System.err.println("==> 1st Subj. Session ID: " + subjectSessionId);
      System.err.println("==> 2nd Subj. Session ID: " + secondSubjectSessionId);
      final FinalResponse expectedFinalResponseEntity = CUT.provide(secondSessionId, SOME_DOMAIN_ID, OPENID_SCOPE, REMEMBER_CONSENT)
              .dispatchedBy(new ProvideFinalResponseDispatcher());
      final URI redirectionURI = expectedFinalResponseEntity.getParameters().getUri();
      final URI plainRedirectionURI = new URI(redirectionURI.getScheme(), redirectionURI.getUserInfo(),
              redirectionURI.getHost(), redirectionURI.getPort(), redirectionURI.getPath(), null, null);
      final Optional<String> optionalAccessCode = URLEncodedUtils.parse(redirectionURI.getQuery(), UTF8).stream()
              .filter(nameValuePair -> "code".equals(nameValuePair.getName()))
              .map(NameValuePair::getValue).findFirst();
      final Jws<Claims> accessTokenJws = exchangeCodeForAccessTokenAtTokenEndpoint(optionalAccessCode.get(),
              INTEGRATION_TEST_CLIENT_ID, INTEGRATION_TEST_CLIENT_SECRET, plainRedirectionURI);
      final String subject = accessTokenJws.getBody().getSubject();
      assertEquals("Subject from Access Token", otherLoginHint, subject);
    }
  }

  private Jws<Claims> exchangeCodeForAccessTokenAtTokenEndpoint(final String accessCode, final ClientId clientId,
                                                                final String clientSecret, final URI redirectionURI) throws URISyntaxException {
    final TokenEndpointResponseEntity tokenEndpointResponseEntity = new TokenEndpointClient.Impl(restApiClient)
            .exchange(CONNECT2ID_URL.toURI().resolve("/token"), clientId, clientSecret, redirectionURI, accessCode);
    final String accessToken = tokenEndpointResponseEntity.getAccessToken();
    assertNotNull("Received an Access Token", accessToken);
    final URI jwkSetURI = new OpenIdConfigurationClient.Impl(restApiClient).getFromBaseURI(CONNECT2ID_URL.toURI())
            .getJwkSetURI();
    final PublicKey firstPublicKey = new JWKSetClient.Impl(restApiClient, new JWKSetParser.Impl())
            .getPublicRSAKeysFrom(jwkSetURI).get(0).getValue();
    final Jws<Claims> accessTokenJws = parseStrippingSignature("ACCESS TOKEN:", accessToken, firstPublicKey);
    parseStrippingSignature("ID TOKEN:", tokenEndpointResponseEntity.getIdToken(), firstPublicKey);
    // parseStrippingSignature("REFRESH TOKEN:", tokenEndpointResponseEntity.getRefreshToken(), firstPublicKey);
    return accessTokenJws;
  }

  private Jws<Claims> parseStrippingSignature(final String description, final String token, final PublicKey publicKey) {
    System.out.println(description);
    final Jws<Claims> result = Jwts.parser().setSigningKey(publicKey).parseClaimsJws(token);
    System.out.println(result);
    return result;
  }

  private static final class MyPrincipal implements Principal {

    private final String name;

    MyPrincipal(final String name) {
      this.name = name;
    }

    @Override
    public String getName() {
      return name;
    }

  }

}
