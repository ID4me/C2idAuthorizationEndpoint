/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.authzsession;

import de.denic.domainid.DomainId;
import de.denic.domainid.connect2id.Connect2IdIntegrationInstanceParameters;
import de.denic.domainid.connect2id.authzsession.entity.AuthenticationPromptResponse;
import de.denic.domainid.connect2id.authzsession.entity.ConsentPromptOrFinalResponse;
import de.denic.domainid.connect2id.common.Claim;
import de.denic.domainid.connect2id.common.ClientId;
import de.denic.domainid.connect2id.common.NotFoundException;
import de.denic.domainid.connect2id.common.SubjectSessionId;
import de.denic.domainid.subject.PseudonymSubject;
import org.junit.*;

import javax.security.auth.Subject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.security.Principal;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static de.denic.domainid.connect2id.common.AuthenticationMethodReference.Value.pwd;
import static de.denic.domainid.connect2id.common.AuthenticationMethodReference.ofMany;
import static java.net.URLEncoder.encode;
import static java.util.Arrays.asList;
import static java.util.Collections.emptySet;
import static org.apache.commons.lang3.StringUtils.reverse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class Connect2IdAuthorizationSessionAPIUpdateWithSubjectTest extends Connect2IdIntegrationInstanceParameters {

  private static final String UTF_8 = "UTF-8";
  private static final Set<?> NO_PRIVATE_CREDENTIALS = emptySet();
  private static final Set<?> NO_PUBLIC_CREDENTIALS = emptySet();
  private static final String SOME_PSEUDONYM_USER_ID = "pseudonym.test-user";
  private static final Principal SOME_USER_ID_PRINCIPAL = new PseudonymSubject(SOME_PSEUDONYM_USER_ID);
  private static final DomainId SOME_DOMAIN_ID = new DomainId("test.domain.id");
  private static final Set<? extends Principal> SOME_PRINCIPALS = new HashSet<>(asList(SOME_DOMAIN_ID, SOME_USER_ID_PRINCIPAL));
  private static final boolean READ_ONLY = true;
  private static final Subject SOME_SUBJECT = new Subject(READ_ONLY, SOME_PRINCIPALS, NO_PUBLIC_CREDENTIALS,
          NO_PRIVATE_CREDENTIALS);
  private static final SubjectSessionId NO_SUBJECT_SESSION_ID = null;
  private static final Map<Claim, String> NO_REASONS_OF_CLAIMS = null;

  private static Client restApiClient;

  private Connect2IdAuthorizationSessionAPI CUT;
  private SessionId sessionId;

  @BeforeClass
  public static void setUpClass() {
    setSystemsSSLTrustStoreConfig();
    safelyUnregisterIntegrationTestRelyingParty();
    registerIntegrationTestRelyingParty();
    restApiClient = ClientBuilder.newClient();
  }

  @AfterClass
  public static void tearDownClass() {
    if (restApiClient != null) {
      restApiClient.close();
    }
    safelyUnregisterIntegrationTestRelyingParty();
    resetSystemsSSLTrustStoreConfig();
  }

  @Before
  public void setUp() {
    CUT = new Connect2IdAuthorizationSessionAPI.Impl(CONNECT2ID_AUTHORISATION_SESSION_URL,
            getAuthSessionApiAccessToken(), restApiClient);
  }

  @After
  public void tearDown() {
    if (CUT != null && sessionId != null) {
      try {
        CUT.cancelSession(sessionId);
      } catch (final NotFoundException possible) {
        // Intentionally left empty
      }
    }
  }

  @Test
  public void updatingSessionApplyingMismatchingSessionID() throws UnsupportedEncodingException {
    sessionId = initiateSession(INTEGRATION_TEST_CLIENT_ID).getConnect2IdAuthorisationSessionId();
    try {
      final SessionId mismatchingSessionId = new SessionId(reverse(sessionId.getValue()));
      fail("Expecting NotFoundException, but got: " + CUT.authenticate(mismatchingSessionId, SOME_SUBJECT, ofMany(pwd)));
    } catch (final NotFoundException expected) {
      expected.printStackTrace();
    }
  }

  @Test
  public void successfullyUpdatingSessionWithSubject() throws UnsupportedEncodingException {
    sessionId = initiateSession(INTEGRATION_TEST_CLIENT_ID).getConnect2IdAuthorisationSessionId();
    final ConsentPromptOrFinalResponse consentPromptOrFinalResponseEntity = CUT
            .authenticate(sessionId, SOME_SUBJECT, ofMany(pwd)).dispatchedBy(new ProvideConsentPromptOrFinalResponseDispatcher());
    if (!consentPromptOrFinalResponseEntity.hasFinalResponseEntity()) {
      assertEquals("Session ID", sessionId,
              consentPromptOrFinalResponseEntity.getConsentPromptEntity().getConnect2IdAuthorisationSessionId());
    }
  }

  private AuthenticationPromptResponse initiateSession(final ClientId clientId)
          throws UnsupportedEncodingException {
    return CUT.initiateSession(URI.create("response_type=code&scope=openid&client_id=" + clientId
                    + "&state=SomeState&redirect_uri=" + encode(REDIRECT_URI.toString(), UTF_8)), NO_SUBJECT_SESSION_ID,
            NO_REASONS_OF_CLAIMS).dispatchedBy(new ProvideAuthenticationPromptResponseDispatcher());
  }

  private static final class MyPrincipal implements Principal {

    private final String name;

    public MyPrincipal(final String name) {
      this.name = name;
    }

    @Override
    public String getName() {
      return name;
    }

  }

}
