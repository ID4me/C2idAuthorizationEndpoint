/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.authzsession;

import de.denic.domainid.connect2id.Connect2IdIntegrationInstanceParameters;
import de.denic.domainid.connect2id.authzsession.Connect2IdAuthorizationSessionAPI.InitiateSessionResponseDispatcher;
import de.denic.domainid.connect2id.authzsession.entity.AuthenticationPromptResponse;
import de.denic.domainid.connect2id.authzsession.entity.FinalResponse;
import de.denic.domainid.connect2id.authzsession.entity.NonRedirectableErrorResponse;
import de.denic.domainid.connect2id.common.*;
import org.apache.commons.text.RandomStringGenerator;
import org.easymock.Capture;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import java.net.URI;
import java.util.Map;

import static de.denic.domainid.connect2id.common.Display.Value.popup;
import static org.apache.commons.lang3.StringUtils.reverse;
import static org.easymock.EasyMock.*;
import static org.junit.Assert.*;

public class Connect2IdAuthorizationSessionAPIInitiatingTest extends Connect2IdIntegrationInstanceParameters {

  private static final RandomStringGenerator RANDOM_STATE_GENERATOR = new RandomStringGenerator.Builder()
          .withinRange('a', 'z').build();
  private static final Object FOO = new Object();
  private static final SubjectSessionId NO_SUBJECT_SESSION_ID = null;
  private static final Map<Claim, String> NO_REASONS_OF_CLAIMS = null;

  private static Client restApiClient;

  private Connect2IdAuthorizationSessionAPI CUT;
  private InitiateSessionResponseDispatcher<Object> initiateSessionResponseVisitorMock;
  private String randomState;

  @BeforeClass
  public static void setUpClass() {
    setSystemsSSLTrustStoreConfig();
    restApiClient = ClientBuilder.newClient();
    safelyUnregisterIntegrationTestRelyingParty();
    registerIntegrationTestRelyingParty();
  }

  @AfterClass
  public static void tearDownClass() {
    safelyUnregisterIntegrationTestRelyingParty();
    if (restApiClient != null) {
      restApiClient.close();
    }
    resetSystemsSSLTrustStoreConfig();
  }

  @SuppressWarnings("unchecked")
  @Before
  public void setUp() {
    CUT = new Connect2IdAuthorizationSessionAPI.Impl(CONNECT2ID_AUTHORISATION_SESSION_URL,
            getAuthSessionApiAccessToken(), restApiClient);
    initiateSessionResponseVisitorMock = createMock(InitiateSessionResponseDispatcher.class);
    randomState = RANDOM_STATE_GENERATOR.generate(16);
  }

  @Test
  public void mismatchingBearerAccessToken() {
    final AccessToken mismatchingBearerAccessToken = new AccessToken(reverse(getAuthSessionApiAccessToken().getValue()));
    final Connect2IdAuthorizationSessionAPI misconfiguredCUT = new Connect2IdAuthorizationSessionAPI.Impl(
            CONNECT2ID_AUTHORISATION_SESSION_URL, mismatchingBearerAccessToken, restApiClient);
    try {
      fail("Expecting UnauthorizedException, but got: "
              + misconfiguredCUT.initiateSession(REDIRECT_URI, NO_SUBJECT_SESSION_ID, NO_REASONS_OF_CLAIMS));
    } catch (final UnauthorizedException expected) {
      // EXPECTED
    }
  }

  @Test
  public void missingClientIdParamInOpenIdAuthenticationRequestURI() {
    expect(initiateSessionResponseVisitorMock.handle(anyObject(NonRedirectableErrorResponse.class)))
            .andReturn(FOO);
    replay(initiateSessionResponseVisitorMock);
    {
      assertSame(FOO,
              CUT.initiateSession(URI.create(
                      "response_type=code&scope=openid&state=" + randomState + "&redirect_uri=" + ENCODED_REDIRECT_URI_VALUE),
                      NO_SUBJECT_SESSION_ID, NO_REASONS_OF_CLAIMS).dispatchedBy(initiateSessionResponseVisitorMock));
      verify(initiateSessionResponseVisitorMock);
    }
  }

  @Test
  public void mismatchingClientIdParamInOpenIdAuthenticationRequestURI() {
    final String mismatchingClientId = "mismatchingClientId";
    expect(initiateSessionResponseVisitorMock.handle(anyObject(NonRedirectableErrorResponse.class)))
            .andReturn(FOO);
    replay(initiateSessionResponseVisitorMock);
    {
      assertSame(FOO,
              CUT.initiateSession(URI.create("response_type=code&scope=openid&client_id=" + mismatchingClientId + "&state="
                              + randomState + "&redirect_uri=" + ENCODED_REDIRECT_URI_VALUE), NO_SUBJECT_SESSION_ID,
                      NO_REASONS_OF_CLAIMS).dispatchedBy(initiateSessionResponseVisitorMock));
      verify(initiateSessionResponseVisitorMock);
    }
  }

  @Test
  public void cancellingSuccessfullyOpenedAuthorisationSessionOnC2IdServer() {
    final AuthenticationPromptResponse entity = CUT
            .initiateSession(URI.create("response_type=code&scope=openid&client_id=" + INTEGRATION_TEST_CLIENT_ID + "&state="
                    + randomState + "&redirect_uri=" + ENCODED_REDIRECT_URI_VALUE), NO_SUBJECT_SESSION_ID, NO_REASONS_OF_CLAIMS)
            .dispatchedBy(new ProvideAuthenticationPromptResponseDispatcher());
    final SessionId sessionId = entity.getConnect2IdAuthorisationSessionId();
    assertNotNull("Precondition: Received session ID", sessionId);
    {
      final FinalResponse finalResponse = CUT.cancelSession(sessionId);
      assertNotNull("Final response entity", finalResponse);
      assertNotNull("Redirect URI available", finalResponse.getParameters().getUri());
      assertFalse("No subject session ID available cause subject not authenticated yet",
              finalResponse.getSubjectSessionId().isPresent());
    }
  }

  @Test
  public void successfulOpenIdAuthenticationRequest_AndGetDataBackAgainByAnotherRequest() {
    SessionId sessionId = null;
    try {
      final AuthenticationPromptResponse entity = CUT
              .initiateSession(URI.create("response_type=code&scope=openid&client_id=" + INTEGRATION_TEST_CLIENT_ID + "&state="
                              + randomState + "&redirect_uri=" + ENCODED_REDIRECT_URI_VALUE), NO_SUBJECT_SESSION_ID,
                      NO_REASONS_OF_CLAIMS)
              .dispatchedBy(new ProvideAuthenticationPromptResponseDispatcher());
      sessionId = entity.getConnect2IdAuthorisationSessionId();
      assertNotNull("Received session ID", sessionId);
      assertNotNull("Got data of freshly initiated session", CUT.getSession(sessionId));
    } finally {
      if (sessionId != null) {
        CUT.cancelSession(sessionId);
      }
    }
  }

  @Test
  public void openIdAuthenticationRequestWithSubjectSessionId() {
    final SubjectSessionId subjectSessionId = randomSubjectSessionId();
    SessionId sessionId = null;
    try {
      final AuthenticationPromptResponse entity = CUT
              .initiateSession(URI.create("response_type=code&scope=openid&client_id=" + INTEGRATION_TEST_CLIENT_ID + "&state="
                      + randomState + "&redirect_uri=" + ENCODED_REDIRECT_URI_VALUE), subjectSessionId, NO_REASONS_OF_CLAIMS)
              .dispatchedBy(new ProvideAuthenticationPromptResponseDispatcher());
      sessionId = entity.getConnect2IdAuthorisationSessionId();
      assertNotNull("Received session ID", sessionId);
    } finally {
      if (sessionId != null) {
        CUT.cancelSession(sessionId);
      }
    }
  }

  private SubjectSessionId randomSubjectSessionId() {
    final String randomValue = RANDOM_STATE_GENERATOR.generate(24);
    return new SubjectSessionId(randomValue);
  }

  @Test
  public void openIdAuthenticationRequestWithMaxAgeParameter() {
    SessionId sessionId = null;
    try {
      final AuthenticationPromptResponse entity = CUT.initiateSession(
              URI.create("response_type=code&scope=openid&client_id=" + INTEGRATION_TEST_CLIENT_ID + "&state=" + randomState
                      + "&redirect_uri=" + ENCODED_REDIRECT_URI_VALUE + "&max_age=5"),
              NO_SUBJECT_SESSION_ID, NO_REASONS_OF_CLAIMS)
              .dispatchedBy(new ProvideAuthenticationPromptResponseDispatcher());
      sessionId = entity.getConnect2IdAuthorisationSessionId();
      assertNotNull("Received session ID", sessionId);
      // assertNotNull("Got data of freshly initiated session", CUT.getSession(sessionId));
    } finally {
      if (sessionId != null) {
        CUT.cancelSession(sessionId);
      }
    }
  }

  @Test
  public void openIdAuthenticationRequestWithPromptNONE() {
    final Capture<FinalResponse> capturedResponseEntity = newCapture();
    expect(initiateSessionResponseVisitorMock.handle(capture(capturedResponseEntity))).andReturn(FOO);
    replay(initiateSessionResponseVisitorMock);
    {
      final Object response = CUT.initiateSession(
              URI.create("response_type=code&scope=openid&client_id=" + INTEGRATION_TEST_CLIENT_ID + "&state=" + randomState
                      + "&redirect_uri=" + ENCODED_REDIRECT_URI_VALUE + "&prompt=none"),
              NO_SUBJECT_SESSION_ID, NO_REASONS_OF_CLAIMS).dispatchedBy(initiateSessionResponseVisitorMock);
      verify(initiateSessionResponseVisitorMock);
      assertTrue("Captured " + FinalResponse.class, capturedResponseEntity.hasCaptured());
      assertTrue("URI parameter contains login_required query element",
              capturedResponseEntity.getValue().getParameters().getUri().getQuery().contains("error=login_required"));
      assertSame(FOO, response);
    }
  }

  @Test
  public void openIdAuthenticationRequestWithDisplayPOPUP() {
    final Capture<AuthenticationPromptResponse> capturedResponseEntity = newCapture();
    expect(initiateSessionResponseVisitorMock.handle(capture(capturedResponseEntity))).andReturn(FOO);
    SessionId sessionId = null;
    replay(initiateSessionResponseVisitorMock);
    try {
      final Object response = CUT.initiateSession(
              URI.create("response_type=code&scope=openid&client_id=" + INTEGRATION_TEST_CLIENT_ID + "&state=" + randomState
                      + "&redirect_uri=" + ENCODED_REDIRECT_URI_VALUE + "&display=popup"),
              NO_SUBJECT_SESSION_ID, NO_REASONS_OF_CLAIMS).dispatchedBy(initiateSessionResponseVisitorMock);
      verify(initiateSessionResponseVisitorMock);
      assertTrue("Captured " + AuthenticationPromptResponse.class, capturedResponseEntity.hasCaptured());
      assertEquals("Response states POPUP as display value", popup,
              capturedResponseEntity.getValue().getDisplay().getEnumValue());
      assertSame(FOO, response);
      sessionId = capturedResponseEntity.getValue().getConnect2IdAuthorisationSessionId();
    } finally {
      if (sessionId != null) {
        CUT.cancelSession(sessionId);
      }
    }
  }

  @Test
  public void getDataOfUnknownAuthorisationSessionFromConnect2IdServer() {
    try {
      fail("Expecting NotFoundException, but got: " + CUT.getSession(new SessionId("unknown-session-id")));
    } catch (final NotFoundException expected) {
      expected.printStackTrace();
    }
  }

}
