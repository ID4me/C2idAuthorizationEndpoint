/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id;

import de.denic.domainid.connect2id.clientregistration.OpenIdConnectClientRegistration;
import de.denic.domainid.connect2id.clientregistration.entity.OpenIdClientMetadata;
import de.denic.domainid.connect2id.common.AccessToken;
import de.denic.domainid.connect2id.common.ClientId;
import de.denic.domainid.connect2id.common.Connect2IdException;
import org.apache.commons.lang3.Validate;

import javax.ws.rs.client.ClientBuilder;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.nio.charset.Charset;

import static java.net.URLEncoder.encode;
import static java.util.Collections.singletonList;
import static org.apache.commons.lang3.SystemUtils.USER_DIR;

/**
 * Just to hold all parameters to target integrations tests against Connect2Id instance running on
 * <code>domainid-1.dev.stage.denic.de</code> on one place.
 */
public abstract class Connect2IdIntegrationInstanceParameters {

  public static final Charset ASCII = Charset.forName("ASCII");
  public static final Charset UTF8 = Charset.forName("UTF8");
  public static final String LOCAL_PATH_TO_DOMAINID_CA_TRUST_STORE = USER_DIR
          + "/src/test/resources/trusted-certificates.jks";
  public static final String REDIRECT_URI_VALUE = "https://foo.redirect-uri.bar";
  public static final String ENCODED_REDIRECT_URI_VALUE;
  public static final URL CONNECT2ID_URL, CONNECT2ID_AUTHORISATION_SESSION_URL, CONNECT2ID_SUBJECT_SESSION_STORE_URL,
          CONNECT2ID_AUTHORISATION_STORE_URL, CONNECT2ID_LOGOUT_SESSION_URL;
  public static final ClientId INTEGRATION_TEST_CLIENT_ID = new ClientId("integration-test-rp");
  public static final String INTEGRATION_TEST_CLIENT_NAME = "Integration Test Client Name";
  public static final String INTEGRATION_TEST_CLIENT_SECRET = "UnitTestClientSecret01UnitTestClientSecret02";
  public static final URI REDIRECT_URI = URI.create(REDIRECT_URI_VALUE);

  private static final String JVM_SSL_TRUST_STORE_CONFIG = "javax.net.ssl.trustStore";
  private static final String JVM_SSL_TRUST_STORE_PASSWORD_CONFIG = "javax.net.ssl.trustStorePassword";
  private static final String TRUST_STORE_FILE_PASSWORD = "changeme";
  /**
   * K8s cluster's internal DNS name of Connect2Id server in "staging" namespace (aka stage):
   */
  private static final String CONNECT2ID_URL_VALUE = "http://c2id.staging.svc.cluster.local:8080";
  private static final OpenIdConnectClientRegistration OPEN_ID_CONNECT_CLIENT_REGISTRATION;
  private static AccessToken authSessionApiAccessToken, clientRegistrationApiAccessToken, sessionStoreApiAccessToken;

  static {
    try {
      CONNECT2ID_URL = new URL(CONNECT2ID_URL_VALUE);
      CONNECT2ID_AUTHORISATION_SESSION_URL = new URL(CONNECT2ID_URL, "/authz-sessions/rest/v3/");
      CONNECT2ID_SUBJECT_SESSION_STORE_URL = new URL(CONNECT2ID_URL, "/session-store/rest/v2/");
      CONNECT2ID_AUTHORISATION_STORE_URL = new URL(CONNECT2ID_URL, "/authz-store/rest/v2/");
      CONNECT2ID_LOGOUT_SESSION_URL = new URL(CONNECT2ID_URL, "/logout-sessions/rest/v1/");
      ENCODED_REDIRECT_URI_VALUE = encode(REDIRECT_URI_VALUE, ASCII.name());
      OPEN_ID_CONNECT_CLIENT_REGISTRATION = new
              OpenIdConnectClientRegistration.Impl(CONNECT2ID_URL, getClientRegistrationApiAccessToken(),
              ClientBuilder.newClient());
    } catch (final MalformedURLException | UnsupportedEncodingException e) {
      throw new ExceptionInInitializerError(e);
    }
  }

  public static void assertConnect2IdServerIsReachable() {
    System.out.println(
            "### Setting System property: " + JVM_SSL_TRUST_STORE_CONFIG + ":" + LOCAL_PATH_TO_DOMAINID_CA_TRUST_STORE);
    System.setProperty(JVM_SSL_TRUST_STORE_CONFIG, LOCAL_PATH_TO_DOMAINID_CA_TRUST_STORE);
    System.setProperty(JVM_SSL_TRUST_STORE_PASSWORD_CONFIG, TRUST_STORE_FILE_PASSWORD);
  }

  public static void setSystemsSSLTrustStoreConfig() {
    System.out.println(
            "### Setting System property: " + JVM_SSL_TRUST_STORE_CONFIG + ":" + LOCAL_PATH_TO_DOMAINID_CA_TRUST_STORE);
    System.setProperty(JVM_SSL_TRUST_STORE_CONFIG, LOCAL_PATH_TO_DOMAINID_CA_TRUST_STORE);
    System.setProperty(JVM_SSL_TRUST_STORE_PASSWORD_CONFIG, TRUST_STORE_FILE_PASSWORD);
  }

  public static void resetSystemsSSLTrustStoreConfig() {
    System.out.println("### Clearing System property " + JVM_SSL_TRUST_STORE_CONFIG);
    System.clearProperty(JVM_SSL_TRUST_STORE_CONFIG);
  }

  public static void registerIntegrationTestRelyingParty() throws Connect2IdException {
    final OpenIdClientMetadata clientMetadata = new OpenIdClientMetadata(singletonList(REDIRECT_URI),
            INTEGRATION_TEST_CLIENT_ID, INTEGRATION_TEST_CLIENT_NAME, INTEGRATION_TEST_CLIENT_SECRET);
    System.out.println("### Registering Relying Party " + clientMetadata);
    OPEN_ID_CONNECT_CLIENT_REGISTRATION.registerClient(clientMetadata);
  }

  /**
   * Tries to delete Relying Party, logs Exception if this fails.
   */
  public static void safelyUnregisterIntegrationTestRelyingParty() {
    System.out.println("### De-registering Relying Party '" + INTEGRATION_TEST_CLIENT_ID + "'");
    try {
      OPEN_ID_CONNECT_CLIENT_REGISTRATION.deleteClientWith(INTEGRATION_TEST_CLIENT_ID);
    } catch (Connect2IdException e) {
      System.out.println("Deleting client '" + INTEGRATION_TEST_CLIENT_ID + "' failed: " + e);
    }
  }

  public static AccessToken getAuthSessionApiAccessToken() {
    synchronized (Connect2IdIntegrationInstanceParameters.class) {
      if (authSessionApiAccessToken == null) {
        authSessionApiAccessToken = new AccessToken(requiredSystemProperty("C2ID_AUTH_SESSION_API_TOKEN"));
      }
    }
    return authSessionApiAccessToken;
  }

  public static AccessToken getClientRegistrationApiAccessToken() {
    synchronized (Connect2IdIntegrationInstanceParameters.class) {
      if (clientRegistrationApiAccessToken == null) {
        clientRegistrationApiAccessToken = new AccessToken(requiredSystemProperty
                ("C2ID_CLIENT_REGISTRATION_API_TOKEN"));
      }
    }
    return clientRegistrationApiAccessToken;
  }

  public static AccessToken getSessionStoreApiAccessToken() {
    synchronized (Connect2IdIntegrationInstanceParameters.class) {
      if (sessionStoreApiAccessToken == null) {
        sessionStoreApiAccessToken = new AccessToken(requiredSystemProperty
                ("C2ID_SESSION_STORE_API_TOKEN"));
      }
    }
    return sessionStoreApiAccessToken;
  }

  private static String requiredSystemProperty(final String propertyName) {
    Validate.notEmpty(propertyName, "Missing name of System Property");
    final String propertyValue = System.getProperty(propertyName);
    Validate.notEmpty(propertyValue, "Missing value of System Property " + propertyName);
    return propertyValue.replaceAll("\"", "");
  }

}
