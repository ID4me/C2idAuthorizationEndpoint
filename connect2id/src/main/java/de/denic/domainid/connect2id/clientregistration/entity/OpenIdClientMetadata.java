/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.clientregistration.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.denic.domainid.connect2id.authzsession.ResponseType;
import de.denic.domainid.connect2id.common.ClientId;
import org.apache.commons.lang3.Validate;

import javax.annotation.concurrent.Immutable;
import java.net.URI;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;
import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;
import static de.denic.domainid.connect2id.clientregistration.entity.ClientAuthenticationMethod.client_secret_basic;
import static de.denic.domainid.connect2id.clientregistration.entity.GrantType.authorization_code;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;

/**
 * Representation of <a href="https://openid.net/specs/openid-connect-registration-1_0.html#ClientMetadata"></a>Client Metadata from OpenID specification</a>
 */
@JsonInclude(NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Immutable
public class OpenIdClientMetadata {

  // Required:
  private final List<URI> redirectURIs = new LinkedList<>();
  // Optional:
  private final List<ResponseType> responseTypes;
  private final List<GrantType> grantTypes;
  private final String applicationType;
  private final List<String> contacts;
  private final String clientName;
  private final URL logoURI, clientURI, policyURI, termsOfServiceURI, jwksURI, sectorIdentifierURI;
  //private final JWKs jwks;
  private final SubjectType subjectType;
  private final String idTokenSignedResponseAlg, idTokenEncryptedResponseAlg, idTokenEncryptedResponseEnc,
          userinfoSignedResponseAlg, userinfoEncryptedResponseAlg, userinfoEncryptedResponseEnc,
          requestObjectSigningAlg, requestObjectEncryptionAlg, requestObjectEncryptionEnc,
          tokenEndpointAuthSigningAlg;
  private final ClientAuthenticationMethod tokenEndpointAuthMethod;
  private final Integer defaultMaxAge;
  private final boolean requireAuthTime;
  private final List<String> defaultAcrValues;
  private final URI initiateLoginURI;
  private final List<URI> requestURIs;
  // Additional optional C2ID attributes:
  private final String preferredClientSecret;
  private final ClientId preferredClientId;

  /**
   * Default values are applied.
   *
   * @param redirectURIs          Required to be not empty.
   * @param clientName                  Required
   * @param preferredClientId     Optional
   * @param preferredClientSecret Optional
   */
  public OpenIdClientMetadata(final List<URI> redirectURIs, final ClientId preferredClientId, final String clientName,
                              final String preferredClientSecret) {
    Validate.notEmpty(redirectURIs, "Missing redirect URIs");
    Validate.noNullElements(redirectURIs, "NULL values are prohibited");
    Validate.notEmpty(clientName, "Missing name");
    this.redirectURIs.addAll(redirectURIs);
    this.responseTypes = singletonList(new ResponseType("code"));
    this.grantTypes = singletonList(authorization_code);
    this.applicationType = "web";
    this.contacts = emptyList();
    this.clientName = clientName;
    this.logoURI = null;
    this.clientURI = null;
    this.policyURI = null;
    this.termsOfServiceURI = null;
    this.jwksURI = null;
    this.sectorIdentifierURI = null;
    this.subjectType = null;
    this.idTokenSignedResponseAlg = null;
    this.idTokenEncryptedResponseAlg = null;
    this.idTokenEncryptedResponseEnc = null;
    this.userinfoSignedResponseAlg = null;
    this.requestObjectEncryptionAlg = null;
    this.userinfoEncryptedResponseEnc = null;
    this.requestObjectEncryptionEnc = null;
    this.requestObjectSigningAlg = null;
    this.userinfoEncryptedResponseAlg = null;
    this.tokenEndpointAuthMethod = client_secret_basic;
    this.tokenEndpointAuthSigningAlg = null;
    this.defaultMaxAge = null;
    this.requireAuthTime = false;
    this.defaultAcrValues = emptyList();
    this.initiateLoginURI = null;
    this.requestURIs = emptyList();
    this.preferredClientId = preferredClientId;
    this.preferredClientSecret = preferredClientSecret;
  }

  /**
   * @return Never <code>null</code> nor empty.
   */
  @JsonProperty("redirect_uris")
  public List<URI> getRedirectURIs() {
    return redirectURIs;
  }

  /**
   * @return Never <code>null</code>.
   */
  @JsonInclude(NON_EMPTY)
  @JsonProperty("response_types")
  public List<ResponseType> getResponseTypes() {
    return responseTypes;
  }

  /**
   * @return Never <code>null</code>.
   */
  @JsonInclude(NON_EMPTY)
  @JsonProperty("grant_types")
  public List<GrantType> getGrantTypes() {
    return grantTypes;
  }

  /**
   * @return Optional.
   */
  @JsonProperty("application_type")
  public String getApplicationType() {
    return applicationType;
  }

  /**
   * @return Never <code>null</code>.
   */
  @JsonInclude(NON_EMPTY)
  @JsonProperty("contacts")
  public List<String> getContacts() {
    return contacts;
  }

  /**
   * @return Optional.
   */
  @JsonProperty("client_name")
  public String getClientName() {
    return clientName;
  }

  /**
   * @return Optional.
   */
  @JsonProperty("logo_uri")
  public URL getLogoURI() {
    return logoURI;
  }

  /**
   * @return Optional.
   */
  @JsonProperty("client_uri")
  public URL getClientURI() {
    return clientURI;
  }

  /**
   * @return Optional.
   */
  @JsonProperty("policy_uri")
  public URL getPolicyURI() {
    return policyURI;
  }

  /**
   * @return Optional.
   */
  @JsonProperty("tos_uri")
  public URL getTermsOfServiceURI() {
    return termsOfServiceURI;
  }

  /**
   * @return Optional.
   */
  @JsonProperty("jwks_uri")
  public URL getJwksURI() {
    return jwksURI;
  }

  /**
   * @return Optional.
   */
  @JsonProperty("sector_identifier_uri")
  public URL getSectorIdentifierURI() {
    return sectorIdentifierURI;
  }

  /**
   * @return Optional.
   */
  @JsonProperty("subject_type")
  public SubjectType getSubjectType() {
    return subjectType;
  }

  /**
   * @return Optional.
   */
  @JsonProperty("id_token_signed_response_alg")
  public String getIdTokenSignedResponseAlg() {
    return idTokenSignedResponseAlg;
  }

  /**
   * @return Optional.
   */
  @JsonProperty("id_token_encrypted_response_alg")
  public String getIdTokenEncryptedResponseAlg() {
    return idTokenEncryptedResponseAlg;
  }

  /**
   * @return Optional.
   */
  @JsonProperty("id_token_encrypted_response_enc")
  public String getIdTokenEncryptedResponseEnc() {
    return idTokenEncryptedResponseEnc;
  }

  /**
   * @return Optional.
   */
  @JsonProperty("userinfo_signed_response_alg")
  public String getUserinfoSignedResponseAlg() {
    return userinfoSignedResponseAlg;
  }

  /**
   * @return Optional.
   */
  @JsonProperty("userinfo_encrypted_response_alg")
  public String getUserinfoEncryptedResponseAlg() {
    return userinfoEncryptedResponseAlg;
  }

  /**
   * @return Optional.
   */
  @JsonProperty("userinfo_encrypted_response_enc")
  public String getUserinfoEncryptedResponseEnc() {
    return userinfoEncryptedResponseEnc;
  }

  /**
   * @return Optional.
   */
  @JsonProperty("request_object_signing_alg")
  public String getRequestObjectSigningAlg() {
    return requestObjectSigningAlg;
  }

  /**
   * @return Optional.
   */
  @JsonProperty("request_object_encryption_alg")
  public String getRequestObjectEncryptionAlg() {
    return requestObjectEncryptionAlg;
  }

  /**
   * @return Optional.
   */
  @JsonProperty("request_object_encryption_enc")
  public String getRequestObjectEncryptionEnc() {
    return requestObjectEncryptionEnc;
  }

  /**
   * @return Optional.
   */
  @JsonProperty("token_endpoint_auth_signing_alg")
  public String getTokenEndpointAuthSigningAlg() {
    return tokenEndpointAuthSigningAlg;
  }

  /**
   * @return Optional.
   */
  @JsonProperty("token_endpoint_auth_method")
  public ClientAuthenticationMethod getTokenEndpointAuthMethod() {
    return tokenEndpointAuthMethod;
  }

  /**
   * @return Optional.
   */
  @JsonProperty("default_max_age")
  public Integer getDefaultMaxAge() {
    return defaultMaxAge;
  }

  @JsonProperty("require_auth_time")
  public boolean isRequireAuthTime() {
    return requireAuthTime;
  }

  /**
   * @return Never <code>null</code>.
   */
  @JsonInclude(NON_EMPTY)
  @JsonProperty("default_acr_values")
  public List<String> getDefaultAcrValues() {
    return defaultAcrValues;
  }

  /**
   * @return Optional.
   */
  @JsonProperty("initiate_login_uri")
  public URI getInitiateLoginURI() {
    return initiateLoginURI;
  }

  /**
   * @return Never <code>null</code>.
   */
  @JsonInclude(NON_EMPTY)
  @JsonProperty("request_uris")
  public List<URI> getRequestURIs() {
    return requestURIs;
  }

  /**
   * @return Optional.
   */
  @JsonProperty("preferred_client_id")
  public String getPreferredClientId() {
    return (preferredClientId == null ? null : preferredClientId.getPlain());
  }

  /**
   * @return Optional.
   */
  @JsonProperty("preferred_client_secret")
  public String getPreferredClientSecret() {
    return preferredClientSecret;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    OpenIdClientMetadata that = (OpenIdClientMetadata) o;
    return requireAuthTime == that.requireAuthTime &&
            Objects.equals(redirectURIs, that.redirectURIs) &&
            Objects.equals(responseTypes, that.responseTypes) &&
            Objects.equals(grantTypes, that.grantTypes) &&
            Objects.equals(applicationType, that.applicationType) &&
            Objects.equals(contacts, that.contacts) &&
            Objects.equals(clientName, that.clientName) &&
            Objects.equals(logoURI, that.logoURI) &&
            Objects.equals(clientURI, that.clientURI) &&
            Objects.equals(policyURI, that.policyURI) &&
            Objects.equals(termsOfServiceURI, that.termsOfServiceURI) &&
            Objects.equals(jwksURI, that.jwksURI) &&
            Objects.equals(sectorIdentifierURI, that.sectorIdentifierURI) &&
            subjectType == that.subjectType &&
            Objects.equals(idTokenSignedResponseAlg, that.idTokenSignedResponseAlg) &&
            Objects.equals(idTokenEncryptedResponseAlg, that.idTokenEncryptedResponseAlg) &&
            Objects.equals(idTokenEncryptedResponseEnc, that.idTokenEncryptedResponseEnc) &&
            Objects.equals(userinfoSignedResponseAlg, that.userinfoSignedResponseAlg) &&
            Objects.equals(userinfoEncryptedResponseAlg, that.userinfoEncryptedResponseAlg) &&
            Objects.equals(userinfoEncryptedResponseEnc, that.userinfoEncryptedResponseEnc) &&
            Objects.equals(requestObjectSigningAlg, that.requestObjectSigningAlg) &&
            Objects.equals(requestObjectEncryptionAlg, that.requestObjectEncryptionAlg) &&
            Objects.equals(requestObjectEncryptionEnc, that.requestObjectEncryptionEnc) &&
            Objects.equals(tokenEndpointAuthSigningAlg, that.tokenEndpointAuthSigningAlg) &&
            tokenEndpointAuthMethod == that.tokenEndpointAuthMethod &&
            Objects.equals(defaultMaxAge, that.defaultMaxAge) &&
            Objects.equals(defaultAcrValues, that.defaultAcrValues) &&
            Objects.equals(initiateLoginURI, that.initiateLoginURI) &&
            Objects.equals(requestURIs, that.requestURIs) &&
            Objects.equals(preferredClientId, that.preferredClientId) &&
            Objects.equals(preferredClientSecret, that.preferredClientSecret);
  }

  @Override
  public int hashCode() {
    return Objects.hash(redirectURIs, responseTypes, grantTypes, applicationType, contacts, clientName, logoURI, clientURI, policyURI, termsOfServiceURI, jwksURI, sectorIdentifierURI, subjectType, idTokenSignedResponseAlg, idTokenEncryptedResponseAlg, idTokenEncryptedResponseEnc, userinfoSignedResponseAlg, userinfoEncryptedResponseAlg, userinfoEncryptedResponseEnc, requestObjectSigningAlg, requestObjectEncryptionAlg, requestObjectEncryptionEnc, tokenEndpointAuthSigningAlg, tokenEndpointAuthMethod, defaultMaxAge, requireAuthTime, defaultAcrValues, initiateLoginURI, requestURIs, preferredClientId, preferredClientSecret);
  }

  @Override
  public String toString() {
    return "OpenIdClientMetadata{" +
            "redirectURIs=" + redirectURIs +
            ", responseTypes=" + responseTypes +
            ", grantTypes=" + grantTypes +
            ", applicationType='" + applicationType + '\'' +
            ", contacts=" + contacts +
            ", clientName='" + clientName + '\'' +
            ", logoURI=" + logoURI +
            ", clientURI=" + clientURI +
            ", policyURI=" + policyURI +
            ", termsOfServiceURI=" + termsOfServiceURI +
            ", jwksURI=" + jwksURI +
            ", sectorIdentifierURI=" + sectorIdentifierURI +
            ", subjectType=" + subjectType +
            ", idTokenSignedResponseAlg='" + idTokenSignedResponseAlg + '\'' +
            ", idTokenEncryptedResponseAlg='" + idTokenEncryptedResponseAlg + '\'' +
            ", idTokenEncryptedResponseEnc='" + idTokenEncryptedResponseEnc + '\'' +
            ", userinfoSignedResponseAlg='" + userinfoSignedResponseAlg + '\'' +
            ", userinfoEncryptedResponseAlg='" + userinfoEncryptedResponseAlg + '\'' +
            ", userinfoEncryptedResponseEnc='" + userinfoEncryptedResponseEnc + '\'' +
            ", requestObjectSigningAlg='" + requestObjectSigningAlg + '\'' +
            ", requestObjectEncryptionAlg='" + requestObjectEncryptionAlg + '\'' +
            ", requestObjectEncryptionEnc='" + requestObjectEncryptionEnc + '\'' +
            ", tokenEndpointAuthSigningAlg='" + tokenEndpointAuthSigningAlg + '\'' +
            ", tokenEndpointAuthMethod=" + tokenEndpointAuthMethod +
            ", defaultMaxAge=" + defaultMaxAge +
            ", requireAuthTime=" + requireAuthTime +
            ", defaultAcrValues=" + defaultAcrValues +
            ", initiateLoginURI=" + initiateLoginURI +
            ", requestURIs=" + requestURIs +
            ", preferredClientSecret=[SECRET]'" +
            ", preferredClientId=" + preferredClientId +
            '}';
  }

}
