/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import de.denic.domainid.connect2id.common.*;
import de.denic.domainid.connect2id.common.BadRequestException.BadRequestEntity;
import de.denic.domainid.connect2id.common.ForbiddenException.ForbiddenEntity;
import de.denic.domainid.connect2id.common.NotFoundException.NotFoundEntity;
import de.denic.domainid.connect2id.common.UnauthorizedException.UnauthorizedEntity;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;

import javax.annotation.concurrent.Immutable;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URI;
import java.time.Duration;
import java.time.Instant;

import static javax.ws.rs.client.Entity.json;
import static org.apache.commons.lang3.StringUtils.defaultIfEmpty;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.eclipse.jetty.http.HttpStatus.*;

@Immutable
public abstract class BaseConnect2IdClient {

  protected static final String AUTHORIZATION_HEADER = "Authorization";
  protected static final String EMPTY_BODY_CONTENT = "";
  protected static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
  // Readers:
  protected static final ObjectReader NOT_FOUND_ENTITY_READER = OBJECT_MAPPER.readerFor(NotFoundEntity.class);
  protected static final ObjectReader UNAUTHORIZED_ENTITY_READER = OBJECT_MAPPER.readerFor(UnauthorizedEntity.class);
  protected static final ObjectReader FORBIDDEN_ENTITY_READER = OBJECT_MAPPER.readerFor(ForbiddenEntity.class);
  protected static final ObjectReader BAD_REQUEST_ENTITY_READER = OBJECT_MAPPER.readerFor(BadRequestEntity.class);

  private final Logger logger;

  /**
   * @param logger Required
   */
  protected BaseConnect2IdClient(final Logger logger) {
    Validate.notNull(logger, "Missing logger");
    this.logger = logger;
  }

  protected static NotFoundException notFoundException(final WebTarget target, final String responseBody)
          throws IOException {
    final NotFoundEntity notFoundEntity = NOT_FOUND_ENTITY_READER.readValue(responseBody);
    return new NotFoundException(notFoundEntity, target.getUri());
  }

  protected static UnauthorizedException unauthorizedException(final WebTarget target, final String responseBody)
          throws IOException {
    final UnauthorizedEntity unauthorizedEntity = (isEmpty(responseBody) ? new UnauthorizedEntity(null, null)
            : UNAUTHORIZED_ENTITY_READER.readValue(responseBody));
    return new UnauthorizedException(unauthorizedEntity, target.getUri());
  }

  protected static ForbiddenException forbiddenException(final WebTarget target, final String requestBody,
                                                         final String responseBody) throws IOException {
    final ForbiddenEntity forbiddenEntity = FORBIDDEN_ENTITY_READER.readValue(responseBody);
    return new ForbiddenException(forbiddenEntity, target.getUri(), requestBody);
  }

  protected static ServerException serverException(final WebTarget target, final String responseBody) {
    return new ServerException(target.getUri(), responseBody);
  }

  protected static BadRequestException badRequestException(final WebTarget target, final String requestBody,
                                                           final String responseBody) throws IOException {
    final BadRequestEntity badRequestEntity = BAD_REQUEST_ENTITY_READER.readValue(responseBody);
    return new BadRequestException(badRequestEntity, target.getUri(), requestBody);
  }

  protected static NotImplementedException notImplementedException(final WebTarget target, final Response response) {
    return new NotImplementedException(
            "Response status: " + response.getStatus() + " (target: " + target + "), response: " + response);
  }

  /**
   * @param invocationResult Required
   * @param parsingFunction  Required
   * @param target           Required, for {@link Connect2IdException}-throwing purpose.
   * @param requestBody      Optional, for {@link Connect2IdException}-throwing purpose.
   */
  protected static <RESULT> RESULT parseInvocationResult(final Pair<Response, String> invocationResult,
                                                         final FunctionOptionallyThrowingIOException<String, RESULT> parsingFunction, final WebTarget target,
                                                         final String requestBody) {
    Validate.notNull(invocationResult, "Missing invocation result");
    Validate.notNull(parsingFunction, "Missing function to apply");
    final String responseBody = invocationResult.getRight();
    final Response response = invocationResult.getLeft();
    try {
      switch (response.getStatus()) {
        case OK_200:
          return parsingFunction.apply(responseBody);
        case BAD_REQUEST_400:
          throw badRequestException(target, requestBody, responseBody);
        case UNAUTHORIZED_401:
          throw unauthorizedException(target, responseBody);
        case FORBIDDEN_403:
          throw forbiddenException(target, requestBody, responseBody);
        case NOT_FOUND_404:
          throw notFoundException(target, responseBody);
        case INTERNAL_SERVER_ERROR_500:
          throw serverException(target, responseBody);
        default:
          throw notImplementedException(target, response);
      }
    } catch (final IOException e) {
      throw new RuntimeException("Internal error unmarshalling " + responseBody, e);
    } finally {
      response.close();
    }
  }

  /**
   * @param invocation                        Required
   * @param targetUriForLoggingOnly           Required, for logging purposes only
   * @param httpMethodForLoggingOnly          Required, for logging purposes only
   * @param optionalRequestBodyForLoggingOnly Optional, for logging purposes only
   * @return Never <code>null</code>, contains {@link Response} instance as well as response's body as {@link String}.
   */
  protected final Pair<Response, String> invoke(final Invocation invocation, final URI targetUriForLoggingOnly,
                                                final String httpMethodForLoggingOnly, final String optionalRequestBodyForLoggingOnly) {
    Validate.notNull(invocation, "Missing invocation");
    Validate.notNull(httpMethodForLoggingOnly, "Missing HTTP method");
    Validate.notNull(targetUriForLoggingOnly, "Missing target URI");
    final String requestBody = defaultIfEmpty(optionalRequestBodyForLoggingOnly, "");
    logger.debug("==> {}: >{}< ({})", httpMethodForLoggingOnly, requestBody, targetUriForLoggingOnly);
    final Instant startingAt = Instant.now();
    final Response invocationResponse = invocation.invoke();
    // {
    // final SSLSessionContext serverSessionContext = restApiClient.getSslContext().getServerSessionContext();
    // final Enumeration<byte[]> sslSessionIDs = serverSessionContext.getIds();
    // System.out.println("===> Session IDs: " + sslSessionIDs);
    // try {
    // while (sslSessionIDs.hasMoreElements()) {
    // final byte[] sslSessionID = sslSessionIDs.nextElement();
    // System.out.println("===> Session ID: " + sslSessionID);
    // final SSLSession sslSession = serverSessionContext.getSession(sslSessionID);
    // X509Certificate[] peerCertificateChain;
    // peerCertificateChain = sslSession.getPeerCertificateChain();
    // for (int i = 0; i < peerCertificateChain.length; i++) {
    // final X509Certificate x509Certificate = peerCertificateChain[i];
    // System.out.println("===> Checking validity of: " + x509Certificate);
    // x509Certificate.checkValidity();
    // }
    // }
    // } catch (final SSLPeerUnverifiedException | CertificateExpiredException | CertificateNotYetValidException e) {
    // e.printStackTrace();
    // }
    // }
    final Duration invocationDuration = Duration.between(startingAt, Instant.now());
    final String responseEntity = invocationResponse.readEntity(String.class);
    logger.debug("<== {} {}: >{}< ({}, elapsed {})", invocationResponse.getStatus(), invocationResponse.getStatusInfo(),
            responseEntity, targetUriForLoggingOnly, invocationDuration);
    return new ImmutablePair<>(invocationResponse, responseEntity);
  }

  /**
   * @param invocation              Required
   * @param targetUriForLoggingOnly Required, for logging purposes only
   * @return Never <code>null</code>, contains {@link Response} instance as well as response's body as {@link String}.
   */
  protected final Pair<Response, String> invokeGET(final Invocation invocation, final URI targetUriForLoggingOnly) {
    return invoke(invocation, targetUriForLoggingOnly, "GET", EMPTY_BODY_CONTENT);
  }

  /**
   * @param builder                 Required
   * @param jsonRequestBody         Required to be not empty.
   * @param targetUriForLoggingOnly Required, for logging purposes only
   * @return Never <code>null</code>, contains {@link Response} instance as well as response's body as {@link String}.
   */
  protected final Pair<Response, String> invokePUTWithJSON(final Builder builder, final String jsonRequestBody,
                                                           final URI targetUriForLoggingOnly) {
    Validate.notNull(builder, "Missing invocation builder");
    Validate.notEmpty(jsonRequestBody, "Missing JSON request body");
    return invoke(builder.buildPut(json(jsonRequestBody)), targetUriForLoggingOnly, "PUT", jsonRequestBody);
  }

  /**
   * @param builder                 Required
   * @param jsonRequestBody         Required to be not empty.
   * @param targetUriForLoggingOnly Required, for logging purposes only
   * @return Never <code>null</code>, contains {@link Response} instance as well as response's body as {@link String}.
   */
  protected final Pair<Response, String> invokePOSTWithJSON(final Builder builder, final String jsonRequestBody,
                                                            final URI targetUriForLoggingOnly) {
    Validate.notNull(builder, "Missing invocation builder");
    Validate.notEmpty(jsonRequestBody, "Missing JSON request body");
    return invoke(builder.buildPost(json(jsonRequestBody)), targetUriForLoggingOnly, "POST", jsonRequestBody);
  }

}
