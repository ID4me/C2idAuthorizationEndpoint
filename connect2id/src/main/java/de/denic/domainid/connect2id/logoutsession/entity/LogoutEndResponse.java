/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.logoutsession.entity;

import java.net.URI;

import javax.annotation.concurrent.Immutable;

import org.apache.commons.lang3.Validate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@Immutable
public final class LogoutEndResponse {

  public static final String EXPECTED_TYPE_VALUE = "end";

  private final URI postLogoutRedirectionURI;

  /**
   * @param type
   *          Required to be equals to {@link #EXPECTED_TYPE_VALUE}
   * @param postLogoutRedirectionURI
   *          Optional
   */
  public LogoutEndResponse(@JsonProperty("type") final String type,
      @JsonProperty("post_logout_redirect_uri") final URI postLogoutRedirectionURI) {
    Validate.isTrue(EXPECTED_TYPE_VALUE.equals(type), "Data does NOT describe a Logout End response! Type was not '"
        + EXPECTED_TYPE_VALUE + "' as expected, but '%s'", type);
    this.postLogoutRedirectionURI = postLogoutRedirectionURI;
  }

  /**
   * @return Optional
   */
  public URI getPostLogoutRedirectionURI() {
    return postLogoutRedirectionURI;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((postLogoutRedirectionURI == null) ? 0 : postLogoutRedirectionURI.hashCode());
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final LogoutEndResponse other = (LogoutEndResponse) obj;
    if (postLogoutRedirectionURI == null) {
      if (other.postLogoutRedirectionURI != null) {
        return false;
      }
    } else if (!postLogoutRedirectionURI.equals(other.postLogoutRedirectionURI)) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "postLogoutRedirectionURI=" + postLogoutRedirectionURI;
  }

}
