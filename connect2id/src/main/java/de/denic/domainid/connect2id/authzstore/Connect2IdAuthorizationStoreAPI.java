/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.authzstore;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.node.ObjectNode;
import de.denic.domainid.connect2id.BaseConnect2IdClient;
import de.denic.domainid.connect2id.authzstore.entity.OpenIdConnectAuthorization;
import de.denic.domainid.connect2id.common.AccessToken;
import de.denic.domainid.connect2id.common.ClientId;
import de.denic.domainid.connect2id.common.Connect2IdException;
import de.denic.domainid.connect2id.subjectsessionstore.entity.SubjectSessionOptionalData;
import de.denic.domainid.subject.OpenIdSubject;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.concurrent.Immutable;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;
import java.util.Optional;

import static java.util.Optional.empty;
import static java.util.Optional.of;
import static javax.ws.rs.client.Entity.form;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
import static javax.ws.rs.core.MediaType.APPLICATION_FORM_URLENCODED;
import static org.eclipse.jetty.http.HttpStatus.*;

/**
 * Client to access <a href="https://connect2id.com/products/server/docs/integration/authz-store">Connect2Id's
 * Authorisation Store API</a>.
 */
public interface Connect2IdAuthorizationStoreAPI {

  /**
   * @param subject Required
   */
  void revokeAllAuthorizationsOf(OpenIdSubject subject) throws Connect2IdException;

  /**
   * @param subject Required
   * @return Never <code>null</code>
   */
  Map<ClientId, OpenIdConnectAuthorization> loadAllAuthorizationsOf(OpenIdSubject subject) throws Connect2IdException;

  /**
   * @param subject  Required
   * @param clientId Required
   * @return Never <code>null</code>
   */
  Optional<OpenIdConnectAuthorization> loadAuthorizationOf(OpenIdSubject subject, ClientId clientId) throws Connect2IdException;

  /**
   * @param authorisationToChange Required
   */
  void changeAuthorization(OpenIdConnectAuthorization authorisationToChange) throws Connect2IdException;

  /**
   * @param subject      Required
   * @param clientId     Required
   * @param optionalData Optional
   */
  void changeOptionalData(OpenIdSubject subject, ClientId clientId, SubjectSessionOptionalData optionalData) throws Connect2IdException;

  /**
   * @param subject  Required
   * @param clientId Required
   * @return Never <code>null</code>: Revoked Authorisation
   */
  Optional<OpenIdConnectAuthorization> revokeAuthorizationOf(OpenIdSubject subject, ClientId clientId) throws Connect2IdException;

  @Immutable
  final class Impl extends BaseConnect2IdClient implements Connect2IdAuthorizationStoreAPI {

    private static final Logger LOG = LoggerFactory.getLogger(Impl.class);
    // Readers:
    private static final ObjectReader OPEN_ID_CONNECT_AUTHORISATION_RESPONSE_ENTITY_READER = OBJECT_MAPPER.readerFor(OpenIdConnectAuthorization.class);
    // Writers:
    private static final ObjectWriter OPEN_ID_CONNECT_AUTHORISATION_RESPONSE_ENTITY_WRITER = OBJECT_MAPPER.writerFor(OpenIdConnectAuthorization.class);

    private final Client restClient;
    private final URI connect2IdAuthorisationStoreURL;
    private final AccessToken bearerAccessToken;

    /**
     * @param connect2IdAuthorisationStoreURL Required
     * @param bearerAccessToken               Required
     * @param restClient                      Required
     */
    public Impl(final URL connect2IdAuthorisationStoreURL, final AccessToken bearerAccessToken, final Client restClient) {
      super(LOG);
      Validate.notNull(restClient, "Missing REST client");
      Validate.notNull(connect2IdAuthorisationStoreURL, "Missing C2ID Authz Store API URL");
      Validate.notNull(bearerAccessToken, "Missing bearer access token");
      this.restClient = restClient;
      this.bearerAccessToken = bearerAccessToken;
      try {
        this.connect2IdAuthorisationStoreURL = connect2IdAuthorisationStoreURL.toURI();
      } catch (final URISyntaxException e) {
        throw new IllegalArgumentException("C2ID Authz Store API URL " + connect2IdAuthorisationStoreURL, e);
      }
    }

    @Override
    public void revokeAllAuthorizationsOf(final OpenIdSubject subject) throws Connect2IdException {
      Validate.notNull(subject, "Missing subject");
      final WebTarget target = restClient.target(connect2IdAuthorisationStoreURL).path("/revocation");
      final Invocation invocation = target.request(APPLICATION_JSON_TYPE).header(AUTHORIZATION_HEADER, bearerAccessToken.withPrefix("Bearer"))
          .buildPost(form(new Form().param("subject", subject.getName())));
      LOG.info("Revoking all authorizations on C2ID Authz Store for subject '{}'", subject);
      final Pair<Response, String> invocationResult = invoke(invocation, target.getUri(), "POST", EMPTY_BODY_CONTENT);
      final String responseBody = invocationResult.getRight();
      final Response response = invocationResult.getLeft();
      try {
        switch (response.getStatus()) {
          case OK_200:
            LOG.info("Revoked all authorization sessions with subject '{}'", subject);
            break;
          case UNAUTHORIZED_401:
            throw unauthorizedException(target, responseBody);
          case FORBIDDEN_403:
            throw forbiddenException(target, EMPTY_BODY_CONTENT, responseBody);
          case NOT_FOUND_404:
            throw notFoundException(target, responseBody);
          case INTERNAL_SERVER_ERROR_500:
            throw serverException(target, responseBody);
          default:
            throw new NotImplementedException("Status: " + response.getStatus() + ", response: " + response);
        }
      } catch (final IOException e) {
        throw new RuntimeException("Internal error unmarshalling " + responseBody, e);
      } finally {
        response.close();
      }
    }

    @Override
    public Map<ClientId, OpenIdConnectAuthorization> loadAllAuthorizationsOf(final OpenIdSubject subject) throws Connect2IdException {
      Validate.notNull(subject, "Missing subject");
      final WebTarget target = restClient.target(connect2IdAuthorisationStoreURL).path("/authorizations").queryParam("subject", subject.getName());
      final Invocation invocation = target.request(APPLICATION_JSON_TYPE).header(AUTHORIZATION_HEADER, bearerAccessToken.withPrefix("Bearer")).buildGet();
      LOG.info("Loading all authorizations on C2ID Authz Store for subject '{}'", subject);
      final Pair<Response, String> invocationResult = invokeGET(invocation, target.getUri());
      final String responseBody = invocationResult.getRight();
      final Response response = invocationResult.getLeft();
      try {
        switch (response.getStatus()) {
          case OK_200:
            final Map<ClientId, OpenIdConnectAuthorization> readValue = OBJECT_MAPPER.readValue(responseBody,
                new TypeReference<Map<ClientId, OpenIdConnectAuthorization>>() {
                  // Intentionally left empty
                });
            LOG.info("Loaded all authorization sessions of subject '{}': {}", subject, readValue);
            return readValue;
          case UNAUTHORIZED_401:
            throw unauthorizedException(target, responseBody);
          case FORBIDDEN_403:
            throw forbiddenException(target, EMPTY_BODY_CONTENT, responseBody);
          case INTERNAL_SERVER_ERROR_500:
            throw serverException(target, responseBody);
          default:
            throw new NotImplementedException("Status: " + response.getStatus() + ", response: " + response);
        }
      } catch (final IOException e) {
        throw new RuntimeException("Internal error unmarshalling " + responseBody, e);
      } finally {
        response.close();
      }
    }

    @Override
    public Optional<OpenIdConnectAuthorization> loadAuthorizationOf(final OpenIdSubject subject, final ClientId clientId) throws Connect2IdException {
      Validate.notNull(subject, "Missing subject");
      Validate.notNull(clientId, "Missing client ID");
      final WebTarget target = restClient.target(connect2IdAuthorisationStoreURL).path("/authorizations").queryParam("subject", subject.getName()).queryParam("client_id",
          clientId.getEncoded());
      final Invocation invocation = target.request(APPLICATION_JSON_TYPE).header(AUTHORIZATION_HEADER, bearerAccessToken.withPrefix("Bearer")).buildGet();
      LOG.info("Loading authorization on C2ID Authz Store for: Subject '{}', client ID '{}'", subject, clientId);
      final Pair<Response, String> invocationResult = invokeGET(invocation, target.getUri());
      final String responseBody = invocationResult.getRight();
      final Response response = invocationResult.getLeft();
      try {
        switch (response.getStatus()) {
          case OK_200:
            final OpenIdConnectAuthorization readValue = OPEN_ID_CONNECT_AUTHORISATION_RESPONSE_ENTITY_READER.readValue(responseBody);
            LOG.info("Loaded authorization session: {}", readValue);
            return of(readValue);
          case UNAUTHORIZED_401:
            throw unauthorizedException(target, responseBody);
          case FORBIDDEN_403:
            throw forbiddenException(target, EMPTY_BODY_CONTENT, responseBody);
          case NOT_FOUND_404:
            LOG.info("NO authorization session with subject '{}' and client ID '{}'", subject, clientId);
            return empty();
          case INTERNAL_SERVER_ERROR_500:
            throw serverException(target, responseBody);
          default:
            throw new NotImplementedException("Status: " + response.getStatus() + ", response: " + response);
        }
      } catch (final IOException e) {
        throw new RuntimeException("Internal error unmarshalling " + responseBody, e);
      } finally {
        response.close();
      }
    }

    @Override
    public void changeAuthorization(final OpenIdConnectAuthorization authorisationToChange) throws Connect2IdException {
      Validate.notNull(authorisationToChange, "Missing entity to change");
      final String jsonRequestBody;
      try {
        jsonRequestBody = OPEN_ID_CONNECT_AUTHORISATION_RESPONSE_ENTITY_WRITER.writeValueAsString(authorisationToChange);
      } catch (final IOException e) {
        throw new RuntimeException("Internal error marshalling " + authorisationToChange, e);
      }

      LOG.info("Modifying authorization on C2ID Authz Store: {}", authorisationToChange);
      putAuthorizationPayload(jsonRequestBody);
    }

    private void putAuthorizationPayload(final String jsonRequestBody) {
      final WebTarget target = restClient.target(connect2IdAuthorisationStoreURL).path("/authorizations");
      final Builder builder = target.request(APPLICATION_JSON_TYPE).header(AUTHORIZATION_HEADER, bearerAccessToken.withPrefix("Bearer"));
      final Pair<Response, String> invocationResult = invokePUTWithJSON(builder, jsonRequestBody, target.getUri());
      final String responseBody = invocationResult.getRight();
      final Response response = invocationResult.getLeft();
      try {
        switch (response.getStatus()) {
          case NO_CONTENT_204:
            LOG.info("Modified authorization");
            return;
          case UNAUTHORIZED_401:
            throw unauthorizedException(target, responseBody);
          case FORBIDDEN_403:
            throw forbiddenException(target, EMPTY_BODY_CONTENT, responseBody);
          case NOT_FOUND_404:
            throw notFoundException(target, responseBody);
          case INTERNAL_SERVER_ERROR_500:
            throw serverException(target, responseBody);
          default:
            throw new NotImplementedException("Status: " + response.getStatus() + ", response: " + response);
        }
      } catch (final IOException e) {
        throw new RuntimeException("Internal error unmarshalling " + responseBody, e);
      } finally {
        response.close();
      }
    }

    @Override
    public Optional<OpenIdConnectAuthorization> revokeAuthorizationOf(final OpenIdSubject subject, final ClientId clientId) throws Connect2IdException {
      Validate.notNull(subject, "Missing subject");
      Validate.notNull(clientId, "Missing client ID");
      final Form formToPost = new Form("subject", subject.getName()).param("client_id", clientId.getPlain());
      final WebTarget target = restClient.target(connect2IdAuthorisationStoreURL).path("/revocation");
      final Invocation invocation = target.request(APPLICATION_JSON_TYPE).header(AUTHORIZATION_HEADER, bearerAccessToken.withPrefix("Bearer"))
          .buildPost(form(formToPost));
      LOG.info("Revoking authorization on C2ID Authz Store: Subject '{}', client ID '{}'", subject, clientId);
      final Pair<Response, String> invocationResult = invoke(invocation, target.getUri(), "POST", formToPost.asMap().toString());
      final String responseBody = invocationResult.getRight();
      final Response response = invocationResult.getLeft();
      try {
        switch (response.getStatus()) {
          case OK_200:
            final OpenIdConnectAuthorization readValue = OPEN_ID_CONNECT_AUTHORISATION_RESPONSE_ENTITY_READER.readValue(responseBody);
            LOG.info("Revoked authorization: {}", readValue);
            return of(readValue);
          case UNAUTHORIZED_401:
            throw unauthorizedException(target, responseBody);
          case FORBIDDEN_403:
            throw forbiddenException(target, EMPTY_BODY_CONTENT, responseBody);
          case NOT_FOUND_404:
            return empty();
          case INTERNAL_SERVER_ERROR_500:
            throw serverException(target, responseBody);
          default:
            throw new NotImplementedException("Status: " + response.getStatus() + ", response: " + response);
        }
      } catch (final IOException e) {
        throw new RuntimeException("Internal error unmarshalling " + responseBody, e);
      } finally {
        response.close();
      }
    }

    @Override
    public void changeOptionalData(final OpenIdSubject subject, final ClientId clientId, final SubjectSessionOptionalData newOptionalData) throws Connect2IdException {
      Validate.notNull(subject, "Missing subject");
      Validate.notNull(clientId, "Missing client ID");
      final WebTarget target = restClient.target(connect2IdAuthorisationStoreURL).path("/authorizations").queryParam("subject", subject.getName()).queryParam("client_id",
          clientId.getEncoded());
      final Invocation invocation = target.request(APPLICATION_JSON_TYPE).header(AUTHORIZATION_HEADER, bearerAccessToken.withPrefix("Bearer")).buildGet();
      LOG.debug("Loading authorization on C2ID Authz Store for: Subject '{}', client ID '{}'", subject, clientId);
      final Pair<Response, String> invocationResult = invokeGET(invocation, target.getUri());
      final String responseBody = invocationResult.getRight();
      final Response response = invocationResult.getLeft();
      JsonNode currentAuthzSessionJsonNode = null;
      try {
        switch (response.getStatus()) {
          case OK_200:
            currentAuthzSessionJsonNode = OPEN_ID_CONNECT_AUTHORISATION_RESPONSE_ENTITY_READER.readTree(responseBody);
            LOG.debug("Loaded authorization session: {}", currentAuthzSessionJsonNode);
            break;
          case UNAUTHORIZED_401:
            throw unauthorizedException(target, responseBody);
          case FORBIDDEN_403:
            throw forbiddenException(target, EMPTY_BODY_CONTENT, responseBody);
          case NOT_FOUND_404:
            throw notFoundException(target, responseBody);
          case INTERNAL_SERVER_ERROR_500:
            throw serverException(target, responseBody);
          default:
            throw new NotImplementedException("Status: " + response.getStatus() + ", response: " + response);
        }
      } catch (final IOException e) {
        throw new RuntimeException("Internal error unmarshalling " + responseBody, e);
      } finally {
        response.close();
      }

      final JsonNode modifiedAuthzSessionJsonNode = ((ObjectNode) currentAuthzSessionJsonNode).set("dat", OBJECT_MAPPER.valueToTree(newOptionalData));
      LOG.debug("Modifying optional data of authorization on C2ID Authz Store for subject '{}', client ID '{}': {}", subject, clientId, modifiedAuthzSessionJsonNode);
      putAuthorizationPayload(modifiedAuthzSessionJsonNode.toString());
    }

  }

}
