/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.subjectsessionstore.entity;

import com.fasterxml.jackson.annotation.*;
import de.denic.domainid.connect2id.common.AuthenticationMethodReference;
import de.denic.domainid.connect2id.common.SubjectSessionId;
import de.denic.domainid.subject.PseudonymSubject;
import org.apache.commons.lang3.Validate;

import javax.annotation.concurrent.Immutable;
import javax.validation.constraints.NotNull;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.*;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;
import static java.lang.Integer.MAX_VALUE;
import static java.time.Duration.ofMinutes;
import static java.time.LocalDateTime.ofEpochSecond;
import static java.time.ZoneOffset.UTC;
import static java.time.ZonedDateTime.of;
import static java.util.Arrays.asList;
import static java.util.Collections.*;

@JsonInclude(NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Immutable
public final class SubjectSession implements Comparable<SubjectSession> {

  private static final int NO_NANO_SECONDS = 0;

  private final PseudonymSubject subject;
  private final SubjectSessionId sessionId;
  private final Duration authenticationLifetime;
  private final ZonedDateTime creationTimestamp;
  private final Duration maxLifetime;
  private final ZonedDateTime authenticationTimestamp;
  private final Duration maxIdle;
  private final Set<AuthenticationMethodReference> authenticationMethodReferences;
  private final SubjectSessionOptionalData additionalData;
  private final Map<String, String> claims;

  /**
   * Numerical values of durations represent <code>minutes</code> according to
   * <a href="https://connect2id.com/products/server/docs/integration/session-store#subject-session">Documentation of
   * ConnectId server</a>!
   *
   * @param subject                        Required
   * @param sessionId                      Optional
   * @param authenticationTimestampEpoch   Required
   * @param creationTimestampEpoch         Required
   * @param maxLifetimeMins                Required
   * @param authenticationLifetimeMins     Required
   * @param maxIdleMins                    Required
   * @param authenticationMethodReferences Optional
   * @param additionalData                 Optional
   */
  public SubjectSession(@NotNull @JsonProperty("sub") final PseudonymSubject subject,
                        @JsonProperty("sid") final SubjectSessionId sessionId,
                        @NotNull @JsonProperty("auth_time") final long authenticationTimestampEpoch,
                        @NotNull @JsonProperty("creation_time") final long creationTimestampEpoch,
                        @NotNull @JsonProperty("max_life") final int maxLifetimeMins,
                        @NotNull @JsonProperty("auth_life") final int authenticationLifetimeMins,
                        @NotNull @JsonProperty("max_idle") final int maxIdleMins,
                        @JsonProperty("amr") final Collection<AuthenticationMethodReference> authenticationMethodReferences,
                        @JsonProperty("claims") final Map<String, String> claims,
                        @JsonProperty("data") final SubjectSessionOptionalData additionalData) {
    this(subject, sessionId, of(ofEpochSecond(authenticationTimestampEpoch, NO_NANO_SECONDS, UTC), UTC),
            of(ofEpochSecond(creationTimestampEpoch, NO_NANO_SECONDS, UTC), UTC),
            ofMinutes(maxLifetimeMins < 0 ? MAX_VALUE : maxLifetimeMins),
            ofMinutes(authenticationLifetimeMins < 0 ? MAX_VALUE : authenticationLifetimeMins),
            ofMinutes(maxIdleMins < 0 ? MAX_VALUE : maxIdleMins), authenticationMethodReferences, claims,
            additionalData);
  }

  /**
   * @see #SubjectSession(PseudonymSubject, SubjectSessionId, long, long, int, int, int, Collection, Map, SubjectSessionOptionalData)
   */
  public SubjectSession(final PseudonymSubject subject, final SubjectSessionId sessionId,
                        final ZonedDateTime authenticationTimestamp, final ZonedDateTime creationTimestamp, final Duration maxLifetime,
                        final Duration authenticationLifetime, final Duration maxIdle,
                        final Collection<AuthenticationMethodReference> authenticationMethodReferences,
                        final Map<String, String> claims,
                        final SubjectSessionOptionalData additionalData) {
    Validate.notNull(subject, "Missing subject");
    Validate.notNull(authenticationLifetime, "Missing authentication lifetime");
    Validate.notNull(creationTimestamp, "Missing creation timestamp");
    Validate.notNull(maxLifetime, "Missing max lifetime");
    Validate.notNull(authenticationLifetime, "Missing authentication lifetime");
    Validate.notNull(maxIdle, "Missing max idle");
    this.subject = subject;
    this.sessionId = sessionId;
    this.authenticationLifetime = authenticationLifetime;
    this.creationTimestamp = creationTimestamp;
    this.maxLifetime = maxLifetime;
    this.authenticationTimestamp = authenticationTimestamp;
    this.maxIdle = maxIdle;
    this.authenticationMethodReferences = (authenticationMethodReferences == null ? emptySet()
            : unmodifiableSet(new HashSet<>(authenticationMethodReferences)));
    this.additionalData = (additionalData == null ? new SubjectSessionOptionalData(null) : additionalData);
    this.claims = (claims == null ? emptyMap() : new HashMap<>(claims));
  }

  @Override
  public int compareTo(final SubjectSession other) {
    return this.subject.compareTo(other.subject);
  }

  /**
   * @return Never <code>null</code>
   */
  public PseudonymSubject getSubject() {
    return subject;
  }

  /**
   * @return Optional
   */
  public SubjectSessionId getId() {
    return sessionId;
  }

  /**
   * @return Never <code>null</code>
   */
  public Duration getAuthenticationLifetime() {
    return authenticationLifetime;
  }

  /**
   * @return Never <code>null</code>
   */
  public ZonedDateTime getCreationTimestamp() {
    return creationTimestamp;
  }

  /**
   * @return Never <code>null</code>
   */
  public Duration getMaxLifetime() {
    return maxLifetime;
  }

  /**
   * @return Never <code>null</code>
   */
  public ZonedDateTime getAuthenticationTimestamp() {
    return authenticationTimestamp;
  }

  /**
   * @return Never <code>null</code>
   */
  public Duration getMaxIdle() {
    return maxIdle;
  }

  /**
   * @return Never <code>null</code> but unmodifiable.
   */
  public Set<AuthenticationMethodReference> getAuthenticationMethodReferences() {
    return authenticationMethodReferences;
  }

  /**
   * @return Never <code>null</code>
   */
  public SubjectSessionOptionalData getOptionalData() {
    return additionalData;
  }

  /**
   * Minimum of {@link #getAuthenticationLifetime()}, {@link #getMaxLifetime()}, and {@link #getMaxIdle()}.
   *
   * @return Never <code>null</code>
   */
  public Duration getMinOfLifetimes() {
    final List<Duration> allLifetimes = asList(getAuthenticationLifetime(), getMaxLifetime(), getMaxIdle());
    sort(allLifetimes);
    return allLifetimes.get(0);
  }

  /**
   * @return Never <code>null</code> but unmodifiable.
   */
  public Map<String, String> getClaims() {
    return claims;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    SubjectSession that = (SubjectSession) o;
    return Objects.equals(subject, that.subject) &&
            Objects.equals(sessionId, that.sessionId) &&
            Objects.equals(authenticationLifetime, that.authenticationLifetime) &&
            Objects.equals(creationTimestamp, that.creationTimestamp) &&
            Objects.equals(maxLifetime, that.maxLifetime) &&
            Objects.equals(authenticationTimestamp, that.authenticationTimestamp) &&
            Objects.equals(maxIdle, that.maxIdle) &&
            Objects.equals(authenticationMethodReferences, that.authenticationMethodReferences) &&
            Objects.equals(additionalData, that.additionalData);
  }

  @Override
  public int hashCode() {
    return Objects.hash(subject, sessionId, authenticationLifetime, creationTimestamp, maxLifetime, authenticationTimestamp, maxIdle, authenticationMethodReferences, additionalData);
  }

  @Override
  public String toString() {
    return "ID=" + sessionId + ", subject=" + subject + ", authentTS=" + authenticationTimestamp + ", creationTS="
            + creationTimestamp + ", maxLifetime=" + maxLifetime + ", authentLifetime=" + authenticationLifetime
            + ", maxIdle=" + maxIdle + ", amr=" + authenticationMethodReferences + ", data=[" +
            additionalData + "]";
  }

}
