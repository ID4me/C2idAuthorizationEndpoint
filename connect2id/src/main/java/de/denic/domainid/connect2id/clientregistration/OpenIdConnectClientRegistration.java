/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.clientregistration;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import de.denic.domainid.connect2id.BaseConnect2IdClient;
import de.denic.domainid.connect2id.clientregistration.entity.OpenIdClientMetadata;
import de.denic.domainid.connect2id.clientregistration.entity.OpenIdConnectClient;
import de.denic.domainid.connect2id.common.AccessToken;
import de.denic.domainid.connect2id.common.ClientId;
import de.denic.domainid.connect2id.common.Connect2IdException;
import de.denic.domainid.connect2id.common.NotFoundException;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.concurrent.Immutable;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.Optional;

import static java.util.Optional.empty;
import static java.util.Optional.of;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
import static org.eclipse.jetty.http.HttpStatus.*;

/**
 * Client to access <a href="https://connect2id.com/products/server/docs/api/client-registration">Connect2Id's OpenID
 * Connect client registration endpoint</a>.
 */
public interface OpenIdConnectClientRegistration {

  /**
   * @param clientMetadata Required
   * @return Never <code>null</code>
   */
  OpenIdConnectClient registerClient(OpenIdClientMetadata clientMetadata) throws Connect2IdException;

  /**
   * @param clientId Required
   * @return Never <code>null</code>
   */
  Optional<OpenIdConnectClient> getClientOf(ClientId clientId) throws Connect2IdException;

  /**
   * @return Never <code>null</code>
   */
  List<OpenIdConnectClient> getAllClients() throws Connect2IdException;

  /**
   * @param clientId Required
   */
  void deleteClientWith(ClientId clientId) throws Connect2IdException;

  @Immutable
  final class Impl extends BaseConnect2IdClient implements OpenIdConnectClientRegistration {

    private static final Logger LOG = LoggerFactory.getLogger(Impl.class);
    // Readers:
    private static final ObjectReader OPEN_ID_CONNECT_CLIENT_RESPONSE_ENTITY_READER = OBJECT_MAPPER
            .readerFor(OpenIdConnectClient.class);
    // Writers:
    private static final ObjectWriter OPEN_ID_CLIENT_METADATA_WRITER = OBJECT_MAPPER
            .writerFor(OpenIdClientMetadata.class);

    private final Client restClient;
    private final URI connect2IdURL;
    private final AccessToken bearerAccessToken;

    /**
     * @param connect2IdURL     Required, something like <code>http://c2id.staging.svc.cluster.local:8080</code>
     * @param bearerAccessToken Required
     * @param restClient        Required
     */
    public Impl(final URL connect2IdURL, final AccessToken bearerAccessToken, final Client restClient) {
      super(LOG);
      Validate.notNull(restClient, "Missing REST client");
      Validate.notNull(connect2IdURL, "Missing Connect2Id URL");
      Validate.notNull(bearerAccessToken, "Missing bearer access token");
      this.restClient = restClient;
      this.bearerAccessToken = bearerAccessToken;
      try {
        this.connect2IdURL = connect2IdURL.toURI();
      } catch (final URISyntaxException e) {
        throw new IllegalArgumentException("Connect2Id URL " + connect2IdURL, e);
      }
    }

    @Override
    public Optional<OpenIdConnectClient> getClientOf(final ClientId clientId) throws Connect2IdException {
      Validate.notNull(clientId, "Missing client ID");
      final WebTarget target = restClient.target(connect2IdURL).path("/clients").path(clientId.getPlain());
      final Invocation invocation = target.request(APPLICATION_JSON_TYPE)
              .header(AUTHORIZATION_HEADER, bearerAccessToken.withPrefix("Bearer")).buildGet();
      LOG.info("Loading client '{}' on C2ID client registration endpoint", clientId);
      final Pair<Response, String> invocationResult = invokeGET(invocation, target.getUri());
      final String responseBody = invocationResult.getRight();
      final Response getResponse = invocationResult.getLeft();
      try {
        switch (getResponse.getStatus()) {
          case OK_200:
            final OpenIdConnectClient openIdConnectClient = OPEN_ID_CONNECT_CLIENT_RESPONSE_ENTITY_READER
                    .readValue(responseBody);
            LOG.info("Loaded client '{}': {}", clientId, openIdConnectClient);
            return of(openIdConnectClient);
          case UNAUTHORIZED_401:
            throw unauthorizedException(target, responseBody);
          case NOT_FOUND_404:
            return empty();
          case INTERNAL_SERVER_ERROR_500:
            throw serverException(target, responseBody);
          default:
            throw new NotImplementedException("Status: " + getResponse.getStatus() + ", response: " + getResponse);
        }
      } catch (final IOException e) {
        throw new RuntimeException("Internal error unmarshalling " + responseBody, e);
      } finally {
        getResponse.close();
      }
    }

    @Override
    public List<OpenIdConnectClient> getAllClients() throws Connect2IdException {
      final WebTarget target = restClient.target(connect2IdURL).path("/clients");
      final Invocation invocation = target.request(APPLICATION_JSON_TYPE)
              .header(AUTHORIZATION_HEADER, bearerAccessToken.withPrefix("Bearer")).buildGet();
      LOG.info("Loading all clients on C2ID client registration endpoint");
      final Pair<Response, String> invocationResult = invokeGET(invocation, target.getUri());
      return parseInvocationResult(invocationResult, (responseBody -> {
        final MappingIterator<OpenIdConnectClient> mappingIterator = OPEN_ID_CONNECT_CLIENT_RESPONSE_ENTITY_READER
                .readValues(responseBody);
        final List<OpenIdConnectClient> clientList = mappingIterator.readAll();
        LOG.info("Number of clients loaded: {}", clientList.size());
        return clientList;
      }), target, EMPTY_BODY_CONTENT);
    }

    @Override
    public void deleteClientWith(final ClientId clientId) throws Connect2IdException {
      Validate.notNull(clientId, "Missing client ID");
      final WebTarget target = restClient.target(connect2IdURL).path("/clients").path(clientId.getPlain());
      final Invocation invocation = target.request(APPLICATION_JSON_TYPE)
              .header(AUTHORIZATION_HEADER, bearerAccessToken.withPrefix("Bearer")).buildDelete();
      LOG.info("Deleting client '{}' on C2ID client registration endpoint", clientId);
      final Pair<Response, String> invocationResult = invoke(invocation, target.getUri(), "DELETE", EMPTY_BODY_CONTENT);
      final String responseBody = invocationResult.getRight();
      final Response getResponse = invocationResult.getLeft();
      try {
        switch (getResponse.getStatus()) {
          case NO_CONTENT_204:
            LOG.info("Deleted client (ID '{}')", clientId);
            break;
          case UNAUTHORIZED_401:
            throw unauthorizedException(target, responseBody);
          case NOT_FOUND_404:
            throw new NotFoundException(responseBody);
          case INTERNAL_SERVER_ERROR_500:
            throw serverException(target, responseBody);
          default:
            throw new NotImplementedException("Status: " + getResponse.getStatus() + ", response: " + getResponse);
        }
      } catch (final IOException e) {
        throw new RuntimeException("Internal error unmarshalling " + responseBody, e);
      } finally {
        getResponse.close();
      }
    }

    @Override
    public OpenIdConnectClient registerClient(final OpenIdClientMetadata clientMetadata) throws Connect2IdException {
      Validate.notNull(clientMetadata, "Missing OpenID Client metadata");
      final String jsonRequestBody;
      try {
        jsonRequestBody = OPEN_ID_CLIENT_METADATA_WRITER.writeValueAsString(clientMetadata);
      } catch (final IOException e) {
        throw new RuntimeException("Internal error marshalling " + clientMetadata, e);
      }

      final WebTarget target = restClient.target(connect2IdURL).path("/clients");
      final Builder builder = target.request(APPLICATION_JSON_TYPE).header(AUTHORIZATION_HEADER, bearerAccessToken.withPrefix("Bearer"));
      LOG.info("Registering client on C2ID client registration endpoint: {}", clientMetadata);
      final Pair<Response, String> invocationResult = invokePOSTWithJSON(builder, jsonRequestBody, target.getUri());
      final String responseBody = invocationResult.getRight();
      final Response getResponse = invocationResult.getLeft();
      try {
        switch (getResponse.getStatus()) {
          case OK_200:
            final OpenIdConnectClient openIdConnectClient = OPEN_ID_CONNECT_CLIENT_RESPONSE_ENTITY_READER
                    .readValue(responseBody);
            LOG.info("Registered client: {}", openIdConnectClient);
            return openIdConnectClient;
          case UNAUTHORIZED_401:
            throw unauthorizedException(target, responseBody);
          case NOT_FOUND_404:
            throw notFoundException(target, responseBody);
          case INTERNAL_SERVER_ERROR_500:
            throw serverException(target, responseBody);
          default:
            throw new NotImplementedException("Status: " + getResponse.getStatus() + ", response: " + getResponse);
        }
      } catch (final IOException e) {
        throw new RuntimeException("Internal error unmarshalling " + responseBody, e);
      } finally {
        getResponse.close();
      }
    }
  }

}
