/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.authzsession.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.denic.domainid.connect2id.common.SubjectSessionId;
import org.apache.commons.lang3.Validate;

import javax.annotation.concurrent.Immutable;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.util.Objects;
import java.util.Optional;

import static java.util.Optional.ofNullable;

@JsonIgnoreProperties(ignoreUnknown = true)
@Immutable
public final class FinalResponse {

  private static final String EXPECTED_TYPE_VALUE = "response";

  private final String mode;
  private final Parameters parameters;
  private final Optional<SubjectSessionId> optSubjectSessionId;

  /**
   * @param type             Required
   * @param mode             Required
   * @param parameters       Required
   * @param subjectSessionId Optional
   */
  public FinalResponse(@NotNull @JsonProperty("type") final String type,
                       @NotNull @JsonProperty("mode") final String mode,
                       @JsonProperty("sub_sid") final SubjectSessionId subjectSessionId,
                       @NotNull @JsonProperty("parameters") final Parameters parameters) {
    Validate.isTrue(EXPECTED_TYPE_VALUE.equals(type),
            "Data does NOT describe a Final response! Type was not '" + EXPECTED_TYPE_VALUE + "' as expected, but '%s'",
            type);
    Validate.notEmpty(mode, "Missing mode");
    Validate.notNull(parameters, "Missing parameters");
    this.mode = mode;
    this.parameters = parameters;
    this.optSubjectSessionId = ofNullable(subjectSessionId);
  }

  /**
   * @return Never <code>null</code>
   */
  public Parameters getParameters() {
    return parameters;
  }

  /**
   * @return Never <code>null</code>
   */
  public Optional<SubjectSessionId> getSubjectSessionId() {
    return optSubjectSessionId;
  }

  /**
   * @return Never <code>null</code> nor empty.
   */
  public String getMode() {
    return mode;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    FinalResponse that = (FinalResponse) o;
    return Objects.equals(mode, that.mode) &&
            Objects.equals(parameters, that.parameters) &&
            Objects.equals(optSubjectSessionId, that.optSubjectSessionId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(mode, parameters, optSubjectSessionId);
  }

  @Override
  public String toString() {
    return "mode=" + mode + ", subject session ID=" + optSubjectSessionId + ", params (" + parameters + ")";
  }

  @JsonIgnoreProperties(ignoreUnknown = true)
  @Immutable
  public static final class Parameters {

    private final URI uri;

    /**
     * @param uri Required
     */
    public Parameters(@NotNull @JsonProperty("uri") final URI uri) {
      Validate.notNull(uri, "Missing URI");
      this.uri = uri;
    }

    /**
     * @return Never <code>null</code>
     */
    public URI getUri() {
      return uri;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((uri == null) ? 0 : uri.hashCode());
      return result;
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      final Parameters other = (Parameters) obj;
      if (uri == null) {
        if (other.uri != null) {
          return false;
        }
      } else if (!uri.equals(other.uri)) {
        return false;
      }
      return true;
    }

    @Override
    public String toString() {
      return "uri=" + uri;
    }

  }

}
