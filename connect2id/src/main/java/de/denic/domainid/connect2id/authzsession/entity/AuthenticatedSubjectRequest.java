/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.authzsession.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.denic.domainid.DomainId;
import de.denic.domainid.connect2id.common.AuthenticationMethodReference;
import de.denic.domainid.connect2id.subjectsessionstore.entity.SubjectSessionOptionalData;
import de.denic.domainid.subject.PseudonymSubject;
import org.apache.commons.lang3.Validate;

import javax.annotation.concurrent.Immutable;
import javax.security.auth.Subject;
import java.security.Principal;
import java.util.*;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;
import static de.denic.domainid.subject.SubjectUtils.from;
import static java.util.Collections.*;

@Immutable
public final class AuthenticatedSubjectRequest {

  private final String subject;
  private final Set<AuthenticationMethodReference> authMethodReferences;
  private final SubjectSessionOptionalData optionalData;
  private final Map<String, String> additionalClaims;

  /**
   * @param authenticatedSubject Required to provide at least one {@link Principal} of type {@link PseudonymSubject}
   *                             as well as of type {@link DomainId}.
   * @param authMethodReferences Optional
   */
  public AuthenticatedSubjectRequest(final Subject authenticatedSubject,
                                     final Collection<AuthenticationMethodReference> authMethodReferences) {
    Validate.notNull(authenticatedSubject, "Missing authenticated subject");
    final DomainId domainId = from(authenticatedSubject).single(DomainId.class);
    this.optionalData = new SubjectSessionOptionalData(domainId);
    this.subject = from(authenticatedSubject).single(PseudonymSubject.class).getName();
    this.authMethodReferences = (authMethodReferences == null ? emptySet() : unmodifiableSet(new HashSet<>(authMethodReferences)));
    final Map<String, String> tempAdditionalClaims = new HashMap<>();
    tempAdditionalClaims.put("id4me.identifier", domainId.getName());
    this.additionalClaims = unmodifiableMap(tempAdditionalClaims);
  }

  /**
   * @see #AuthenticatedSubjectRequest(Subject, Collection)
   */
  public AuthenticatedSubjectRequest(final Subject authenticatedSubject) {
    this(authenticatedSubject, null);
  }

  @JsonProperty("sub")
  public String getSubject() {
    return subject;
  }

  /**
   * @return Never <code>null</code> but unmodifiable.
   */
  @JsonInclude(NON_EMPTY)
  @JsonProperty("amr")
  public Set<AuthenticationMethodReference> getAuthenticationMethodReferences() {
    return authMethodReferences;
  }

  /**
   * @return Never <code>null</code> but unmodifiable.
   */
  @JsonInclude(NON_EMPTY)
  @JsonProperty("claims")
  public Map<String, String> getAdditionalClaims() {
    return additionalClaims;
  }

  /**
   * @return Never <code>null</code>.
   */
  @JsonProperty("data")
  public SubjectSessionOptionalData getOptionalData() {
    return optionalData;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    AuthenticatedSubjectRequest that = (AuthenticatedSubjectRequest) o;
    return Objects.equals(subject, that.subject) &&
            Objects.equals(optionalData, that.optionalData) &&
            Objects.equals(additionalClaims, that.additionalClaims) &&
            Objects.equals(authMethodReferences, that.authMethodReferences);
  }

  @Override
  public int hashCode() {
    return Objects.hash(subject, authMethodReferences, optionalData, additionalClaims);
  }

  @Override
  public String toString() {
    return "sub=" + subject + ", amr=" + authMethodReferences + ", claims=" + additionalClaims + ", data=(" + optionalData + ")";
  }

}
