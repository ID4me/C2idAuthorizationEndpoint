/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.common;

import com.fasterxml.jackson.annotation.JsonValue;
import org.apache.commons.lang3.Validate;

import javax.annotation.concurrent.Immutable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Objects;

/**
 * Provides some kind of <code>value</code> as plain text or
 * <a href="https://de.wikipedia.org/wiki/URL-Encoding#Prozentdarstellung">URL-encoded</a> with {@linkplain #toString()}
 * providing the <strong>URL-encoded</strong> value. This makes marshalling/unmarshalling of values URL-safe!
 */
@Immutable
public abstract class UrlEncodedValue {

  private static final String UTF_8 = "UTF-8";

  private final String plainValue, encodedValue;
  private final int hashCode;

  /**
   * @param plainValue Required
   */
  public UrlEncodedValue(final String plainValue) {
    Validate.notNull(plainValue, "Missing plain value");
    this.plainValue = plainValue;
    try {
      this.encodedValue = URLEncoder.encode(plainValue, UTF_8);
    } catch (final UnsupportedEncodingException e) {
      throw new RuntimeException("Internal error", e);
    }
    this.hashCode = calcHashCode();
  }

  /**
   * @return Never <code>null</code>
   */
  @JsonValue
  public String getPlain() {
    return plainValue;
  }

  /**
   * @return Never <code>null</code>
   */
  public String getEncoded() {
    return encodedValue;
  }

  private int calcHashCode() {
    return Objects.hash(plainValue, encodedValue);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    UrlEncodedValue that = (UrlEncodedValue) o;
    return Objects.equals(plainValue, that.plainValue) &&
            Objects.equals(encodedValue, that.encodedValue);
  }

  @Override
  public int hashCode() {
    return hashCode;
  }

  @Override
  public String toString() {
    return encodedValue;
  }

}
