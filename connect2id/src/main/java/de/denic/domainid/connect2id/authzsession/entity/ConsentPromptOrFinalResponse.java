/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.authzsession.entity;

import javax.annotation.concurrent.Immutable;

import org.apache.commons.lang3.Validate;

/**
 * TODO: Nach Refactoring wahrscheinlich ueberfluessig!
 */
@Immutable
public final class ConsentPromptOrFinalResponse {

  private final ConsentPromptResponse consentPromptEntity;
  private final FinalResponse finalResponseEntity;

  private ConsentPromptOrFinalResponse(final ConsentPromptResponse consentPromptEntity,
      final FinalResponse finalResponseEntity) {
    Validate.isTrue(consentPromptEntity != null || finalResponseEntity != null,
        "Missing one of Consent Prompt or Final Response entity");
    this.consentPromptEntity = consentPromptEntity;
    this.finalResponseEntity = finalResponseEntity;
  }

  /**
   * @return Never <code>null</code> if {@link #getFinalResponseEntity()} returns <code>null</code>.
   */
  public ConsentPromptResponse getConsentPromptEntity() {
    return consentPromptEntity;
  }

  /**
   * @return Never <code>null</code> if {@link #getConsentPromptEntity()} returns <code>null</code>.
   */
  public FinalResponse getFinalResponseEntity() {
    return finalResponseEntity;
  }

  /**
   * @return Whether {@link #getFinalResponseEntity()} returns <code>null</code>.
   */
  public boolean hasFinalResponseEntity() {
    return finalResponseEntity != null;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((consentPromptEntity == null) ? 0 : consentPromptEntity.hashCode());
    result = prime * result + ((finalResponseEntity == null) ? 0 : finalResponseEntity.hashCode());
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final ConsentPromptOrFinalResponse other = (ConsentPromptOrFinalResponse) obj;
    if (consentPromptEntity == null) {
      if (other.consentPromptEntity != null) {
        return false;
      }
    } else if (!consentPromptEntity.equals(other.consentPromptEntity)) {
      return false;
    }
    if (finalResponseEntity == null) {
      if (other.finalResponseEntity != null) {
        return false;
      }
    } else if (!finalResponseEntity.equals(other.finalResponseEntity)) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return (consentPromptEntity == null ? "Final Response: " + finalResponseEntity
        : "Consent Prompt: " + consentPromptEntity);
  }

  /**
   * @param consentPromptEntity
   *          Required
   * @return Never <code>null</code>
   */
  public static final ConsentPromptOrFinalResponse withConsentPrompt(final ConsentPromptResponse consentPromptEntity) {
    return new ConsentPromptOrFinalResponse(consentPromptEntity, null);
  }

  /**
   * @param finalResponseEntity
   *          Required
   * @return Never <code>null</code>
   */
  public static final ConsentPromptOrFinalResponse withFinalResponse(final FinalResponse finalResponseEntity) {
    return new ConsentPromptOrFinalResponse(null, finalResponseEntity);
  }

}
