/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.authzsession.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.denic.domainid.connect2id.authzsession.SessionId;
import de.denic.domainid.connect2id.common.Claim;
import de.denic.domainid.connect2id.common.ClientId;
import de.denic.domainid.connect2id.common.Scope;
import de.denic.domainid.connect2id.subjectsessionstore.entity.SubjectSession;
import org.apache.commons.lang3.Validate;

import javax.annotation.concurrent.Immutable;
import java.net.URI;
import java.util.*;

import static java.util.Collections.*;
import static org.apache.commons.lang3.StringUtils.defaultIfBlank;
import static org.apache.commons.lang3.StringUtils.isEmpty;

@JsonIgnoreProperties(ignoreUnknown = true)
@Immutable
public final class ConsentPromptResponse {

  private static final String EXPECTED_TYPE_VALUE = "consent";

  private final SessionId connect2IdAuthorisationSessionId;
  private final SubjectSession subjectSession;
  private final Client client;
  private final Scopes scopes;
  private final Claims claims;
  private final AuthzSessionOptionalData sessionData;

  public ConsentPromptResponse(@JsonProperty("type") final String type,
                               @JsonProperty("sid") final SessionId connect2IdAuthorisationSessionId,
                               @JsonProperty("sub_session") final SubjectSession subjectSession,
                               @JsonProperty("client") final Client client, @JsonProperty("scope") final Scopes scopes,
                               @JsonProperty("claims") final Claims claims, @JsonProperty("data") final AuthzSessionOptionalData sessionData) {
    Validate.isTrue(EXPECTED_TYPE_VALUE.equals(type),
            "Data does NOT describe a Consent prompt! Type was not '" + EXPECTED_TYPE_VALUE + "' as expected, but '%s'",
            type);
    Validate.notNull(connect2IdAuthorisationSessionId, "Missing Connect2Id Authorisation Session ID");
    Validate.notNull(subjectSession, "Missing subject's session");
    Validate.notNull(client, "Missing client");
    this.connect2IdAuthorisationSessionId = connect2IdAuthorisationSessionId;
    this.subjectSession = subjectSession;
    this.client = client;
    this.scopes = (scopes == null ? new Scopes() : scopes);
    this.claims = (claims == null ? new Claims() : claims);
    this.sessionData = (sessionData == null ? new AuthzSessionOptionalData() : sessionData);
  }

  /**
   * @return Never <code>null</code>
   */
  public SessionId getConnect2IdAuthorisationSessionId() {
    return connect2IdAuthorisationSessionId;
  }

  /**
   * @return Never <code>null</code>
   */
  public Client getClient() {
    return client;
  }

  /**
   * @return Never <code>null</code>
   */
  public SubjectSession getSubjectSession() {
    return subjectSession;
  }

  /**
   * @return Never <code>null</code>
   */
  public Scopes getScopes() {
    return scopes;
  }

  /**
   * @return Never <code>null</code>
   */
  public Claims getClaims() {
    return claims;
  }

  /**
   * @return Never <code>null</code>
   */
  public AuthzSessionOptionalData getSessionData() {
    return sessionData;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((claims == null) ? 0 : claims.hashCode());
    result = prime * result + ((client == null) ? 0 : client.hashCode());
    result = prime * result
            + ((connect2IdAuthorisationSessionId == null) ? 0 : connect2IdAuthorisationSessionId.hashCode());
    result = prime * result + ((scopes == null) ? 0 : scopes.hashCode());
    result = prime * result + ((sessionData == null) ? 0 : sessionData.hashCode());
    result = prime * result + ((subjectSession == null) ? 0 : subjectSession.hashCode());
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final ConsentPromptResponse other = (ConsentPromptResponse) obj;
    if (claims == null) {
      if (other.claims != null) {
        return false;
      }
    } else if (!claims.equals(other.claims)) {
      return false;
    }
    if (client == null) {
      if (other.client != null) {
        return false;
      }
    } else if (!client.equals(other.client)) {
      return false;
    }
    if (connect2IdAuthorisationSessionId == null) {
      if (other.connect2IdAuthorisationSessionId != null) {
        return false;
      }
    } else if (!connect2IdAuthorisationSessionId.equals(other.connect2IdAuthorisationSessionId)) {
      return false;
    }
    if (scopes == null) {
      if (other.scopes != null) {
        return false;
      }
    } else if (!scopes.equals(other.scopes)) {
      return false;
    }
    if (sessionData == null) {
      if (other.sessionData != null) {
        return false;
      }
    } else if (!sessionData.equals(other.sessionData)) {
      return false;
    }
    if (subjectSession == null) {
      if (other.subjectSession != null) {
        return false;
      }
    } else if (!subjectSession.equals(other.subjectSession)) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return connect2IdAuthorisationSessionId + ", client (" + client + "), subject's session (" + subjectSession
            + "), scope (" + scopes + "), claims (" + claims + "), data (" + sessionData + ")";
  }

  @JsonIgnoreProperties(ignoreUnknown = true)
  @Immutable
  public static final class Client {

    private final String clientType, applicationType, name;
    private final ClientId clientId;
    private final URI uri, logoUri, policyUri, termsOfServiceUri;

    public Client(@JsonProperty("client_id") final ClientId clientId,
                  @JsonProperty("client_type") final String clientType,
                  @JsonProperty("application_type") final String applicationType, @JsonProperty("name") final String name,
                  @JsonProperty("uri") final URI uri, @JsonProperty("logo_uri") final URI logoUri,
                  @JsonProperty("policy_uri") final URI policyUri, @JsonProperty("tos_uri") final URI termsOfServiceUri) {
      Validate.notNull(clientId, "Missing client ID");
      this.name = defaultIfBlank(name, "ID " + clientId.toString());
      this.uri = (uri == null ? null : isEmpty(uri.toString().trim()) ? null : uri);
      this.clientType = clientType;
      this.applicationType = applicationType;
      this.clientId = clientId;
      this.termsOfServiceUri = termsOfServiceUri;
      this.logoUri = logoUri;
      this.policyUri = policyUri;
    }

    /**
     * @return Never <code>null</code> nor empty.
     */
    public String getName() {
      return name;
    }

    /**
     * @return Never <code>null</code>
     */
    public ClientId getId() {
      return clientId;
    }

    /**
     * @return Optional
     */
    public String getType() {
      return clientType;
    }

    /**
     * @return Optional
     */
    public String getApplicationType() {
      return applicationType;
    }

    /**
     * @return Optional
     */
    public URI getUri() {
      return uri;
    }

    /**
     * @return Optional
     */
    public URI getLogoUri() {
      return logoUri;
    }

    /**
     * @return Optional
     */
    public URI getPolicyUri() {
      return policyUri;
    }

    /**
     * @return Optional
     */
    public URI getTermsOfServiceUri() {
      return termsOfServiceUri;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((applicationType == null) ? 0 : applicationType.hashCode());
      result = prime * result + ((clientId == null) ? 0 : clientId.hashCode());
      result = prime * result + ((clientType == null) ? 0 : clientType.hashCode());
      result = prime * result + ((logoUri == null) ? 0 : logoUri.hashCode());
      result = prime * result + ((name == null) ? 0 : name.hashCode());
      result = prime * result + ((policyUri == null) ? 0 : policyUri.hashCode());
      result = prime * result + ((termsOfServiceUri == null) ? 0 : termsOfServiceUri.hashCode());
      result = prime * result + ((uri == null) ? 0 : uri.hashCode());
      return result;
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      final Client other = (Client) obj;
      if (applicationType == null) {
        if (other.applicationType != null) {
          return false;
        }
      } else if (!applicationType.equals(other.applicationType)) {
        return false;
      }
      if (clientId == null) {
        if (other.clientId != null) {
          return false;
        }
      } else if (!clientId.equals(other.clientId)) {
        return false;
      }
      if (clientType == null) {
        if (other.clientType != null) {
          return false;
        }
      } else if (!clientType.equals(other.clientType)) {
        return false;
      }
      if (logoUri == null) {
        if (other.logoUri != null) {
          return false;
        }
      } else if (!logoUri.equals(other.logoUri)) {
        return false;
      }
      if (name == null) {
        if (other.name != null) {
          return false;
        }
      } else if (!name.equals(other.name)) {
        return false;
      }
      if (policyUri == null) {
        if (other.policyUri != null) {
          return false;
        }
      } else if (!policyUri.equals(other.policyUri)) {
        return false;
      }
      if (termsOfServiceUri == null) {
        if (other.termsOfServiceUri != null) {
          return false;
        }
      } else if (!termsOfServiceUri.equals(other.termsOfServiceUri)) {
        return false;
      }
      if (uri == null) {
        if (other.uri != null) {
          return false;
        }
      } else if (!uri.equals(other.uri)) {
        return false;
      }
      return true;
    }

    @Override
    public String toString() {
      return name + " (" + clientId + "), type '" + clientType + "', appl. type '" + applicationType + "', URI '" + uri
              + "', tos URI '" + termsOfServiceUri + "', logo URI '" + logoUri + "', policy URI '" + policyUri;
    }

  }

  @JsonIgnoreProperties(ignoreUnknown = true)
  @Immutable
  public static final class Claims {

    private final EssentialAndVoluntaryClaims newClaims;
    private final EssentialAndVoluntaryClaims consentedClaims;

    public Claims(@JsonProperty("new") final EssentialAndVoluntaryClaims newClaims,
                  @JsonProperty("consented") final EssentialAndVoluntaryClaims consentedClaims) {
      this.newClaims = (newClaims == null ? new EssentialAndVoluntaryClaims() : newClaims);
      this.consentedClaims = (consentedClaims == null ? new EssentialAndVoluntaryClaims() : consentedClaims);
    }

    private Claims() {
      this(null, null);
    }

    /**
     * @return Never <code>null</code>
     */
    public EssentialAndVoluntaryClaims getNew() {
      return newClaims;
    }

    /**
     * @return Never <code>null</code>
     */
    public EssentialAndVoluntaryClaims getConsented() {
      return consentedClaims;
    }

    /**
     * @return Never <code>null</code> but unmodifiable.
     */
    public List<Claim> getAll() {
      final List<Claim> allClaims = new LinkedList<>(consentedClaims.getAll());
      allClaims.addAll(newClaims.getAll());
      return unmodifiableList(allClaims);
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((consentedClaims == null) ? 0 : consentedClaims.hashCode());
      result = prime * result + ((newClaims == null) ? 0 : newClaims.hashCode());
      return result;
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      final Claims other = (Claims) obj;
      if (consentedClaims == null) {
        if (other.consentedClaims != null) {
          return false;
        }
      } else if (!consentedClaims.equals(other.consentedClaims)) {
        return false;
      }
      if (newClaims == null) {
        if (other.newClaims != null) {
          return false;
        }
      } else if (!newClaims.equals(other.newClaims)) {
        return false;
      }
      return true;
    }

    @Override
    public String toString() {
      return "NEW: " + newClaims + ", CONSENTED: " + consentedClaims;
    }

  }

  @JsonIgnoreProperties(ignoreUnknown = true)
  @Immutable
  public static final class EssentialAndVoluntaryClaims {

    private final List<Claim> voluntaryClaims = new LinkedList<>();
    private final List<Claim> essentialClaims = new LinkedList<>();

    public EssentialAndVoluntaryClaims(@JsonProperty("voluntary") final List<Claim> voluntaryClaims,
                                       @JsonProperty("essential") final List<Claim> essentialClaims) {
      if (voluntaryClaims != null) {
        this.voluntaryClaims.addAll(voluntaryClaims);
      }
      if (essentialClaims != null) {
        this.essentialClaims.addAll(essentialClaims);
      }
    }

    private EssentialAndVoluntaryClaims() {
      this(emptyList(), emptyList());
    }

    /**
     * @return Never <code>null</code> but unmodifiable.
     */
    public List<Claim> getAll() {
      final List<Claim> allClaims = new LinkedList<>(essentialClaims);
      allClaims.addAll(voluntaryClaims);
      return unmodifiableList(allClaims);
    }

    /**
     * @return Never <code>null</code> but unmodifiable.
     */
    public List<Claim> getVoluntary() {
      return unmodifiableList(voluntaryClaims);
    }

    /**
     * @return Never <code>null</code> but unmodifiable.
     */
    public List<Claim> getEssential() {
      return unmodifiableList(essentialClaims);
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((essentialClaims == null) ? 0 : essentialClaims.hashCode());
      result = prime * result + ((voluntaryClaims == null) ? 0 : voluntaryClaims.hashCode());
      return result;
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      final EssentialAndVoluntaryClaims other = (EssentialAndVoluntaryClaims) obj;
      if (essentialClaims == null) {
        if (other.essentialClaims != null) {
          return false;
        }
      } else if (!essentialClaims.equals(other.essentialClaims)) {
        return false;
      }
      if (voluntaryClaims == null) {
        if (other.voluntaryClaims != null) {
          return false;
        }
      } else if (!voluntaryClaims.equals(other.voluntaryClaims)) {
        return false;
      }
      return true;
    }

    @Override
    public String toString() {
      return "voluntary " + voluntaryClaims + ", essential " + essentialClaims;
    }

  }

  @JsonIgnoreProperties(ignoreUnknown = true)
  @Immutable
  public static final class Scopes {

    private final Set<Scope> newScopes = new HashSet<>();
    private final Set<Scope> consentedScopes = new HashSet<>();

    public Scopes(@JsonProperty("new") final Collection<Scope> newScopes,
                  @JsonProperty("consented") final Collection<Scope> consentedScopes) {
      if (newScopes != null) {
        this.newScopes.addAll(newScopes);
      }
      if (consentedScopes != null) {
        this.consentedScopes.addAll(consentedScopes);
      }
    }

    private Scopes() {
      this(emptyList(), emptyList());
    }

    /**
     * @return Never <code>null</code> but unmodifiable.
     */
    public Set<Scope> getNew() {
      return unmodifiableSet(newScopes);
    }

    /**
     * @return Never <code>null</code> but unmodifiable.
     */
    public Set<Scope> getConsented() {
      return unmodifiableSet(consentedScopes);
    }

    /**
     * @return Never <code>null</code> but unmodifiable.
     */
    public Set<Scope> getAll() {
      final Set<Scope> allScopes = new HashSet<>(consentedScopes);
      allScopes.addAll(newScopes);
      return unmodifiableSet(allScopes);
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((consentedScopes == null) ? 0 : consentedScopes.hashCode());
      result = prime * result + ((newScopes == null) ? 0 : newScopes.hashCode());
      return result;
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      final Scopes other = (Scopes) obj;
      if (consentedScopes == null) {
        if (other.consentedScopes != null) {
          return false;
        }
      } else if (!consentedScopes.equals(other.consentedScopes)) {
        return false;
      }
      if (newScopes == null) {
        if (other.newScopes != null) {
          return false;
        }
      } else if (!newScopes.equals(other.newScopes)) {
        return false;
      }
      return true;
    }

    @Override
    public String toString() {
      return "new " + newScopes + ", consented " + consentedScopes;
    }

  }

}
