/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.clientregistration.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.denic.domainid.connect2id.common.ClientId;
import org.apache.commons.lang3.Validate;

import javax.annotation.concurrent.Immutable;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
@Immutable
public final class OpenIdConnectClient {

  private final ClientId clientId;
  private final String name;
  private final URI logoURI, policyURI, termsOfServiceURI;
  private final int hashCode;

  /**
   * @param clientId          Required
   * @param name              Optional
   * @param logoURI           Optional
   * @param termsOfServiceURI Optional
   * @param policyURI         Optional
   */
  public OpenIdConnectClient(@JsonProperty("client_id") @NotNull final ClientId clientId,
                             @JsonProperty("client_name") final String name, @JsonProperty("logo_uri") final URI logoURI,
                             @JsonProperty("tos_uri") final URI termsOfServiceURI,
                             @JsonProperty("policy_uri") final URI policyURI) {
    Validate.notNull(clientId, "Missing client ID");
    this.clientId = clientId;
    this.name = name;
    this.logoURI = (isEmpty(logoURI) ? null : logoURI);
    this.policyURI = (isEmpty(policyURI) ? null : policyURI);
    this.termsOfServiceURI = (isEmpty(termsOfServiceURI) ? null : termsOfServiceURI);
    this.hashCode = calcHashCode();
  }

  private static boolean isEmpty(final URI uri) {
    return (uri == null || uri.toASCIIString().length() < 1);
  }

  /**
   * @return Never <code>null</code>.
   */
  public ClientId getId() {
    return clientId;
  }

  /**
   * @return Optional.
   */
  public String getName() {
    return name;
  }

  /**
   * @return Optional.
   */
  public URI getLogoURI() {
    return logoURI;
  }

  /**
   * @return Optional.
   */
  public URI getPolicyURI() {
    return policyURI;
  }

  /**
   * @return Optional.
   */
  public URI getTermsOfServiceURI() {
    return termsOfServiceURI;
  }

  private int calcHashCode() {
    return Objects.hash(clientId, name, logoURI, policyURI, termsOfServiceURI, hashCode);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    OpenIdConnectClient that = (OpenIdConnectClient) o;
    return hashCode == that.hashCode &&
            Objects.equals(clientId, that.clientId) &&
            Objects.equals(name, that.name) &&
            Objects.equals(logoURI, that.logoURI) &&
            Objects.equals(policyURI, that.policyURI) &&
            Objects.equals(termsOfServiceURI, that.termsOfServiceURI);
  }

  @Override
  public int hashCode() {
    return hashCode;
  }

  @Override
  public String toString() {
    return "clientId=" + clientId + ", name=" + name + ", logoURI=" + logoURI + ", tosURI=" + termsOfServiceURI +
            ", policyURI=" + policyURI;
  }

}
