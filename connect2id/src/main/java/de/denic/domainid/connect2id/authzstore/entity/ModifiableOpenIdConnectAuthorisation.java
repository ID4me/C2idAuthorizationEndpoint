/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.authzstore.entity;

import java.net.URI;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import javax.annotation.concurrent.NotThreadSafe;

import de.denic.domainid.connect2id.subjectsessionstore.entity.SubjectSessionOptionalData;
import de.denic.domainid.connect2id.common.Claim;
import de.denic.domainid.connect2id.common.ClientId;
import de.denic.domainid.connect2id.common.Scope;

@NotThreadSafe
public final class ModifiableOpenIdConnectAuthorisation {

  private final String subject;
  private final ClientId clientId;
  private final boolean longLived;
  private final List<Scope> authorisationScopes = new LinkedList<>();
  private final List<Scope> savedScopes = new LinkedList<>();
  private final List<Claim> consentedClaims = new LinkedList<>();
  private final List<Claim> savedConsentedClaims = new LinkedList<>();
  private final URI redirectionURI;
  private final Boolean issueRefreshToken;
  private final String refreshToken;
  private final Duration refreshTokenLifetime;
  private final Duration accessTokenLifetime;
  private final String accessTokenEncoding;
  private final Boolean accessTokenEncryption;
  private final String issuer;
  private final ZonedDateTime issuedAt;
  private final ZonedDateTime updatedAt;
  private final String authorisationAudience;
  private final String openIdConnectIdToken;
  private final List<Locale> preferredClaimLocales = new LinkedList<>();
  private final String pkceCodeChallenge;
  private final String pkceCodeChallengeMethod;
  private final SubjectSessionOptionalData optionalData;

  public ModifiableOpenIdConnectAuthorisation(final OpenIdConnectAuthorization presetValues) {
    this.subject = presetValues.getSubject();
    this.clientId = presetValues.getClientId();
    this.longLived = presetValues.isLongLived();
    this.authorisationScopes.addAll(presetValues.getAuthorisationScopes());
    this.consentedClaims.addAll(presetValues.getConsentedClaims());
    this.savedConsentedClaims.addAll(presetValues.getConsentedClaims());
    this.savedScopes.addAll(presetValues.getSavedScopes());
    this.redirectionURI = presetValues.getRedirectionURI();
    this.issueRefreshToken = presetValues.getIssueRefreshToken();
    this.refreshToken = presetValues.getRefreshToken();
    this.refreshTokenLifetime = presetValues.getRefreshTokenLifetime();
    this.accessTokenLifetime = presetValues.getAccessTokenLifetime();
    this.accessTokenEncoding = presetValues.getAccessTokenEncoding();
    this.accessTokenEncryption = presetValues.getAccessTokenEncryption();
    this.issuer = presetValues.getIssuer();
    this.issuedAt = presetValues.getIssuedAt();
    this.updatedAt = presetValues.getUpdatedAt();
    this.authorisationAudience = presetValues.getAuthorisationAudience();
    this.openIdConnectIdToken = presetValues.getOpenIdConnectIdToken();
    this.preferredClaimLocales.addAll(presetValues.getPreferredClaimLocales());
    this.pkceCodeChallenge = presetValues.getPkceCodeChallenge();
    this.pkceCodeChallengeMethod = presetValues.getPkceCodeChallengeMethod();
    this.optionalData = presetValues.getOptionalData();
  }

  /**
   * Manipulates state of instance directly!
   *
   * @return Never <code>null</code>
   */
  public List<Scope> getAuthorisationScopes() {
    return authorisationScopes;
  }

  /**
   * Manipulates state of instance directly!
   *
   * @return Never <code>null</code>
   */
  public List<Scope> getSavedScopes() {
    return savedScopes;
  }

  /**
   * Manipulates state of instance directly!
   *
   * @return Never <code>null</code>
   */
  public List<Claim> getConsentedClaims() {
    return consentedClaims;
  }

  /**
   * Manipulates state of instance directly!
   *
   * @return Never <code>null</code>
   */
  public List<Claim> getSavedConsentedClaims() {
    return savedConsentedClaims;
  }

  /**
   * @return Never <code>null</code>
   */
  public OpenIdConnectAuthorization unmodifiable() {
    return new OpenIdConnectAuthorization(subject, clientId, authorisationScopes, savedScopes, redirectionURI,
        longLived, issueRefreshToken, refreshToken,
        (refreshTokenLifetime == null ? null : refreshTokenLifetime.getSeconds()),
        (accessTokenLifetime == null ? null : accessTokenLifetime.getSeconds()), accessTokenEncoding,
        accessTokenEncryption, issuer, (issuedAt == null ? null : issuedAt.toEpochSecond()),
        (updatedAt == null ? null : updatedAt.toEpochSecond()), authorisationAudience, openIdConnectIdToken,
        consentedClaims, savedConsentedClaims, preferredClaimLocales, pkceCodeChallenge, pkceCodeChallengeMethod,
            optionalData);
  }

}
