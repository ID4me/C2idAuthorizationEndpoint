/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.authzsession.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.denic.domainid.DomainId;
import de.denic.domainid.connect2id.authzsession.ConsentedData;
import de.denic.domainid.connect2id.subjectsessionstore.entity.SubjectSessionOptionalData;
import org.apache.commons.lang3.Validate;

import javax.annotation.concurrent.Immutable;
import java.util.List;
import java.util.Objects;

@Immutable
public final class ConsentedDataSubmissionRequest {

  private final ConsentedData consentedData;
  private final boolean rememberConsent;
  private final SubjectSessionOptionalData optionalData;

  /**
   * @param domainId        Required
   * @param consentedData   Required
   * @param rememberConsent Required
   */
  public ConsentedDataSubmissionRequest(final DomainId domainId, final ConsentedData consentedData, final boolean
          rememberConsent) {
    Validate.notNull(consentedData, "Missing consented data");
    this.consentedData = consentedData;
    this.rememberConsent = rememberConsent;
    this.optionalData = new SubjectSessionOptionalData(domainId, consentedData.getRejectedClaims());
  }

  @JsonProperty("scope")
  public List<String> getScopes() {
    return consentedData.getScopeValues();
  }

  @JsonProperty("claims")
  public List<String> getClaims() {
    return consentedData.getClaimValues();
  }

  @JsonProperty("long_lived")
  public boolean isLongLived() {
    return rememberConsent;
  }

  @JsonProperty("data")
  public SubjectSessionOptionalData getOptionalData() {
    return optionalData;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    ConsentedDataSubmissionRequest that = (ConsentedDataSubmissionRequest) o;
    return rememberConsent == that.rememberConsent &&
            Objects.equals(consentedData, that.consentedData) &&
            Objects.equals(optionalData, that.optionalData);
  }

  @Override
  public int hashCode() {
    return Objects.hash(consentedData, rememberConsent, optionalData);
  }

  @Override
  public String toString() {
    return "consentedData=" + consentedData + ", remember=" + rememberConsent + ", data=" + optionalData;
  }

}
