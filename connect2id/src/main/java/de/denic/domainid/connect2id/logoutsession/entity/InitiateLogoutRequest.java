/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.logoutsession.entity;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

import java.net.URI;

import javax.annotation.concurrent.Immutable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import de.denic.domainid.connect2id.common.SubjectSessionId;

@Immutable
public final class InitiateLogoutRequest {

  private final URI optLogoutRequestQuery;
  private final SubjectSessionId optSubjectSessionId;

  /**
   * @param optLogoutRequestQuery
   *          Optional
   * @param optSubjectSessionId
   *          Optional
   */
  public InitiateLogoutRequest(final URI optLogoutRequestQuery, final SubjectSessionId optSubjectSessionId) {
    this.optLogoutRequestQuery = optLogoutRequestQuery;
    this.optSubjectSessionId = optSubjectSessionId;
  }

  /**
   * @return Optional
   */
  @JsonProperty("query")
  @JsonInclude(NON_NULL)
  public URI getOptLogoutRequestQuery() {
    return optLogoutRequestQuery;
  }

  /**
   * @return Optional
   */
  @JsonProperty("sub_sid")
  @JsonInclude(NON_NULL)
  public SubjectSessionId getOptSubjectSessionId() {
    return optSubjectSessionId;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((optLogoutRequestQuery == null) ? 0 : optLogoutRequestQuery.hashCode());
    result = prime * result + ((optSubjectSessionId == null) ? 0 : optSubjectSessionId.hashCode());
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final InitiateLogoutRequest other = (InitiateLogoutRequest) obj;
    if (optLogoutRequestQuery == null) {
      if (other.optLogoutRequestQuery != null) {
        return false;
      }
    } else if (!optLogoutRequestQuery.equals(other.optLogoutRequestQuery)) {
      return false;
    }
    if (optSubjectSessionId == null) {
      if (other.optSubjectSessionId != null) {
        return false;
      }
    } else if (!optSubjectSessionId.equals(other.optSubjectSessionId)) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "logoutRequestQuery=" + optLogoutRequestQuery + ", subjectSessionId=" + optSubjectSessionId;
  }

}
