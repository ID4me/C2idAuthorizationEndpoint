/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.logoutsession.entity;

import javax.annotation.concurrent.Immutable;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.Validate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@Immutable
public final class LogoutErrorResponse {

  public static final String EXPECTED_TYPE_VALUE = "error";

  private final String error;
  private final String description;

  /**
   * @param type
   *          Required to be equals to {@link #EXPECTED_TYPE_VALUE}
   * @param error
   *          Required
   * @param description
   *          Required
   */
  public LogoutErrorResponse(@JsonProperty("type") final String type,
      @JsonProperty("error") @NotNull final String error,
      @JsonProperty("error_description") @NotNull final String description) {
    Validate.isTrue(EXPECTED_TYPE_VALUE.equals(type), "Data does NOT describe a Logout Error response! Type was not '"
        + EXPECTED_TYPE_VALUE + "' as expected, but '%s'", type);
    Validate.notEmpty(error, "Missing error value");
    Validate.notEmpty(description, "Missing description value");
    this.error = error;
    this.description = description;
  }

  /**
   * @return Never <code>null</code>.
   */
  public String getError() {
    return error;
  }

  /**
   * @return Never <code>null</code>.
   */
  public String getDescription() {
    return description;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((description == null) ? 0 : description.hashCode());
    result = prime * result + ((error == null) ? 0 : error.hashCode());
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final LogoutErrorResponse other = (LogoutErrorResponse) obj;
    if (description == null) {
      if (other.description != null) {
        return false;
      }
    } else if (!description.equals(other.description)) {
      return false;
    }
    if (error == null) {
      if (other.error != null) {
        return false;
      }
    } else if (!error.equals(other.error)) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "error=" + error + ", description=" + description;
  }

}
