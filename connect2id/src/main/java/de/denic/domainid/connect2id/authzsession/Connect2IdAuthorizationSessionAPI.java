/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.authzsession;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import de.denic.domainid.DomainId;
import de.denic.domainid.connect2id.BaseConnect2IdClient;
import de.denic.domainid.connect2id.authzsession.entity.*;
import de.denic.domainid.connect2id.common.*;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.concurrent.Immutable;
import javax.security.auth.Subject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collection;
import java.util.Map;

import static javax.ws.rs.client.Entity.json;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
import static org.eclipse.jetty.http.HttpStatus.*;

/**
 * Client to access <a href="https://connect2id.com/products/server/docs/integration/authz-session">Connect2Id's
 * Authorisation Session API</a>.
 */
public interface Connect2IdAuthorizationSessionAPI {

  /**
   * @param openIdAuthenticationRequestQuery Required
   * @param optSubjectSessionId              Optional
   * @param reasonsOfClaims                  Optional
   * @return Never <code>null</code>
   */
  InitiateSessionResponse initiateSession(URI openIdAuthenticationRequestQuery, SubjectSessionId optSubjectSessionId,
                                          Map<Claim, String> reasonsOfClaims) throws Connect2IdException;

  /**
   * @param sessionId            Required
   * @param authenticatedSubject Required
   * @param authMethodReferences Optional
   * @return Never <code>null</code>
   */
  AuthenticateSubjectResponse authenticate(SessionId sessionId, Subject authenticatedSubject, Collection<AuthenticationMethodReference> authMethodReferences)
          throws Connect2IdException;

  /**
   * @param sessionId       Required
   * @param domainId        Required (to get read from 'data' section by C2ID ClaimSource)
   * @param consentedData   Required
   * @param rememberConsent Required
   * @return Never <code>null</code>
   */
  ProvisionOfConsentedDataResponse provide(SessionId sessionId, DomainId domainId, ConsentedData consentedData, boolean rememberConsent);

  /**
   * @param sessionId Required
   * @return Never <code>null</code>
   */
  SessionResponse getSession(SessionId sessionId);

  /**
   * @param sessionId Required
   * @return Never <code>null</code>
   */
  AuthzSessionOptionalData getOptionalDataOf(SessionId sessionId);

  /**
   * @param sessionId Required
   */
  FinalResponse cancelSession(SessionId sessionId);

  interface InitiateSessionResponse {

    /**
     * @param dispatcher Required
     * @return Depends on contract of provided <code>dispatcher</code>
     */
    <RETURN_TYPE> RETURN_TYPE dispatchedBy(InitiateSessionResponseDispatcher<RETURN_TYPE> dispatcher);

  }

  interface AuthenticateSubjectResponse {

    /**
     * @param dispatcher Required
     * @return Depends on contract of provided <code>dispatcher</code>
     */
    <RETURN_TYPE> RETURN_TYPE dispatchedBy(AuthenticateSubjectResponseDispatcher<RETURN_TYPE> dispatcher);

  }

  interface ProvisionOfConsentedDataResponse {

    /**
     * @param dispatcher Required
     * @return Depends on contract of provided <code>dispatcher</code>
     */
    <RETURN_TYPE> RETURN_TYPE dispatchedBy(ProvisionOfConsentedDataResponseDispatcher<RETURN_TYPE> dispatcher);

  }

  /**
   * Implementors should think of extending {@link ResponseDispatcherBase} or one of it's subclasses.
   */
  interface InitiateSessionResponseDispatcher<TYPE> extends AuthenticationPromptHandler<TYPE>,
          ConsentPromptHandler<TYPE>, FinalResponseHandler<TYPE>, NonRedirectableErrorHandler<TYPE> {

    // Intentionally left empty

  }

  /**
   * @see InitiateSessionResponseDispatcher
   */
  interface AuthenticateSubjectResponseDispatcher<TYPE>
          extends ConsentPromptHandler<TYPE>, FinalResponseHandler<TYPE>, NonRedirectableErrorHandler<TYPE> {

    // Intentionally left empty

  }

  /**
   * @see InitiateSessionResponseDispatcher
   */
  interface ProvisionOfConsentedDataResponseDispatcher<TYPE>
          extends ConsentPromptHandler<TYPE>, FinalResponseHandler<TYPE>, NonRedirectableErrorHandler<TYPE> {

    // Intentionally left empty

  }

  /**
   * @see InitiateSessionResponseDispatcher
   */
  interface FinalResponseHandler<RETURN_TYPE> {

    /**
     * @param response Required
     * @return Not <code>null</code>
     */
    RETURN_TYPE handle(FinalResponse response);

  }

  /**
   * @see InitiateSessionResponseDispatcher
   */
  interface ConsentPromptHandler<RETURN_TYPE> {

    /**
     * @param response Required
     * @return Not <code>null</code>
     */
    RETURN_TYPE handle(ConsentPromptResponse response);

  }

  /**
   * @see InitiateSessionResponseDispatcher
   */
  interface AuthenticationPromptHandler<RETURN_TYPE> {

    /**
     * @param response Required
     * @return Not <code>null</code>
     */
    RETURN_TYPE handle(AuthenticationPromptResponse response);

  }

  /**
   * @see InitiateSessionResponseDispatcher
   */
  interface NonRedirectableErrorHandler<RETURN_TYPE> {

    /**
     * @param response Required
     * @return Not <code>null</code>
     */
    RETURN_TYPE handle(NonRedirectableErrorResponse response);

  }

  @Immutable
  final class Impl extends BaseConnect2IdClient implements Connect2IdAuthorizationSessionAPI {

    private static final Logger LOG = LoggerFactory.getLogger(Impl.class);
    private static final String EMPTY_BODY_CONTENT = "";
    // Readers:
    private static final ObjectReader SESSION_RESPONSE_ENTITY_READER = OBJECT_MAPPER.readerFor(SessionResponse.class);
    private static final ObjectReader OPTIONAL_DATA_ENTITY_READER = OBJECT_MAPPER.readerFor(AuthzSessionOptionalData.class);
    private static final ObjectReader FINAL_RESPONSE_ENTITY_READER = OBJECT_MAPPER.readerFor(FinalResponse.class);
    // Writers:
    private static final ObjectWriter CONSENTED_DATA_SUBMISSION_REQUEST_ENTITY_WRITER = OBJECT_MAPPER
            .writerFor(ConsentedDataSubmissionRequest.class);
    private static final ObjectWriter AUTHENTICATED_SUBJECT_REQUEST_ENTITY_WRITER = OBJECT_MAPPER
            .writerFor(AuthenticatedSubjectRequest.class);
    private static final ObjectWriter INITIATE_SESSION_REQUEST_ENTITY_WRITER = OBJECT_MAPPER
            .writerFor(InitiateSessionRequest.class);

    private final Client restClient;
    private final URI connect2IdAuthorisationSessionURI;
    private final AccessToken bearerAccessToken;

    /**
     * @param connect2IdAuthorisationSessionURL Required
     * @param bearerAccessToken                 Required
     * @param restClient                        Required
     */
    public Impl(final URL connect2IdAuthorisationSessionURL, final AccessToken bearerAccessToken,
                final Client restClient) {
      super(LOG);
      Validate.notNull(restClient, "Missing REST client");
      Validate.notNull(connect2IdAuthorisationSessionURL, "Missing C2ID Authz Session API URL");
      Validate.notNull(bearerAccessToken, "Missing bearer access token");
      this.restClient = restClient;
      this.bearerAccessToken = bearerAccessToken;
      try {
        this.connect2IdAuthorisationSessionURI = connect2IdAuthorisationSessionURL.toURI();
      } catch (final URISyntaxException e) {
        throw new IllegalArgumentException(
                "C2ID Authz Session API URL " + connect2IdAuthorisationSessionURL, e);
      }
    }

    @Override
    public InitiateSessionResponse initiateSession(final URI openIdAuthenticationRequestQuery,
                                                   final SubjectSessionId optSubjectSessionId, final Map<Claim, String> reasonsOfClaims)
            throws Connect2IdException {
      Validate.notNull(openIdAuthenticationRequestQuery, "Missing OpenID Authentication Request query");
      final String jsonRequestBody;
      final InitiateSessionRequest requestEntity = new InitiateSessionRequest(openIdAuthenticationRequestQuery,
              optSubjectSessionId, reasonsOfClaims);
      try {
        jsonRequestBody = INITIATE_SESSION_REQUEST_ENTITY_WRITER.writeValueAsString(requestEntity);
      } catch (final IOException e) {
        throw new RuntimeException("Internal error marshalling " + requestEntity, e);
      }

      final WebTarget target = restClient.target(connect2IdAuthorisationSessionURI);
      final Invocation initiateSessionRequest = target.request(APPLICATION_JSON_TYPE)
              .header(AUTHORIZATION_HEADER, bearerAccessToken.withPrefix("Bearer")).buildPost(json(jsonRequestBody));
      LOG.info("Initiating C2ID Authz Session: Query '{}', subj. session ID '{}', reasons of claims: {}",
              openIdAuthenticationRequestQuery, optSubjectSessionId, reasonsOfClaims);
      final Pair<Response, String> invocationResult = invoke(initiateSessionRequest, target.getUri(), "POST",
              jsonRequestBody);
      final String responseBody = invocationResult.getRight();
      final Response initiateSessionResponse = invocationResult.getLeft();
      try {
        switch (initiateSessionResponse.getStatus()) {
          case OK_200:
            return new AnyResponseImpl(responseBody);
          case BAD_REQUEST_400:
            throw badRequestException(target, jsonRequestBody, responseBody);
          case UNAUTHORIZED_401:
            throw unauthorizedException(target, responseBody);
          case INTERNAL_SERVER_ERROR_500:
            throw serverException(target, responseBody);
          default:
            throw new NotImplementedException(
                    "Status: " + initiateSessionResponse.getStatus() + ", response: " + initiateSessionResponse);
        }
      } catch (final IOException e) {
        throw new RuntimeException("Internal error unmarshalling " + responseBody, e);
      } finally {
        initiateSessionResponse.close();
      }
    }

    @Override
    public AuthenticateSubjectResponse authenticate(final SessionId sessionId, final Subject authenticatedSubject,
                                                    final Collection<AuthenticationMethodReference> authMethodReferences)
            throws Connect2IdException {
      Validate.notNull(authenticatedSubject, "Missing authenticated subjects");
      final String jsonRequestBody;
      final AuthenticatedSubjectRequest requestEntity = new AuthenticatedSubjectRequest(authenticatedSubject,
              authMethodReferences);
      try {
        jsonRequestBody = AUTHENTICATED_SUBJECT_REQUEST_ENTITY_WRITER.writeValueAsString(requestEntity);
      } catch (final IOException e) {
        throw new RuntimeException("Internal error marshalling " + requestEntity, e);
      }

      final WebTarget target = webTargetOf(sessionId);
      final Builder builder = target.request(APPLICATION_JSON_TYPE).header(AUTHORIZATION_HEADER,
              bearerAccessToken.withPrefix("Bearer"));
      final Pair<Response, String> invocationResult = invokePUTWithJSON(builder, jsonRequestBody, target.getUri());
      final Response updateSessionResponse = invocationResult.getLeft();
      final String responseBody = invocationResult.getRight();
      try {
        switch (updateSessionResponse.getStatus()) {
          case OK_200:
            return new AnyResponseImpl(responseBody);
          case BAD_REQUEST_400:
            throw badRequestException(target, jsonRequestBody, responseBody);
          case NOT_FOUND_404:
            throw notFoundException(target, responseBody);
          case INTERNAL_SERVER_ERROR_500:
            throw serverException(target, responseBody);
          default:
            throw new NotImplementedException(
                    "Status: " + updateSessionResponse.getStatus() + ", response: " + updateSessionResponse);
        }
      } catch (final IOException e) {
        throw new RuntimeException("Internal error unmarshalling " + responseBody, e);
      } finally {
        updateSessionResponse.close();
      }
    }

    @Override
    public ProvisionOfConsentedDataResponse provide(final SessionId sessionId, final DomainId domainId,
                                                    final ConsentedData consentedData, final boolean rememberConsent) {
      Validate.notNull(consentedData, "Missing consented data");
      final String jsonRequestBody;
      final ConsentedDataSubmissionRequest requestEntity = new ConsentedDataSubmissionRequest(domainId, consentedData,
              rememberConsent);
      try {
        jsonRequestBody = CONSENTED_DATA_SUBMISSION_REQUEST_ENTITY_WRITER.writeValueAsString(requestEntity);
      } catch (final IOException e) {
        throw new RuntimeException("Internal error marshalling " + requestEntity, e);
      }

      final WebTarget target = webTargetOf(sessionId);
      final Builder builder = target.request(APPLICATION_JSON_TYPE).header(AUTHORIZATION_HEADER,
              bearerAccessToken.withPrefix("Bearer"));
      LOG.info("Providing consented data to C2ID Authz Session '{}': {}", sessionId, consentedData);
      final Pair<Response, String> invocationResult = invokePUTWithJSON(builder, jsonRequestBody,
              target.getUri());
      final Response updateSessionResponse = invocationResult.getLeft();
      final String responseBody = invocationResult.getRight();
      try {
        switch (updateSessionResponse.getStatus()) {
          case OK_200:
            return new AnyResponseImpl(responseBody);
          case BAD_REQUEST_400:
            throw badRequestException(target, jsonRequestBody, responseBody);
          case NOT_FOUND_404:
            throw notFoundException(target, responseBody);
          case INTERNAL_SERVER_ERROR_500:
            throw serverException(target, responseBody);
          default:
            throw new NotImplementedException(
                    "Status: " + updateSessionResponse.getStatus() + ", response: " + updateSessionResponse);
        }
      } catch (final IOException e) {
        throw new RuntimeException("Internal error unmarshalling " + responseBody, e);
      } finally {
        updateSessionResponse.close();
      }
    }

    @Override
    public SessionResponse getSession(final SessionId sessionId) {
      Validate.notNull(sessionId, "Missing Session ID");
      LOG.info("Loading C2ID Authz Session '{}'", sessionId);
      final WebTarget target = webTargetOf(sessionId);
      final Invocation getDataOfInitiatedSessionRequest = target.request(APPLICATION_JSON_TYPE)
              .header(AUTHORIZATION_HEADER, bearerAccessToken.withPrefix("Bearer")).buildGet();
      final Pair<Response, String> invocationResult = invokeGET(getDataOfInitiatedSessionRequest, target.getUri());
      final Response getDataOfInitiatedSessionResponse = invocationResult.getLeft();
      final String responseBody = invocationResult.getRight();
      try {
        switch (getDataOfInitiatedSessionResponse.getStatus()) {
          case OK_200:
            final SessionResponse response = SESSION_RESPONSE_ENTITY_READER.readValue(responseBody);
            LOG.info("C2ID Authz Session: {}", response);
            return response;
          case BAD_REQUEST_400:
            throw badRequestException(target, EMPTY_BODY_CONTENT, responseBody);
          case UNAUTHORIZED_401:
            throw unauthorizedException(target, responseBody);
          case NOT_FOUND_404:
            throw notFoundException(target, responseBody);
          case INTERNAL_SERVER_ERROR_500:
            throw serverException(target, responseBody);
          default:
            throw new NotImplementedException("Status: " + getDataOfInitiatedSessionResponse.getStatus()
                    + ", response: " + getDataOfInitiatedSessionResponse);
        }
      } catch (final IOException e) {
        throw new RuntimeException("Internal error unmarshalling " + responseBody, e);
      } finally {
        getDataOfInitiatedSessionResponse.close();
      }
    }

    @Override
    public AuthzSessionOptionalData getOptionalDataOf(final SessionId sessionId) {
      Validate.notNull(sessionId, "Missing Session ID");
      LOG.info("Loading optional data of C2ID Authz Session '{}'", sessionId);
      final WebTarget target = webTargetOf(sessionId).path("/data");
      final Invocation getDataOfInitiatedSessionRequest = target.request(APPLICATION_JSON_TYPE)
              .header(AUTHORIZATION_HEADER, bearerAccessToken.withPrefix("Bearer")).buildGet();
      final Pair<Response, String> invocationResult = invokeGET(getDataOfInitiatedSessionRequest, target.getUri());
      final Response getDataOfInitiatedSessionResponse = invocationResult.getLeft();
      final String responseBody = invocationResult.getRight();
      try {
        switch (getDataOfInitiatedSessionResponse.getStatus()) {
          case OK_200:
            final AuthzSessionOptionalData response = OPTIONAL_DATA_ENTITY_READER.readValue(responseBody);
            LOG.info("Optional data of C2ID Authz Session: {}", response);
            return response;
          case BAD_REQUEST_400:
            throw badRequestException(target, EMPTY_BODY_CONTENT, responseBody);
          case UNAUTHORIZED_401:
            throw unauthorizedException(target, responseBody);
          case NOT_FOUND_404:
            throw notFoundException(target, responseBody);
          case INTERNAL_SERVER_ERROR_500:
            throw serverException(target, responseBody);
          default:
            throw new NotImplementedException("Status: " + getDataOfInitiatedSessionResponse.getStatus()
                    + ", response: " + getDataOfInitiatedSessionResponse);
        }
      } catch (final IOException e) {
        throw new RuntimeException("Internal error unmarshalling " + responseBody, e);
      } finally {
        getDataOfInitiatedSessionResponse.close();
      }
    }

    @Override
    public FinalResponse cancelSession(final SessionId sessionId) {
      Validate.notNull(sessionId, "Missing session ID");
      LOG.info("Cancelling C2ID Authz Session '{}'", sessionId);
      final WebTarget target = webTargetOf(sessionId);
      final Invocation clearSessionRequest = target.request().accept(APPLICATION_JSON_TYPE)
              .header(AUTHORIZATION_HEADER, bearerAccessToken.withPrefix("Bearer")).buildDelete();
      final Pair<Response, String> invocationResult = invoke(clearSessionRequest, target.getUri(), "DELETE",
              EMPTY_BODY_CONTENT);
      final String responseBody = invocationResult.getRight();
      final Response cancelSessionResponse = invocationResult.getLeft();
      try {
        switch (cancelSessionResponse.getStatus()) {
          case OK_200:
            final FinalResponse response = FINAL_RESPONSE_ENTITY_READER.readValue(responseBody);
            LOG.info("Cancelled C2ID Authz Session '{}'", sessionId);
            return response;
          case NOT_FOUND_404:
            throw notFoundException(target, responseBody);
          case INTERNAL_SERVER_ERROR_500:
            throw serverException(target, responseBody);
          default:
            throw new NotImplementedException(
                    "Status: " + cancelSessionResponse.getStatus() + ", response: " + cancelSessionResponse);
        }
      } catch (final IOException e) {
        throw new RuntimeException("Internal error unmarshalling " + responseBody, e);
      } finally {
        cancelSessionResponse.close();
      }
    }

    private WebTarget webTargetOf(final SessionId sessionId) {
      return restClient.target(connect2IdAuthorisationSessionURI).path(sessionId.getValue());
    }

  }

  @Immutable
  final class AnyResponseImpl implements InitiateSessionResponse, AuthenticateSubjectResponse,
          ProvisionOfConsentedDataResponse {

    private static final Logger LOG = LoggerFactory.getLogger(AnyResponseImpl.class);
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private static final ObjectReader AUTHENTICATION_PROMPT_RESPONSE_ENTITY_READER = OBJECT_MAPPER
            .readerFor(AuthenticationPromptResponse.class);
    private static final ObjectReader CONSENT_PROMPT_RESPONSE_ENTITY_READER = OBJECT_MAPPER
            .readerFor(ConsentPromptResponse.class);
    private static final ObjectReader NON_REDIRECTABLE_ERROR_RESPONSE_ENTITY_READER = OBJECT_MAPPER
            .readerFor(NonRedirectableErrorResponse.class);

    private final JsonNode responseJSONTree;
    private final String typeValue;

    public AnyResponseImpl(final String responseBody) throws IOException {
      this.responseJSONTree = OBJECT_MAPPER.readTree(responseBody);
      final JsonNode typeNode = responseJSONTree.get("type");
      if (!typeNode.isValueNode()) {
        throw new RuntimeException(
                "Received C2ID Authz Session API response missing response type value node: " + responseJSONTree);
      }

      this.typeValue = typeNode.asText();
    }

    @Override
    public <RETURN_TYPE> RETURN_TYPE dispatchedBy(final InitiateSessionResponseDispatcher<RETURN_TYPE> dispatcher) {
      try {
        switch (typeValue) {
          case "auth":
            return authType(dispatcher);
          case "consent":
            return consentType(dispatcher);
          case "response":
            return responseType(dispatcher);
          case "error":
            return errorType(dispatcher);
          default:
            return unknownType();
        }
      } catch (final IOException e) {
        throw new RuntimeException(
                "Parsing received C2ID Authz Session API response failed: " + responseJSONTree, e);
      }
    }

    @Override
    public <RETURN_TYPE> RETURN_TYPE dispatchedBy(final AuthenticateSubjectResponseDispatcher<RETURN_TYPE> dispatcher) {
      try {
        switch (typeValue) {
          case "consent":
            return consentType(dispatcher);
          case "response":
            return responseType(dispatcher);
          case "error":
            return errorType(dispatcher);
          default:
            return unknownType();
        }
      } catch (final IOException e) {
        throw new RuntimeException(
                "Parsing received C2ID Authz Session API response failed: " + responseJSONTree, e);
      }
    }

    @Override
    public <RETURN_TYPE> RETURN_TYPE dispatchedBy(
            final ProvisionOfConsentedDataResponseDispatcher<RETURN_TYPE> dispatcher) {
      try {
        switch (typeValue) {
          case "consent":
            return consentType(dispatcher);
          case "response":
            return responseType(dispatcher);
          case "error":
            return errorType(dispatcher);
          default:
            return unknownType();
        }
      } catch (final IOException e) {
        throw new RuntimeException(
                "Parsing received C2ID Authz Session API response failed: " + responseJSONTree, e);
      }
    }

    private <RETURN_TYPE> RETURN_TYPE unknownType() {
      throw new RuntimeException("Received C2ID Authz Session API response of unknown response type '"
              + typeValue + "': " + responseJSONTree);
    }

    private <RETURN_TYPE> RETURN_TYPE errorType(final NonRedirectableErrorHandler<RETURN_TYPE> handler)
            throws IOException {
      final NonRedirectableErrorResponse response = NON_REDIRECTABLE_ERROR_RESPONSE_ENTITY_READER
              .readValue(responseJSONTree);
      LOG.info("Non-redirectable error on C2ID Authz Session, response: {}", response);
      return handler.handle(response);
    }

    private <RETURN_TYPE> RETURN_TYPE responseType(final FinalResponseHandler<RETURN_TYPE> handler)
            throws IOException {
      final FinalResponse response = Impl.FINAL_RESPONSE_ENTITY_READER.readValue(responseJSONTree);
      LOG.info("Redirecting to client after finalizing C2ID Authz Session, response: {}",
              response);
      return handler.handle(response);
    }

    private <RETURN_TYPE> RETURN_TYPE consentType(final ConsentPromptHandler<RETURN_TYPE> handler)
            throws IOException {
      final ConsentPromptResponse response = CONSENT_PROMPT_RESPONSE_ENTITY_READER.readValue(responseJSONTree);
      LOG.info("Proceed with consenting step. Response: {}", response);
      return handler.handle(response);
    }

    private <RETURN_TYPE> RETURN_TYPE authType(final AuthenticationPromptHandler<RETURN_TYPE> handler)
            throws IOException {
      final AuthenticationPromptResponse response = AUTHENTICATION_PROMPT_RESPONSE_ENTITY_READER
              .readValue(responseJSONTree);
      LOG.info("Proceed with authentication step. Response: {}", response);
      return handler.handle(response);
    }

  }

}
