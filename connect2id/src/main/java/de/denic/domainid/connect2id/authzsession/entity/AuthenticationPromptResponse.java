/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.authzsession.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.denic.domainid.connect2id.authzsession.SessionId;
import de.denic.domainid.connect2id.common.Display;
import org.apache.commons.lang3.Validate;

import javax.annotation.concurrent.Immutable;
import java.util.Objects;

// TODO: Ignoriere erstmal unbekannte weiter JSON-Objekte, insbesonder "sub_session" ...
@JsonIgnoreProperties(ignoreUnknown = true)
@Immutable
public final class AuthenticationPromptResponse {

  private static final String EXPECTED_TYPE_VALUE = "auth";

  private final SessionId sessionId;
  private final Display display;
  private final boolean selectAccount;

  public AuthenticationPromptResponse(@JsonProperty("type") final String type,
                                      @JsonProperty("sid") final SessionId sessionId, @JsonProperty("display") final Display display,
                                      @JsonProperty("select_account") final boolean selectAccount) {
    Validate.isTrue(EXPECTED_TYPE_VALUE.equals(type), "Data does NOT describe an Authentication prompt! Type was not '"
            + EXPECTED_TYPE_VALUE + "' as expected, but '%s'", type);
    Validate.notNull(sessionId, "Missing session ID");
    Validate.notNull(display, "Missing display field");
    this.sessionId = sessionId;
    this.display = display;
    this.selectAccount = selectAccount;
  }

  /**
   * @return Never <code>null</code>.
   */
  public SessionId getConnect2IdAuthorisationSessionId() {
    return sessionId;
  }

  /**
   * @return Never <code>null</code>.
   */
  public Display getDisplay() {
    return display;
  }

  public boolean isSelectAccount() {
    return selectAccount;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    AuthenticationPromptResponse that = (AuthenticationPromptResponse) o;
    return selectAccount == that.selectAccount &&
            Objects.equals(sessionId, that.sessionId) &&
            Objects.equals(display, that.display);
  }

  @Override
  public int hashCode() {
    return Objects.hash(sessionId, display, selectAccount);
  }

  @Override
  public String toString() {
    return "sessionId=" + sessionId + ", display=" + display + ", selectAccount=" + selectAccount;
  }

}
