/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.authzsession;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.denic.domainid.connect2id.common.Claim;
import de.denic.domainid.connect2id.common.Scope;

import javax.annotation.concurrent.Immutable;
import java.util.*;

import static java.util.Collections.*;
import static java.util.stream.Collectors.toList;

@Immutable
public final class ConsentedData {

  public static final ConsentedData NO_CONSENTED_DATA = new ConsentedData(emptyList(), emptyList(), emptyList());

  private final Set<Scope> scopes;
  private final Set<Claim> claims;
  private final Set<Claim> rejectedClaims;

  /**
   * @param scopes         Optional
   * @param claims         Optional
   * @param rejectedClaims Optional
   */
  public <A extends Collection<Scope>, B extends Collection<Claim>> ConsentedData(final A scopes, final B claims,
                                                                                  final B rejectedClaims) {
    this.scopes = (scopes == null || scopes.isEmpty() ? emptySet() : unmodifiableSet(new HashSet<>(scopes)));
    this.claims = (claims == null || claims.isEmpty() ? emptySet() : unmodifiableSet(new HashSet<>(claims)));
    this.rejectedClaims = (rejectedClaims == null ? emptySet() : unmodifiableSet(new HashSet<>(rejectedClaims)));
  }

  /**
   * @see #ConsentedData(Collection, Collection, Collection)
   */
  public <B extends Collection<Claim>> ConsentedData(final Scope scope, final B claims, final B rejectedClaims) {
    this(singletonList(scope), claims, rejectedClaims);
  }

  /**
   * @see #getScopes()
   */
  @JsonProperty("scope")
  public List<String> getScopeValues() {
    return scopes.stream().map(Scope::toString).collect(toList());
  }

  /**
   * @return Never <code>null</code> but unmodifiable.
   */
  @JsonIgnore
  public Set<Scope> getScopes() {
    return scopes;
  }

  /**
   * @see #getClaims()
   */
  @JsonProperty("claims")
  public List<String> getClaimValues() {
    return claims.stream().map(Claim::toString).collect(toList());
  }

  /**
   * @return Never <code>null</code> but unmodifiable.
   */
  @JsonIgnore
  public Set<Claim> getClaims() {
    return claims;
  }

  /**
   * @return Never <code>null</code> but unmodifiable.
   */
  @JsonIgnore
  public Set<Claim> getRejectedClaims() {
    return rejectedClaims;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    ConsentedData that = (ConsentedData) o;
    return Objects.equals(scopes, that.scopes) &&
            Objects.equals(claims, that.claims) &&
            Objects.equals(rejectedClaims, that.rejectedClaims);
  }

  @Override
  public int hashCode() {
    return Objects.hash(scopes, claims, rejectedClaims);
  }

  @Override
  public String toString() {
    return "scopes " + scopes + ", claims " + claims + ", rejected " + rejectedClaims;
  }

}
