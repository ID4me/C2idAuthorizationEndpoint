/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.common;

import javax.annotation.concurrent.NotThreadSafe;

import static org.apache.commons.lang3.StringUtils.join;

@NotThreadSafe
public abstract class Connect2IdException extends RuntimeException {

  protected static final String CONTEXT_SEPARATOR = "; ";
  private static final long serialVersionUID = 8508557729135126674L;

  public Connect2IdException(final String message) {
    this(message, null);
  }

  public Connect2IdException(final String message, final Throwable throwable) {
    super(message, throwable);
  }

  protected static final String messageWithContext(final Object message, final Object... context) {
    return message.toString() + (context.length < 1 ? "" : CONTEXT_SEPARATOR + join(context, CONTEXT_SEPARATOR));
  }

}
