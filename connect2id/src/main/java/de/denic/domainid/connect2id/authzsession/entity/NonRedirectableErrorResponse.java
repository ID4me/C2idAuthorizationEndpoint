/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.authzsession.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.Validate;

import javax.annotation.concurrent.Immutable;

@JsonIgnoreProperties(ignoreUnknown = true)
@Immutable
public final class NonRedirectableErrorResponse {

  private static final String EXPECTED_TYPE_VALUE = "error";

  private final String error;
  private final String errorDescription;

  /**
   * @param type             Required
   * @param error            Required
   * @param errorDescription Required
   */
  public NonRedirectableErrorResponse(@JsonProperty("type") final String type,
                                      @JsonProperty("error") final String error, @JsonProperty("error_description") final String errorDescription) {
    Validate.isTrue(EXPECTED_TYPE_VALUE.equals(type), "Data does NOT describe an Non Redirectable Error! Type was not '"
            + EXPECTED_TYPE_VALUE + "' as expected, but '%s'", type);
    Validate.notEmpty(error, "Missing error field");
    Validate.notEmpty(errorDescription, "Missing error description");
    this.error = error;
    this.errorDescription = errorDescription;
  }

  /**
   * @return Never <code>null</code> nor empty.
   */
  @JsonProperty("error")
  public String getError() {
    return error;
  }

  /**
   * @return Never <code>null</code> nor empty.
   */
  @JsonProperty("error_description")
  public String getErrorDescription() {
    return errorDescription;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((error == null) ? 0 : error.hashCode());
    result = prime * result + ((errorDescription == null) ? 0 : errorDescription.hashCode());
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final NonRedirectableErrorResponse other = (NonRedirectableErrorResponse) obj;
    if (error == null) {
      if (other.error != null) {
        return false;
      }
    } else if (!error.equals(other.error)) {
      return false;
    }
    if (errorDescription == null) {
      if (other.errorDescription != null) {
        return false;
      }
    } else if (!errorDescription.equals(other.errorDescription)) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "error=" + error + ", errorDescription=" + errorDescription;
  }

}
