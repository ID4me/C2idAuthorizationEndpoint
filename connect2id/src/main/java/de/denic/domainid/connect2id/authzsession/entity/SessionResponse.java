/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.authzsession.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.denic.domainid.connect2id.authzsession.ResponseType;
import de.denic.domainid.connect2id.authzsession.State;
import de.denic.domainid.connect2id.common.*;
import de.denic.domainid.openid.OpenIdAuthenticationRequestParamKeys;
import org.apache.commons.lang3.Validate;
import org.eclipse.jetty.util.UrlEncoded;

import javax.annotation.concurrent.Immutable;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.*;
import java.util.concurrent.Callable;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;
import static java.util.Collections.*;
import static org.apache.commons.lang3.StringUtils.join;

@JsonIgnoreProperties(ignoreUnknown = true)
@Immutable
public final class SessionResponse {

  private final OpenIdConnectAuthenticationRequest initialRequest;
  private final SubjectSessionId subjectSessionId;
  private final AuthzSessionOptionalData optionalData;

  /**
   * @param initialRequest   Required
   * @param subjectSessionId Optional
   * @param optionalData     Optional
   */
  public SessionResponse(
          @JsonProperty("auth_req") @NotNull final OpenIdConnectAuthenticationRequest initialRequest,
          @JsonProperty("sub_sid") final SubjectSessionId subjectSessionId,
          @JsonProperty("data") final AuthzSessionOptionalData optionalData) {
    Validate.notNull(initialRequest, "Missing data of initial Authentication Request");
    this.initialRequest = initialRequest;
    this.subjectSessionId = subjectSessionId;
    this.optionalData = (optionalData == null ? new AuthzSessionOptionalData(null) : optionalData);
  }

  /**
   * @return Never <code>null</code>
   */
  @JsonProperty("auth_req")
  public OpenIdConnectAuthenticationRequest getInitialRequest() {
    return initialRequest;
  }

  /**
   * @return Optional
   */
  @JsonProperty("sub_sid")
  public SubjectSessionId getSubjectSessionId() {
    return subjectSessionId;
  }

  /**
   * @return Never <code>null</code>
   */
  @JsonProperty("data")
  public AuthzSessionOptionalData getOptionalData() {
    return optionalData;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    SessionResponse that = (SessionResponse) o;
    return Objects.equals(initialRequest, that.initialRequest) &&
            Objects.equals(subjectSessionId, that.subjectSessionId) &&
            Objects.equals(optionalData, that.optionalData);
  }

  @Override
  public int hashCode() {
    return Objects.hash(initialRequest, subjectSessionId, optionalData);
  }

  @Override
  public String toString() {
    return "subjectSessionID=" + subjectSessionId + ", InitialAuthzRequest=[" + initialRequest + "], " +
            "data=[" + optionalData + "]";
  }

  // TODO: Duplizierter Typ!
  @JsonIgnoreProperties(ignoreUnknown = true)
  @JsonInclude(NON_EMPTY)
  @Immutable
  public static final class OpenIdConnectAuthenticationRequest implements OpenIdAuthenticationRequestParamKeys {

    private static final Charset UTF_8 = Charset.forName("UTF-8");

    private final ResponseType responseType;
    private final ClientId clientId;
    private final Set<Scope> scopes;
    private final String nonce;
    private final Display display;
    private final List<Locale> userInterfaceLocales;
    private final RedirectURI redirectURI;
    private final State state;
    private final Set<Prompt> prompts;
    private final Claims claims;
    private final List<Locale> claimLocales;
    private final String pkceCodeChallenge;
    private final String pkceCodeChallengeMethod;
    private final String customParameterName;

    /**
     * @param responseType            Required
     * @param clientId                Required
     * @param redirectURI             Required
     * @param scopes                  Optional
     * @param state                   Optional
     * @param nonce                   Optional
     * @param display                 Optional
     * @param prompts                 Optional
     * @param userInterfaceLocales    Optional
     * @param claims                  Optional
     * @param claimLocales            Optional
     * @param pkceCodeChallenge       Optional
     * @param pkceCodeChallengeMethod Optional
     * @param customParameterName     Optional
     */
    private OpenIdConnectAuthenticationRequest(@NotNull @JsonProperty("response_type") final ResponseType responseType,
                                               @NotNull @JsonProperty("client_id") final ClientId clientId,
                                               @NotNull @JsonProperty("redirect_uri") final RedirectURI redirectURI,
                                               @JsonProperty("scope") final Collection<Scope> scopes, @JsonProperty("state") final State state,
                                               @JsonProperty("nonce") final String nonce, @JsonProperty("display") final Display display,
                                               @JsonProperty("prompt") final Set<Prompt> prompts,
                                               @JsonProperty("ui_locales") final Collection<Locale> userInterfaceLocales,
                                               @JsonProperty("claims") final Claims claims,
                                               @JsonProperty("claims_locales") final Collection<Locale> claimLocales,
                                               @JsonProperty("code_challenge") final String pkceCodeChallenge,
                                               @JsonProperty("code_challenge_method") final String pkceCodeChallengeMethod,
                                               @JsonProperty("custom_parameter_name") final String customParameterName) {
      Validate.notNull(clientId, "Missing Client ID");
      Validate.notNull(responseType, "Missing response type");
      Validate.notNull(redirectURI, "Missing redirect URI");
      this.clientId = clientId;
      this.display = display;
      this.nonce = nonce;
      this.responseType = responseType;
      this.redirectURI = redirectURI;
      this.state = state;
      this.scopes = (scopes == null || scopes.isEmpty() ? emptySet() : unmodifiableSet(new HashSet<>(scopes)));
      this.prompts = (prompts == null || prompts.isEmpty() ? emptySet() : unmodifiableSet(new HashSet<>(prompts)));
      this.userInterfaceLocales = (userInterfaceLocales == null || userInterfaceLocales.isEmpty() ? emptyList()
              : unmodifiableList(new ArrayList<>(userInterfaceLocales)));
      this.claims = claims;
      this.claimLocales = (claimLocales == null || claimLocales.isEmpty() ? emptyList()
              : unmodifiableList(new ArrayList<>(claimLocales)));
      this.pkceCodeChallenge = pkceCodeChallenge;
      this.pkceCodeChallengeMethod = pkceCodeChallengeMethod;
      this.customParameterName = customParameterName;
    }

    /**
     * Initialises all optionally parameters to <code>null</code>.
     *
     * @param responseType Required
     * @param clientId     Required
     * @param redirectURI  Required
     */
    public OpenIdConnectAuthenticationRequest(final ResponseType responseType, final ClientId clientId,
                                              final RedirectURI redirectURI) {
      this(responseType, clientId, redirectURI, null, null, null, null, null, null, null, null, null, null, null);
    }

    private static void appendOptionalParamTo(final StringBuilder target, final String key,
                                              final UrlEncodedValue value) {
      appendOptionalParamTo(target, key, value == null ? null : value::getEncoded);
    }

    private static void appendOptionalParamTo(final StringBuilder target, final String key, final Object value) {
      appendOptionalParamTo(target, key, value == null ? null : value::toString);
    }

    private static void appendOptionalParamTo(final StringBuilder target, final String key,
                                              final Callable<?> valueProvider) {
      if (valueProvider == null) {
        return;
      }

      target.append('&').append(key).append('=');
      try {
        target.append(valueProvider.call());
      } catch (final Exception e) {
        throw new RuntimeException("Internal error", e);
      }
    }

    private static void appendParamTo(final StringBuilder target, final String key, final UrlEncodedValue value) {
      Validate.notNull(value, "Missing value");
      appendOptionalParamTo(target, key, value::getEncoded);
    }

    private static void appendParamTo(final StringBuilder target, final String key, final Object value) {
      Validate.notNull(value, "Missing value");
      appendOptionalParamTo(target, key, value::toString);
    }

    /**
     * @return Never <code>null</code>
     */
    @JsonProperty("client_id")
    public ClientId getClientId() {
      return clientId;
    }

    /**
     * @return Never <code>null</code>
     */
    @JsonProperty("response_type")
    public ResponseType getResponseType() {
      return responseType;
    }

    @JsonProperty("scope")
    public Set<Scope> getScopes() {
      return scopes;
    }

    @JsonProperty("nonce")
    public String getNonce() {
      return nonce;
    }

    @JsonProperty("display")
    public Display getDisplay() {
      return display;
    }

    /**
     * @return Not <code></code> but unmodifiable.
     */
    @JsonProperty("ui_locales")
    public List<Locale> getUserInterfaceLocales() {
      return userInterfaceLocales;
    }

    @JsonProperty("redirect_uri")
    public RedirectURI getRedirectURI() {
      return redirectURI;
    }

    @JsonProperty("state")
    public State getState() {
      return state;
    }

    /**
     * @return Not <code></code> but unmodifiable.
     */
    @JsonProperty("prompt")
    public Set<Prompt> getPrompt() {
      return prompts;
    }

    @JsonProperty("claims")
    public Claims getClaims() {
      return claims;
    }

    /**
     * @return Not <code></code> but unmodifiable.
     */
    @JsonIgnore
    public List<Locale> getClaimLocales() {
      return claimLocales;
    }

    @JsonIgnore
    public String getPkceCodeChallenge() {
      return pkceCodeChallenge;
    }

    @JsonIgnore
    public String getPkceCodeChallengeMethod() {
      return pkceCodeChallengeMethod;
    }

    @JsonIgnore
    public String getCustomParameterName() {
      return customParameterName;
    }

    /**
     * @return Never <code>null</code>
     */
    public URI toURI() {
      return toUriSkippingPrompt(false);
    }

    private URI toUriSkippingPrompt(final boolean skippingPrompt) {
      final String urlEncodedClaimsJsonString;
      try {
        urlEncodedClaimsJsonString = UrlEncoded.encodeString(new ObjectMapper().writeValueAsString(claims), UTF_8);
      } catch (final JsonProcessingException e) {
        throw new RuntimeException("Internal error", e);
      }

      final StringBuilder uriValue = new StringBuilder(64).append(RESPONSE_TYPE).append('=').append(responseType);
      appendParamTo(uriValue, CLIENT_ID, clientId);
      appendParamTo(uriValue, REDIRECT_URI, redirectURI);
      appendParamTo(uriValue, SCOPE, join(scopes, '+'));
      appendParamTo(uriValue, STATE, state);
      appendOptionalParamTo(uriValue, NONCE, nonce);
      appendOptionalParamTo(uriValue, DISPLAY, display);
      if (!skippingPrompt) {
        appendOptionalParamTo(uriValue, PROMPT, join(prompts, '+'));
      }
      appendOptionalParamTo(uriValue, "ui_locales", join(userInterfaceLocales, '+'));
      appendOptionalParamTo(uriValue, CLAIMS, urlEncodedClaimsJsonString);
      return URI.create(uriValue.toString());
    }

    /**
     * @return Never <code>null</code>
     */
    public URI toUriSkippingPrompt() {
      return toUriSkippingPrompt(true);
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      OpenIdConnectAuthenticationRequest that = (OpenIdConnectAuthenticationRequest) o;
      return Objects.equals(responseType, that.responseType) &&
              Objects.equals(clientId, that.clientId) &&
              Objects.equals(scopes, that.scopes) &&
              Objects.equals(nonce, that.nonce) &&
              Objects.equals(display, that.display) &&
              Objects.equals(userInterfaceLocales, that.userInterfaceLocales) &&
              Objects.equals(redirectURI, that.redirectURI) &&
              Objects.equals(state, that.state) &&
              Objects.equals(prompts, that.prompts) &&
              Objects.equals(claims, that.claims) &&
              Objects.equals(claimLocales, that.claimLocales) &&
              Objects.equals(pkceCodeChallenge, that.pkceCodeChallenge) &&
              Objects.equals(pkceCodeChallengeMethod, that.pkceCodeChallengeMethod) &&
              Objects.equals(customParameterName, that.customParameterName);
    }

    @Override
    public int hashCode() {
      return Objects.hash(responseType, clientId, scopes, nonce, display, userInterfaceLocales, redirectURI, state, prompts, claims, claimLocales, pkceCodeChallenge, pkceCodeChallengeMethod, customParameterName);
    }

    @Override
    public String toString() {
      return "auth. request (responseType=" + responseType + ", clientId=" + clientId + ", scopes=" + scopes
              + ", nonce=" + nonce + ", display=" + display + ", prompt=" + prompts + ", userInterfaceLocales="
              + userInterfaceLocales + ", claims=" + claims + ", claimLocales=" + claimLocales + ", pkceCodeChallenge="
              + pkceCodeChallenge + ", pkceCodeChallengeMethode=" + pkceCodeChallengeMethod + ", customParamName="
              + customParameterName + ")";
    }

  }

  @JsonIgnoreProperties(ignoreUnknown = true)
  @JsonInclude(NON_EMPTY)
  @Immutable
  public static final class Claims {

    private final JsonNode userInfo;
    private final JsonNode idToken;

    /**
     * @param userInfo Optional
     * @param idToken  Optional
     */
    public Claims(@JsonProperty("userinfo") final JsonNode userInfo, @JsonProperty("id_token") final JsonNode idToken) {
      this.idToken = idToken;
      this.userInfo = userInfo;
    }

    /**
     * @return Never <code>null</code> but unmodifiable.
     */
    @JsonProperty("id_token")
    public JsonNode getIdToken() {
      return idToken;
    }

    /**
     * @return Never <code>null</code> but unmodifiable.
     */
    @JsonProperty("userinfo")
    public JsonNode getUserInfo() {
      return userInfo;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Claims claims = (Claims) o;
      return Objects.equals(userInfo, claims.userInfo) &&
              Objects.equals(idToken, claims.idToken);
    }

    @Override
    public int hashCode() {
      return Objects.hash(userInfo, idToken);
    }

    @Override
    public String toString() {
      return "idTokens=" + idToken + ", userInfos=" + userInfo;
    }

  }

}
