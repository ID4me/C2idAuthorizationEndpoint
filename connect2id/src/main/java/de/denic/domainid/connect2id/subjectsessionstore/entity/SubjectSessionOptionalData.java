/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.subjectsessionstore.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.denic.domainid.DomainId;
import de.denic.domainid.connect2id.common.Claim;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.concurrent.Immutable;
import java.util.*;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;
import static java.util.Collections.emptySet;
import static java.util.Collections.unmodifiableSet;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toSet;

@JsonIgnoreProperties(ignoreUnknown = true)
@Immutable
public final class SubjectSessionOptionalData {

  private static final Logger LOG = LoggerFactory.getLogger(SubjectSessionOptionalData.class);

  private final DomainId domainId;
  private final Set<Claim> rejectedClaims;

  /**
   * @param domainId       Optional
   * @param rejectedClaims Optional
   */
  public <C extends Collection<Claim>> SubjectSessionOptionalData(
          @JsonProperty("identifier") final DomainId domainId, @JsonProperty("rejected_claims") final C
          rejectedClaims) {
    this.domainId = domainId;
    if (domainId == null) {
      LOG.warn("Missing domain ID within Subject Session optional data field");
    }
    this.rejectedClaims = (rejectedClaims == null ? emptySet() : unmodifiableSet(new HashSet<>(rejectedClaims)));
  }

  /**
   * @see #SubjectSessionOptionalData(DomainId, Collection)
   */
  public <C extends Collection<Claim>> SubjectSessionOptionalData(final DomainId domainId) {
    this(domainId, null);
  }

  /**
   * @return Never <code>null</code> but unmodifiable.
   */
  @JsonIgnore
  public Set<Claim> getRejectedClaims() {
    return rejectedClaims;
  }

  /**
   * @return Never <code>null</code>
   */
  @JsonIgnore
  public Optional<DomainId> getDomainId() {
    return ofNullable(domainId);
  }

  /**
   * @return Optional
   */
  @JsonProperty("identifier")
  @JsonInclude(NON_EMPTY)
  public String getIdentifier() {
    return (domainId == null ? null : domainId.getName());
  }

  /**
   * @return Never <code>null</code> but unmodifiable.
   */
  @JsonProperty("rejected_claims")
  @JsonInclude(NON_EMPTY)
  public Set<String> getRejectedClaimsValue() {
    return rejectedClaims.stream().map(Claim::toString).collect(toSet());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    SubjectSessionOptionalData that = (SubjectSessionOptionalData) o;
    return Objects.equals(domainId, that.domainId) &&
            Objects.equals(rejectedClaims, that.rejectedClaims);
  }

  @Override
  public int hashCode() {
    return Objects.hash(domainId, rejectedClaims);
  }

  @Override
  public String toString() {
    return domainId + " [rejectedClaims=" + rejectedClaims + "]";
  }

  /**
   * @param otherDomainId Optional
   * @return Never <code>null</code>; does <strong>not</strong> modify the current instance but creates a new one!
   */
  public SubjectSessionOptionalData withDomainId(final DomainId otherDomainId) {
    return new SubjectSessionOptionalData(otherDomainId, this.rejectedClaims);
  }

}