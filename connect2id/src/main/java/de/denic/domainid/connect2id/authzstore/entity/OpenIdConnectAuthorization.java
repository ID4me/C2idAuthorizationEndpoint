/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.authzstore.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.denic.domainid.connect2id.subjectsessionstore.entity.SubjectSessionOptionalData;
import de.denic.domainid.connect2id.common.Claim;
import de.denic.domainid.connect2id.common.ClientId;
import de.denic.domainid.connect2id.common.Scope;
import org.apache.commons.lang3.Validate;

import javax.annotation.concurrent.Immutable;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.*;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;
import static java.time.Duration.ofSeconds;
import static java.time.LocalDateTime.ofEpochSecond;
import static java.time.ZoneOffset.UTC;
import static java.time.ZonedDateTime.of;
import static java.util.Collections.emptyList;
import static java.util.Collections.unmodifiableList;
import static java.util.stream.Collectors.toList;

@JsonInclude(NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Immutable
public final class OpenIdConnectAuthorization {

  private static final int NO_NANO_SECONDS = 0;

  private final String subject;
  private final ClientId clientId;
  private final List<Scope> authorisationScopes;
  private final List<Scope> savedScopes;
  private final Boolean longLived;
  private final List<Claim> consentedClaims;
  private final List<Claim> savedConsentedClaims;
  private final List<Locale> preferredClaimLocales;
  private final URI redirectionURI;
  private final Boolean issueRefreshToken;
  private final String refreshToken;
  private final Duration refreshTokenLifetime;
  private final Duration accessTokenLifetime;
  private final String accessTokenEncoding;
  private final Boolean accessTokenEncryption;
  private final String issuer;
  private final ZonedDateTime issuedAt;
  private final ZonedDateTime updatedAt;
  private final String authorisationAudience;
  private final String openIdConnectIdToken;
  private final String pkceCodeChallenge;
  private final String pkceCodeChallengeMethod;
  private final SubjectSessionOptionalData optionalData;

  /**
   * @param subject                  Required
   * @param clientId                 Required
   * @param authorisationScopes      Optional
   * @param savedScopes              Optional
   * @param longLived                Required
   * @param consentedClaims          Optional
   * @param savedConsentedClaims     Optional
   * @param optionalData             Optional
   * @param issueRefreshToken        Optional
   * @param redirectionURI           Optional
   * @param refreshToken             Optional
   * @param refreshTokenLifetimeSecs Optional
   * @param accessTokenLifetimeSecs  Optional
   * @param accessTokenEncoding      Optional
   * @param accessTokenEncryption    Optional
   * @param issuer                   Optional
   * @param issuedAt                 Optional
   * @param updatedAt                Optional
   * @param authorisationAudience    Optional
   * @param openIdConnectIdToken     Optional
   * @param preferredClaimLocales    Optional
   * @param pkceCodeChallenge        Optional
   * @param pkceCodeChallengeMethod  Optional
   */
  public OpenIdConnectAuthorization(@JsonProperty("sub") @NotNull final String subject,
                                    @JsonProperty("cid") @NotNull final ClientId clientId, @JsonProperty("scp") final List<Scope> authorisationScopes,
                                    @JsonProperty("scs") final List<Scope> savedScopes, @JsonProperty("rur") final URI redirectionURI,
                                    @JsonProperty("lng") final Boolean longLived,
          /* @JsonProperty("act") final Actor actor, */@JsonProperty("irt") final Boolean issueRefreshToken,
                                    @JsonProperty("rft") final String refreshToken, @JsonProperty("rtl") final Long refreshTokenLifetimeSecs,
                                    @JsonProperty("atl") final Long accessTokenLifetimeSecs, @JsonProperty("ate") final String accessTokenEncoding,
                                    @JsonProperty("atc") final Boolean accessTokenEncryption, @JsonProperty("iss") final String issuer,
                                    @JsonProperty("iat") final Long issuedAt, @JsonProperty("uat") final Long updatedAt,
                                    @JsonProperty("aud") final String authorisationAudience, @JsonProperty("idt") final String openIdConnectIdToken,
                                    @JsonProperty("clm") final List<Claim> consentedClaims,
                                    @JsonProperty("cls") final List<Claim> savedConsentedClaims,
                                    @JsonProperty("cll") final List<Locale> preferredClaimLocales/*
   * ,
   *
   * @JsonProperty("uip") final UserInfoEndpoint
   * userInfoEndpoint
   */,
                                    @JsonProperty("cch") final String pkceCodeChallenge, @JsonProperty("ccm") final String pkceCodeChallengeMethod,
                                    @JsonProperty("dat") final SubjectSessionOptionalData optionalData) {
    Validate.notEmpty(subject, "Missing subject");
    Validate.notNull(clientId, "Missing client ID");
    this.subject = subject;
    this.clientId = clientId;
    this.longLived = longLived;
    this.redirectionURI = redirectionURI;
    this.issueRefreshToken = issueRefreshToken;
    this.refreshToken = refreshToken;
    this.refreshTokenLifetime = (refreshTokenLifetimeSecs == null ? null : ofSeconds(refreshTokenLifetimeSecs));
    this.accessTokenLifetime = (accessTokenLifetimeSecs == null ? null : ofSeconds(accessTokenLifetimeSecs));
    this.accessTokenEncoding = accessTokenEncoding;
    this.accessTokenEncryption = accessTokenEncryption;
    this.issuer = issuer;
    this.issuedAt = (issuedAt == null ? null : of(ofEpochSecond(issuedAt, NO_NANO_SECONDS, UTC), UTC));
    this.updatedAt = (updatedAt == null ? null : of(ofEpochSecond(updatedAt, NO_NANO_SECONDS, UTC), UTC));
    this.authorisationAudience = authorisationAudience;
    this.openIdConnectIdToken = openIdConnectIdToken;
    this.pkceCodeChallenge = pkceCodeChallenge;
    this.pkceCodeChallengeMethod = pkceCodeChallengeMethod;
    this.preferredClaimLocales = (preferredClaimLocales == null ? emptyList()
            : unmodifiableList(new ArrayList<>(preferredClaimLocales)));
    this.authorisationScopes = (authorisationScopes == null ? emptyList()
            : unmodifiableList(new ArrayList<>(authorisationScopes)));
    this.savedScopes = (savedScopes == null ? emptyList() : unmodifiableList(new ArrayList<>(savedScopes)));
    this.consentedClaims = (consentedClaims == null ? emptyList() : unmodifiableList(new ArrayList<>(consentedClaims)));
    this.savedConsentedClaims = (savedConsentedClaims == null ? emptyList()
            : unmodifiableList(new ArrayList<>(savedConsentedClaims)));
    this.optionalData = optionalData;
  }

  /**
   * @return Never <code>null</code>.
   */
  @JsonProperty("sub")
  public String getSubject() {
    return subject;
  }

  /**
   * @return Never <code>null</code>.
   */
  @JsonIgnore
  public ClientId getClientId() {
    return clientId;
  }

  /**
   * @return Never <code>null</code>.
   */
  @JsonProperty("cid")
  public String getClientIdValue() {
    return clientId.getPlain();
  }

  /**
   * @return Never <code>null</code>, but unmodifiable.
   */
  @JsonIgnore
  public List<Scope> getAuthorisationScopes() {
    return authorisationScopes;
  }

  /**
   * @return Never <code>null</code>.
   */
  @JsonProperty("scp")
  public List<String> getAuthorisationScopeValues() {
    return authorisationScopes.stream().map(Scope::getPlain).collect(toList());
  }

  /**
   * @return Never <code>null</code>, but unmodifiable.
   */
  @JsonIgnore
  public List<Scope> getSavedScopes() {
    return savedScopes;
  }

  /**
   * @return Never <code>null</code>.
   */
  @JsonProperty("scs")
  public List<String> getSavedScopeValues() {
    return savedScopes.stream().map(Scope::getPlain).collect(toList());
  }

  @JsonProperty("lng")
  public boolean isLongLived() {
    return longLived;
  }

  /**
   * @return Never <code>null</code>, but unmodifiable.
   */
  @JsonIgnore
  public List<Claim> getConsentedClaims() {
    return consentedClaims;
  }

  /**
   * @return Never <code>null</code>.
   */
  @JsonProperty("clm")
  public List<String> getConsentedClaimValues() {
    return consentedClaims.stream().map(Claim::getPlain).collect(toList());
  }

  /**
   * @return Never <code>null</code>, but unmodifiable.
   */
  @JsonIgnore
  public List<Claim> getSavedConsentedClaims() {
    return savedConsentedClaims;
  }

  /**
   * @return Never <code>null</code>.
   */
  @JsonProperty("cls")
  public List<String> getSavedConsentedClaimValues() {
    return savedConsentedClaims.stream().map(Claim::getPlain).collect(toList());
  }

  /**
   * Sum of {@link #getConsentedClaims()} and {@link #getSavedConsentedClaims()}
   *
   * @return Never <code>null</code>
   */
  @JsonIgnore
  public Set<Claim> getAllConsentedClaims() {
    final Set<Claim> result = new TreeSet<>(consentedClaims);
    result.addAll(savedConsentedClaims);
    return result;
  }

  /**
   * @return Never <code>null</code>, but unmodifiable.
   */
  @JsonProperty("cll")
  public List<Locale> getPreferredClaimLocales() {
    return preferredClaimLocales;
  }

  @JsonProperty("rur")
  public URI getRedirectionURI() {
    return redirectionURI;
  }

  @JsonProperty("irt")
  public Boolean getIssueRefreshToken() {
    return issueRefreshToken;
  }

  @JsonProperty("rft")
  public String getRefreshToken() {
    return refreshToken;
  }

  @JsonIgnore
  public Duration getRefreshTokenLifetime() {
    return refreshTokenLifetime;
  }

  @JsonProperty("rtl")
  public Long getRefreshTokenLifetimeSecs() {
    return (refreshTokenLifetime == null ? null : refreshTokenLifetime.getSeconds());
  }

  @JsonIgnore
  public Duration getAccessTokenLifetime() {
    return accessTokenLifetime;
  }

  @JsonProperty("atl")
  public Long getAccessTokenLifetimeSecs() {
    return (accessTokenLifetime == null ? null : accessTokenLifetime.getSeconds());
  }

  @JsonProperty("ate")
  public String getAccessTokenEncoding() {
    return accessTokenEncoding;
  }

  @JsonProperty("atc")
  public Boolean getAccessTokenEncryption() {
    return accessTokenEncryption;
  }

  @JsonProperty("iss")
  public String getIssuer() {
    return issuer;
  }

  @JsonIgnore
  public ZonedDateTime getIssuedAt() {
    return issuedAt;
  }

  @JsonProperty("iat")
  public Long getIssuedAtSinceEpoch() {
    return (issuedAt == null ? null : issuedAt.toEpochSecond());
  }

  @JsonIgnore
  public ZonedDateTime getUpdatedAt() {
    return updatedAt;
  }

  @JsonProperty("uat")
  public Long getUpdatedAtSinceEpoch() {
    return (updatedAt == null ? null : updatedAt.toEpochSecond());
  }

  @JsonProperty("aud")
  public String getAuthorisationAudience() {
    return authorisationAudience;
  }

  @JsonProperty("idt")
  public String getOpenIdConnectIdToken() {
    return openIdConnectIdToken;
  }

  @JsonProperty("cch")
  public String getPkceCodeChallenge() {
    return pkceCodeChallenge;
  }

  @JsonProperty("ccm")
  public String getPkceCodeChallengeMethod() {
    return pkceCodeChallengeMethod;
  }

  /**
   * @return Never <code>null</code>
   */
  public ModifiableOpenIdConnectAuthorisation modifiable() {
    return new ModifiableOpenIdConnectAuthorisation(this);
  }

  /**
   * @return Optional!
   */
  @JsonProperty("dat")
  public SubjectSessionOptionalData getOptionalData() {
    return optionalData;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    OpenIdConnectAuthorization that = (OpenIdConnectAuthorization) o;
    return Objects.equals(subject, that.subject) &&
            Objects.equals(clientId, that.clientId) &&
            Objects.equals(authorisationScopes, that.authorisationScopes) &&
            Objects.equals(savedScopes, that.savedScopes) &&
            Objects.equals(longLived, that.longLived) &&
            Objects.equals(consentedClaims, that.consentedClaims) &&
            Objects.equals(savedConsentedClaims, that.savedConsentedClaims) &&
            Objects.equals(preferredClaimLocales, that.preferredClaimLocales) &&
            Objects.equals(redirectionURI, that.redirectionURI) &&
            Objects.equals(issueRefreshToken, that.issueRefreshToken) &&
            Objects.equals(refreshToken, that.refreshToken) &&
            Objects.equals(refreshTokenLifetime, that.refreshTokenLifetime) &&
            Objects.equals(accessTokenLifetime, that.accessTokenLifetime) &&
            Objects.equals(accessTokenEncoding, that.accessTokenEncoding) &&
            Objects.equals(accessTokenEncryption, that.accessTokenEncryption) &&
            Objects.equals(issuer, that.issuer) &&
            Objects.equals(issuedAt, that.issuedAt) &&
            Objects.equals(updatedAt, that.updatedAt) &&
            Objects.equals(authorisationAudience, that.authorisationAudience) &&
            Objects.equals(openIdConnectIdToken, that.openIdConnectIdToken) &&
            Objects.equals(pkceCodeChallenge, that.pkceCodeChallenge) &&
            Objects.equals(pkceCodeChallengeMethod, that.pkceCodeChallengeMethod) &&
            Objects.equals(optionalData, that.optionalData);
  }

  @Override
  public int hashCode() {
    return Objects.hash(subject, clientId, authorisationScopes, savedScopes, longLived, consentedClaims, savedConsentedClaims, preferredClaimLocales, redirectionURI, issueRefreshToken, refreshToken, refreshTokenLifetime, accessTokenLifetime, accessTokenEncoding, accessTokenEncryption, issuer, issuedAt, updatedAt, authorisationAudience, openIdConnectIdToken, pkceCodeChallenge, pkceCodeChallengeMethod, optionalData);
  }

  @Override
  public String toString() {
    return "subject=" + subject + ", clientId=" + clientId + ", authorisationScopes=" + authorisationScopes
            + ", savedScopes=" + savedScopes + ", longLived=" + longLived + ", consentedClaims=" + consentedClaims
            + ", savedConsentedClaims=" + savedConsentedClaims + ", data=" + optionalData;
  }

}
