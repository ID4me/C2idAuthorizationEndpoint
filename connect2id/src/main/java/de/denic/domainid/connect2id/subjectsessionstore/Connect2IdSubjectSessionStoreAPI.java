/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.subjectsessionstore;

import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import de.denic.domainid.connect2id.BaseConnect2IdClient;
import de.denic.domainid.connect2id.common.AccessToken;
import de.denic.domainid.connect2id.common.Connect2IdException;
import de.denic.domainid.connect2id.common.SubjectSessionId;
import de.denic.domainid.connect2id.subjectsessionstore.entity.SubjectSession;
import de.denic.domainid.connect2id.subjectsessionstore.entity.SubjectSessionOptionalData;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.concurrent.Immutable;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static java.util.Collections.emptyList;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static java.util.stream.Collectors.toList;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
import static org.eclipse.jetty.http.HttpStatus.*;

/**
 * Client to access <a href="https://connect2id.com/products/server/docs/integration/session-store">Connect2Id's Subject
 * Session Store API</a>.
 */
public interface Connect2IdSubjectSessionStoreAPI {

  /**
   * @param sessionId Required
   * @return Never <code>null</code>
   */
  SubjectSession sessionOf(SubjectSessionId sessionId) throws Connect2IdException;

  /**
   * @param sessionIds Optional
   * @return Never <code>null</code>
   */
  List<SubjectSession> sessionsOf(Collection<SubjectSessionId> sessionIds) throws Connect2IdException;

  /**
   * @param sessionId Required
   * @return Never <code>null</code>
   */
  Optional<SubjectSession> cancelSession(SubjectSessionId sessionId);

  @Immutable
  final class Impl extends BaseConnect2IdClient implements Connect2IdSubjectSessionStoreAPI {

    private static final Logger LOG = LoggerFactory.getLogger(Impl.class);
    // Readers:
    private static final ObjectReader GET_SUBJECT_SESSION_RESPONSE_ENTITY_READER = OBJECT_MAPPER
            .readerFor(SubjectSession.class);
    // Writers:
    private static final ObjectWriter ADDITIONAL_DATA_WRITER = OBJECT_MAPPER
            .writerFor(SubjectSessionOptionalData.class);

    private final Client restClient;
    private final URI c2IdSubjectSessionStoreURL;
    private final AccessToken bearerAccessToken;

    /**
     * @param c2IdSubjectSessionStoreURL Required
     * @param bearerAccessToken          Required
     * @param restClient                 Required
     */
    public Impl(final URL c2IdSubjectSessionStoreURL, final AccessToken bearerAccessToken, final Client restClient) {
      super(LOG);
      Validate.notNull(restClient, "Missing REST client");
      Validate.notNull(c2IdSubjectSessionStoreURL, "Missing C2ID Subject Session Store API URL");
      Validate.notNull(bearerAccessToken, "Missing bearer access token");
      this.restClient = restClient;
      this.bearerAccessToken = bearerAccessToken;
      try {
        this.c2IdSubjectSessionStoreURL = c2IdSubjectSessionStoreURL.toURI();
      } catch (final URISyntaxException e) {
        throw new IllegalArgumentException("C2ID Subject Session Store API URL " + c2IdSubjectSessionStoreURL, e);
      }
    }

    @Override
    public SubjectSession sessionOf(final SubjectSessionId sessionId) throws Connect2IdException {
      Validate.notNull(sessionId, "Missing Subject Session ID");
      final WebTarget target = createSessionsWebTarget();
      final Invocation querySubjectSessionRequest = target.request(APPLICATION_JSON_TYPE)
              .header(AUTHORIZATION_HEADER, bearerAccessToken.withPrefix("Bearer")).header("SID", sessionId).buildGet();
      LOG.info("Loading session from C2ID Subject Session Store with ID '{}'", sessionId);
      final Pair<Response, String> invocationResult = invokeGET(querySubjectSessionRequest, target.getUri());
      final String responseBody = invocationResult.getRight();
      final Response querySubjectSessionResponse = invocationResult.getLeft();
      try {
        switch (querySubjectSessionResponse.getStatus()) {
          case OK_200:
            final SubjectSession responseEntity = GET_SUBJECT_SESSION_RESPONSE_ENTITY_READER.readValue(responseBody);
            LOG.info("Loaded session: {}", responseEntity);
            return responseEntity;
          case UNAUTHORIZED_401:
            throw unauthorizedException(target, responseBody);
          case FORBIDDEN_403:
            throw forbiddenException(target, EMPTY_BODY_CONTENT, responseBody);
          case NOT_FOUND_404:
            throw notFoundException(target, responseBody);
          case INTERNAL_SERVER_ERROR_500:
            throw serverException(target, responseBody);
          default:
            throw new NotImplementedException(
                    "Status: " + querySubjectSessionResponse.getStatus() + ", response: " + querySubjectSessionResponse);
        }
      } catch (final IOException e) {
        throw new RuntimeException("Internal error unmarshalling " + responseBody, e);
      } finally {
        querySubjectSessionResponse.close();
      }
    }

    @Override
    public List<SubjectSession> sessionsOf(final Collection<SubjectSessionId> sessionIds) throws Connect2IdException {
      if (sessionIds == null || sessionIds.isEmpty()) {
        return emptyList();
      }

      return sessionIds.stream().map(sessionId -> {
        try {
          return sessionOf(sessionId);
        } catch (final Connect2IdException e) {
          return null;
        }
      }).filter(Objects::nonNull).collect(toList());
    }

    @Override
    public Optional<SubjectSession> cancelSession(final SubjectSessionId sessionId) {
      Validate.notNull(sessionId, "Missing Subject Session ID");
      LOG.info("Cancelling session on C2ID Subject Session Store: {}", sessionId);
      final WebTarget target = createSessionsWebTarget();
      final Invocation clearSessionRequest = target.request().accept(APPLICATION_JSON_TYPE)
              .header(AUTHORIZATION_HEADER, bearerAccessToken.withPrefix("Bearer")).header("SID", sessionId).buildDelete();
      final Pair<Response, String> invocationResult = invoke(clearSessionRequest, target.getUri(), "DELETE",
              EMPTY_BODY_CONTENT);
      final String responseBody = invocationResult.getRight();
      final Response clearSessionResponse = invocationResult.getLeft();
      try {
        switch (clearSessionResponse.getStatus()) {
          case OK_200:
            final SubjectSession responseEntity = GET_SUBJECT_SESSION_RESPONSE_ENTITY_READER.readValue(responseBody);
            LOG.info("Canceled session: {}", responseEntity);
            return of(responseEntity);
          case UNAUTHORIZED_401:
            LOG.warn("Cancelling C2ID Subject Session '{}' is unauthorized", sessionId);
            break;
          case FORBIDDEN_403:
            LOG.warn("Cancelling C2ID Subject Session '{}' is forbidden", sessionId);
            break;
          case NOT_FOUND_404:
            LOG.info("C2ID Subject Session '{}' to cancel is unknown", sessionId);
            break;
          case INTERNAL_SERVER_ERROR_500:
            LOG.warn("Cancelling C2ID Subject Session '{}' fails on server side", sessionId);
            break;
          default:
            LOG.warn("Cancelling C2ID Subject Session '{}' failed. {}, {}", sessionId, clearSessionResponse,
                    responseBody);
        }
      } catch (final IOException e) {
        throw new RuntimeException("Internal error unmarshalling " + responseBody, e);
      } finally {
        clearSessionResponse.close();
      }

      return empty();
    }

    private WebTarget createSessionsWebTarget() {
      return restClient.target(c2IdSubjectSessionStoreURL).path("/sessions");
    }

  }

}
