/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.healthcheck;

import static com.codahale.metrics.health.HealthCheck.Result.healthy;
import static com.codahale.metrics.health.HealthCheck.Result.unhealthy;
import static javax.ws.rs.core.Response.Status.Family.SUCCESSFUL;
import static org.apache.commons.lang3.StringUtils.defaultIfBlank;

import java.net.URI;
import java.net.URISyntaxException;

import javax.annotation.concurrent.Immutable;
import javax.ws.rs.client.Client;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.StatusType;

import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.health.HealthCheck;

import de.denic.domainid.connect2id.common.AccessToken;
import de.denic.domainid.connect2id.config.Connect2IdBaseConfig;

@Immutable
public final class Connect2IdServerHealthCheck extends HealthCheck {

  private static final Logger LOG = LoggerFactory.getLogger(Connect2IdServerHealthCheck.class);

  private final Client restClient;
  private final URI healthCheckURI;
  private final AccessToken bearerAccessToken;

  /**
   * @param restClient
   *          Required
   * @param healthCheckURI
   *          Required
   * @param bearerAccessToken
   *          Required
   */
  public Connect2IdServerHealthCheck(final Client restClient, final URI healthCheckURI,
      final AccessToken bearerAccessToken) {
    Validate.notNull(restClient, "Missing REST client");
    Validate.notNull(healthCheckURI, "Missing health check URI");
    Validate.notNull(bearerAccessToken, "Missing bearer access token");
    this.restClient = restClient;
    this.healthCheckURI = healthCheckURI;
    this.bearerAccessToken = bearerAccessToken;
  }

  /**
   * @param restApiClient
   *          Required
   * @param connect2IdConfig
   *          Required
   */
  public Connect2IdServerHealthCheck(final Client restApiClient, final Connect2IdBaseConfig connect2IdConfig)
      throws URISyntaxException {
    this(restApiClient, connect2IdConfig.getHealthCheckURL().toURI(),
        connect2IdConfig.getHealthCheckBearerAccessToken());
  }

  @Override
  protected Result check() {
    Response healthCheckResponse = null;
    try {
      healthCheckResponse = restClient.target(healthCheckURI).request()
          .header("Authorization", bearerAccessToken.withPrefix("Bearer")).get();
      final StatusType statusInfo = healthCheckResponse.getStatusInfo();
      if (SUCCESSFUL.equals(statusInfo.getFamily())) {
        return healthy();
      }

      final String message = "Code " + statusInfo.getStatusCode() + ": "
          + defaultIfBlank(statusInfo.getReasonPhrase(), "-");
      LOG.warn("Connect2Id server health check failed: {}", message);
      return unhealthy(message);
    } catch (final RuntimeException e) {
      LOG.warn("Connect2Id server health check failed", e);
      return unhealthy(e);
    } finally {
      if (healthCheckResponse != null) {
        healthCheckResponse.close();
      }
    }
  }

}
