/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.tokenendpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import de.denic.domainid.connect2id.common.ClientId;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.Charset;

import static javax.ws.rs.client.Entity.form;
import static org.apache.commons.codec.binary.Base64.encodeBase64String;
import static org.eclipse.jetty.http.HttpStatus.OK_200;

public interface TokenEndpointClient {

  /**
   * @param tokenEndpointURI  Required
   * @param clientID          Required
   * @param clientSecret      Required
   * @param redirectionURI    Required
   * @param authorizationCode Required
   * @return Never <code>null</code>
   */
  TokenEndpointResponseEntity exchange(URI tokenEndpointURI, ClientId clientID, String clientSecret, URI redirectionURI,
                                       String authorizationCode);

  final class Impl implements TokenEndpointClient {

    private static final Logger LOG = LoggerFactory.getLogger(Impl.class);
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private static final ObjectReader TOKEN_ENDPOINT_RESPONSE_ENTITY_READER = OBJECT_MAPPER
            .readerFor(TokenEndpointResponseEntity.class);
    private static final Charset UTF8 = Charset.forName("UTF8");

    private final Client restClient;

    /**
     * @param restClient Required
     */
    public Impl(final Client restClient) {
      Validate.notNull(restClient, "Missing client component");
      this.restClient = restClient;
    }

    @Override
    public TokenEndpointResponseEntity exchange(final URI tokenEndpointURI, final ClientId clientID,
                                                final String clientSecret, final URI redirectionURI, final String authorizationCode) {
      Validate.notNull(tokenEndpointURI, "Missing token endpoint URI");
      Validate.notNull(clientID, "Missing client ID");
      Validate.notNull(redirectionURI, "Missing redirection URI");
      Validate.notEmpty(clientSecret, "Missing client secret");
      Validate.notEmpty(authorizationCode, "Missing authorization code");
      final Form tokenEndpointForm = new Form("grant_type", "authorization_code").param("code", authorizationCode)
              .param("redirect_uri", redirectionURI.toASCIIString());
      LOG.info("Exchanging auth code for tokens on Token Endpoint '{}', applying client ID '{}', POSTing form {}",
              tokenEndpointURI, clientID, tokenEndpointForm.asMap());
      final Response tokenEndpointResponse = restClient.target(tokenEndpointURI).request()
              .header("Authorization", "Basic " + encodeBase64String((clientID + ":" + clientSecret).getBytes(UTF8)))
              .post(form(tokenEndpointForm));
      final String response = tokenEndpointResponse.readEntity(String.class);
      try {
        switch (tokenEndpointResponse.getStatus()) {
          case OK_200:
            final TokenEndpointResponseEntity result = TOKEN_ENDPOINT_RESPONSE_ENTITY_READER.readValue(response);
            LOG.info("Response from Token Endpoint: {}", result);
            return result;
          default:
            throw new NotImplementedException(
                    "Handling response with status: " + tokenEndpointResponse.getStatus() + ", body: '" +
                            response + "'");
        }
      } catch (final IOException e) {
        throw new RuntimeException("Internal error parsing token endpoint response: '" + response + "'", e);
      } finally {
        tokenEndpointResponse.close();
      }
    }

  }

}