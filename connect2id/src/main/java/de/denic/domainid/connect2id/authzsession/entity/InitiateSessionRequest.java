/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.authzsession.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.denic.domainid.connect2id.common.Claim;
import de.denic.domainid.connect2id.common.SubjectSessionId;
import org.apache.commons.lang3.Validate;

import javax.annotation.concurrent.Immutable;
import java.net.URI;
import java.util.Map;
import java.util.Objects;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Immutable
public final class InitiateSessionRequest {

  private final URI query;
  private final SubjectSessionId subjectSessionId;
  private final AuthzSessionOptionalData optionalData;

  /**
   * @see #InitiateSessionRequest(URI, SubjectSessionId, Map)
   */
  public InitiateSessionRequest(final URI openIdAuthenticationRequestQuery) {
    this(openIdAuthenticationRequestQuery, null, null);
  }

  /**
   * @see #InitiateSessionRequest(URI, SubjectSessionId, Map)
   */
  public InitiateSessionRequest(final URI openIdAuthenticationRequestQuery, final SubjectSessionId subjectSessionId) {
    this(openIdAuthenticationRequestQuery, subjectSessionId, null);
  }

  /**
   * @param openIdAuthenticationRequestQuery Required
   * @param subjectSessionId                 Optional
   * @param reasonsOfClaims                  Optional
   */
  public InitiateSessionRequest(final URI openIdAuthenticationRequestQuery, final SubjectSessionId subjectSessionId,
                                final Map<Claim, String> reasonsOfClaims) {
    Validate.notNull(openIdAuthenticationRequestQuery, "Missing OpenID Authentication Request query");
    this.query = openIdAuthenticationRequestQuery;
    this.subjectSessionId = subjectSessionId;
    this.optionalData = (reasonsOfClaims == null || reasonsOfClaims.isEmpty() ? null : new AuthzSessionOptionalData(reasonsOfClaims));
  }

  /**
   * @return Never <code>null</code>
   */
  @JsonProperty("query")
  public URI getQuery() {
    return query;
  }

  /**
   * @return Optional
   */
  @JsonIgnore
  public SubjectSessionId getSubjectSessionId() {
    return subjectSessionId;
  }

  /**
   * @return Optional
   */
  @JsonProperty("sub_sid")
  @JsonInclude(NON_NULL)
  public String getSubjectSessionIdValue() {
    return (subjectSessionId == null ? null : subjectSessionId.getPlain());
  }

  /**
   * @return Optional
   */
  @JsonProperty("data")
  @JsonInclude(NON_NULL)
  public AuthzSessionOptionalData getOptionalData() {
    return optionalData;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    InitiateSessionRequest that = (InitiateSessionRequest) o;
    return Objects.equals(query, that.query) &&
            Objects.equals(subjectSessionId, that.subjectSessionId) &&
            Objects.equals(optionalData, that.optionalData);
  }

  @Override
  public int hashCode() {
    return Objects.hash(query, subjectSessionId, optionalData);
  }

  @Override
  public String toString() {
    return "query=" + query + (subjectSessionId == null ? "" : ", sub_sid=" + subjectSessionId)
            + (optionalData == null ? "" : ", data=" + optionalData);
  }

}
