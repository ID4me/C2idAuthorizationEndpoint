/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.tokenendpoint;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.concurrent.Immutable;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
@Immutable
public final class TokenEndpointResponseEntity {

  private final String accessToken;
  private final String refreshToken;
  private final String idToken;
  private final String scope;
  private final String tokenType;
  private final int expiresIn;

  public TokenEndpointResponseEntity(@JsonProperty("access_token") final String accessToken,
                                     @JsonProperty("refresh_token") final String refreshToken, @JsonProperty("id_token") final String idToken,
                                     @JsonProperty("scope") final String scope, @JsonProperty("token_type") final String tokenType,
                                     @JsonProperty("expires_in") final int expiresIn) {
    this.accessToken = accessToken;
    this.refreshToken = refreshToken;
    this.idToken = idToken;
    this.scope = scope;
    this.tokenType = tokenType;
    this.expiresIn = expiresIn;
  }

  public String getAccessToken() {
    return accessToken;
  }

  public String getRefreshToken() {
    return refreshToken;
  }

  public String getIdToken() {
    return idToken;
  }

  public String getScope() {
    return scope;
  }

  public String getTokenType() {
    return tokenType;
  }

  public int getExpiresIn() {
    return expiresIn;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    TokenEndpointResponseEntity that = (TokenEndpointResponseEntity) o;
    return expiresIn == that.expiresIn &&
            Objects.equals(accessToken, that.accessToken) &&
            Objects.equals(refreshToken, that.refreshToken) &&
            Objects.equals(idToken, that.idToken) &&
            Objects.equals(scope, that.scope) &&
            Objects.equals(tokenType, that.tokenType);
  }

  @Override
  public int hashCode() {
    return Objects.hash(accessToken, refreshToken, idToken, scope, tokenType, expiresIn);
  }

  @Override
  public String toString() {
    return "accessToken=" + accessToken + ", refreshToken=" + refreshToken + ", idToken=" + idToken + ", scope="
            + scope + ", tokenType=" + tokenType + ", expiresIn=" + expiresIn;
  }

}