/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.config;

import java.net.MalformedURLException;
import java.net.URL;

import javax.annotation.concurrent.ThreadSafe;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import de.denic.domainid.connect2id.common.AccessToken;

@ThreadSafe
public class Connect2IdAuthorisationStoreAPIConfig extends Connect2IdBaseConfig {

  @Valid
  @NotNull
  @JsonProperty("authorisationStoreAPIPath")
  private volatile String authorisationStoreAPIPath;
  private volatile URL authorisationStoreAPIUrl;
  @Valid
  @NotNull
  @JsonProperty("authorisationStoreAPIBearerAccessToken")
  private volatile AccessToken authorisationStoreAPIBearerAccessToken;

  public final void setAuthorisationStoreAPIPath(final String authorisationStoreAPIPath) throws MalformedURLException {
    this.authorisationStoreAPIPath = authorisationStoreAPIPath;
    authorisationStoreAPIUrl = new URL(getBaseURL(), this.authorisationStoreAPIPath);
  }

  public final void setAuthorisationStoreAPIBearerAccessToken(
      final AccessToken authorisationStoreAPIBearerAccessToken) {
    this.authorisationStoreAPIBearerAccessToken = authorisationStoreAPIBearerAccessToken;
  }

  public final AccessToken getAuthorisationStoreAPIBearerAccessToken() {
    return authorisationStoreAPIBearerAccessToken;
  }

  public final URL getAuthorisationStoreAPIUrl() {
    return authorisationStoreAPIUrl;
  }

}
