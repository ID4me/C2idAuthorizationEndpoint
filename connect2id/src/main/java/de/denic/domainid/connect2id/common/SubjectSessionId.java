/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.common;

import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.apache.commons.lang3.StringUtils.isEmpty;

import java.util.Optional;
import java.util.regex.Pattern;

import javax.annotation.concurrent.Immutable;

import org.apache.commons.lang3.Validate;

@Immutable
public final class SubjectSessionId extends UrlEncodedValue {

  private static final Pattern ALLOWED_CHARACTERS = Pattern.compile("[\\w\\-\\.]+");

  /**
   * @see UrlEncodedValue#UrlEncodedValue(String)
   */
  public SubjectSessionId(final String plainValue) {
    super(plainValue);
    Validate.isTrue(ALLOWED_CHARACTERS.matcher(plainValue).matches(), "Illegal session ID value: '%s'", plainValue);
  }

  /**
   * @param cookieValue
   *          Optional
   * @return Never <code>null</code>.
   */
  public static Optional<SubjectSessionId> optional(final String cookieValue) {
    return isEmpty(cookieValue) ? empty() : of(new SubjectSessionId(cookieValue));
  }

}
