/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.authzsession;

import javax.annotation.concurrent.Immutable;

import de.denic.domainid.connect2id.authzsession.Connect2IdAuthorizationSessionAPI.AuthenticateSubjectResponseDispatcher;
import de.denic.domainid.connect2id.authzsession.Connect2IdAuthorizationSessionAPI.InitiateSessionResponseDispatcher;
import de.denic.domainid.connect2id.authzsession.Connect2IdAuthorizationSessionAPI.ProvisionOfConsentedDataResponseDispatcher;
import de.denic.domainid.connect2id.authzsession.entity.AuthenticationPromptResponse;
import de.denic.domainid.connect2id.authzsession.entity.ConsentPromptResponse;
import de.denic.domainid.connect2id.authzsession.entity.FinalResponse;
import de.denic.domainid.connect2id.authzsession.entity.NonRedirectableErrorResponse;

/**
 * Base implementation of all the available Dispatchers ({@link InitiateSessionResponseDispatcher},
 * {@link AuthenticateSubjectResponseDispatcher}, and {@link ProvisionOfConsentedDataResponseDispatcher}), throwing
 * {@link UnexpectedResponseException} on every invocation.
 */
@Immutable
public abstract class ResponseDispatcherBase<RETURN_TYPE> implements InitiateSessionResponseDispatcher<RETURN_TYPE>,
    AuthenticateSubjectResponseDispatcher<RETURN_TYPE>, ProvisionOfConsentedDataResponseDispatcher<RETURN_TYPE> {

  @Override
  public RETURN_TYPE handle(final AuthenticationPromptResponse response) {
    throw unexpectedReponseException(response);
  }

  @Override
  public RETURN_TYPE handle(final ConsentPromptResponse response) {
    throw unexpectedReponseException(response);
  }

  @Override
  public RETURN_TYPE handle(final FinalResponse response) {
    throw unexpectedReponseException(response);
  }

  @Override
  public RETURN_TYPE handle(final NonRedirectableErrorResponse response) {
    throw unexpectedReponseException(response);
  }

  private RuntimeException unexpectedReponseException(final Object response) {
    return new UnexpectedResponseException(response, getClass());
  }

}
