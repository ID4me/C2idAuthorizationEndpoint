/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.common;

import de.denic.domainid.connect2id.common.AuthenticationMethodReference.Value;
import org.apache.commons.lang3.Validate;

import javax.annotation.concurrent.Immutable;
import java.util.List;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;

/**
 * Allowed values according to <a href="https://tools.ietf.org/html/rfc8176">RFC 8176 Authentication Method Reference
 * Values"</a>.
 */
@Immutable
public final class AuthenticationMethodReference extends UrlEncodedEnumValue<Value> {

  /**
   * @param plainValue Required
   */
  public AuthenticationMethodReference(final String plainValue) {
    super(plainValue, Value.class);
  }

  /**
   * @param value Required
   */
  public AuthenticationMethodReference(final Value value) {
    super(value);
  }

  /**
   * Factory method.
   *
   * @param value Required
   * @return Never <code>null</code>
   */
  public static AuthenticationMethodReference of(final Value value) {
    Validate.notNull(value, "Missing value");
    return new AuthenticationMethodReference(value);
  }

  /**
   * Factory method.
   *
   * @param values Optional
   * @return Never <code>null</code> but unmodifiable
   */
  public static List<AuthenticationMethodReference> ofMany(final Value... values) {
    return stream(values).map(AuthenticationMethodReference::of).collect(toList());
  }

  public enum Value {
    face, fpt, geo, hwk, iris, kba, mca, mfa, otp, pin, pwd, rba, retina, sc, sms, swk, tel, user, vbm, wia;
  }

}
