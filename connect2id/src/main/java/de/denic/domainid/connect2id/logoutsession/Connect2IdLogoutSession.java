/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.logoutsession;

import static javax.ws.rs.client.Entity.json;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import javax.annotation.concurrent.Immutable;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;

import de.denic.domainid.connect2id.BaseConnect2IdClient;
import de.denic.domainid.connect2id.authzsession.ResponseDispatcherBase;
import de.denic.domainid.connect2id.common.AccessToken;
import de.denic.domainid.connect2id.common.Connect2IdException;
import de.denic.domainid.connect2id.common.SubjectSessionId;
import de.denic.domainid.connect2id.logoutsession.entity.ConfirmLogoutResponse;
import de.denic.domainid.connect2id.logoutsession.entity.InitiateLogoutRequest;
import de.denic.domainid.connect2id.logoutsession.entity.LogoutEndResponse;
import de.denic.domainid.connect2id.logoutsession.entity.LogoutErrorResponse;

/**
 * Client to access <a href="https://connect2id.com/products/server/docs/integration/logout-session">Connect2Id's Logout
 * session API</a>.
 */
public interface Connect2IdLogoutSession {

  /**
   * @param optLogoutRequestQuery
   *          Optional
   * @param optSubjectSessionId
   *          Optional
   * @return Never <code>null</code>
   */
  InitiateLogoutResponse initiateLogout(URI optLogoutRequestQuery, SubjectSessionId optSubjectSessionId)
      throws Connect2IdException;

  /**
   * @param sessionId
   *          Required
   * @param logoutConfirmed
   *          Required
   * @return Never <code>null</code>
   */
  LogoutEndResponse confirmed(SessionId sessionId, boolean logoutConfirmed);

  interface InitiateLogoutResponse {

    /**
     * @param visitor
     *          Required
     * @return Depends on contract of provided <code>visitor</code>
     */
    <RETURN_TYPE> RETURN_TYPE visitedBy(InitiateLogoutResponseVisitor<RETURN_TYPE> visitor);

  }

  /**
   * Implementors should think of extending {@link ResponseDispatcherBase} or one of it's subclasses.
   */
  interface InitiateLogoutResponseVisitor<TYPE> extends ConfirmLogoutVisitor<TYPE>, LogoutErrorVisitor<TYPE> {

    // Intentionally left empty

  }

  /**
   * @see InitiateLogoutResponseVisitor
   */
  interface LogoutErrorVisitor<RETURN_TYPE> {

    /**
     * @param responseEntity
     *          Required
     * @return Not <code>null</code>
     */
    RETURN_TYPE visit(LogoutErrorResponse responseEntity);

  }

  /**
   * @see InitiateLogoutResponseVisitor
   */
  interface ConfirmLogoutVisitor<RETURN_TYPE> {

    /**
     * @param responseEntity
     *          Required
     * @return Not <code>null</code>
     */
    RETURN_TYPE visit(ConfirmLogoutResponse responseEntity);

  }

  @Immutable
  final class Impl extends BaseConnect2IdClient implements Connect2IdLogoutSession {

    private static final Logger LOG = LoggerFactory.getLogger(Impl.class);
    private static final String CONFIRM_LOGOUT_TRUE_BODY_CONTENT = "{\"confirm_logout\":true}";
    private static final String CONFIRM_LOGOUT_FALSE_BODY_CONTENT = "{\"confirm_logout\":false}";
    // Readers:
    private static final ObjectReader LOGOUT_END_RESPONSE_ENTITY_READER = OBJECT_MAPPER
        .readerFor(LogoutEndResponse.class);
    // Writers:
    private static final ObjectWriter INITIATE_LOGOUT_REQUEST_ENTITY_WRITER = OBJECT_MAPPER
        .writerFor(InitiateLogoutRequest.class);

    private final Client restClient;
    private final URI connect2IdLogoutSessionURL;
    private final AccessToken bearerAccessToken;

    /**
     * @param connect2IdLogoutSessionURL
     *          Required
     * @param bearerAccessToken
     *          Required
     * @param restClient
     *          Required
     */
    public Impl(final URL connect2IdLogoutSessionURL, final AccessToken bearerAccessToken, final Client restClient) {
      super(LOG);
      Validate.notNull(restClient, "Missing REST client");
      Validate.notNull(connect2IdLogoutSessionURL, "Missing Connect2Id Logout Session API URL");
      Validate.notNull(bearerAccessToken, "Missing bearer access token");
      this.restClient = restClient;
      this.bearerAccessToken = bearerAccessToken;
      try {
        this.connect2IdLogoutSessionURL = connect2IdLogoutSessionURL.toURI();
      } catch (final URISyntaxException e) {
        throw new IllegalArgumentException("Connect2Id Logout Session API URL " + connect2IdLogoutSessionURL, e);
      }
    }

    @Override
    public InitiateLogoutResponse initiateLogout(final URI optLogoutRequestQuery,
        final SubjectSessionId optSubjectSessionId) throws Connect2IdException {
      final String jsonRequestBody;
      final InitiateLogoutRequest requestEntity = new InitiateLogoutRequest(optLogoutRequestQuery,
          optSubjectSessionId);
      try {
        jsonRequestBody = INITIATE_LOGOUT_REQUEST_ENTITY_WRITER.writeValueAsString(requestEntity);
      } catch (final IOException e) {
        throw new RuntimeException("Internal error marshalling " + requestEntity, e);
      }

      final WebTarget target = restClient.target(connect2IdLogoutSessionURL);
      final Invocation initiateSessionRequest = target.request(APPLICATION_JSON_TYPE)
          .header(AUTHORIZATION_HEADER, bearerAccessToken.withPrefix("Bearer")).buildPut(json(jsonRequestBody));
      LOG.info("Initiating Connect2Id Logout Session: Query: '{}', subject session ID '{}'", optLogoutRequestQuery,
          optSubjectSessionId);
      final Pair<Response, String> invocationResult = invoke(initiateSessionRequest, target.getUri(), "PUT",
          jsonRequestBody);
      return parseInvocationResult(invocationResult, (responseBody -> new AnyResponseImpl(responseBody)), target,
          jsonRequestBody);
    }

    @Override
    public LogoutEndResponse confirmed(final SessionId sessionId, final boolean logoutConfirmed) {
      Validate.notNull(sessionId, "Missing session ID");
      final String jsonRequestBody = (logoutConfirmed ? CONFIRM_LOGOUT_TRUE_BODY_CONTENT
          : CONFIRM_LOGOUT_FALSE_BODY_CONTENT);
      final WebTarget target = webTargetOf(sessionId);
      final Builder builder = target.request(APPLICATION_JSON_TYPE).header(AUTHORIZATION_HEADER,
          bearerAccessToken.withPrefix("Bearer"));
      final Pair<Response, String> invocationResult = invokePUTWithJSON(builder, jsonRequestBody, target.getUri());
      return parseInvocationResult(invocationResult,
          (responseBody -> LOGOUT_END_RESPONSE_ENTITY_READER.readValue(responseBody)), target, jsonRequestBody);
    }

    private WebTarget webTargetOf(final SessionId sessionId) {
      return restClient.target(connect2IdLogoutSessionURL).path(sessionId.getValue());
    }

  }

  @Immutable
  final class AnyResponseImpl implements InitiateLogoutResponse {

    private static final Logger LOG = LoggerFactory.getLogger(AnyResponseImpl.class);
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private static final ObjectReader CONFIRM_LOGOUT_RESPONSE_ENTITY_READER = OBJECT_MAPPER
        .readerFor(ConfirmLogoutResponse.class);
    private static final ObjectReader LOGOUT_ERROR_RESPONSE_ENTITY_READER = OBJECT_MAPPER
        .readerFor(LogoutErrorResponse.class);

    private final JsonNode responseJSONTree;
    private final String typeValue;

    public AnyResponseImpl(final String responseBody) throws JsonProcessingException, IOException {
      this.responseJSONTree = OBJECT_MAPPER.readTree(responseBody);
      final JsonNode typeNode = responseJSONTree.get("type");
      if (!typeNode.isValueNode()) {
        throw new RuntimeException(
            "Received Connect2Id Logout session API response missing response type value node: " + responseJSONTree);
      }

      this.typeValue = typeNode.asText();
    }

    @Override
    public <RETURN_TYPE> RETURN_TYPE visitedBy(final InitiateLogoutResponseVisitor<RETURN_TYPE> visitor) {
      try {
        switch (typeValue) {
          case "confirm":
            return confirmType(visitor);
          case "error":
            return errorType(visitor);
          default:
            return unknownType();
        }
      } catch (final IOException e) {
        throw new RuntimeException(
            "Parsing received Connect2Id Logout session API response failed: " + responseJSONTree, e);
      }
    }

    private <RETURN_TYPE> RETURN_TYPE unknownType() {
      throw new RuntimeException("Received Connect2Id Logout session API response of unknown response type '"
          + typeValue + "': " + responseJSONTree);
    }

    private <RETURN_TYPE> RETURN_TYPE errorType(final LogoutErrorVisitor<RETURN_TYPE> visitor)
        throws IOException, JsonProcessingException {
      final LogoutErrorResponse responseEntity = LOGOUT_ERROR_RESPONSE_ENTITY_READER.readValue(responseJSONTree);
      LOG.info("Initiated Connect2Id Logout session, response: {}", responseEntity);
      return visitor.visit(responseEntity);
    }

    private <RETURN_TYPE> RETURN_TYPE confirmType(final ConfirmLogoutVisitor<RETURN_TYPE> visitor)
        throws IOException, JsonProcessingException {
      final ConfirmLogoutResponse responseEntity = CONFIRM_LOGOUT_RESPONSE_ENTITY_READER
          .readValue(responseJSONTree);
      LOG.info("Initiated Connect2Id Logout session, response: {}", responseEntity);
      return visitor.visit(responseEntity);
    }

  }

}
