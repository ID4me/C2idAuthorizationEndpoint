/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.common;

import javax.annotation.concurrent.Immutable;

import org.apache.commons.lang3.Validate;

@Immutable
public final class AccessToken {

  private final String accessTokenValue;

  public AccessToken(final String accessTokenValue) {
    Validate.notEmpty(accessTokenValue, "Missing access token value");
    this.accessTokenValue = accessTokenValue;
  }

  public String getValue() {
    return accessTokenValue;
  }

  public String withPrefix(final String prefix) {
    return prefix + " " + accessTokenValue;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((accessTokenValue == null) ? 0 : accessTokenValue.hashCode());
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final AccessToken other = (AccessToken) obj;
    if (accessTokenValue == null) {
      if (other.accessTokenValue != null) {
        return false;
      }
    } else if (!accessTokenValue.equals(other.accessTokenValue)) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return accessTokenValue;
  }

}
