![id4me](http://id4me.org/wp-content/uploads/2018/03/ID4me_logo_2c_pos_rgb.png)

This is an example project which is not maintained anymore.

# Run an Identity Provider

## Preparation
Create a working dir to have your stuff later.
```
mkdir idauth
cd idauth
export IDAUTH_DIR=$(pwd)
mkdir $IDAUTH_DIR/{c2id,common,auth-endpoint,user-account-board,dns-lookup-ms,did-init}
```

Download the [jwkset generator](https://bitbucket.org/connect2id/server-jwkset-gen/overview) and generate a jwkset:
```
wget https://bitbucket.org/connect2id/server-jwkset-gen/downloads/jwkset-gen.jar
java -jar jwkset-gen.jar $IDAUTH_DIR/common/c2id-jwkSet.json
 ```

## Setup a postgres database
```
mkdir $IDAUTH_DIR/database
docker run -d \
  -e PGDATA=/data \
  -e POSTGRES_USER=postgres \
  -e POSTGRES_PASSWORD=postgres \
  -v $IDAUTH_DIR/database:/data \
  -p 5432:5432 \
  --name pg-master \
  postgres:10-alpine
```

Create the databases and users
```
docker run -i -t --link pg-master:5432 postgres:10-alpine psql -h pg-master -U postgres

CREATE USER "auth-endpoint" WITH PASSWORD 'auth-endpointpass';
CREATE DATABASE "auth-endpoint";
GRANT ALL PRIVILEGES ON DATABASE "auth-endpoint" TO "auth-endpoint";
\q
```

 ## c2id server
 
To run the [c2id server in a docker](https://connect2id.com/products/server/docs/guides/docker)
```
docker run -p 8080:8080 -d --link pg-master:5432 --name c2id --rm c2id/c2id-server:7.0
```

* adjust the [configuration files for the c2id server](https://connect2id.com/products/server/docs/config), the following must fit your environment:
  * auth endpoint
  * userinfo endpoint
  * jwkSet.json

* add the connect2id-claimsource.jar (https://gitlab.com/ID4me/C2idClaimsource.git)
    to the ```WEB-INF/lib``` of c2id servlet directory.
    Or try it with the following [vendor howto](https://connect2id.com/products/server/docs/guides/claims-source)

## dns-lookup-ms

Create the docker image
```
cd $IDAUTH_DIR/C2idAuthorizationEndpoint/dns-lookup-ms
docker build -t dns-lookup-ms:test .
```

Run the docker image
```
docker run --name dns-lookup-ms dns-lookup-ms:test
```

## auth-endpoint
Clone and build 
```
cd $IDAUTH_DIR
git clone https://gitlab.com/ID4me/C2idAuthorizationEndpoint.git
cd C2idAuthorizationEndpoint
./gradlew build
cp auth-endpoint/src/dist/*  $IDAUTH_DIR/auth-endpoint
```

Adjust the config ```$IDAUTH_DIR/auth-endpoint/auth-endpoint.config.yaml```

Create a docker image
```
cd $IDAUTH_DIR/C2idAuthorizationEndpoint/auth-endpoint/build
docker build -t auth-endpoint:test .
```

Run the docker image
```
docker run \
  -v $IDAUTH_DIR/common:/root \
  -v $IDAUTH_DIR/auth-endpoint:/config \
  --link c2id:8080 \
  --link pg-master:5432 \
  --link dns-lookup-ms:8123 \
  auth-endpoint:test server /config/auth-endpoint.config.yaml
```

## user-account-board
Create a docker image
```
cd $IDAUTH_DIR/C2idAuthorizationEndpoint/user-account-board/build
docker build -t user-account-board:test .
```

Copy the config and adjust it
```
cd $IDAUTH_DIR/C2idAuthorizationEndpoint/user-account-board/build
cp src/dist/*  $IDAUTH_DIR/user-account-board
```

Run the docker image
```
docker run \
  -v $IDAUTH_DIR/user-account-board:/config \
  --link c2id:8080 \
  --link pg-master:5432 \
  --link dns-lookup-ms:8123 \
  user-account-board:test server /config/user-account-board.config.default.yaml
```


