/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid;

import com.fasterxml.jackson.annotation.JsonValue;
import com.ibm.icu.text.IDNA;
import com.ibm.icu.text.IDNA.*;
import org.apache.commons.lang3.Validate;
import org.xbill.DNS.Name;
import org.xbill.DNS.TextParseException;

import javax.annotation.concurrent.Immutable;
import java.security.Principal;
import java.util.Objects;
import java.util.Optional;

import static com.ibm.icu.text.IDNA.*;
import static java.util.Locale.ENGLISH;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.xbill.DNS.Name.root;

/**
 * Case-<strong>insensitive</strong> representation of a Domain-ID.
 */
@Immutable
public final class DomainId implements Principal, Comparable<DomainId> {

  private static final boolean OMIT_FINAL_DOT = true;
  private static final IDNA IDNA_TO_USE = IDNA.getUTS46Instance(
          USE_STD3_RULES | NONTRANSITIONAL_TO_ASCII | NONTRANSITIONAL_TO_UNICODE | CHECK_CONTEXTJ | CHECK_CONTEXTO);

  private final String name;
  private final Name aceEncodedValue;
  private final int hashCode;

  /**
   * @param value Required to be not empty.
   */
  public DomainId(final String value) {
    Validate.notEmpty(value, "Missing value");
    final StringBuilder tmpValueBuilder = new StringBuilder();
    final Info idnConversionInfo = new Info();
    IDNA_TO_USE.nameToASCII(value, tmpValueBuilder, idnConversionInfo);
    try {
      this.aceEncodedValue = Name.fromString(tmpValueBuilder.toString().toLowerCase(ENGLISH), root);
    } catch (final TextParseException e) {
      throw new IllegalArgumentException(value, e);
    }

    IDNA_TO_USE.nameToUnicode(aceEncodedValue.toString(OMIT_FINAL_DOT), tmpValueBuilder, idnConversionInfo);
    name = tmpValueBuilder.toString();
    hashCode = calculateHashCode();
  }

  /**
   * @param value Optional
   * @return Never <code>null</code>
   */
  public static Optional<DomainId> from(final CharSequence value) {
    try {
      return (value == null ? empty() : of(new DomainId(value.toString())));
    } catch (final RuntimeException e) {
      return empty();
    }
  }

  /**
   * {@inheritDoc}
   *
   * @return Lower-case IDN-representation without final dot.
   */
  @Override
  @JsonValue
  public String getName() {
    return name;
  }

  public String getAceEncodedName() {
    return aceEncodedValue.toString(OMIT_FINAL_DOT);
  }

  @Override
  public int compareTo(final DomainId other) {
    return aceEncodedValue.compareTo(other.aceEncodedValue);
  }

  private int calculateHashCode() {
    return Objects.hash(name);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    DomainId domainId = (DomainId) o;
    return Objects.equals(name, domainId.name);
  }

  @Override
  public int hashCode() {
    return hashCode;
  }

  @Override
  public String toString() {
    return getName();
  }

}
