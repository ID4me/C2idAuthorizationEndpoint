/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.subject;

import org.apache.commons.lang3.Validate;

import javax.annotation.concurrent.Immutable;
import javax.security.auth.Subject;
import java.security.Principal;
import java.util.HashSet;
import java.util.Set;

import static java.util.Arrays.asList;
import static java.util.Collections.emptySet;

@Immutable
public final class SubjectUtils {

  public static final Set<?> NO_CREDENTIALS = emptySet();
  public static final boolean READ_ONLY = true;

  private final Subject subject;

  public SubjectUtils(final Subject subject) {
    Validate.notNull(subject, "Missing subject");
    this.subject = subject;
  }

  /**
   * @param subject Required
   * @return Never <code>null</code>
   */
  public static SubjectUtils from(final Subject subject) {
    return new SubjectUtils(subject);
  }

  /**
   * @param principals Required
   * @return Never <code>null</code>
   */
  public static Subject subjectWith(final Principal... principals) {
    final Set<Principal> setOfPrincipals = new HashSet<>(asList(principals));
    return new Subject(READ_ONLY, setOfPrincipals, NO_CREDENTIALS, NO_CREDENTIALS);
  }

  /**
   * @param principalType Required
   * @param <P>           Type of {@link Principal}
   * @return Never <code>null</code>
   */
  public <P extends Principal> P single(final Class<P> principalType) {
    Validate.notNull(principalType, "Missing subtype of " + Principal.class);
    final Set<P> principals = subject.getPrincipals(principalType);
    Validate.isTrue(principals.size() == 1, "Subject '%s': Expected single principal of %s, but it has %d",
            subject, principalType, principals.size());
    return principals.stream().findAny().orElseThrow(() -> new RuntimeException("Internal error"));
  }

}
