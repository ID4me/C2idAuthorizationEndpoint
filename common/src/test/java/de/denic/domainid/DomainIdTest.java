/*^
  ===========================================================================
  Connect2ID-based ID4me OpenID Connect Authorization Endpoint
  ===========================================================================
  Copyright (C) 2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid;

import org.junit.Ignore;
import org.junit.Test;

import static java.util.Locale.ENGLISH;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class DomainIdTest {

  private static final String SMALL_O_UMLAUT = "\u00f6";
  private static final String SMALL_E_ACUTE = "\u00e9";
  private static final String SMALL_U_UMLAUT = "\u00fc";
  private static final String SMALL_SHARP_S = "\u00df";

  @Test
  public void correctDomainFormRepresentation() {
    assertGetNameProvidesDomainFormValueFedToConstructor("domainid.denic.test");
    assertGetNameProvidesConvertedDomainFormValue("domainid.xn--mller-kva.test",
            "domainid.m" + SMALL_U_UMLAUT + "ller.test");
    assertGetNameProvidesDomainFormValueFedToConstructor(
            "d" + SMALL_O_UMLAUT + "mainid.m" + SMALL_U_UMLAUT + "ller.t" + SMALL_E_ACUTE + "st");
    assertGetNameProvidesDomainFormValueFedToConstructor("sharp.small-" + SMALL_SHARP_S + "-id.test");
  }

  @Test
  public void equalWithOrWithoutTrailingDot() {
    final String valueWithoutTrailingDot = "christian.denic.de";
    final DomainId withoutTrailingDot = new DomainId(valueWithoutTrailingDot);
    final DomainId withTrailingDot = new DomainId(valueWithoutTrailingDot + ".");
    assertEquals("Trailing dot makes no difference", withTrailingDot, withoutTrailingDot);
    assertEquals("Name: Trailing dot makes no difference", withTrailingDot.getName(), withoutTrailingDot.getName());
    assertEquals("Compare: Trailing dot makes no difference", 0, withTrailingDot.compareTo(withoutTrailingDot));
  }

  @Ignore
  @Test
  public void invalidDomainFormRepresentation() {
    assertIllegalArgumentExceptionGetsThrowsApplying("mueller@test.de");
    assertIllegalArgumentExceptionGetsThrowsApplying("*.domain-id.test");
  }

  @Test
  public void caseInsensitiveRepresentation() {
    final String lowerCaseInput = "my.domain-id.test";
    final String upperCaseInput = lowerCaseInput.toUpperCase(ENGLISH);
    assertEquals("Case-insensitivity", new DomainId(lowerCaseInput), new DomainId(upperCaseInput));
    assertGetNameProvidesConvertedDomainFormValue(upperCaseInput, lowerCaseInput);
  }

  private void assertIllegalArgumentExceptionGetsThrowsApplying(final String input) {
    try {
      fail("Expecting IllegalArgumentException, but got: " + new DomainId(input));
    } catch (final IllegalArgumentException expected) {
      // EXPECTED
      expected.printStackTrace();
    }
  }

  private void assertGetNameProvidesConvertedDomainFormValue(final String constructorInput,
                                                             final String expectedGetNameValue) {
    final DomainId CUT = new DomainId(constructorInput);
    assertEquals("Name", expectedGetNameValue, CUT.getName());
    assertEquals("ACE form", constructorInput.toLowerCase(ENGLISH), CUT.getAceEncodedName());
  }

  private void assertGetNameProvidesDomainFormValueFedToConstructor(final String constructorInput) {
    final DomainId CUT = new DomainId(constructorInput);
    assertEquals("Name", constructorInput, CUT.getName());
    // assertEquals("FQDN form", constructorInput + ".", CUT.getNameAsFQDN());
  }

}
